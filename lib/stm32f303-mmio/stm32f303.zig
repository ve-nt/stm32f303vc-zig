// generated using gen_zig_from_svd.py
// DO NOT EDIT
// based on STM32F303 version 1.4
const MMIO = @import("mmio.zig").MMIO;
const Name = "STM32F303";
pub const GPIOA = extern struct {
    pub const Address: u32 = 0x48000000;
    // byte offset: 0 GPIO port mode register
    pub const MODER = MMIO(Address + 0x00000000, u32, packed struct {
        MODER0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        MODER1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        MODER2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        MODER3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        MODER4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        MODER5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        MODER6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        MODER7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        MODER8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        MODER9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        MODER10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        MODER11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        MODER12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        MODER13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        MODER14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        MODER15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 4 GPIO port output type register
    pub const OTYPER = MMIO(Address + 0x00000004, u32, packed struct {
        OT0: bool, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        OT1: bool, // bit offset: 1 desc: Port x configuration bits (y = 0..15)
        OT2: bool, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        OT3: bool, // bit offset: 3 desc: Port x configuration bits (y = 0..15)
        OT4: bool, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        OT5: bool, // bit offset: 5 desc: Port x configuration bits (y = 0..15)
        OT6: bool, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        OT7: bool, // bit offset: 7 desc: Port x configuration bits (y = 0..15)
        OT8: bool, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        OT9: bool, // bit offset: 9 desc: Port x configuration bits (y = 0..15)
        OT10: bool, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        OT11: bool, // bit offset: 11 desc: Port x configuration bits (y = 0..15)
        OT12: bool, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        OT13: bool, // bit offset: 13 desc: Port x configuration bits (y = 0..15)
        OT14: bool, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        OT15: bool, // bit offset: 15 desc: Port x configuration bits (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 8 GPIO port output speed register
    pub const OSPEEDR = MMIO(Address + 0x00000008, u32, packed struct {
        OSPEEDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        OSPEEDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        OSPEEDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        OSPEEDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        OSPEEDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        OSPEEDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        OSPEEDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        OSPEEDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        OSPEEDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        OSPEEDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        OSPEEDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        OSPEEDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        OSPEEDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        OSPEEDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        OSPEEDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        OSPEEDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 12 GPIO port pull-up/pull-down register
    pub const PUPDR = MMIO(Address + 0x0000000c, u32, packed struct {
        PUPDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        PUPDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        PUPDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        PUPDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        PUPDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        PUPDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        PUPDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        PUPDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        PUPDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        PUPDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        PUPDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        PUPDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        PUPDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        PUPDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        PUPDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        PUPDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 16 GPIO port input data register
    pub const IDR = MMIO(Address + 0x00000010, u32, packed struct {
        IDR0: bool, // bit offset: 0 desc: Port input data (y = 0..15)
        IDR1: bool, // bit offset: 1 desc: Port input data (y = 0..15)
        IDR2: bool, // bit offset: 2 desc: Port input data (y = 0..15)
        IDR3: bool, // bit offset: 3 desc: Port input data (y = 0..15)
        IDR4: bool, // bit offset: 4 desc: Port input data (y = 0..15)
        IDR5: bool, // bit offset: 5 desc: Port input data (y = 0..15)
        IDR6: bool, // bit offset: 6 desc: Port input data (y = 0..15)
        IDR7: bool, // bit offset: 7 desc: Port input data (y = 0..15)
        IDR8: bool, // bit offset: 8 desc: Port input data (y = 0..15)
        IDR9: bool, // bit offset: 9 desc: Port input data (y = 0..15)
        IDR10: bool, // bit offset: 10 desc: Port input data (y = 0..15)
        IDR11: bool, // bit offset: 11 desc: Port input data (y = 0..15)
        IDR12: bool, // bit offset: 12 desc: Port input data (y = 0..15)
        IDR13: bool, // bit offset: 13 desc: Port input data (y = 0..15)
        IDR14: bool, // bit offset: 14 desc: Port input data (y = 0..15)
        IDR15: bool, // bit offset: 15 desc: Port input data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 20 GPIO port output data register
    pub const ODR = MMIO(Address + 0x00000014, u32, packed struct {
        ODR0: bool, // bit offset: 0 desc: Port output data (y = 0..15)
        ODR1: bool, // bit offset: 1 desc: Port output data (y = 0..15)
        ODR2: bool, // bit offset: 2 desc: Port output data (y = 0..15)
        ODR3: bool, // bit offset: 3 desc: Port output data (y = 0..15)
        ODR4: bool, // bit offset: 4 desc: Port output data (y = 0..15)
        ODR5: bool, // bit offset: 5 desc: Port output data (y = 0..15)
        ODR6: bool, // bit offset: 6 desc: Port output data (y = 0..15)
        ODR7: bool, // bit offset: 7 desc: Port output data (y = 0..15)
        ODR8: bool, // bit offset: 8 desc: Port output data (y = 0..15)
        ODR9: bool, // bit offset: 9 desc: Port output data (y = 0..15)
        ODR10: bool, // bit offset: 10 desc: Port output data (y = 0..15)
        ODR11: bool, // bit offset: 11 desc: Port output data (y = 0..15)
        ODR12: bool, // bit offset: 12 desc: Port output data (y = 0..15)
        ODR13: bool, // bit offset: 13 desc: Port output data (y = 0..15)
        ODR14: bool, // bit offset: 14 desc: Port output data (y = 0..15)
        ODR15: bool, // bit offset: 15 desc: Port output data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 24 GPIO port bit set/reset register
    pub const BSRR = MMIO(Address + 0x00000018, u32, packed struct {
        BS0: bool, // bit offset: 0 desc: Port x set bit y (y= 0..15)
        BS1: bool, // bit offset: 1 desc: Port x set bit y (y= 0..15)
        BS2: bool, // bit offset: 2 desc: Port x set bit y (y= 0..15)
        BS3: bool, // bit offset: 3 desc: Port x set bit y (y= 0..15)
        BS4: bool, // bit offset: 4 desc: Port x set bit y (y= 0..15)
        BS5: bool, // bit offset: 5 desc: Port x set bit y (y= 0..15)
        BS6: bool, // bit offset: 6 desc: Port x set bit y (y= 0..15)
        BS7: bool, // bit offset: 7 desc: Port x set bit y (y= 0..15)
        BS8: bool, // bit offset: 8 desc: Port x set bit y (y= 0..15)
        BS9: bool, // bit offset: 9 desc: Port x set bit y (y= 0..15)
        BS10: bool, // bit offset: 10 desc: Port x set bit y (y= 0..15)
        BS11: bool, // bit offset: 11 desc: Port x set bit y (y= 0..15)
        BS12: bool, // bit offset: 12 desc: Port x set bit y (y= 0..15)
        BS13: bool, // bit offset: 13 desc: Port x set bit y (y= 0..15)
        BS14: bool, // bit offset: 14 desc: Port x set bit y (y= 0..15)
        BS15: bool, // bit offset: 15 desc: Port x set bit y (y= 0..15)
        BR0: bool, // bit offset: 16 desc: Port x set bit y (y= 0..15)
        BR1: bool, // bit offset: 17 desc: Port x reset bit y (y = 0..15)
        BR2: bool, // bit offset: 18 desc: Port x reset bit y (y = 0..15)
        BR3: bool, // bit offset: 19 desc: Port x reset bit y (y = 0..15)
        BR4: bool, // bit offset: 20 desc: Port x reset bit y (y = 0..15)
        BR5: bool, // bit offset: 21 desc: Port x reset bit y (y = 0..15)
        BR6: bool, // bit offset: 22 desc: Port x reset bit y (y = 0..15)
        BR7: bool, // bit offset: 23 desc: Port x reset bit y (y = 0..15)
        BR8: bool, // bit offset: 24 desc: Port x reset bit y (y = 0..15)
        BR9: bool, // bit offset: 25 desc: Port x reset bit y (y = 0..15)
        BR10: bool, // bit offset: 26 desc: Port x reset bit y (y = 0..15)
        BR11: bool, // bit offset: 27 desc: Port x reset bit y (y = 0..15)
        BR12: bool, // bit offset: 28 desc: Port x reset bit y (y = 0..15)
        BR13: bool, // bit offset: 29 desc: Port x reset bit y (y = 0..15)
        BR14: bool, // bit offset: 30 desc: Port x reset bit y (y = 0..15)
        BR15: bool, // bit offset: 31 desc: Port x reset bit y (y = 0..15)
    });
    // byte offset: 28 GPIO port configuration lock register
    pub const LCKR = MMIO(Address + 0x0000001c, u32, packed struct {
        LCK0: bool, // bit offset: 0 desc: Port x lock bit y (y= 0..15)
        LCK1: bool, // bit offset: 1 desc: Port x lock bit y (y= 0..15)
        LCK2: bool, // bit offset: 2 desc: Port x lock bit y (y= 0..15)
        LCK3: bool, // bit offset: 3 desc: Port x lock bit y (y= 0..15)
        LCK4: bool, // bit offset: 4 desc: Port x lock bit y (y= 0..15)
        LCK5: bool, // bit offset: 5 desc: Port x lock bit y (y= 0..15)
        LCK6: bool, // bit offset: 6 desc: Port x lock bit y (y= 0..15)
        LCK7: bool, // bit offset: 7 desc: Port x lock bit y (y= 0..15)
        LCK8: bool, // bit offset: 8 desc: Port x lock bit y (y= 0..15)
        LCK9: bool, // bit offset: 9 desc: Port x lock bit y (y= 0..15)
        LCK10: bool, // bit offset: 10 desc: Port x lock bit y (y= 0..15)
        LCK11: bool, // bit offset: 11 desc: Port x lock bit y (y= 0..15)
        LCK12: bool, // bit offset: 12 desc: Port x lock bit y (y= 0..15)
        LCK13: bool, // bit offset: 13 desc: Port x lock bit y (y= 0..15)
        LCK14: bool, // bit offset: 14 desc: Port x lock bit y (y= 0..15)
        LCK15: bool, // bit offset: 15 desc: Port x lock bit y (y= 0..15)
        LCKK: bool, // bit offset: 16 desc: Lok Key
        padding: u15 = 0,
    });
    // byte offset: 32 GPIO alternate function low register
    pub const AFRL = MMIO(Address + 0x00000020, u32, packed struct {
        AFRL0: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL1: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL2: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL3: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL4: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL5: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL6: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL7: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 0..7)
    });
    // byte offset: 36 GPIO alternate function high register
    pub const AFRH = MMIO(Address + 0x00000024, u32, packed struct {
        AFRH8: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH9: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH10: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH11: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH12: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH13: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH14: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH15: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 8..15)
    });
    // byte offset: 40 Port bit reset register
    pub const BRR = MMIO(Address + 0x00000028, u32, packed struct {
        BR0: bool, // bit offset: 0 desc: Port x Reset bit y
        BR1: bool, // bit offset: 1 desc: Port x Reset bit y
        BR2: bool, // bit offset: 2 desc: Port x Reset bit y
        BR3: bool, // bit offset: 3 desc: Port x Reset bit y
        BR4: bool, // bit offset: 4 desc: Port x Reset bit y
        BR5: bool, // bit offset: 5 desc: Port x Reset bit y
        BR6: bool, // bit offset: 6 desc: Port x Reset bit y
        BR7: bool, // bit offset: 7 desc: Port x Reset bit y
        BR8: bool, // bit offset: 8 desc: Port x Reset bit y
        BR9: bool, // bit offset: 9 desc: Port x Reset bit y
        BR10: bool, // bit offset: 10 desc: Port x Reset bit y
        BR11: bool, // bit offset: 11 desc: Port x Reset bit y
        BR12: bool, // bit offset: 12 desc: Port x Reset bit y
        BR13: bool, // bit offset: 13 desc: Port x Reset bit y
        BR14: bool, // bit offset: 14 desc: Port x Reset bit y
        BR15: bool, // bit offset: 15 desc: Port x Reset bit y
        padding: u16 = 0,
    });
};
pub const GPIOB = extern struct {
    pub const Address: u32 = 0x48000400;
    // byte offset: 0 GPIO port mode register
    pub const MODER = MMIO(Address + 0x00000000, u32, packed struct {
        MODER0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        MODER1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        MODER2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        MODER3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        MODER4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        MODER5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        MODER6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        MODER7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        MODER8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        MODER9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        MODER10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        MODER11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        MODER12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        MODER13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        MODER14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        MODER15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 4 GPIO port output type register
    pub const OTYPER = MMIO(Address + 0x00000004, u32, packed struct {
        OT0: bool, // bit offset: 0 desc: Port x configuration bit 0
        OT1: bool, // bit offset: 1 desc: Port x configuration bit 1
        OT2: bool, // bit offset: 2 desc: Port x configuration bit 2
        OT3: bool, // bit offset: 3 desc: Port x configuration bit 3
        OT4: bool, // bit offset: 4 desc: Port x configuration bit 4
        OT5: bool, // bit offset: 5 desc: Port x configuration bit 5
        OT6: bool, // bit offset: 6 desc: Port x configuration bit 6
        OT7: bool, // bit offset: 7 desc: Port x configuration bit 7
        OT8: bool, // bit offset: 8 desc: Port x configuration bit 8
        OT9: bool, // bit offset: 9 desc: Port x configuration bit 9
        OT10: bool, // bit offset: 10 desc: Port x configuration bit 10
        OT11: bool, // bit offset: 11 desc: Port x configuration bit 11
        OT12: bool, // bit offset: 12 desc: Port x configuration bit 12
        OT13: bool, // bit offset: 13 desc: Port x configuration bit 13
        OT14: bool, // bit offset: 14 desc: Port x configuration bit 14
        OT15: bool, // bit offset: 15 desc: Port x configuration bit 15
        padding: u16 = 0,
    });
    // byte offset: 8 GPIO port output speed register
    pub const OSPEEDR = MMIO(Address + 0x00000008, u32, packed struct {
        OSPEEDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        OSPEEDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        OSPEEDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        OSPEEDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        OSPEEDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        OSPEEDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        OSPEEDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        OSPEEDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        OSPEEDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        OSPEEDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        OSPEEDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        OSPEEDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        OSPEEDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        OSPEEDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        OSPEEDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        OSPEEDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 12 GPIO port pull-up/pull-down register
    pub const PUPDR = MMIO(Address + 0x0000000c, u32, packed struct {
        PUPDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        PUPDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        PUPDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        PUPDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        PUPDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        PUPDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        PUPDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        PUPDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        PUPDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        PUPDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        PUPDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        PUPDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        PUPDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        PUPDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        PUPDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        PUPDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 16 GPIO port input data register
    pub const IDR = MMIO(Address + 0x00000010, u32, packed struct {
        IDR0: bool, // bit offset: 0 desc: Port input data (y = 0..15)
        IDR1: bool, // bit offset: 1 desc: Port input data (y = 0..15)
        IDR2: bool, // bit offset: 2 desc: Port input data (y = 0..15)
        IDR3: bool, // bit offset: 3 desc: Port input data (y = 0..15)
        IDR4: bool, // bit offset: 4 desc: Port input data (y = 0..15)
        IDR5: bool, // bit offset: 5 desc: Port input data (y = 0..15)
        IDR6: bool, // bit offset: 6 desc: Port input data (y = 0..15)
        IDR7: bool, // bit offset: 7 desc: Port input data (y = 0..15)
        IDR8: bool, // bit offset: 8 desc: Port input data (y = 0..15)
        IDR9: bool, // bit offset: 9 desc: Port input data (y = 0..15)
        IDR10: bool, // bit offset: 10 desc: Port input data (y = 0..15)
        IDR11: bool, // bit offset: 11 desc: Port input data (y = 0..15)
        IDR12: bool, // bit offset: 12 desc: Port input data (y = 0..15)
        IDR13: bool, // bit offset: 13 desc: Port input data (y = 0..15)
        IDR14: bool, // bit offset: 14 desc: Port input data (y = 0..15)
        IDR15: bool, // bit offset: 15 desc: Port input data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 20 GPIO port output data register
    pub const ODR = MMIO(Address + 0x00000014, u32, packed struct {
        ODR0: bool, // bit offset: 0 desc: Port output data (y = 0..15)
        ODR1: bool, // bit offset: 1 desc: Port output data (y = 0..15)
        ODR2: bool, // bit offset: 2 desc: Port output data (y = 0..15)
        ODR3: bool, // bit offset: 3 desc: Port output data (y = 0..15)
        ODR4: bool, // bit offset: 4 desc: Port output data (y = 0..15)
        ODR5: bool, // bit offset: 5 desc: Port output data (y = 0..15)
        ODR6: bool, // bit offset: 6 desc: Port output data (y = 0..15)
        ODR7: bool, // bit offset: 7 desc: Port output data (y = 0..15)
        ODR8: bool, // bit offset: 8 desc: Port output data (y = 0..15)
        ODR9: bool, // bit offset: 9 desc: Port output data (y = 0..15)
        ODR10: bool, // bit offset: 10 desc: Port output data (y = 0..15)
        ODR11: bool, // bit offset: 11 desc: Port output data (y = 0..15)
        ODR12: bool, // bit offset: 12 desc: Port output data (y = 0..15)
        ODR13: bool, // bit offset: 13 desc: Port output data (y = 0..15)
        ODR14: bool, // bit offset: 14 desc: Port output data (y = 0..15)
        ODR15: bool, // bit offset: 15 desc: Port output data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 24 GPIO port bit set/reset register
    pub const BSRR = MMIO(Address + 0x00000018, u32, packed struct {
        BS0: bool, // bit offset: 0 desc: Port x set bit y (y= 0..15)
        BS1: bool, // bit offset: 1 desc: Port x set bit y (y= 0..15)
        BS2: bool, // bit offset: 2 desc: Port x set bit y (y= 0..15)
        BS3: bool, // bit offset: 3 desc: Port x set bit y (y= 0..15)
        BS4: bool, // bit offset: 4 desc: Port x set bit y (y= 0..15)
        BS5: bool, // bit offset: 5 desc: Port x set bit y (y= 0..15)
        BS6: bool, // bit offset: 6 desc: Port x set bit y (y= 0..15)
        BS7: bool, // bit offset: 7 desc: Port x set bit y (y= 0..15)
        BS8: bool, // bit offset: 8 desc: Port x set bit y (y= 0..15)
        BS9: bool, // bit offset: 9 desc: Port x set bit y (y= 0..15)
        BS10: bool, // bit offset: 10 desc: Port x set bit y (y= 0..15)
        BS11: bool, // bit offset: 11 desc: Port x set bit y (y= 0..15)
        BS12: bool, // bit offset: 12 desc: Port x set bit y (y= 0..15)
        BS13: bool, // bit offset: 13 desc: Port x set bit y (y= 0..15)
        BS14: bool, // bit offset: 14 desc: Port x set bit y (y= 0..15)
        BS15: bool, // bit offset: 15 desc: Port x set bit y (y= 0..15)
        BR0: bool, // bit offset: 16 desc: Port x set bit y (y= 0..15)
        BR1: bool, // bit offset: 17 desc: Port x reset bit y (y = 0..15)
        BR2: bool, // bit offset: 18 desc: Port x reset bit y (y = 0..15)
        BR3: bool, // bit offset: 19 desc: Port x reset bit y (y = 0..15)
        BR4: bool, // bit offset: 20 desc: Port x reset bit y (y = 0..15)
        BR5: bool, // bit offset: 21 desc: Port x reset bit y (y = 0..15)
        BR6: bool, // bit offset: 22 desc: Port x reset bit y (y = 0..15)
        BR7: bool, // bit offset: 23 desc: Port x reset bit y (y = 0..15)
        BR8: bool, // bit offset: 24 desc: Port x reset bit y (y = 0..15)
        BR9: bool, // bit offset: 25 desc: Port x reset bit y (y = 0..15)
        BR10: bool, // bit offset: 26 desc: Port x reset bit y (y = 0..15)
        BR11: bool, // bit offset: 27 desc: Port x reset bit y (y = 0..15)
        BR12: bool, // bit offset: 28 desc: Port x reset bit y (y = 0..15)
        BR13: bool, // bit offset: 29 desc: Port x reset bit y (y = 0..15)
        BR14: bool, // bit offset: 30 desc: Port x reset bit y (y = 0..15)
        BR15: bool, // bit offset: 31 desc: Port x reset bit y (y = 0..15)
    });
    // byte offset: 28 GPIO port configuration lock register
    pub const LCKR = MMIO(Address + 0x0000001c, u32, packed struct {
        LCK0: bool, // bit offset: 0 desc: Port x lock bit y (y= 0..15)
        LCK1: bool, // bit offset: 1 desc: Port x lock bit y (y= 0..15)
        LCK2: bool, // bit offset: 2 desc: Port x lock bit y (y= 0..15)
        LCK3: bool, // bit offset: 3 desc: Port x lock bit y (y= 0..15)
        LCK4: bool, // bit offset: 4 desc: Port x lock bit y (y= 0..15)
        LCK5: bool, // bit offset: 5 desc: Port x lock bit y (y= 0..15)
        LCK6: bool, // bit offset: 6 desc: Port x lock bit y (y= 0..15)
        LCK7: bool, // bit offset: 7 desc: Port x lock bit y (y= 0..15)
        LCK8: bool, // bit offset: 8 desc: Port x lock bit y (y= 0..15)
        LCK9: bool, // bit offset: 9 desc: Port x lock bit y (y= 0..15)
        LCK10: bool, // bit offset: 10 desc: Port x lock bit y (y= 0..15)
        LCK11: bool, // bit offset: 11 desc: Port x lock bit y (y= 0..15)
        LCK12: bool, // bit offset: 12 desc: Port x lock bit y (y= 0..15)
        LCK13: bool, // bit offset: 13 desc: Port x lock bit y (y= 0..15)
        LCK14: bool, // bit offset: 14 desc: Port x lock bit y (y= 0..15)
        LCK15: bool, // bit offset: 15 desc: Port x lock bit y (y= 0..15)
        LCKK: bool, // bit offset: 16 desc: Lok Key
        padding: u15 = 0,
    });
    // byte offset: 32 GPIO alternate function low register
    pub const AFRL = MMIO(Address + 0x00000020, u32, packed struct {
        AFRL0: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL1: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL2: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL3: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL4: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL5: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL6: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL7: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 0..7)
    });
    // byte offset: 36 GPIO alternate function high register
    pub const AFRH = MMIO(Address + 0x00000024, u32, packed struct {
        AFRH8: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH9: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH10: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH11: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH12: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH13: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH14: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH15: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 8..15)
    });
    // byte offset: 40 Port bit reset register
    pub const BRR = MMIO(Address + 0x00000028, u32, packed struct {
        BR0: bool, // bit offset: 0 desc: Port x Reset bit y
        BR1: bool, // bit offset: 1 desc: Port x Reset bit y
        BR2: bool, // bit offset: 2 desc: Port x Reset bit y
        BR3: bool, // bit offset: 3 desc: Port x Reset bit y
        BR4: bool, // bit offset: 4 desc: Port x Reset bit y
        BR5: bool, // bit offset: 5 desc: Port x Reset bit y
        BR6: bool, // bit offset: 6 desc: Port x Reset bit y
        BR7: bool, // bit offset: 7 desc: Port x Reset bit y
        BR8: bool, // bit offset: 8 desc: Port x Reset bit y
        BR9: bool, // bit offset: 9 desc: Port x Reset bit y
        BR10: bool, // bit offset: 10 desc: Port x Reset bit y
        BR11: bool, // bit offset: 11 desc: Port x Reset bit y
        BR12: bool, // bit offset: 12 desc: Port x Reset bit y
        BR13: bool, // bit offset: 13 desc: Port x Reset bit y
        BR14: bool, // bit offset: 14 desc: Port x Reset bit y
        BR15: bool, // bit offset: 15 desc: Port x Reset bit y
        padding: u16 = 0,
    });
};
pub const GPIOC = extern struct {
    pub const Address: u32 = 0x48000800;
    // byte offset: 0 GPIO port mode register
    pub const MODER = MMIO(Address + 0x00000000, u32, packed struct {
        MODER0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        MODER1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        MODER2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        MODER3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        MODER4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        MODER5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        MODER6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        MODER7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        MODER8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        MODER9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        MODER10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        MODER11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        MODER12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        MODER13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        MODER14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        MODER15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 4 GPIO port output type register
    pub const OTYPER = MMIO(Address + 0x00000004, u32, packed struct {
        OT0: bool, // bit offset: 0 desc: Port x configuration bit 0
        OT1: bool, // bit offset: 1 desc: Port x configuration bit 1
        OT2: bool, // bit offset: 2 desc: Port x configuration bit 2
        OT3: bool, // bit offset: 3 desc: Port x configuration bit 3
        OT4: bool, // bit offset: 4 desc: Port x configuration bit 4
        OT5: bool, // bit offset: 5 desc: Port x configuration bit 5
        OT6: bool, // bit offset: 6 desc: Port x configuration bit 6
        OT7: bool, // bit offset: 7 desc: Port x configuration bit 7
        OT8: bool, // bit offset: 8 desc: Port x configuration bit 8
        OT9: bool, // bit offset: 9 desc: Port x configuration bit 9
        OT10: bool, // bit offset: 10 desc: Port x configuration bit 10
        OT11: bool, // bit offset: 11 desc: Port x configuration bit 11
        OT12: bool, // bit offset: 12 desc: Port x configuration bit 12
        OT13: bool, // bit offset: 13 desc: Port x configuration bit 13
        OT14: bool, // bit offset: 14 desc: Port x configuration bit 14
        OT15: bool, // bit offset: 15 desc: Port x configuration bit 15
        padding: u16 = 0,
    });
    // byte offset: 8 GPIO port output speed register
    pub const OSPEEDR = MMIO(Address + 0x00000008, u32, packed struct {
        OSPEEDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        OSPEEDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        OSPEEDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        OSPEEDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        OSPEEDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        OSPEEDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        OSPEEDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        OSPEEDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        OSPEEDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        OSPEEDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        OSPEEDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        OSPEEDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        OSPEEDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        OSPEEDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        OSPEEDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        OSPEEDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 12 GPIO port pull-up/pull-down register
    pub const PUPDR = MMIO(Address + 0x0000000c, u32, packed struct {
        PUPDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        PUPDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        PUPDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        PUPDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        PUPDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        PUPDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        PUPDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        PUPDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        PUPDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        PUPDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        PUPDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        PUPDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        PUPDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        PUPDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        PUPDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        PUPDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 16 GPIO port input data register
    pub const IDR = MMIO(Address + 0x00000010, u32, packed struct {
        IDR0: bool, // bit offset: 0 desc: Port input data (y = 0..15)
        IDR1: bool, // bit offset: 1 desc: Port input data (y = 0..15)
        IDR2: bool, // bit offset: 2 desc: Port input data (y = 0..15)
        IDR3: bool, // bit offset: 3 desc: Port input data (y = 0..15)
        IDR4: bool, // bit offset: 4 desc: Port input data (y = 0..15)
        IDR5: bool, // bit offset: 5 desc: Port input data (y = 0..15)
        IDR6: bool, // bit offset: 6 desc: Port input data (y = 0..15)
        IDR7: bool, // bit offset: 7 desc: Port input data (y = 0..15)
        IDR8: bool, // bit offset: 8 desc: Port input data (y = 0..15)
        IDR9: bool, // bit offset: 9 desc: Port input data (y = 0..15)
        IDR10: bool, // bit offset: 10 desc: Port input data (y = 0..15)
        IDR11: bool, // bit offset: 11 desc: Port input data (y = 0..15)
        IDR12: bool, // bit offset: 12 desc: Port input data (y = 0..15)
        IDR13: bool, // bit offset: 13 desc: Port input data (y = 0..15)
        IDR14: bool, // bit offset: 14 desc: Port input data (y = 0..15)
        IDR15: bool, // bit offset: 15 desc: Port input data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 20 GPIO port output data register
    pub const ODR = MMIO(Address + 0x00000014, u32, packed struct {
        ODR0: bool, // bit offset: 0 desc: Port output data (y = 0..15)
        ODR1: bool, // bit offset: 1 desc: Port output data (y = 0..15)
        ODR2: bool, // bit offset: 2 desc: Port output data (y = 0..15)
        ODR3: bool, // bit offset: 3 desc: Port output data (y = 0..15)
        ODR4: bool, // bit offset: 4 desc: Port output data (y = 0..15)
        ODR5: bool, // bit offset: 5 desc: Port output data (y = 0..15)
        ODR6: bool, // bit offset: 6 desc: Port output data (y = 0..15)
        ODR7: bool, // bit offset: 7 desc: Port output data (y = 0..15)
        ODR8: bool, // bit offset: 8 desc: Port output data (y = 0..15)
        ODR9: bool, // bit offset: 9 desc: Port output data (y = 0..15)
        ODR10: bool, // bit offset: 10 desc: Port output data (y = 0..15)
        ODR11: bool, // bit offset: 11 desc: Port output data (y = 0..15)
        ODR12: bool, // bit offset: 12 desc: Port output data (y = 0..15)
        ODR13: bool, // bit offset: 13 desc: Port output data (y = 0..15)
        ODR14: bool, // bit offset: 14 desc: Port output data (y = 0..15)
        ODR15: bool, // bit offset: 15 desc: Port output data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 24 GPIO port bit set/reset register
    pub const BSRR = MMIO(Address + 0x00000018, u32, packed struct {
        BS0: bool, // bit offset: 0 desc: Port x set bit y (y= 0..15)
        BS1: bool, // bit offset: 1 desc: Port x set bit y (y= 0..15)
        BS2: bool, // bit offset: 2 desc: Port x set bit y (y= 0..15)
        BS3: bool, // bit offset: 3 desc: Port x set bit y (y= 0..15)
        BS4: bool, // bit offset: 4 desc: Port x set bit y (y= 0..15)
        BS5: bool, // bit offset: 5 desc: Port x set bit y (y= 0..15)
        BS6: bool, // bit offset: 6 desc: Port x set bit y (y= 0..15)
        BS7: bool, // bit offset: 7 desc: Port x set bit y (y= 0..15)
        BS8: bool, // bit offset: 8 desc: Port x set bit y (y= 0..15)
        BS9: bool, // bit offset: 9 desc: Port x set bit y (y= 0..15)
        BS10: bool, // bit offset: 10 desc: Port x set bit y (y= 0..15)
        BS11: bool, // bit offset: 11 desc: Port x set bit y (y= 0..15)
        BS12: bool, // bit offset: 12 desc: Port x set bit y (y= 0..15)
        BS13: bool, // bit offset: 13 desc: Port x set bit y (y= 0..15)
        BS14: bool, // bit offset: 14 desc: Port x set bit y (y= 0..15)
        BS15: bool, // bit offset: 15 desc: Port x set bit y (y= 0..15)
        BR0: bool, // bit offset: 16 desc: Port x set bit y (y= 0..15)
        BR1: bool, // bit offset: 17 desc: Port x reset bit y (y = 0..15)
        BR2: bool, // bit offset: 18 desc: Port x reset bit y (y = 0..15)
        BR3: bool, // bit offset: 19 desc: Port x reset bit y (y = 0..15)
        BR4: bool, // bit offset: 20 desc: Port x reset bit y (y = 0..15)
        BR5: bool, // bit offset: 21 desc: Port x reset bit y (y = 0..15)
        BR6: bool, // bit offset: 22 desc: Port x reset bit y (y = 0..15)
        BR7: bool, // bit offset: 23 desc: Port x reset bit y (y = 0..15)
        BR8: bool, // bit offset: 24 desc: Port x reset bit y (y = 0..15)
        BR9: bool, // bit offset: 25 desc: Port x reset bit y (y = 0..15)
        BR10: bool, // bit offset: 26 desc: Port x reset bit y (y = 0..15)
        BR11: bool, // bit offset: 27 desc: Port x reset bit y (y = 0..15)
        BR12: bool, // bit offset: 28 desc: Port x reset bit y (y = 0..15)
        BR13: bool, // bit offset: 29 desc: Port x reset bit y (y = 0..15)
        BR14: bool, // bit offset: 30 desc: Port x reset bit y (y = 0..15)
        BR15: bool, // bit offset: 31 desc: Port x reset bit y (y = 0..15)
    });
    // byte offset: 28 GPIO port configuration lock register
    pub const LCKR = MMIO(Address + 0x0000001c, u32, packed struct {
        LCK0: bool, // bit offset: 0 desc: Port x lock bit y (y= 0..15)
        LCK1: bool, // bit offset: 1 desc: Port x lock bit y (y= 0..15)
        LCK2: bool, // bit offset: 2 desc: Port x lock bit y (y= 0..15)
        LCK3: bool, // bit offset: 3 desc: Port x lock bit y (y= 0..15)
        LCK4: bool, // bit offset: 4 desc: Port x lock bit y (y= 0..15)
        LCK5: bool, // bit offset: 5 desc: Port x lock bit y (y= 0..15)
        LCK6: bool, // bit offset: 6 desc: Port x lock bit y (y= 0..15)
        LCK7: bool, // bit offset: 7 desc: Port x lock bit y (y= 0..15)
        LCK8: bool, // bit offset: 8 desc: Port x lock bit y (y= 0..15)
        LCK9: bool, // bit offset: 9 desc: Port x lock bit y (y= 0..15)
        LCK10: bool, // bit offset: 10 desc: Port x lock bit y (y= 0..15)
        LCK11: bool, // bit offset: 11 desc: Port x lock bit y (y= 0..15)
        LCK12: bool, // bit offset: 12 desc: Port x lock bit y (y= 0..15)
        LCK13: bool, // bit offset: 13 desc: Port x lock bit y (y= 0..15)
        LCK14: bool, // bit offset: 14 desc: Port x lock bit y (y= 0..15)
        LCK15: bool, // bit offset: 15 desc: Port x lock bit y (y= 0..15)
        LCKK: bool, // bit offset: 16 desc: Lok Key
        padding: u15 = 0,
    });
    // byte offset: 32 GPIO alternate function low register
    pub const AFRL = MMIO(Address + 0x00000020, u32, packed struct {
        AFRL0: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL1: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL2: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL3: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL4: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL5: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL6: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL7: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 0..7)
    });
    // byte offset: 36 GPIO alternate function high register
    pub const AFRH = MMIO(Address + 0x00000024, u32, packed struct {
        AFRH8: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH9: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH10: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH11: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH12: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH13: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH14: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH15: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 8..15)
    });
    // byte offset: 40 Port bit reset register
    pub const BRR = MMIO(Address + 0x00000028, u32, packed struct {
        BR0: bool, // bit offset: 0 desc: Port x Reset bit y
        BR1: bool, // bit offset: 1 desc: Port x Reset bit y
        BR2: bool, // bit offset: 2 desc: Port x Reset bit y
        BR3: bool, // bit offset: 3 desc: Port x Reset bit y
        BR4: bool, // bit offset: 4 desc: Port x Reset bit y
        BR5: bool, // bit offset: 5 desc: Port x Reset bit y
        BR6: bool, // bit offset: 6 desc: Port x Reset bit y
        BR7: bool, // bit offset: 7 desc: Port x Reset bit y
        BR8: bool, // bit offset: 8 desc: Port x Reset bit y
        BR9: bool, // bit offset: 9 desc: Port x Reset bit y
        BR10: bool, // bit offset: 10 desc: Port x Reset bit y
        BR11: bool, // bit offset: 11 desc: Port x Reset bit y
        BR12: bool, // bit offset: 12 desc: Port x Reset bit y
        BR13: bool, // bit offset: 13 desc: Port x Reset bit y
        BR14: bool, // bit offset: 14 desc: Port x Reset bit y
        BR15: bool, // bit offset: 15 desc: Port x Reset bit y
        padding: u16 = 0,
    });
};
pub const GPIOD = extern struct {
    pub const Address: u32 = 0x48000c00;
    // byte offset: 0 GPIO port mode register
    pub const MODER = MMIO(Address + 0x00000000, u32, packed struct {
        MODER0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        MODER1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        MODER2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        MODER3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        MODER4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        MODER5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        MODER6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        MODER7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        MODER8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        MODER9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        MODER10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        MODER11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        MODER12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        MODER13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        MODER14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        MODER15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 4 GPIO port output type register
    pub const OTYPER = MMIO(Address + 0x00000004, u32, packed struct {
        OT0: bool, // bit offset: 0 desc: Port x configuration bit 0
        OT1: bool, // bit offset: 1 desc: Port x configuration bit 1
        OT2: bool, // bit offset: 2 desc: Port x configuration bit 2
        OT3: bool, // bit offset: 3 desc: Port x configuration bit 3
        OT4: bool, // bit offset: 4 desc: Port x configuration bit 4
        OT5: bool, // bit offset: 5 desc: Port x configuration bit 5
        OT6: bool, // bit offset: 6 desc: Port x configuration bit 6
        OT7: bool, // bit offset: 7 desc: Port x configuration bit 7
        OT8: bool, // bit offset: 8 desc: Port x configuration bit 8
        OT9: bool, // bit offset: 9 desc: Port x configuration bit 9
        OT10: bool, // bit offset: 10 desc: Port x configuration bit 10
        OT11: bool, // bit offset: 11 desc: Port x configuration bit 11
        OT12: bool, // bit offset: 12 desc: Port x configuration bit 12
        OT13: bool, // bit offset: 13 desc: Port x configuration bit 13
        OT14: bool, // bit offset: 14 desc: Port x configuration bit 14
        OT15: bool, // bit offset: 15 desc: Port x configuration bit 15
        padding: u16 = 0,
    });
    // byte offset: 8 GPIO port output speed register
    pub const OSPEEDR = MMIO(Address + 0x00000008, u32, packed struct {
        OSPEEDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        OSPEEDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        OSPEEDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        OSPEEDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        OSPEEDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        OSPEEDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        OSPEEDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        OSPEEDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        OSPEEDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        OSPEEDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        OSPEEDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        OSPEEDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        OSPEEDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        OSPEEDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        OSPEEDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        OSPEEDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 12 GPIO port pull-up/pull-down register
    pub const PUPDR = MMIO(Address + 0x0000000c, u32, packed struct {
        PUPDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        PUPDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        PUPDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        PUPDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        PUPDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        PUPDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        PUPDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        PUPDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        PUPDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        PUPDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        PUPDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        PUPDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        PUPDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        PUPDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        PUPDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        PUPDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 16 GPIO port input data register
    pub const IDR = MMIO(Address + 0x00000010, u32, packed struct {
        IDR0: bool, // bit offset: 0 desc: Port input data (y = 0..15)
        IDR1: bool, // bit offset: 1 desc: Port input data (y = 0..15)
        IDR2: bool, // bit offset: 2 desc: Port input data (y = 0..15)
        IDR3: bool, // bit offset: 3 desc: Port input data (y = 0..15)
        IDR4: bool, // bit offset: 4 desc: Port input data (y = 0..15)
        IDR5: bool, // bit offset: 5 desc: Port input data (y = 0..15)
        IDR6: bool, // bit offset: 6 desc: Port input data (y = 0..15)
        IDR7: bool, // bit offset: 7 desc: Port input data (y = 0..15)
        IDR8: bool, // bit offset: 8 desc: Port input data (y = 0..15)
        IDR9: bool, // bit offset: 9 desc: Port input data (y = 0..15)
        IDR10: bool, // bit offset: 10 desc: Port input data (y = 0..15)
        IDR11: bool, // bit offset: 11 desc: Port input data (y = 0..15)
        IDR12: bool, // bit offset: 12 desc: Port input data (y = 0..15)
        IDR13: bool, // bit offset: 13 desc: Port input data (y = 0..15)
        IDR14: bool, // bit offset: 14 desc: Port input data (y = 0..15)
        IDR15: bool, // bit offset: 15 desc: Port input data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 20 GPIO port output data register
    pub const ODR = MMIO(Address + 0x00000014, u32, packed struct {
        ODR0: bool, // bit offset: 0 desc: Port output data (y = 0..15)
        ODR1: bool, // bit offset: 1 desc: Port output data (y = 0..15)
        ODR2: bool, // bit offset: 2 desc: Port output data (y = 0..15)
        ODR3: bool, // bit offset: 3 desc: Port output data (y = 0..15)
        ODR4: bool, // bit offset: 4 desc: Port output data (y = 0..15)
        ODR5: bool, // bit offset: 5 desc: Port output data (y = 0..15)
        ODR6: bool, // bit offset: 6 desc: Port output data (y = 0..15)
        ODR7: bool, // bit offset: 7 desc: Port output data (y = 0..15)
        ODR8: bool, // bit offset: 8 desc: Port output data (y = 0..15)
        ODR9: bool, // bit offset: 9 desc: Port output data (y = 0..15)
        ODR10: bool, // bit offset: 10 desc: Port output data (y = 0..15)
        ODR11: bool, // bit offset: 11 desc: Port output data (y = 0..15)
        ODR12: bool, // bit offset: 12 desc: Port output data (y = 0..15)
        ODR13: bool, // bit offset: 13 desc: Port output data (y = 0..15)
        ODR14: bool, // bit offset: 14 desc: Port output data (y = 0..15)
        ODR15: bool, // bit offset: 15 desc: Port output data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 24 GPIO port bit set/reset register
    pub const BSRR = MMIO(Address + 0x00000018, u32, packed struct {
        BS0: bool, // bit offset: 0 desc: Port x set bit y (y= 0..15)
        BS1: bool, // bit offset: 1 desc: Port x set bit y (y= 0..15)
        BS2: bool, // bit offset: 2 desc: Port x set bit y (y= 0..15)
        BS3: bool, // bit offset: 3 desc: Port x set bit y (y= 0..15)
        BS4: bool, // bit offset: 4 desc: Port x set bit y (y= 0..15)
        BS5: bool, // bit offset: 5 desc: Port x set bit y (y= 0..15)
        BS6: bool, // bit offset: 6 desc: Port x set bit y (y= 0..15)
        BS7: bool, // bit offset: 7 desc: Port x set bit y (y= 0..15)
        BS8: bool, // bit offset: 8 desc: Port x set bit y (y= 0..15)
        BS9: bool, // bit offset: 9 desc: Port x set bit y (y= 0..15)
        BS10: bool, // bit offset: 10 desc: Port x set bit y (y= 0..15)
        BS11: bool, // bit offset: 11 desc: Port x set bit y (y= 0..15)
        BS12: bool, // bit offset: 12 desc: Port x set bit y (y= 0..15)
        BS13: bool, // bit offset: 13 desc: Port x set bit y (y= 0..15)
        BS14: bool, // bit offset: 14 desc: Port x set bit y (y= 0..15)
        BS15: bool, // bit offset: 15 desc: Port x set bit y (y= 0..15)
        BR0: bool, // bit offset: 16 desc: Port x set bit y (y= 0..15)
        BR1: bool, // bit offset: 17 desc: Port x reset bit y (y = 0..15)
        BR2: bool, // bit offset: 18 desc: Port x reset bit y (y = 0..15)
        BR3: bool, // bit offset: 19 desc: Port x reset bit y (y = 0..15)
        BR4: bool, // bit offset: 20 desc: Port x reset bit y (y = 0..15)
        BR5: bool, // bit offset: 21 desc: Port x reset bit y (y = 0..15)
        BR6: bool, // bit offset: 22 desc: Port x reset bit y (y = 0..15)
        BR7: bool, // bit offset: 23 desc: Port x reset bit y (y = 0..15)
        BR8: bool, // bit offset: 24 desc: Port x reset bit y (y = 0..15)
        BR9: bool, // bit offset: 25 desc: Port x reset bit y (y = 0..15)
        BR10: bool, // bit offset: 26 desc: Port x reset bit y (y = 0..15)
        BR11: bool, // bit offset: 27 desc: Port x reset bit y (y = 0..15)
        BR12: bool, // bit offset: 28 desc: Port x reset bit y (y = 0..15)
        BR13: bool, // bit offset: 29 desc: Port x reset bit y (y = 0..15)
        BR14: bool, // bit offset: 30 desc: Port x reset bit y (y = 0..15)
        BR15: bool, // bit offset: 31 desc: Port x reset bit y (y = 0..15)
    });
    // byte offset: 28 GPIO port configuration lock register
    pub const LCKR = MMIO(Address + 0x0000001c, u32, packed struct {
        LCK0: bool, // bit offset: 0 desc: Port x lock bit y (y= 0..15)
        LCK1: bool, // bit offset: 1 desc: Port x lock bit y (y= 0..15)
        LCK2: bool, // bit offset: 2 desc: Port x lock bit y (y= 0..15)
        LCK3: bool, // bit offset: 3 desc: Port x lock bit y (y= 0..15)
        LCK4: bool, // bit offset: 4 desc: Port x lock bit y (y= 0..15)
        LCK5: bool, // bit offset: 5 desc: Port x lock bit y (y= 0..15)
        LCK6: bool, // bit offset: 6 desc: Port x lock bit y (y= 0..15)
        LCK7: bool, // bit offset: 7 desc: Port x lock bit y (y= 0..15)
        LCK8: bool, // bit offset: 8 desc: Port x lock bit y (y= 0..15)
        LCK9: bool, // bit offset: 9 desc: Port x lock bit y (y= 0..15)
        LCK10: bool, // bit offset: 10 desc: Port x lock bit y (y= 0..15)
        LCK11: bool, // bit offset: 11 desc: Port x lock bit y (y= 0..15)
        LCK12: bool, // bit offset: 12 desc: Port x lock bit y (y= 0..15)
        LCK13: bool, // bit offset: 13 desc: Port x lock bit y (y= 0..15)
        LCK14: bool, // bit offset: 14 desc: Port x lock bit y (y= 0..15)
        LCK15: bool, // bit offset: 15 desc: Port x lock bit y (y= 0..15)
        LCKK: bool, // bit offset: 16 desc: Lok Key
        padding: u15 = 0,
    });
    // byte offset: 32 GPIO alternate function low register
    pub const AFRL = MMIO(Address + 0x00000020, u32, packed struct {
        AFRL0: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL1: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL2: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL3: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL4: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL5: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL6: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL7: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 0..7)
    });
    // byte offset: 36 GPIO alternate function high register
    pub const AFRH = MMIO(Address + 0x00000024, u32, packed struct {
        AFRH8: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH9: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH10: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH11: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH12: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH13: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH14: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH15: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 8..15)
    });
    // byte offset: 40 Port bit reset register
    pub const BRR = MMIO(Address + 0x00000028, u32, packed struct {
        BR0: bool, // bit offset: 0 desc: Port x Reset bit y
        BR1: bool, // bit offset: 1 desc: Port x Reset bit y
        BR2: bool, // bit offset: 2 desc: Port x Reset bit y
        BR3: bool, // bit offset: 3 desc: Port x Reset bit y
        BR4: bool, // bit offset: 4 desc: Port x Reset bit y
        BR5: bool, // bit offset: 5 desc: Port x Reset bit y
        BR6: bool, // bit offset: 6 desc: Port x Reset bit y
        BR7: bool, // bit offset: 7 desc: Port x Reset bit y
        BR8: bool, // bit offset: 8 desc: Port x Reset bit y
        BR9: bool, // bit offset: 9 desc: Port x Reset bit y
        BR10: bool, // bit offset: 10 desc: Port x Reset bit y
        BR11: bool, // bit offset: 11 desc: Port x Reset bit y
        BR12: bool, // bit offset: 12 desc: Port x Reset bit y
        BR13: bool, // bit offset: 13 desc: Port x Reset bit y
        BR14: bool, // bit offset: 14 desc: Port x Reset bit y
        BR15: bool, // bit offset: 15 desc: Port x Reset bit y
        padding: u16 = 0,
    });
};
pub const GPIOE = extern struct {
    pub const Address: u32 = 0x48001000;
    // byte offset: 0 GPIO port mode register
    pub const MODER = MMIO(Address + 0x00000000, u32, packed struct {
        MODER0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        MODER1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        MODER2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        MODER3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        MODER4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        MODER5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        MODER6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        MODER7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        MODER8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        MODER9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        MODER10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        MODER11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        MODER12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        MODER13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        MODER14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        MODER15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 4 GPIO port output type register
    pub const OTYPER = MMIO(Address + 0x00000004, u32, packed struct {
        OT0: bool, // bit offset: 0 desc: Port x configuration bit 0
        OT1: bool, // bit offset: 1 desc: Port x configuration bit 1
        OT2: bool, // bit offset: 2 desc: Port x configuration bit 2
        OT3: bool, // bit offset: 3 desc: Port x configuration bit 3
        OT4: bool, // bit offset: 4 desc: Port x configuration bit 4
        OT5: bool, // bit offset: 5 desc: Port x configuration bit 5
        OT6: bool, // bit offset: 6 desc: Port x configuration bit 6
        OT7: bool, // bit offset: 7 desc: Port x configuration bit 7
        OT8: bool, // bit offset: 8 desc: Port x configuration bit 8
        OT9: bool, // bit offset: 9 desc: Port x configuration bit 9
        OT10: bool, // bit offset: 10 desc: Port x configuration bit 10
        OT11: bool, // bit offset: 11 desc: Port x configuration bit 11
        OT12: bool, // bit offset: 12 desc: Port x configuration bit 12
        OT13: bool, // bit offset: 13 desc: Port x configuration bit 13
        OT14: bool, // bit offset: 14 desc: Port x configuration bit 14
        OT15: bool, // bit offset: 15 desc: Port x configuration bit 15
        padding: u16 = 0,
    });
    // byte offset: 8 GPIO port output speed register
    pub const OSPEEDR = MMIO(Address + 0x00000008, u32, packed struct {
        OSPEEDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        OSPEEDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        OSPEEDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        OSPEEDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        OSPEEDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        OSPEEDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        OSPEEDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        OSPEEDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        OSPEEDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        OSPEEDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        OSPEEDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        OSPEEDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        OSPEEDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        OSPEEDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        OSPEEDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        OSPEEDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 12 GPIO port pull-up/pull-down register
    pub const PUPDR = MMIO(Address + 0x0000000c, u32, packed struct {
        PUPDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        PUPDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        PUPDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        PUPDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        PUPDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        PUPDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        PUPDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        PUPDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        PUPDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        PUPDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        PUPDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        PUPDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        PUPDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        PUPDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        PUPDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        PUPDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 16 GPIO port input data register
    pub const IDR = MMIO(Address + 0x00000010, u32, packed struct {
        IDR0: bool, // bit offset: 0 desc: Port input data (y = 0..15)
        IDR1: bool, // bit offset: 1 desc: Port input data (y = 0..15)
        IDR2: bool, // bit offset: 2 desc: Port input data (y = 0..15)
        IDR3: bool, // bit offset: 3 desc: Port input data (y = 0..15)
        IDR4: bool, // bit offset: 4 desc: Port input data (y = 0..15)
        IDR5: bool, // bit offset: 5 desc: Port input data (y = 0..15)
        IDR6: bool, // bit offset: 6 desc: Port input data (y = 0..15)
        IDR7: bool, // bit offset: 7 desc: Port input data (y = 0..15)
        IDR8: bool, // bit offset: 8 desc: Port input data (y = 0..15)
        IDR9: bool, // bit offset: 9 desc: Port input data (y = 0..15)
        IDR10: bool, // bit offset: 10 desc: Port input data (y = 0..15)
        IDR11: bool, // bit offset: 11 desc: Port input data (y = 0..15)
        IDR12: bool, // bit offset: 12 desc: Port input data (y = 0..15)
        IDR13: bool, // bit offset: 13 desc: Port input data (y = 0..15)
        IDR14: bool, // bit offset: 14 desc: Port input data (y = 0..15)
        IDR15: bool, // bit offset: 15 desc: Port input data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 20 GPIO port output data register
    pub const ODR = MMIO(Address + 0x00000014, u32, packed struct {
        ODR0: bool, // bit offset: 0 desc: Port output data (y = 0..15)
        ODR1: bool, // bit offset: 1 desc: Port output data (y = 0..15)
        ODR2: bool, // bit offset: 2 desc: Port output data (y = 0..15)
        ODR3: bool, // bit offset: 3 desc: Port output data (y = 0..15)
        ODR4: bool, // bit offset: 4 desc: Port output data (y = 0..15)
        ODR5: bool, // bit offset: 5 desc: Port output data (y = 0..15)
        ODR6: bool, // bit offset: 6 desc: Port output data (y = 0..15)
        ODR7: bool, // bit offset: 7 desc: Port output data (y = 0..15)
        ODR8: bool, // bit offset: 8 desc: Port output data (y = 0..15)
        ODR9: bool, // bit offset: 9 desc: Port output data (y = 0..15)
        ODR10: bool, // bit offset: 10 desc: Port output data (y = 0..15)
        ODR11: bool, // bit offset: 11 desc: Port output data (y = 0..15)
        ODR12: bool, // bit offset: 12 desc: Port output data (y = 0..15)
        ODR13: bool, // bit offset: 13 desc: Port output data (y = 0..15)
        ODR14: bool, // bit offset: 14 desc: Port output data (y = 0..15)
        ODR15: bool, // bit offset: 15 desc: Port output data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 24 GPIO port bit set/reset register
    pub const BSRR = MMIO(Address + 0x00000018, u32, packed struct {
        BS0: bool, // bit offset: 0 desc: Port x set bit y (y= 0..15)
        BS1: bool, // bit offset: 1 desc: Port x set bit y (y= 0..15)
        BS2: bool, // bit offset: 2 desc: Port x set bit y (y= 0..15)
        BS3: bool, // bit offset: 3 desc: Port x set bit y (y= 0..15)
        BS4: bool, // bit offset: 4 desc: Port x set bit y (y= 0..15)
        BS5: bool, // bit offset: 5 desc: Port x set bit y (y= 0..15)
        BS6: bool, // bit offset: 6 desc: Port x set bit y (y= 0..15)
        BS7: bool, // bit offset: 7 desc: Port x set bit y (y= 0..15)
        BS8: bool, // bit offset: 8 desc: Port x set bit y (y= 0..15)
        BS9: bool, // bit offset: 9 desc: Port x set bit y (y= 0..15)
        BS10: bool, // bit offset: 10 desc: Port x set bit y (y= 0..15)
        BS11: bool, // bit offset: 11 desc: Port x set bit y (y= 0..15)
        BS12: bool, // bit offset: 12 desc: Port x set bit y (y= 0..15)
        BS13: bool, // bit offset: 13 desc: Port x set bit y (y= 0..15)
        BS14: bool, // bit offset: 14 desc: Port x set bit y (y= 0..15)
        BS15: bool, // bit offset: 15 desc: Port x set bit y (y= 0..15)
        BR0: bool, // bit offset: 16 desc: Port x set bit y (y= 0..15)
        BR1: bool, // bit offset: 17 desc: Port x reset bit y (y = 0..15)
        BR2: bool, // bit offset: 18 desc: Port x reset bit y (y = 0..15)
        BR3: bool, // bit offset: 19 desc: Port x reset bit y (y = 0..15)
        BR4: bool, // bit offset: 20 desc: Port x reset bit y (y = 0..15)
        BR5: bool, // bit offset: 21 desc: Port x reset bit y (y = 0..15)
        BR6: bool, // bit offset: 22 desc: Port x reset bit y (y = 0..15)
        BR7: bool, // bit offset: 23 desc: Port x reset bit y (y = 0..15)
        BR8: bool, // bit offset: 24 desc: Port x reset bit y (y = 0..15)
        BR9: bool, // bit offset: 25 desc: Port x reset bit y (y = 0..15)
        BR10: bool, // bit offset: 26 desc: Port x reset bit y (y = 0..15)
        BR11: bool, // bit offset: 27 desc: Port x reset bit y (y = 0..15)
        BR12: bool, // bit offset: 28 desc: Port x reset bit y (y = 0..15)
        BR13: bool, // bit offset: 29 desc: Port x reset bit y (y = 0..15)
        BR14: bool, // bit offset: 30 desc: Port x reset bit y (y = 0..15)
        BR15: bool, // bit offset: 31 desc: Port x reset bit y (y = 0..15)
    });
    // byte offset: 28 GPIO port configuration lock register
    pub const LCKR = MMIO(Address + 0x0000001c, u32, packed struct {
        LCK0: bool, // bit offset: 0 desc: Port x lock bit y (y= 0..15)
        LCK1: bool, // bit offset: 1 desc: Port x lock bit y (y= 0..15)
        LCK2: bool, // bit offset: 2 desc: Port x lock bit y (y= 0..15)
        LCK3: bool, // bit offset: 3 desc: Port x lock bit y (y= 0..15)
        LCK4: bool, // bit offset: 4 desc: Port x lock bit y (y= 0..15)
        LCK5: bool, // bit offset: 5 desc: Port x lock bit y (y= 0..15)
        LCK6: bool, // bit offset: 6 desc: Port x lock bit y (y= 0..15)
        LCK7: bool, // bit offset: 7 desc: Port x lock bit y (y= 0..15)
        LCK8: bool, // bit offset: 8 desc: Port x lock bit y (y= 0..15)
        LCK9: bool, // bit offset: 9 desc: Port x lock bit y (y= 0..15)
        LCK10: bool, // bit offset: 10 desc: Port x lock bit y (y= 0..15)
        LCK11: bool, // bit offset: 11 desc: Port x lock bit y (y= 0..15)
        LCK12: bool, // bit offset: 12 desc: Port x lock bit y (y= 0..15)
        LCK13: bool, // bit offset: 13 desc: Port x lock bit y (y= 0..15)
        LCK14: bool, // bit offset: 14 desc: Port x lock bit y (y= 0..15)
        LCK15: bool, // bit offset: 15 desc: Port x lock bit y (y= 0..15)
        LCKK: bool, // bit offset: 16 desc: Lok Key
        padding: u15 = 0,
    });
    // byte offset: 32 GPIO alternate function low register
    pub const AFRL = MMIO(Address + 0x00000020, u32, packed struct {
        AFRL0: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL1: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL2: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL3: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL4: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL5: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL6: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL7: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 0..7)
    });
    // byte offset: 36 GPIO alternate function high register
    pub const AFRH = MMIO(Address + 0x00000024, u32, packed struct {
        AFRH8: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH9: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH10: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH11: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH12: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH13: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH14: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH15: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 8..15)
    });
    // byte offset: 40 Port bit reset register
    pub const BRR = MMIO(Address + 0x00000028, u32, packed struct {
        BR0: bool, // bit offset: 0 desc: Port x Reset bit y
        BR1: bool, // bit offset: 1 desc: Port x Reset bit y
        BR2: bool, // bit offset: 2 desc: Port x Reset bit y
        BR3: bool, // bit offset: 3 desc: Port x Reset bit y
        BR4: bool, // bit offset: 4 desc: Port x Reset bit y
        BR5: bool, // bit offset: 5 desc: Port x Reset bit y
        BR6: bool, // bit offset: 6 desc: Port x Reset bit y
        BR7: bool, // bit offset: 7 desc: Port x Reset bit y
        BR8: bool, // bit offset: 8 desc: Port x Reset bit y
        BR9: bool, // bit offset: 9 desc: Port x Reset bit y
        BR10: bool, // bit offset: 10 desc: Port x Reset bit y
        BR11: bool, // bit offset: 11 desc: Port x Reset bit y
        BR12: bool, // bit offset: 12 desc: Port x Reset bit y
        BR13: bool, // bit offset: 13 desc: Port x Reset bit y
        BR14: bool, // bit offset: 14 desc: Port x Reset bit y
        BR15: bool, // bit offset: 15 desc: Port x Reset bit y
        padding: u16 = 0,
    });
};
pub const GPIOF = extern struct {
    pub const Address: u32 = 0x48001400;
    // byte offset: 0 GPIO port mode register
    pub const MODER = MMIO(Address + 0x00000000, u32, packed struct {
        MODER0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        MODER1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        MODER2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        MODER3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        MODER4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        MODER5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        MODER6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        MODER7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        MODER8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        MODER9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        MODER10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        MODER11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        MODER12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        MODER13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        MODER14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        MODER15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 4 GPIO port output type register
    pub const OTYPER = MMIO(Address + 0x00000004, u32, packed struct {
        OT0: bool, // bit offset: 0 desc: Port x configuration bit 0
        OT1: bool, // bit offset: 1 desc: Port x configuration bit 1
        OT2: bool, // bit offset: 2 desc: Port x configuration bit 2
        OT3: bool, // bit offset: 3 desc: Port x configuration bit 3
        OT4: bool, // bit offset: 4 desc: Port x configuration bit 4
        OT5: bool, // bit offset: 5 desc: Port x configuration bit 5
        OT6: bool, // bit offset: 6 desc: Port x configuration bit 6
        OT7: bool, // bit offset: 7 desc: Port x configuration bit 7
        OT8: bool, // bit offset: 8 desc: Port x configuration bit 8
        OT9: bool, // bit offset: 9 desc: Port x configuration bit 9
        OT10: bool, // bit offset: 10 desc: Port x configuration bit 10
        OT11: bool, // bit offset: 11 desc: Port x configuration bit 11
        OT12: bool, // bit offset: 12 desc: Port x configuration bit 12
        OT13: bool, // bit offset: 13 desc: Port x configuration bit 13
        OT14: bool, // bit offset: 14 desc: Port x configuration bit 14
        OT15: bool, // bit offset: 15 desc: Port x configuration bit 15
        padding: u16 = 0,
    });
    // byte offset: 8 GPIO port output speed register
    pub const OSPEEDR = MMIO(Address + 0x00000008, u32, packed struct {
        OSPEEDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        OSPEEDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        OSPEEDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        OSPEEDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        OSPEEDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        OSPEEDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        OSPEEDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        OSPEEDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        OSPEEDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        OSPEEDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        OSPEEDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        OSPEEDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        OSPEEDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        OSPEEDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        OSPEEDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        OSPEEDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 12 GPIO port pull-up/pull-down register
    pub const PUPDR = MMIO(Address + 0x0000000c, u32, packed struct {
        PUPDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        PUPDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        PUPDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        PUPDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        PUPDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        PUPDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        PUPDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        PUPDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        PUPDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        PUPDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        PUPDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        PUPDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        PUPDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        PUPDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        PUPDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        PUPDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 16 GPIO port input data register
    pub const IDR = MMIO(Address + 0x00000010, u32, packed struct {
        IDR0: bool, // bit offset: 0 desc: Port input data (y = 0..15)
        IDR1: bool, // bit offset: 1 desc: Port input data (y = 0..15)
        IDR2: bool, // bit offset: 2 desc: Port input data (y = 0..15)
        IDR3: bool, // bit offset: 3 desc: Port input data (y = 0..15)
        IDR4: bool, // bit offset: 4 desc: Port input data (y = 0..15)
        IDR5: bool, // bit offset: 5 desc: Port input data (y = 0..15)
        IDR6: bool, // bit offset: 6 desc: Port input data (y = 0..15)
        IDR7: bool, // bit offset: 7 desc: Port input data (y = 0..15)
        IDR8: bool, // bit offset: 8 desc: Port input data (y = 0..15)
        IDR9: bool, // bit offset: 9 desc: Port input data (y = 0..15)
        IDR10: bool, // bit offset: 10 desc: Port input data (y = 0..15)
        IDR11: bool, // bit offset: 11 desc: Port input data (y = 0..15)
        IDR12: bool, // bit offset: 12 desc: Port input data (y = 0..15)
        IDR13: bool, // bit offset: 13 desc: Port input data (y = 0..15)
        IDR14: bool, // bit offset: 14 desc: Port input data (y = 0..15)
        IDR15: bool, // bit offset: 15 desc: Port input data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 20 GPIO port output data register
    pub const ODR = MMIO(Address + 0x00000014, u32, packed struct {
        ODR0: bool, // bit offset: 0 desc: Port output data (y = 0..15)
        ODR1: bool, // bit offset: 1 desc: Port output data (y = 0..15)
        ODR2: bool, // bit offset: 2 desc: Port output data (y = 0..15)
        ODR3: bool, // bit offset: 3 desc: Port output data (y = 0..15)
        ODR4: bool, // bit offset: 4 desc: Port output data (y = 0..15)
        ODR5: bool, // bit offset: 5 desc: Port output data (y = 0..15)
        ODR6: bool, // bit offset: 6 desc: Port output data (y = 0..15)
        ODR7: bool, // bit offset: 7 desc: Port output data (y = 0..15)
        ODR8: bool, // bit offset: 8 desc: Port output data (y = 0..15)
        ODR9: bool, // bit offset: 9 desc: Port output data (y = 0..15)
        ODR10: bool, // bit offset: 10 desc: Port output data (y = 0..15)
        ODR11: bool, // bit offset: 11 desc: Port output data (y = 0..15)
        ODR12: bool, // bit offset: 12 desc: Port output data (y = 0..15)
        ODR13: bool, // bit offset: 13 desc: Port output data (y = 0..15)
        ODR14: bool, // bit offset: 14 desc: Port output data (y = 0..15)
        ODR15: bool, // bit offset: 15 desc: Port output data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 24 GPIO port bit set/reset register
    pub const BSRR = MMIO(Address + 0x00000018, u32, packed struct {
        BS0: bool, // bit offset: 0 desc: Port x set bit y (y= 0..15)
        BS1: bool, // bit offset: 1 desc: Port x set bit y (y= 0..15)
        BS2: bool, // bit offset: 2 desc: Port x set bit y (y= 0..15)
        BS3: bool, // bit offset: 3 desc: Port x set bit y (y= 0..15)
        BS4: bool, // bit offset: 4 desc: Port x set bit y (y= 0..15)
        BS5: bool, // bit offset: 5 desc: Port x set bit y (y= 0..15)
        BS6: bool, // bit offset: 6 desc: Port x set bit y (y= 0..15)
        BS7: bool, // bit offset: 7 desc: Port x set bit y (y= 0..15)
        BS8: bool, // bit offset: 8 desc: Port x set bit y (y= 0..15)
        BS9: bool, // bit offset: 9 desc: Port x set bit y (y= 0..15)
        BS10: bool, // bit offset: 10 desc: Port x set bit y (y= 0..15)
        BS11: bool, // bit offset: 11 desc: Port x set bit y (y= 0..15)
        BS12: bool, // bit offset: 12 desc: Port x set bit y (y= 0..15)
        BS13: bool, // bit offset: 13 desc: Port x set bit y (y= 0..15)
        BS14: bool, // bit offset: 14 desc: Port x set bit y (y= 0..15)
        BS15: bool, // bit offset: 15 desc: Port x set bit y (y= 0..15)
        BR0: bool, // bit offset: 16 desc: Port x set bit y (y= 0..15)
        BR1: bool, // bit offset: 17 desc: Port x reset bit y (y = 0..15)
        BR2: bool, // bit offset: 18 desc: Port x reset bit y (y = 0..15)
        BR3: bool, // bit offset: 19 desc: Port x reset bit y (y = 0..15)
        BR4: bool, // bit offset: 20 desc: Port x reset bit y (y = 0..15)
        BR5: bool, // bit offset: 21 desc: Port x reset bit y (y = 0..15)
        BR6: bool, // bit offset: 22 desc: Port x reset bit y (y = 0..15)
        BR7: bool, // bit offset: 23 desc: Port x reset bit y (y = 0..15)
        BR8: bool, // bit offset: 24 desc: Port x reset bit y (y = 0..15)
        BR9: bool, // bit offset: 25 desc: Port x reset bit y (y = 0..15)
        BR10: bool, // bit offset: 26 desc: Port x reset bit y (y = 0..15)
        BR11: bool, // bit offset: 27 desc: Port x reset bit y (y = 0..15)
        BR12: bool, // bit offset: 28 desc: Port x reset bit y (y = 0..15)
        BR13: bool, // bit offset: 29 desc: Port x reset bit y (y = 0..15)
        BR14: bool, // bit offset: 30 desc: Port x reset bit y (y = 0..15)
        BR15: bool, // bit offset: 31 desc: Port x reset bit y (y = 0..15)
    });
    // byte offset: 28 GPIO port configuration lock register
    pub const LCKR = MMIO(Address + 0x0000001c, u32, packed struct {
        LCK0: bool, // bit offset: 0 desc: Port x lock bit y (y= 0..15)
        LCK1: bool, // bit offset: 1 desc: Port x lock bit y (y= 0..15)
        LCK2: bool, // bit offset: 2 desc: Port x lock bit y (y= 0..15)
        LCK3: bool, // bit offset: 3 desc: Port x lock bit y (y= 0..15)
        LCK4: bool, // bit offset: 4 desc: Port x lock bit y (y= 0..15)
        LCK5: bool, // bit offset: 5 desc: Port x lock bit y (y= 0..15)
        LCK6: bool, // bit offset: 6 desc: Port x lock bit y (y= 0..15)
        LCK7: bool, // bit offset: 7 desc: Port x lock bit y (y= 0..15)
        LCK8: bool, // bit offset: 8 desc: Port x lock bit y (y= 0..15)
        LCK9: bool, // bit offset: 9 desc: Port x lock bit y (y= 0..15)
        LCK10: bool, // bit offset: 10 desc: Port x lock bit y (y= 0..15)
        LCK11: bool, // bit offset: 11 desc: Port x lock bit y (y= 0..15)
        LCK12: bool, // bit offset: 12 desc: Port x lock bit y (y= 0..15)
        LCK13: bool, // bit offset: 13 desc: Port x lock bit y (y= 0..15)
        LCK14: bool, // bit offset: 14 desc: Port x lock bit y (y= 0..15)
        LCK15: bool, // bit offset: 15 desc: Port x lock bit y (y= 0..15)
        LCKK: bool, // bit offset: 16 desc: Lok Key
        padding: u15 = 0,
    });
    // byte offset: 32 GPIO alternate function low register
    pub const AFRL = MMIO(Address + 0x00000020, u32, packed struct {
        AFRL0: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL1: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL2: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL3: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL4: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL5: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL6: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL7: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 0..7)
    });
    // byte offset: 36 GPIO alternate function high register
    pub const AFRH = MMIO(Address + 0x00000024, u32, packed struct {
        AFRH8: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH9: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH10: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH11: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH12: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH13: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH14: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH15: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 8..15)
    });
    // byte offset: 40 Port bit reset register
    pub const BRR = MMIO(Address + 0x00000028, u32, packed struct {
        BR0: bool, // bit offset: 0 desc: Port x Reset bit y
        BR1: bool, // bit offset: 1 desc: Port x Reset bit y
        BR2: bool, // bit offset: 2 desc: Port x Reset bit y
        BR3: bool, // bit offset: 3 desc: Port x Reset bit y
        BR4: bool, // bit offset: 4 desc: Port x Reset bit y
        BR5: bool, // bit offset: 5 desc: Port x Reset bit y
        BR6: bool, // bit offset: 6 desc: Port x Reset bit y
        BR7: bool, // bit offset: 7 desc: Port x Reset bit y
        BR8: bool, // bit offset: 8 desc: Port x Reset bit y
        BR9: bool, // bit offset: 9 desc: Port x Reset bit y
        BR10: bool, // bit offset: 10 desc: Port x Reset bit y
        BR11: bool, // bit offset: 11 desc: Port x Reset bit y
        BR12: bool, // bit offset: 12 desc: Port x Reset bit y
        BR13: bool, // bit offset: 13 desc: Port x Reset bit y
        BR14: bool, // bit offset: 14 desc: Port x Reset bit y
        BR15: bool, // bit offset: 15 desc: Port x Reset bit y
        padding: u16 = 0,
    });
};
pub const GPIOG = extern struct {
    pub const Address: u32 = 0x48001800;
    // byte offset: 0 GPIO port mode register
    pub const MODER = MMIO(Address + 0x00000000, u32, packed struct {
        MODER0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        MODER1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        MODER2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        MODER3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        MODER4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        MODER5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        MODER6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        MODER7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        MODER8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        MODER9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        MODER10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        MODER11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        MODER12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        MODER13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        MODER14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        MODER15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 4 GPIO port output type register
    pub const OTYPER = MMIO(Address + 0x00000004, u32, packed struct {
        OT0: bool, // bit offset: 0 desc: Port x configuration bit 0
        OT1: bool, // bit offset: 1 desc: Port x configuration bit 1
        OT2: bool, // bit offset: 2 desc: Port x configuration bit 2
        OT3: bool, // bit offset: 3 desc: Port x configuration bit 3
        OT4: bool, // bit offset: 4 desc: Port x configuration bit 4
        OT5: bool, // bit offset: 5 desc: Port x configuration bit 5
        OT6: bool, // bit offset: 6 desc: Port x configuration bit 6
        OT7: bool, // bit offset: 7 desc: Port x configuration bit 7
        OT8: bool, // bit offset: 8 desc: Port x configuration bit 8
        OT9: bool, // bit offset: 9 desc: Port x configuration bit 9
        OT10: bool, // bit offset: 10 desc: Port x configuration bit 10
        OT11: bool, // bit offset: 11 desc: Port x configuration bit 11
        OT12: bool, // bit offset: 12 desc: Port x configuration bit 12
        OT13: bool, // bit offset: 13 desc: Port x configuration bit 13
        OT14: bool, // bit offset: 14 desc: Port x configuration bit 14
        OT15: bool, // bit offset: 15 desc: Port x configuration bit 15
        padding: u16 = 0,
    });
    // byte offset: 8 GPIO port output speed register
    pub const OSPEEDR = MMIO(Address + 0x00000008, u32, packed struct {
        OSPEEDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        OSPEEDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        OSPEEDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        OSPEEDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        OSPEEDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        OSPEEDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        OSPEEDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        OSPEEDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        OSPEEDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        OSPEEDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        OSPEEDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        OSPEEDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        OSPEEDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        OSPEEDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        OSPEEDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        OSPEEDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 12 GPIO port pull-up/pull-down register
    pub const PUPDR = MMIO(Address + 0x0000000c, u32, packed struct {
        PUPDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        PUPDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        PUPDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        PUPDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        PUPDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        PUPDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        PUPDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        PUPDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        PUPDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        PUPDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        PUPDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        PUPDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        PUPDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        PUPDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        PUPDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        PUPDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 16 GPIO port input data register
    pub const IDR = MMIO(Address + 0x00000010, u32, packed struct {
        IDR0: bool, // bit offset: 0 desc: Port input data (y = 0..15)
        IDR1: bool, // bit offset: 1 desc: Port input data (y = 0..15)
        IDR2: bool, // bit offset: 2 desc: Port input data (y = 0..15)
        IDR3: bool, // bit offset: 3 desc: Port input data (y = 0..15)
        IDR4: bool, // bit offset: 4 desc: Port input data (y = 0..15)
        IDR5: bool, // bit offset: 5 desc: Port input data (y = 0..15)
        IDR6: bool, // bit offset: 6 desc: Port input data (y = 0..15)
        IDR7: bool, // bit offset: 7 desc: Port input data (y = 0..15)
        IDR8: bool, // bit offset: 8 desc: Port input data (y = 0..15)
        IDR9: bool, // bit offset: 9 desc: Port input data (y = 0..15)
        IDR10: bool, // bit offset: 10 desc: Port input data (y = 0..15)
        IDR11: bool, // bit offset: 11 desc: Port input data (y = 0..15)
        IDR12: bool, // bit offset: 12 desc: Port input data (y = 0..15)
        IDR13: bool, // bit offset: 13 desc: Port input data (y = 0..15)
        IDR14: bool, // bit offset: 14 desc: Port input data (y = 0..15)
        IDR15: bool, // bit offset: 15 desc: Port input data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 20 GPIO port output data register
    pub const ODR = MMIO(Address + 0x00000014, u32, packed struct {
        ODR0: bool, // bit offset: 0 desc: Port output data (y = 0..15)
        ODR1: bool, // bit offset: 1 desc: Port output data (y = 0..15)
        ODR2: bool, // bit offset: 2 desc: Port output data (y = 0..15)
        ODR3: bool, // bit offset: 3 desc: Port output data (y = 0..15)
        ODR4: bool, // bit offset: 4 desc: Port output data (y = 0..15)
        ODR5: bool, // bit offset: 5 desc: Port output data (y = 0..15)
        ODR6: bool, // bit offset: 6 desc: Port output data (y = 0..15)
        ODR7: bool, // bit offset: 7 desc: Port output data (y = 0..15)
        ODR8: bool, // bit offset: 8 desc: Port output data (y = 0..15)
        ODR9: bool, // bit offset: 9 desc: Port output data (y = 0..15)
        ODR10: bool, // bit offset: 10 desc: Port output data (y = 0..15)
        ODR11: bool, // bit offset: 11 desc: Port output data (y = 0..15)
        ODR12: bool, // bit offset: 12 desc: Port output data (y = 0..15)
        ODR13: bool, // bit offset: 13 desc: Port output data (y = 0..15)
        ODR14: bool, // bit offset: 14 desc: Port output data (y = 0..15)
        ODR15: bool, // bit offset: 15 desc: Port output data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 24 GPIO port bit set/reset register
    pub const BSRR = MMIO(Address + 0x00000018, u32, packed struct {
        BS0: bool, // bit offset: 0 desc: Port x set bit y (y= 0..15)
        BS1: bool, // bit offset: 1 desc: Port x set bit y (y= 0..15)
        BS2: bool, // bit offset: 2 desc: Port x set bit y (y= 0..15)
        BS3: bool, // bit offset: 3 desc: Port x set bit y (y= 0..15)
        BS4: bool, // bit offset: 4 desc: Port x set bit y (y= 0..15)
        BS5: bool, // bit offset: 5 desc: Port x set bit y (y= 0..15)
        BS6: bool, // bit offset: 6 desc: Port x set bit y (y= 0..15)
        BS7: bool, // bit offset: 7 desc: Port x set bit y (y= 0..15)
        BS8: bool, // bit offset: 8 desc: Port x set bit y (y= 0..15)
        BS9: bool, // bit offset: 9 desc: Port x set bit y (y= 0..15)
        BS10: bool, // bit offset: 10 desc: Port x set bit y (y= 0..15)
        BS11: bool, // bit offset: 11 desc: Port x set bit y (y= 0..15)
        BS12: bool, // bit offset: 12 desc: Port x set bit y (y= 0..15)
        BS13: bool, // bit offset: 13 desc: Port x set bit y (y= 0..15)
        BS14: bool, // bit offset: 14 desc: Port x set bit y (y= 0..15)
        BS15: bool, // bit offset: 15 desc: Port x set bit y (y= 0..15)
        BR0: bool, // bit offset: 16 desc: Port x set bit y (y= 0..15)
        BR1: bool, // bit offset: 17 desc: Port x reset bit y (y = 0..15)
        BR2: bool, // bit offset: 18 desc: Port x reset bit y (y = 0..15)
        BR3: bool, // bit offset: 19 desc: Port x reset bit y (y = 0..15)
        BR4: bool, // bit offset: 20 desc: Port x reset bit y (y = 0..15)
        BR5: bool, // bit offset: 21 desc: Port x reset bit y (y = 0..15)
        BR6: bool, // bit offset: 22 desc: Port x reset bit y (y = 0..15)
        BR7: bool, // bit offset: 23 desc: Port x reset bit y (y = 0..15)
        BR8: bool, // bit offset: 24 desc: Port x reset bit y (y = 0..15)
        BR9: bool, // bit offset: 25 desc: Port x reset bit y (y = 0..15)
        BR10: bool, // bit offset: 26 desc: Port x reset bit y (y = 0..15)
        BR11: bool, // bit offset: 27 desc: Port x reset bit y (y = 0..15)
        BR12: bool, // bit offset: 28 desc: Port x reset bit y (y = 0..15)
        BR13: bool, // bit offset: 29 desc: Port x reset bit y (y = 0..15)
        BR14: bool, // bit offset: 30 desc: Port x reset bit y (y = 0..15)
        BR15: bool, // bit offset: 31 desc: Port x reset bit y (y = 0..15)
    });
    // byte offset: 28 GPIO port configuration lock register
    pub const LCKR = MMIO(Address + 0x0000001c, u32, packed struct {
        LCK0: bool, // bit offset: 0 desc: Port x lock bit y (y= 0..15)
        LCK1: bool, // bit offset: 1 desc: Port x lock bit y (y= 0..15)
        LCK2: bool, // bit offset: 2 desc: Port x lock bit y (y= 0..15)
        LCK3: bool, // bit offset: 3 desc: Port x lock bit y (y= 0..15)
        LCK4: bool, // bit offset: 4 desc: Port x lock bit y (y= 0..15)
        LCK5: bool, // bit offset: 5 desc: Port x lock bit y (y= 0..15)
        LCK6: bool, // bit offset: 6 desc: Port x lock bit y (y= 0..15)
        LCK7: bool, // bit offset: 7 desc: Port x lock bit y (y= 0..15)
        LCK8: bool, // bit offset: 8 desc: Port x lock bit y (y= 0..15)
        LCK9: bool, // bit offset: 9 desc: Port x lock bit y (y= 0..15)
        LCK10: bool, // bit offset: 10 desc: Port x lock bit y (y= 0..15)
        LCK11: bool, // bit offset: 11 desc: Port x lock bit y (y= 0..15)
        LCK12: bool, // bit offset: 12 desc: Port x lock bit y (y= 0..15)
        LCK13: bool, // bit offset: 13 desc: Port x lock bit y (y= 0..15)
        LCK14: bool, // bit offset: 14 desc: Port x lock bit y (y= 0..15)
        LCK15: bool, // bit offset: 15 desc: Port x lock bit y (y= 0..15)
        LCKK: bool, // bit offset: 16 desc: Lok Key
        padding: u15 = 0,
    });
    // byte offset: 32 GPIO alternate function low register
    pub const AFRL = MMIO(Address + 0x00000020, u32, packed struct {
        AFRL0: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL1: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL2: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL3: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL4: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL5: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL6: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL7: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 0..7)
    });
    // byte offset: 36 GPIO alternate function high register
    pub const AFRH = MMIO(Address + 0x00000024, u32, packed struct {
        AFRH8: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH9: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH10: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH11: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH12: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH13: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH14: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH15: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 8..15)
    });
    // byte offset: 40 Port bit reset register
    pub const BRR = MMIO(Address + 0x00000028, u32, packed struct {
        BR0: bool, // bit offset: 0 desc: Port x Reset bit y
        BR1: bool, // bit offset: 1 desc: Port x Reset bit y
        BR2: bool, // bit offset: 2 desc: Port x Reset bit y
        BR3: bool, // bit offset: 3 desc: Port x Reset bit y
        BR4: bool, // bit offset: 4 desc: Port x Reset bit y
        BR5: bool, // bit offset: 5 desc: Port x Reset bit y
        BR6: bool, // bit offset: 6 desc: Port x Reset bit y
        BR7: bool, // bit offset: 7 desc: Port x Reset bit y
        BR8: bool, // bit offset: 8 desc: Port x Reset bit y
        BR9: bool, // bit offset: 9 desc: Port x Reset bit y
        BR10: bool, // bit offset: 10 desc: Port x Reset bit y
        BR11: bool, // bit offset: 11 desc: Port x Reset bit y
        BR12: bool, // bit offset: 12 desc: Port x Reset bit y
        BR13: bool, // bit offset: 13 desc: Port x Reset bit y
        BR14: bool, // bit offset: 14 desc: Port x Reset bit y
        BR15: bool, // bit offset: 15 desc: Port x Reset bit y
        padding: u16 = 0,
    });
};
pub const GPIOH = extern struct {
    pub const Address: u32 = 0x48001c00;
    // byte offset: 0 GPIO port mode register
    pub const MODER = MMIO(Address + 0x00000000, u32, packed struct {
        MODER0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        MODER1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        MODER2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        MODER3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        MODER4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        MODER5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        MODER6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        MODER7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        MODER8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        MODER9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        MODER10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        MODER11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        MODER12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        MODER13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        MODER14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        MODER15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 4 GPIO port output type register
    pub const OTYPER = MMIO(Address + 0x00000004, u32, packed struct {
        OT0: bool, // bit offset: 0 desc: Port x configuration bit 0
        OT1: bool, // bit offset: 1 desc: Port x configuration bit 1
        OT2: bool, // bit offset: 2 desc: Port x configuration bit 2
        OT3: bool, // bit offset: 3 desc: Port x configuration bit 3
        OT4: bool, // bit offset: 4 desc: Port x configuration bit 4
        OT5: bool, // bit offset: 5 desc: Port x configuration bit 5
        OT6: bool, // bit offset: 6 desc: Port x configuration bit 6
        OT7: bool, // bit offset: 7 desc: Port x configuration bit 7
        OT8: bool, // bit offset: 8 desc: Port x configuration bit 8
        OT9: bool, // bit offset: 9 desc: Port x configuration bit 9
        OT10: bool, // bit offset: 10 desc: Port x configuration bit 10
        OT11: bool, // bit offset: 11 desc: Port x configuration bit 11
        OT12: bool, // bit offset: 12 desc: Port x configuration bit 12
        OT13: bool, // bit offset: 13 desc: Port x configuration bit 13
        OT14: bool, // bit offset: 14 desc: Port x configuration bit 14
        OT15: bool, // bit offset: 15 desc: Port x configuration bit 15
        padding: u16 = 0,
    });
    // byte offset: 8 GPIO port output speed register
    pub const OSPEEDR = MMIO(Address + 0x00000008, u32, packed struct {
        OSPEEDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        OSPEEDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        OSPEEDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        OSPEEDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        OSPEEDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        OSPEEDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        OSPEEDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        OSPEEDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        OSPEEDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        OSPEEDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        OSPEEDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        OSPEEDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        OSPEEDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        OSPEEDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        OSPEEDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        OSPEEDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 12 GPIO port pull-up/pull-down register
    pub const PUPDR = MMIO(Address + 0x0000000c, u32, packed struct {
        PUPDR0: u2, // bit offset: 0 desc: Port x configuration bits (y = 0..15)
        PUPDR1: u2, // bit offset: 2 desc: Port x configuration bits (y = 0..15)
        PUPDR2: u2, // bit offset: 4 desc: Port x configuration bits (y = 0..15)
        PUPDR3: u2, // bit offset: 6 desc: Port x configuration bits (y = 0..15)
        PUPDR4: u2, // bit offset: 8 desc: Port x configuration bits (y = 0..15)
        PUPDR5: u2, // bit offset: 10 desc: Port x configuration bits (y = 0..15)
        PUPDR6: u2, // bit offset: 12 desc: Port x configuration bits (y = 0..15)
        PUPDR7: u2, // bit offset: 14 desc: Port x configuration bits (y = 0..15)
        PUPDR8: u2, // bit offset: 16 desc: Port x configuration bits (y = 0..15)
        PUPDR9: u2, // bit offset: 18 desc: Port x configuration bits (y = 0..15)
        PUPDR10: u2, // bit offset: 20 desc: Port x configuration bits (y = 0..15)
        PUPDR11: u2, // bit offset: 22 desc: Port x configuration bits (y = 0..15)
        PUPDR12: u2, // bit offset: 24 desc: Port x configuration bits (y = 0..15)
        PUPDR13: u2, // bit offset: 26 desc: Port x configuration bits (y = 0..15)
        PUPDR14: u2, // bit offset: 28 desc: Port x configuration bits (y = 0..15)
        PUPDR15: u2, // bit offset: 30 desc: Port x configuration bits (y = 0..15)
    });
    // byte offset: 16 GPIO port input data register
    pub const IDR = MMIO(Address + 0x00000010, u32, packed struct {
        IDR0: bool, // bit offset: 0 desc: Port input data (y = 0..15)
        IDR1: bool, // bit offset: 1 desc: Port input data (y = 0..15)
        IDR2: bool, // bit offset: 2 desc: Port input data (y = 0..15)
        IDR3: bool, // bit offset: 3 desc: Port input data (y = 0..15)
        IDR4: bool, // bit offset: 4 desc: Port input data (y = 0..15)
        IDR5: bool, // bit offset: 5 desc: Port input data (y = 0..15)
        IDR6: bool, // bit offset: 6 desc: Port input data (y = 0..15)
        IDR7: bool, // bit offset: 7 desc: Port input data (y = 0..15)
        IDR8: bool, // bit offset: 8 desc: Port input data (y = 0..15)
        IDR9: bool, // bit offset: 9 desc: Port input data (y = 0..15)
        IDR10: bool, // bit offset: 10 desc: Port input data (y = 0..15)
        IDR11: bool, // bit offset: 11 desc: Port input data (y = 0..15)
        IDR12: bool, // bit offset: 12 desc: Port input data (y = 0..15)
        IDR13: bool, // bit offset: 13 desc: Port input data (y = 0..15)
        IDR14: bool, // bit offset: 14 desc: Port input data (y = 0..15)
        IDR15: bool, // bit offset: 15 desc: Port input data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 20 GPIO port output data register
    pub const ODR = MMIO(Address + 0x00000014, u32, packed struct {
        ODR0: bool, // bit offset: 0 desc: Port output data (y = 0..15)
        ODR1: bool, // bit offset: 1 desc: Port output data (y = 0..15)
        ODR2: bool, // bit offset: 2 desc: Port output data (y = 0..15)
        ODR3: bool, // bit offset: 3 desc: Port output data (y = 0..15)
        ODR4: bool, // bit offset: 4 desc: Port output data (y = 0..15)
        ODR5: bool, // bit offset: 5 desc: Port output data (y = 0..15)
        ODR6: bool, // bit offset: 6 desc: Port output data (y = 0..15)
        ODR7: bool, // bit offset: 7 desc: Port output data (y = 0..15)
        ODR8: bool, // bit offset: 8 desc: Port output data (y = 0..15)
        ODR9: bool, // bit offset: 9 desc: Port output data (y = 0..15)
        ODR10: bool, // bit offset: 10 desc: Port output data (y = 0..15)
        ODR11: bool, // bit offset: 11 desc: Port output data (y = 0..15)
        ODR12: bool, // bit offset: 12 desc: Port output data (y = 0..15)
        ODR13: bool, // bit offset: 13 desc: Port output data (y = 0..15)
        ODR14: bool, // bit offset: 14 desc: Port output data (y = 0..15)
        ODR15: bool, // bit offset: 15 desc: Port output data (y = 0..15)
        padding: u16 = 0,
    });
    // byte offset: 24 GPIO port bit set/reset register
    pub const BSRR = MMIO(Address + 0x00000018, u32, packed struct {
        BS0: bool, // bit offset: 0 desc: Port x set bit y (y= 0..15)
        BS1: bool, // bit offset: 1 desc: Port x set bit y (y= 0..15)
        BS2: bool, // bit offset: 2 desc: Port x set bit y (y= 0..15)
        BS3: bool, // bit offset: 3 desc: Port x set bit y (y= 0..15)
        BS4: bool, // bit offset: 4 desc: Port x set bit y (y= 0..15)
        BS5: bool, // bit offset: 5 desc: Port x set bit y (y= 0..15)
        BS6: bool, // bit offset: 6 desc: Port x set bit y (y= 0..15)
        BS7: bool, // bit offset: 7 desc: Port x set bit y (y= 0..15)
        BS8: bool, // bit offset: 8 desc: Port x set bit y (y= 0..15)
        BS9: bool, // bit offset: 9 desc: Port x set bit y (y= 0..15)
        BS10: bool, // bit offset: 10 desc: Port x set bit y (y= 0..15)
        BS11: bool, // bit offset: 11 desc: Port x set bit y (y= 0..15)
        BS12: bool, // bit offset: 12 desc: Port x set bit y (y= 0..15)
        BS13: bool, // bit offset: 13 desc: Port x set bit y (y= 0..15)
        BS14: bool, // bit offset: 14 desc: Port x set bit y (y= 0..15)
        BS15: bool, // bit offset: 15 desc: Port x set bit y (y= 0..15)
        BR0: bool, // bit offset: 16 desc: Port x set bit y (y= 0..15)
        BR1: bool, // bit offset: 17 desc: Port x reset bit y (y = 0..15)
        BR2: bool, // bit offset: 18 desc: Port x reset bit y (y = 0..15)
        BR3: bool, // bit offset: 19 desc: Port x reset bit y (y = 0..15)
        BR4: bool, // bit offset: 20 desc: Port x reset bit y (y = 0..15)
        BR5: bool, // bit offset: 21 desc: Port x reset bit y (y = 0..15)
        BR6: bool, // bit offset: 22 desc: Port x reset bit y (y = 0..15)
        BR7: bool, // bit offset: 23 desc: Port x reset bit y (y = 0..15)
        BR8: bool, // bit offset: 24 desc: Port x reset bit y (y = 0..15)
        BR9: bool, // bit offset: 25 desc: Port x reset bit y (y = 0..15)
        BR10: bool, // bit offset: 26 desc: Port x reset bit y (y = 0..15)
        BR11: bool, // bit offset: 27 desc: Port x reset bit y (y = 0..15)
        BR12: bool, // bit offset: 28 desc: Port x reset bit y (y = 0..15)
        BR13: bool, // bit offset: 29 desc: Port x reset bit y (y = 0..15)
        BR14: bool, // bit offset: 30 desc: Port x reset bit y (y = 0..15)
        BR15: bool, // bit offset: 31 desc: Port x reset bit y (y = 0..15)
    });
    // byte offset: 28 GPIO port configuration lock register
    pub const LCKR = MMIO(Address + 0x0000001c, u32, packed struct {
        LCK0: bool, // bit offset: 0 desc: Port x lock bit y (y= 0..15)
        LCK1: bool, // bit offset: 1 desc: Port x lock bit y (y= 0..15)
        LCK2: bool, // bit offset: 2 desc: Port x lock bit y (y= 0..15)
        LCK3: bool, // bit offset: 3 desc: Port x lock bit y (y= 0..15)
        LCK4: bool, // bit offset: 4 desc: Port x lock bit y (y= 0..15)
        LCK5: bool, // bit offset: 5 desc: Port x lock bit y (y= 0..15)
        LCK6: bool, // bit offset: 6 desc: Port x lock bit y (y= 0..15)
        LCK7: bool, // bit offset: 7 desc: Port x lock bit y (y= 0..15)
        LCK8: bool, // bit offset: 8 desc: Port x lock bit y (y= 0..15)
        LCK9: bool, // bit offset: 9 desc: Port x lock bit y (y= 0..15)
        LCK10: bool, // bit offset: 10 desc: Port x lock bit y (y= 0..15)
        LCK11: bool, // bit offset: 11 desc: Port x lock bit y (y= 0..15)
        LCK12: bool, // bit offset: 12 desc: Port x lock bit y (y= 0..15)
        LCK13: bool, // bit offset: 13 desc: Port x lock bit y (y= 0..15)
        LCK14: bool, // bit offset: 14 desc: Port x lock bit y (y= 0..15)
        LCK15: bool, // bit offset: 15 desc: Port x lock bit y (y= 0..15)
        LCKK: bool, // bit offset: 16 desc: Lok Key
        padding: u15 = 0,
    });
    // byte offset: 32 GPIO alternate function low register
    pub const AFRL = MMIO(Address + 0x00000020, u32, packed struct {
        AFRL0: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL1: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL2: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL3: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL4: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL5: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL6: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 0..7)
        AFRL7: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 0..7)
    });
    // byte offset: 36 GPIO alternate function high register
    pub const AFRH = MMIO(Address + 0x00000024, u32, packed struct {
        AFRH8: u4, // bit offset: 0 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH9: u4, // bit offset: 4 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH10: u4, // bit offset: 8 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH11: u4, // bit offset: 12 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH12: u4, // bit offset: 16 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH13: u4, // bit offset: 20 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH14: u4, // bit offset: 24 desc: Alternate function selection for port x bit y (y = 8..15)
        AFRH15: u4, // bit offset: 28 desc: Alternate function selection for port x bit y (y = 8..15)
    });
    // byte offset: 40 Port bit reset register
    pub const BRR = MMIO(Address + 0x00000028, u32, packed struct {
        BR0: bool, // bit offset: 0 desc: Port x Reset bit y
        BR1: bool, // bit offset: 1 desc: Port x Reset bit y
        BR2: bool, // bit offset: 2 desc: Port x Reset bit y
        BR3: bool, // bit offset: 3 desc: Port x Reset bit y
        BR4: bool, // bit offset: 4 desc: Port x Reset bit y
        BR5: bool, // bit offset: 5 desc: Port x Reset bit y
        BR6: bool, // bit offset: 6 desc: Port x Reset bit y
        BR7: bool, // bit offset: 7 desc: Port x Reset bit y
        BR8: bool, // bit offset: 8 desc: Port x Reset bit y
        BR9: bool, // bit offset: 9 desc: Port x Reset bit y
        BR10: bool, // bit offset: 10 desc: Port x Reset bit y
        BR11: bool, // bit offset: 11 desc: Port x Reset bit y
        BR12: bool, // bit offset: 12 desc: Port x Reset bit y
        BR13: bool, // bit offset: 13 desc: Port x Reset bit y
        BR14: bool, // bit offset: 14 desc: Port x Reset bit y
        BR15: bool, // bit offset: 15 desc: Port x Reset bit y
        padding: u16 = 0,
    });
};
pub const TSC = extern struct {
    pub const Address: u32 = 0x40024000;
    // byte offset: 0 control register
    pub const CR = MMIO(Address + 0x00000000, u32, packed struct {
        TSCE: bool, // bit offset: 0 desc: Touch sensing controller enable
        START: bool, // bit offset: 1 desc: Start a new acquisition
        AM: bool, // bit offset: 2 desc: Acquisition mode
        SYNCPOL: bool, // bit offset: 3 desc: Synchronization pin polarity
        IODEF: bool, // bit offset: 4 desc: I/O Default mode
        MCV: u3, // bit offset: 5 desc: Max count value
        reserved0: u4 = 0,
        PGPSC: u3, // bit offset: 12 desc: pulse generator prescaler
        SSPSC: bool, // bit offset: 15 desc: Spread spectrum prescaler
        SSE: bool, // bit offset: 16 desc: Spread spectrum enable
        SSD: u7, // bit offset: 17 desc: Spread spectrum deviation
        CTPL: u4, // bit offset: 24 desc: Charge transfer pulse low
        CTPH: u4, // bit offset: 28 desc: Charge transfer pulse high
    });
    // byte offset: 4 interrupt enable register
    pub const IER = MMIO(Address + 0x00000004, u32, packed struct {
        EOAIE: bool, // bit offset: 0 desc: End of acquisition interrupt enable
        MCEIE: bool, // bit offset: 1 desc: Max count error interrupt enable
        padding: u30 = 0,
    });
    // byte offset: 8 interrupt clear register
    pub const ICR = MMIO(Address + 0x00000008, u32, packed struct {
        EOAIC: bool, // bit offset: 0 desc: End of acquisition interrupt clear
        MCEIC: bool, // bit offset: 1 desc: Max count error interrupt clear
        padding: u30 = 0,
    });
    // byte offset: 12 interrupt status register
    pub const ISR = MMIO(Address + 0x0000000c, u32, packed struct {
        EOAF: bool, // bit offset: 0 desc: End of acquisition flag
        MCEF: bool, // bit offset: 1 desc: Max count error flag
        padding: u30 = 0,
    });
    // byte offset: 16 I/O hysteresis control register
    pub const IOHCR = MMIO(Address + 0x00000010, u32, packed struct {
        G1_IO1: bool, // bit offset: 0 desc: G1_IO1 Schmitt trigger hysteresis mode
        G1_IO2: bool, // bit offset: 1 desc: G1_IO2 Schmitt trigger hysteresis mode
        G1_IO3: bool, // bit offset: 2 desc: G1_IO3 Schmitt trigger hysteresis mode
        G1_IO4: bool, // bit offset: 3 desc: G1_IO4 Schmitt trigger hysteresis mode
        G2_IO1: bool, // bit offset: 4 desc: G2_IO1 Schmitt trigger hysteresis mode
        G2_IO2: bool, // bit offset: 5 desc: G2_IO2 Schmitt trigger hysteresis mode
        G2_IO3: bool, // bit offset: 6 desc: G2_IO3 Schmitt trigger hysteresis mode
        G2_IO4: bool, // bit offset: 7 desc: G2_IO4 Schmitt trigger hysteresis mode
        G3_IO1: bool, // bit offset: 8 desc: G3_IO1 Schmitt trigger hysteresis mode
        G3_IO2: bool, // bit offset: 9 desc: G3_IO2 Schmitt trigger hysteresis mode
        G3_IO3: bool, // bit offset: 10 desc: G3_IO3 Schmitt trigger hysteresis mode
        G3_IO4: bool, // bit offset: 11 desc: G3_IO4 Schmitt trigger hysteresis mode
        G4_IO1: bool, // bit offset: 12 desc: G4_IO1 Schmitt trigger hysteresis mode
        G4_IO2: bool, // bit offset: 13 desc: G4_IO2 Schmitt trigger hysteresis mode
        G4_IO3: bool, // bit offset: 14 desc: G4_IO3 Schmitt trigger hysteresis mode
        G4_IO4: bool, // bit offset: 15 desc: G4_IO4 Schmitt trigger hysteresis mode
        G5_IO1: bool, // bit offset: 16 desc: G5_IO1 Schmitt trigger hysteresis mode
        G5_IO2: bool, // bit offset: 17 desc: G5_IO2 Schmitt trigger hysteresis mode
        G5_IO3: bool, // bit offset: 18 desc: G5_IO3 Schmitt trigger hysteresis mode
        G5_IO4: bool, // bit offset: 19 desc: G5_IO4 Schmitt trigger hysteresis mode
        G6_IO1: bool, // bit offset: 20 desc: G6_IO1 Schmitt trigger hysteresis mode
        G6_IO2: bool, // bit offset: 21 desc: G6_IO2 Schmitt trigger hysteresis mode
        G6_IO3: bool, // bit offset: 22 desc: G6_IO3 Schmitt trigger hysteresis mode
        G6_IO4: bool, // bit offset: 23 desc: G6_IO4 Schmitt trigger hysteresis mode
        G7_IO1: bool, // bit offset: 24 desc: G7_IO1 Schmitt trigger hysteresis mode
        G7_IO2: bool, // bit offset: 25 desc: G7_IO2 Schmitt trigger hysteresis mode
        G7_IO3: bool, // bit offset: 26 desc: G7_IO3 Schmitt trigger hysteresis mode
        G7_IO4: bool, // bit offset: 27 desc: G7_IO4 Schmitt trigger hysteresis mode
        G8_IO1: bool, // bit offset: 28 desc: G8_IO1 Schmitt trigger hysteresis mode
        G8_IO2: bool, // bit offset: 29 desc: G8_IO2 Schmitt trigger hysteresis mode
        G8_IO3: bool, // bit offset: 30 desc: G8_IO3 Schmitt trigger hysteresis mode
        G8_IO4: bool, // bit offset: 31 desc: G8_IO4 Schmitt trigger hysteresis mode
    });
    // byte offset: 24 I/O analog switch control register
    pub const IOASCR = MMIO(Address + 0x00000018, u32, packed struct {
        G1_IO1: bool, // bit offset: 0 desc: G1_IO1 analog switch enable
        G1_IO2: bool, // bit offset: 1 desc: G1_IO2 analog switch enable
        G1_IO3: bool, // bit offset: 2 desc: G1_IO3 analog switch enable
        G1_IO4: bool, // bit offset: 3 desc: G1_IO4 analog switch enable
        G2_IO1: bool, // bit offset: 4 desc: G2_IO1 analog switch enable
        G2_IO2: bool, // bit offset: 5 desc: G2_IO2 analog switch enable
        G2_IO3: bool, // bit offset: 6 desc: G2_IO3 analog switch enable
        G2_IO4: bool, // bit offset: 7 desc: G2_IO4 analog switch enable
        G3_IO1: bool, // bit offset: 8 desc: G3_IO1 analog switch enable
        G3_IO2: bool, // bit offset: 9 desc: G3_IO2 analog switch enable
        G3_IO3: bool, // bit offset: 10 desc: G3_IO3 analog switch enable
        G3_IO4: bool, // bit offset: 11 desc: G3_IO4 analog switch enable
        G4_IO1: bool, // bit offset: 12 desc: G4_IO1 analog switch enable
        G4_IO2: bool, // bit offset: 13 desc: G4_IO2 analog switch enable
        G4_IO3: bool, // bit offset: 14 desc: G4_IO3 analog switch enable
        G4_IO4: bool, // bit offset: 15 desc: G4_IO4 analog switch enable
        G5_IO1: bool, // bit offset: 16 desc: G5_IO1 analog switch enable
        G5_IO2: bool, // bit offset: 17 desc: G5_IO2 analog switch enable
        G5_IO3: bool, // bit offset: 18 desc: G5_IO3 analog switch enable
        G5_IO4: bool, // bit offset: 19 desc: G5_IO4 analog switch enable
        G6_IO1: bool, // bit offset: 20 desc: G6_IO1 analog switch enable
        G6_IO2: bool, // bit offset: 21 desc: G6_IO2 analog switch enable
        G6_IO3: bool, // bit offset: 22 desc: G6_IO3 analog switch enable
        G6_IO4: bool, // bit offset: 23 desc: G6_IO4 analog switch enable
        G7_IO1: bool, // bit offset: 24 desc: G7_IO1 analog switch enable
        G7_IO2: bool, // bit offset: 25 desc: G7_IO2 analog switch enable
        G7_IO3: bool, // bit offset: 26 desc: G7_IO3 analog switch enable
        G7_IO4: bool, // bit offset: 27 desc: G7_IO4 analog switch enable
        G8_IO1: bool, // bit offset: 28 desc: G8_IO1 analog switch enable
        G8_IO2: bool, // bit offset: 29 desc: G8_IO2 analog switch enable
        G8_IO3: bool, // bit offset: 30 desc: G8_IO3 analog switch enable
        G8_IO4: bool, // bit offset: 31 desc: G8_IO4 analog switch enable
    });
    // byte offset: 32 I/O sampling control register
    pub const IOSCR = MMIO(Address + 0x00000020, u32, packed struct {
        G1_IO1: bool, // bit offset: 0 desc: G1_IO1 sampling mode
        G1_IO2: bool, // bit offset: 1 desc: G1_IO2 sampling mode
        G1_IO3: bool, // bit offset: 2 desc: G1_IO3 sampling mode
        G1_IO4: bool, // bit offset: 3 desc: G1_IO4 sampling mode
        G2_IO1: bool, // bit offset: 4 desc: G2_IO1 sampling mode
        G2_IO2: bool, // bit offset: 5 desc: G2_IO2 sampling mode
        G2_IO3: bool, // bit offset: 6 desc: G2_IO3 sampling mode
        G2_IO4: bool, // bit offset: 7 desc: G2_IO4 sampling mode
        G3_IO1: bool, // bit offset: 8 desc: G3_IO1 sampling mode
        G3_IO2: bool, // bit offset: 9 desc: G3_IO2 sampling mode
        G3_IO3: bool, // bit offset: 10 desc: G3_IO3 sampling mode
        G3_IO4: bool, // bit offset: 11 desc: G3_IO4 sampling mode
        G4_IO1: bool, // bit offset: 12 desc: G4_IO1 sampling mode
        G4_IO2: bool, // bit offset: 13 desc: G4_IO2 sampling mode
        G4_IO3: bool, // bit offset: 14 desc: G4_IO3 sampling mode
        G4_IO4: bool, // bit offset: 15 desc: G4_IO4 sampling mode
        G5_IO1: bool, // bit offset: 16 desc: G5_IO1 sampling mode
        G5_IO2: bool, // bit offset: 17 desc: G5_IO2 sampling mode
        G5_IO3: bool, // bit offset: 18 desc: G5_IO3 sampling mode
        G5_IO4: bool, // bit offset: 19 desc: G5_IO4 sampling mode
        G6_IO1: bool, // bit offset: 20 desc: G6_IO1 sampling mode
        G6_IO2: bool, // bit offset: 21 desc: G6_IO2 sampling mode
        G6_IO3: bool, // bit offset: 22 desc: G6_IO3 sampling mode
        G6_IO4: bool, // bit offset: 23 desc: G6_IO4 sampling mode
        G7_IO1: bool, // bit offset: 24 desc: G7_IO1 sampling mode
        G7_IO2: bool, // bit offset: 25 desc: G7_IO2 sampling mode
        G7_IO3: bool, // bit offset: 26 desc: G7_IO3 sampling mode
        G7_IO4: bool, // bit offset: 27 desc: G7_IO4 sampling mode
        G8_IO1: bool, // bit offset: 28 desc: G8_IO1 sampling mode
        G8_IO2: bool, // bit offset: 29 desc: G8_IO2 sampling mode
        G8_IO3: bool, // bit offset: 30 desc: G8_IO3 sampling mode
        G8_IO4: bool, // bit offset: 31 desc: G8_IO4 sampling mode
    });
    // byte offset: 40 I/O channel control register
    pub const IOCCR = MMIO(Address + 0x00000028, u32, packed struct {
        G1_IO1: bool, // bit offset: 0 desc: G1_IO1 channel mode
        G1_IO2: bool, // bit offset: 1 desc: G1_IO2 channel mode
        G1_IO3: bool, // bit offset: 2 desc: G1_IO3 channel mode
        G1_IO4: bool, // bit offset: 3 desc: G1_IO4 channel mode
        G2_IO1: bool, // bit offset: 4 desc: G2_IO1 channel mode
        G2_IO2: bool, // bit offset: 5 desc: G2_IO2 channel mode
        G2_IO3: bool, // bit offset: 6 desc: G2_IO3 channel mode
        G2_IO4: bool, // bit offset: 7 desc: G2_IO4 channel mode
        G3_IO1: bool, // bit offset: 8 desc: G3_IO1 channel mode
        G3_IO2: bool, // bit offset: 9 desc: G3_IO2 channel mode
        G3_IO3: bool, // bit offset: 10 desc: G3_IO3 channel mode
        G3_IO4: bool, // bit offset: 11 desc: G3_IO4 channel mode
        G4_IO1: bool, // bit offset: 12 desc: G4_IO1 channel mode
        G4_IO2: bool, // bit offset: 13 desc: G4_IO2 channel mode
        G4_IO3: bool, // bit offset: 14 desc: G4_IO3 channel mode
        G4_IO4: bool, // bit offset: 15 desc: G4_IO4 channel mode
        G5_IO1: bool, // bit offset: 16 desc: G5_IO1 channel mode
        G5_IO2: bool, // bit offset: 17 desc: G5_IO2 channel mode
        G5_IO3: bool, // bit offset: 18 desc: G5_IO3 channel mode
        G5_IO4: bool, // bit offset: 19 desc: G5_IO4 channel mode
        G6_IO1: bool, // bit offset: 20 desc: G6_IO1 channel mode
        G6_IO2: bool, // bit offset: 21 desc: G6_IO2 channel mode
        G6_IO3: bool, // bit offset: 22 desc: G6_IO3 channel mode
        G6_IO4: bool, // bit offset: 23 desc: G6_IO4 channel mode
        G7_IO1: bool, // bit offset: 24 desc: G7_IO1 channel mode
        G7_IO2: bool, // bit offset: 25 desc: G7_IO2 channel mode
        G7_IO3: bool, // bit offset: 26 desc: G7_IO3 channel mode
        G7_IO4: bool, // bit offset: 27 desc: G7_IO4 channel mode
        G8_IO1: bool, // bit offset: 28 desc: G8_IO1 channel mode
        G8_IO2: bool, // bit offset: 29 desc: G8_IO2 channel mode
        G8_IO3: bool, // bit offset: 30 desc: G8_IO3 channel mode
        G8_IO4: bool, // bit offset: 31 desc: G8_IO4 channel mode
    });
    // byte offset: 48 I/O group control status register
    pub const IOGCSR = MMIO(Address + 0x00000030, u32, packed struct {
        G1E: bool, // bit offset: 0 desc: Analog I/O group x enable
        G2E: bool, // bit offset: 1 desc: Analog I/O group x enable
        G3E: bool, // bit offset: 2 desc: Analog I/O group x enable
        G4E: bool, // bit offset: 3 desc: Analog I/O group x enable
        G5E: bool, // bit offset: 4 desc: Analog I/O group x enable
        G6E: bool, // bit offset: 5 desc: Analog I/O group x enable
        G7E: bool, // bit offset: 6 desc: Analog I/O group x enable
        G8E: bool, // bit offset: 7 desc: Analog I/O group x enable
        reserved0: u8 = 0,
        G1S: bool, // bit offset: 16 desc: Analog I/O group x status
        G2S: bool, // bit offset: 17 desc: Analog I/O group x status
        G3S: bool, // bit offset: 18 desc: Analog I/O group x status
        G4S: bool, // bit offset: 19 desc: Analog I/O group x status
        G5S: bool, // bit offset: 20 desc: Analog I/O group x status
        G6S: bool, // bit offset: 21 desc: Analog I/O group x status
        G7S: bool, // bit offset: 22 desc: Analog I/O group x status
        G8S: bool, // bit offset: 23 desc: Analog I/O group x status
        padding: u8 = 0,
    });
    // byte offset: 52 I/O group x counter register
    pub const IOG1CR = MMIO(Address + 0x00000034, u32, packed struct {
        CNT: u14, // bit offset: 0 desc: Counter value
        padding: u18 = 0,
    });
    // byte offset: 56 I/O group x counter register
    pub const IOG2CR = MMIO(Address + 0x00000038, u32, packed struct {
        CNT: u14, // bit offset: 0 desc: Counter value
        padding: u18 = 0,
    });
    // byte offset: 60 I/O group x counter register
    pub const IOG3CR = MMIO(Address + 0x0000003c, u32, packed struct {
        CNT: u14, // bit offset: 0 desc: Counter value
        padding: u18 = 0,
    });
    // byte offset: 64 I/O group x counter register
    pub const IOG4CR = MMIO(Address + 0x00000040, u32, packed struct {
        CNT: u14, // bit offset: 0 desc: Counter value
        padding: u18 = 0,
    });
    // byte offset: 68 I/O group x counter register
    pub const IOG5CR = MMIO(Address + 0x00000044, u32, packed struct {
        CNT: u14, // bit offset: 0 desc: Counter value
        padding: u18 = 0,
    });
    // byte offset: 72 I/O group x counter register
    pub const IOG6CR = MMIO(Address + 0x00000048, u32, packed struct {
        CNT: u14, // bit offset: 0 desc: Counter value
        padding: u18 = 0,
    });
    // byte offset: 76 I/O group x counter register
    pub const IOG7CR = MMIO(Address + 0x0000004c, u32, packed struct {
        CNT: u14, // bit offset: 0 desc: Counter value
        padding: u18 = 0,
    });
    // byte offset: 80 I/O group x counter register
    pub const IOG8CR = MMIO(Address + 0x00000050, u32, packed struct {
        CNT: u14, // bit offset: 0 desc: Counter value
        padding: u18 = 0,
    });
};
pub const CRC = extern struct {
    pub const Address: u32 = 0x40023000;
    // byte offset: 0 Data register
    pub const DR = MMIO(Address + 0x00000000, u32, packed struct {
        DR: u32, // bit offset: 0 desc: Data register bits
    });
    // byte offset: 4 Independent data register
    pub const IDR = MMIO(Address + 0x00000004, u32, packed struct {
        IDR: u8, // bit offset: 0 desc: General-purpose 8-bit data register bits
        padding: u24 = 0,
    });
    // byte offset: 8 Control register
    pub const CR = MMIO(Address + 0x00000008, u32, packed struct {
        RESET: bool, // bit offset: 0 desc: reset bit
        reserved0: u2 = 0,
        POLYSIZE: u2, // bit offset: 3 desc: Polynomial size
        REV_IN: u2, // bit offset: 5 desc: Reverse input data
        REV_OUT: bool, // bit offset: 7 desc: Reverse output data
        padding: u24 = 0,
    });
    // byte offset: 16 Initial CRC value
    pub const INIT = MMIO(Address + 0x00000010, u32, packed struct {
        INIT: u32, // bit offset: 0 desc: Programmable initial CRC value
    });
    // byte offset: 20 CRC polynomial
    pub const POL = MMIO(Address + 0x00000014, u32, packed struct {
        POL: u32, // bit offset: 0 desc: Programmable polynomial
    });
};
pub const Flash = extern struct {
    pub const Address: u32 = 0x40022000;
    // byte offset: 0 Flash access control register
    pub const ACR = MMIO(Address + 0x00000000, u32, packed struct {
        LATENCY: u3, // bit offset: 0 desc: LATENCY
        reserved0: u1 = 0,
        PRFTBE: bool, // bit offset: 4 desc: PRFTBE
        PRFTBS: bool, // bit offset: 5 desc: PRFTBS
        padding: u26 = 0,
    });
    // byte offset: 4 Flash key register
    pub const KEYR = MMIO(Address + 0x00000004, u32, packed struct {
        FKEYR: u32, // bit offset: 0 desc: Flash Key
    });
    // byte offset: 8 Flash option key register
    pub const OPTKEYR = MMIO(Address + 0x00000008, u32, packed struct {
        OPTKEYR: u32, // bit offset: 0 desc: Option byte key
    });
    // byte offset: 12 Flash status register
    pub const SR = MMIO(Address + 0x0000000c, u32, packed struct {
        BSY: bool, // bit offset: 0 desc: Busy
        reserved0: u1 = 0,
        PGERR: bool, // bit offset: 2 desc: Programming error
        reserved1: u1 = 0,
        WRPRT: bool, // bit offset: 4 desc: Write protection error
        EOP: bool, // bit offset: 5 desc: End of operation
        padding: u26 = 0,
    });
    // byte offset: 16 Flash control register
    pub const CR = MMIO(Address + 0x00000010, u32, packed struct {
        PG: bool, // bit offset: 0 desc: Programming
        PER: bool, // bit offset: 1 desc: Page erase
        MER: bool, // bit offset: 2 desc: Mass erase
        reserved0: u1 = 0,
        OPTPG: bool, // bit offset: 4 desc: Option byte programming
        OPTER: bool, // bit offset: 5 desc: Option byte erase
        STRT: bool, // bit offset: 6 desc: Start
        LOCK: bool, // bit offset: 7 desc: Lock
        reserved1: u1 = 0,
        OPTWRE: bool, // bit offset: 9 desc: Option bytes write enable
        ERRIE: bool, // bit offset: 10 desc: Error interrupt enable
        reserved2: u1 = 0,
        EOPIE: bool, // bit offset: 12 desc: End of operation interrupt enable
        FORCE_OPTLOAD: bool, // bit offset: 13 desc: Force option byte loading
        padding: u18 = 0,
    });
    // byte offset: 20 Flash address register
    pub const AR = MMIO(Address + 0x00000014, u32, packed struct {
        FAR: u32, // bit offset: 0 desc: Flash address
    });
    // byte offset: 28 Option byte register
    pub const OBR = MMIO(Address + 0x0000001c, u32, packed struct {
        OPTERR: bool, // bit offset: 0 desc: Option byte error
        LEVEL1_PROT: bool, // bit offset: 1 desc: Level 1 protection status
        LEVEL2_PROT: bool, // bit offset: 2 desc: Level 2 protection status
        reserved0: u5 = 0,
        WDG_SW: bool, // bit offset: 8 desc: WDG_SW
        nRST_STOP: bool, // bit offset: 9 desc: nRST_STOP
        nRST_STDBY: bool, // bit offset: 10 desc: nRST_STDBY
        reserved1: u1 = 0,
        BOOT1: bool, // bit offset: 12 desc: BOOT1
        VDDA_MONITOR: bool, // bit offset: 13 desc: VDDA_MONITOR
        SRAM_PARITY_CHECK: bool, // bit offset: 14 desc: SRAM_PARITY_CHECK
        reserved2: u1 = 0,
        Data0: u8, // bit offset: 16 desc: Data0
        Data1: u8, // bit offset: 24 desc: Data1
    });
    // byte offset: 32 Write protection register
    pub const WRPR = MMIO(Address + 0x00000020, u32, packed struct {
        WRP: u32, // bit offset: 0 desc: Write protect
    });
};
pub const RCC = extern struct {
    pub const Address: u32 = 0x40021000;
    // byte offset: 0 Clock control register
    pub const CR = MMIO(Address + 0x00000000, u32, packed struct {
        HSION: bool, // bit offset: 0 desc: Internal High Speed clock enable
        HSIRDY: bool, // bit offset: 1 desc: Internal High Speed clock ready flag
        reserved0: u1 = 0,
        HSITRIM: u5, // bit offset: 3 desc: Internal High Speed clock trimming
        HSICAL: u8, // bit offset: 8 desc: Internal High Speed clock Calibration
        HSEON: bool, // bit offset: 16 desc: External High Speed clock enable
        HSERDY: bool, // bit offset: 17 desc: External High Speed clock ready flag
        HSEBYP: bool, // bit offset: 18 desc: External High Speed clock Bypass
        CSSON: bool, // bit offset: 19 desc: Clock Security System enable
        reserved1: u4 = 0,
        PLLON: bool, // bit offset: 24 desc: PLL enable
        PLLRDY: bool, // bit offset: 25 desc: PLL clock ready flag
        padding: u6 = 0,
    });
    // byte offset: 4 Clock configuration register (RCC_CFGR)
    pub const CFGR = MMIO(Address + 0x00000004, u32, packed struct {
        SW: u2, // bit offset: 0 desc: System clock Switch
        SWS: u2, // bit offset: 2 desc: System Clock Switch Status
        HPRE: u4, // bit offset: 4 desc: AHB prescaler
        PPRE1: u3, // bit offset: 8 desc: APB Low speed prescaler (APB1)
        PPRE2: u3, // bit offset: 11 desc: APB high speed prescaler (APB2)
        reserved0: u1 = 0,
        PLLSRC: u2, // bit offset: 15 desc: PLL entry clock source
        PLLXTPRE: bool, // bit offset: 17 desc: HSE divider for PLL entry
        PLLMUL: u4, // bit offset: 18 desc: PLL Multiplication Factor
        USBPRES: bool, // bit offset: 22 desc: USB prescaler
        I2SSRC: bool, // bit offset: 23 desc: I2S external clock source selection
        MCO: u3, // bit offset: 24 desc: Microcontroller clock output
        reserved1: u1 = 0,
        MCOF: bool, // bit offset: 28 desc: Microcontroller Clock Output Flag
        padding: u3 = 0,
    });
    // byte offset: 8 Clock interrupt register (RCC_CIR)
    pub const CIR = MMIO(Address + 0x00000008, u32, packed struct {
        LSIRDYF: bool, // bit offset: 0 desc: LSI Ready Interrupt flag
        LSERDYF: bool, // bit offset: 1 desc: LSE Ready Interrupt flag
        HSIRDYF: bool, // bit offset: 2 desc: HSI Ready Interrupt flag
        HSERDYF: bool, // bit offset: 3 desc: HSE Ready Interrupt flag
        PLLRDYF: bool, // bit offset: 4 desc: PLL Ready Interrupt flag
        reserved0: u2 = 0,
        CSSF: bool, // bit offset: 7 desc: Clock Security System Interrupt flag
        LSIRDYIE: bool, // bit offset: 8 desc: LSI Ready Interrupt Enable
        LSERDYIE: bool, // bit offset: 9 desc: LSE Ready Interrupt Enable
        HSIRDYIE: bool, // bit offset: 10 desc: HSI Ready Interrupt Enable
        HSERDYIE: bool, // bit offset: 11 desc: HSE Ready Interrupt Enable
        PLLRDYIE: bool, // bit offset: 12 desc: PLL Ready Interrupt Enable
        reserved1: u3 = 0,
        LSIRDYC: bool, // bit offset: 16 desc: LSI Ready Interrupt Clear
        LSERDYC: bool, // bit offset: 17 desc: LSE Ready Interrupt Clear
        HSIRDYC: bool, // bit offset: 18 desc: HSI Ready Interrupt Clear
        HSERDYC: bool, // bit offset: 19 desc: HSE Ready Interrupt Clear
        PLLRDYC: bool, // bit offset: 20 desc: PLL Ready Interrupt Clear
        reserved2: u2 = 0,
        CSSC: bool, // bit offset: 23 desc: Clock security system interrupt clear
        padding: u8 = 0,
    });
    // byte offset: 12 APB2 peripheral reset register (RCC_APB2RSTR)
    pub const APB2RSTR = MMIO(Address + 0x0000000c, u32, packed struct {
        SYSCFGRST: bool, // bit offset: 0 desc: SYSCFG and COMP reset
        reserved0: u10 = 0,
        TIM1RST: bool, // bit offset: 11 desc: TIM1 timer reset
        SPI1RST: bool, // bit offset: 12 desc: SPI 1 reset
        TIM8RST: bool, // bit offset: 13 desc: TIM8 timer reset
        USART1RST: bool, // bit offset: 14 desc: USART1 reset
        reserved1: u1 = 0,
        TIM15RST: bool, // bit offset: 16 desc: TIM15 timer reset
        TIM16RST: bool, // bit offset: 17 desc: TIM16 timer reset
        TIM17RST: bool, // bit offset: 18 desc: TIM17 timer reset
        padding: u13 = 0,
    });
    // byte offset: 16 APB1 peripheral reset register (RCC_APB1RSTR)
    pub const APB1RSTR = MMIO(Address + 0x00000010, u32, packed struct {
        TIM2RST: bool, // bit offset: 0 desc: Timer 2 reset
        TIM3RST: bool, // bit offset: 1 desc: Timer 3 reset
        TIM4RST: bool, // bit offset: 2 desc: Timer 14 reset
        reserved0: u1 = 0,
        TIM6RST: bool, // bit offset: 4 desc: Timer 6 reset
        TIM7RST: bool, // bit offset: 5 desc: Timer 7 reset
        reserved1: u5 = 0,
        WWDGRST: bool, // bit offset: 11 desc: Window watchdog reset
        reserved2: u2 = 0,
        SPI2RST: bool, // bit offset: 14 desc: SPI2 reset
        SPI3RST: bool, // bit offset: 15 desc: SPI3 reset
        reserved3: u1 = 0,
        USART2RST: bool, // bit offset: 17 desc: USART 2 reset
        USART3RST: bool, // bit offset: 18 desc: USART3 reset
        UART4RST: bool, // bit offset: 19 desc: UART 4 reset
        UART5RST: bool, // bit offset: 20 desc: UART 5 reset
        I2C1RST: bool, // bit offset: 21 desc: I2C1 reset
        I2C2RST: bool, // bit offset: 22 desc: I2C2 reset
        USBRST: bool, // bit offset: 23 desc: USB reset
        reserved4: u1 = 0,
        CANRST: bool, // bit offset: 25 desc: CAN reset
        reserved5: u2 = 0,
        PWRRST: bool, // bit offset: 28 desc: Power interface reset
        DACRST: bool, // bit offset: 29 desc: DAC interface reset
        I2C3RST: bool, // bit offset: 30 desc: I2C3 reset
        padding: u1 = 0,
    });
    // byte offset: 20 AHB Peripheral Clock enable register (RCC_AHBENR)
    pub const AHBENR = MMIO(Address + 0x00000014, u32, packed struct {
        DMAEN: bool, // bit offset: 0 desc: DMA1 clock enable
        DMA2EN: bool, // bit offset: 1 desc: DMA2 clock enable
        SRAMEN: bool, // bit offset: 2 desc: SRAM interface clock enable
        reserved0: u1 = 0,
        FLITFEN: bool, // bit offset: 4 desc: FLITF clock enable
        FMCEN: bool, // bit offset: 5 desc: FMC clock enable
        CRCEN: bool, // bit offset: 6 desc: CRC clock enable
        reserved1: u9 = 0,
        IOPHEN: bool, // bit offset: 16 desc: IO port H clock enable
        IOPAEN: bool, // bit offset: 17 desc: I/O port A clock enable
        IOPBEN: bool, // bit offset: 18 desc: I/O port B clock enable
        IOPCEN: bool, // bit offset: 19 desc: I/O port C clock enable
        IOPDEN: bool, // bit offset: 20 desc: I/O port D clock enable
        IOPEEN: bool, // bit offset: 21 desc: I/O port E clock enable
        IOPFEN: bool, // bit offset: 22 desc: I/O port F clock enable
        IOPGEN: bool, // bit offset: 23 desc: I/O port G clock enable
        TSCEN: bool, // bit offset: 24 desc: Touch sensing controller clock enable
        reserved2: u3 = 0,
        ADC12EN: bool, // bit offset: 28 desc: ADC1 and ADC2 clock enable
        ADC34EN: bool, // bit offset: 29 desc: ADC3 and ADC4 clock enable
        padding: u2 = 0,
    });
    // byte offset: 24 APB2 peripheral clock enable register (RCC_APB2ENR)
    pub const APB2ENR = MMIO(Address + 0x00000018, u32, packed struct {
        SYSCFGEN: bool, // bit offset: 0 desc: SYSCFG clock enable
        reserved0: u10 = 0,
        TIM1EN: bool, // bit offset: 11 desc: TIM1 Timer clock enable
        SPI1EN: bool, // bit offset: 12 desc: SPI 1 clock enable
        TIM8EN: bool, // bit offset: 13 desc: TIM8 Timer clock enable
        USART1EN: bool, // bit offset: 14 desc: USART1 clock enable
        reserved1: u1 = 0,
        TIM15EN: bool, // bit offset: 16 desc: TIM15 timer clock enable
        TIM16EN: bool, // bit offset: 17 desc: TIM16 timer clock enable
        TIM17EN: bool, // bit offset: 18 desc: TIM17 timer clock enable
        padding: u13 = 0,
    });
    // byte offset: 28 APB1 peripheral clock enable register (RCC_APB1ENR)
    pub const APB1ENR = MMIO(Address + 0x0000001c, u32, packed struct {
        TIM2EN: bool, // bit offset: 0 desc: Timer 2 clock enable
        TIM3EN: bool, // bit offset: 1 desc: Timer 3 clock enable
        TIM4EN: bool, // bit offset: 2 desc: Timer 4 clock enable
        reserved0: u1 = 0,
        TIM6EN: bool, // bit offset: 4 desc: Timer 6 clock enable
        TIM7EN: bool, // bit offset: 5 desc: Timer 7 clock enable
        reserved1: u5 = 0,
        WWDGEN: bool, // bit offset: 11 desc: Window watchdog clock enable
        reserved2: u2 = 0,
        SPI2EN: bool, // bit offset: 14 desc: SPI 2 clock enable
        SPI3EN: bool, // bit offset: 15 desc: SPI 3 clock enable
        reserved3: u1 = 0,
        USART2EN: bool, // bit offset: 17 desc: USART 2 clock enable
        USART3EN: bool, // bit offset: 18 desc: USART 3 clock enable
        USART4EN: bool, // bit offset: 19 desc: USART 4 clock enable
        USART5EN: bool, // bit offset: 20 desc: USART 5 clock enable
        I2C1EN: bool, // bit offset: 21 desc: I2C 1 clock enable
        I2C2EN: bool, // bit offset: 22 desc: I2C 2 clock enable
        USBEN: bool, // bit offset: 23 desc: USB clock enable
        reserved4: u1 = 0,
        CANEN: bool, // bit offset: 25 desc: CAN clock enable
        DAC2EN: bool, // bit offset: 26 desc: DAC2 interface clock enable
        reserved5: u1 = 0,
        PWREN: bool, // bit offset: 28 desc: Power interface clock enable
        DACEN: bool, // bit offset: 29 desc: DAC interface clock enable
        I2C3EN: bool, // bit offset: 30 desc: I2C3 clock enable
        padding: u1 = 0,
    });
    // byte offset: 32 Backup domain control register (RCC_BDCR)
    pub const BDCR = MMIO(Address + 0x00000020, u32, packed struct {
        LSEON: bool, // bit offset: 0 desc: External Low Speed oscillator enable
        LSERDY: bool, // bit offset: 1 desc: External Low Speed oscillator ready
        LSEBYP: bool, // bit offset: 2 desc: External Low Speed oscillator bypass
        LSEDRV: u2, // bit offset: 3 desc: LSE oscillator drive capability
        reserved0: u3 = 0,
        RTCSEL: u2, // bit offset: 8 desc: RTC clock source selection
        reserved1: u5 = 0,
        RTCEN: bool, // bit offset: 15 desc: RTC clock enable
        BDRST: bool, // bit offset: 16 desc: Backup domain software reset
        padding: u15 = 0,
    });
    // byte offset: 36 Control/status register (RCC_CSR)
    pub const CSR = MMIO(Address + 0x00000024, u32, packed struct {
        LSION: bool, // bit offset: 0 desc: Internal low speed oscillator enable
        LSIRDY: bool, // bit offset: 1 desc: Internal low speed oscillator ready
        reserved0: u22 = 0,
        RMVF: bool, // bit offset: 24 desc: Remove reset flag
        OBLRSTF: bool, // bit offset: 25 desc: Option byte loader reset flag
        PINRSTF: bool, // bit offset: 26 desc: PIN reset flag
        PORRSTF: bool, // bit offset: 27 desc: POR/PDR reset flag
        SFTRSTF: bool, // bit offset: 28 desc: Software reset flag
        IWDGRSTF: bool, // bit offset: 29 desc: Independent watchdog reset flag
        WWDGRSTF: bool, // bit offset: 30 desc: Window watchdog reset flag
        LPWRRSTF: bool, // bit offset: 31 desc: Low-power reset flag
    });
    // byte offset: 40 AHB peripheral reset register
    pub const AHBRSTR = MMIO(Address + 0x00000028, u32, packed struct {
        reserved0: u5 = 0,
        FMCRST: bool, // bit offset: 5 desc: FMC reset
        reserved1: u10 = 0,
        IOPHRST: bool, // bit offset: 16 desc: I/O port H reset
        IOPARST: bool, // bit offset: 17 desc: I/O port A reset
        IOPBRST: bool, // bit offset: 18 desc: I/O port B reset
        IOPCRST: bool, // bit offset: 19 desc: I/O port C reset
        IOPDRST: bool, // bit offset: 20 desc: I/O port D reset
        IOPERST: bool, // bit offset: 21 desc: I/O port E reset
        IOPFRST: bool, // bit offset: 22 desc: I/O port F reset
        IOPGRST: bool, // bit offset: 23 desc: Touch sensing controller reset
        TSCRST: bool, // bit offset: 24 desc: Touch sensing controller reset
        reserved2: u3 = 0,
        ADC12RST: bool, // bit offset: 28 desc: ADC1 and ADC2 reset
        ADC34RST: bool, // bit offset: 29 desc: ADC3 and ADC4 reset
        padding: u2 = 0,
    });
    // byte offset: 44 Clock configuration register 2
    pub const CFGR2 = MMIO(Address + 0x0000002c, u32, packed struct {
        PREDIV: u4, // bit offset: 0 desc: PREDIV division factor
        ADC12PRES: u5, // bit offset: 4 desc: ADC1 and ADC2 prescaler
        ADC34PRES: u5, // bit offset: 9 desc: ADC3 and ADC4 prescaler
        padding: u18 = 0,
    });
    // byte offset: 48 Clock configuration register 3
    pub const CFGR3 = MMIO(Address + 0x00000030, u32, packed struct {
        USART1SW: u2, // bit offset: 0 desc: USART1 clock source selection
        reserved0: u2 = 0,
        I2C1SW: bool, // bit offset: 4 desc: I2C1 clock source selection
        I2C2SW: bool, // bit offset: 5 desc: I2C2 clock source selection
        I2C3SW: bool, // bit offset: 6 desc: I2C3 clock source selection
        reserved1: u1 = 0,
        TIM1SW: bool, // bit offset: 8 desc: Timer1 clock source selection
        TIM8SW: bool, // bit offset: 9 desc: Timer8 clock source selection
        reserved2: u6 = 0,
        USART2SW: u2, // bit offset: 16 desc: USART2 clock source selection
        USART3SW: u2, // bit offset: 18 desc: USART3 clock source selection
        UART4SW: u2, // bit offset: 20 desc: UART4 clock source selection
        UART5SW: u2, // bit offset: 22 desc: UART5 clock source selection
        padding: u8 = 0,
    });
};
pub const DMA1 = extern struct {
    pub const Address: u32 = 0x40020000;
    // byte offset: 0 DMA interrupt status register (DMA_ISR)
    pub const ISR = MMIO(Address + 0x00000000, u32, packed struct {
        GIF1: bool, // bit offset: 0 desc: Channel 1 Global interrupt flag
        TCIF1: bool, // bit offset: 1 desc: Channel 1 Transfer Complete flag
        HTIF1: bool, // bit offset: 2 desc: Channel 1 Half Transfer Complete flag
        TEIF1: bool, // bit offset: 3 desc: Channel 1 Transfer Error flag
        GIF2: bool, // bit offset: 4 desc: Channel 2 Global interrupt flag
        TCIF2: bool, // bit offset: 5 desc: Channel 2 Transfer Complete flag
        HTIF2: bool, // bit offset: 6 desc: Channel 2 Half Transfer Complete flag
        TEIF2: bool, // bit offset: 7 desc: Channel 2 Transfer Error flag
        GIF3: bool, // bit offset: 8 desc: Channel 3 Global interrupt flag
        TCIF3: bool, // bit offset: 9 desc: Channel 3 Transfer Complete flag
        HTIF3: bool, // bit offset: 10 desc: Channel 3 Half Transfer Complete flag
        TEIF3: bool, // bit offset: 11 desc: Channel 3 Transfer Error flag
        GIF4: bool, // bit offset: 12 desc: Channel 4 Global interrupt flag
        TCIF4: bool, // bit offset: 13 desc: Channel 4 Transfer Complete flag
        HTIF4: bool, // bit offset: 14 desc: Channel 4 Half Transfer Complete flag
        TEIF4: bool, // bit offset: 15 desc: Channel 4 Transfer Error flag
        GIF5: bool, // bit offset: 16 desc: Channel 5 Global interrupt flag
        TCIF5: bool, // bit offset: 17 desc: Channel 5 Transfer Complete flag
        HTIF5: bool, // bit offset: 18 desc: Channel 5 Half Transfer Complete flag
        TEIF5: bool, // bit offset: 19 desc: Channel 5 Transfer Error flag
        GIF6: bool, // bit offset: 20 desc: Channel 6 Global interrupt flag
        TCIF6: bool, // bit offset: 21 desc: Channel 6 Transfer Complete flag
        HTIF6: bool, // bit offset: 22 desc: Channel 6 Half Transfer Complete flag
        TEIF6: bool, // bit offset: 23 desc: Channel 6 Transfer Error flag
        GIF7: bool, // bit offset: 24 desc: Channel 7 Global interrupt flag
        TCIF7: bool, // bit offset: 25 desc: Channel 7 Transfer Complete flag
        HTIF7: bool, // bit offset: 26 desc: Channel 7 Half Transfer Complete flag
        TEIF7: bool, // bit offset: 27 desc: Channel 7 Transfer Error flag
        padding: u4 = 0,
    });
    // byte offset: 4 DMA interrupt flag clear register (DMA_IFCR)
    pub const IFCR = MMIO(Address + 0x00000004, u32, packed struct {
        CGIF1: bool, // bit offset: 0 desc: Channel 1 Global interrupt clear
        CTCIF1: bool, // bit offset: 1 desc: Channel 1 Transfer Complete clear
        CHTIF1: bool, // bit offset: 2 desc: Channel 1 Half Transfer clear
        CTEIF1: bool, // bit offset: 3 desc: Channel 1 Transfer Error clear
        CGIF2: bool, // bit offset: 4 desc: Channel 2 Global interrupt clear
        CTCIF2: bool, // bit offset: 5 desc: Channel 2 Transfer Complete clear
        CHTIF2: bool, // bit offset: 6 desc: Channel 2 Half Transfer clear
        CTEIF2: bool, // bit offset: 7 desc: Channel 2 Transfer Error clear
        CGIF3: bool, // bit offset: 8 desc: Channel 3 Global interrupt clear
        CTCIF3: bool, // bit offset: 9 desc: Channel 3 Transfer Complete clear
        CHTIF3: bool, // bit offset: 10 desc: Channel 3 Half Transfer clear
        CTEIF3: bool, // bit offset: 11 desc: Channel 3 Transfer Error clear
        CGIF4: bool, // bit offset: 12 desc: Channel 4 Global interrupt clear
        CTCIF4: bool, // bit offset: 13 desc: Channel 4 Transfer Complete clear
        CHTIF4: bool, // bit offset: 14 desc: Channel 4 Half Transfer clear
        CTEIF4: bool, // bit offset: 15 desc: Channel 4 Transfer Error clear
        CGIF5: bool, // bit offset: 16 desc: Channel 5 Global interrupt clear
        CTCIF5: bool, // bit offset: 17 desc: Channel 5 Transfer Complete clear
        CHTIF5: bool, // bit offset: 18 desc: Channel 5 Half Transfer clear
        CTEIF5: bool, // bit offset: 19 desc: Channel 5 Transfer Error clear
        CGIF6: bool, // bit offset: 20 desc: Channel 6 Global interrupt clear
        CTCIF6: bool, // bit offset: 21 desc: Channel 6 Transfer Complete clear
        CHTIF6: bool, // bit offset: 22 desc: Channel 6 Half Transfer clear
        CTEIF6: bool, // bit offset: 23 desc: Channel 6 Transfer Error clear
        CGIF7: bool, // bit offset: 24 desc: Channel 7 Global interrupt clear
        CTCIF7: bool, // bit offset: 25 desc: Channel 7 Transfer Complete clear
        CHTIF7: bool, // bit offset: 26 desc: Channel 7 Half Transfer clear
        CTEIF7: bool, // bit offset: 27 desc: Channel 7 Transfer Error clear
        padding: u4 = 0,
    });
    // byte offset: 8 DMA channel configuration register (DMA_CCR)
    pub const CCR1 = MMIO(Address + 0x00000008, u32, packed struct {
        EN: bool, // bit offset: 0 desc: Channel enable
        TCIE: bool, // bit offset: 1 desc: Transfer complete interrupt enable
        HTIE: bool, // bit offset: 2 desc: Half Transfer interrupt enable
        TEIE: bool, // bit offset: 3 desc: Transfer error interrupt enable
        DIR: bool, // bit offset: 4 desc: Data transfer direction
        CIRC: bool, // bit offset: 5 desc: Circular mode
        PINC: bool, // bit offset: 6 desc: Peripheral increment mode
        MINC: bool, // bit offset: 7 desc: Memory increment mode
        PSIZE: u2, // bit offset: 8 desc: Peripheral size
        MSIZE: u2, // bit offset: 10 desc: Memory size
        PL: u2, // bit offset: 12 desc: Channel Priority level
        MEM2MEM: bool, // bit offset: 14 desc: Memory to memory mode
        padding: u17 = 0,
    });
    // byte offset: 12 DMA channel 1 number of data register
    pub const CNDTR1 = MMIO(Address + 0x0000000c, u32, packed struct {
        NDT: u16, // bit offset: 0 desc: Number of data to transfer
        padding: u16 = 0,
    });
    // byte offset: 16 DMA channel 1 peripheral address register
    pub const CPAR1 = MMIO(Address + 0x00000010, u32, packed struct {
        PA: u32, // bit offset: 0 desc: Peripheral address
    });
    // byte offset: 20 DMA channel 1 memory address register
    pub const CMAR1 = MMIO(Address + 0x00000014, u32, packed struct {
        MA: u32, // bit offset: 0 desc: Memory address
    });
    // byte offset: 28 DMA channel configuration register (DMA_CCR)
    pub const CCR2 = MMIO(Address + 0x0000001c, u32, packed struct {
        EN: bool, // bit offset: 0 desc: Channel enable
        TCIE: bool, // bit offset: 1 desc: Transfer complete interrupt enable
        HTIE: bool, // bit offset: 2 desc: Half Transfer interrupt enable
        TEIE: bool, // bit offset: 3 desc: Transfer error interrupt enable
        DIR: bool, // bit offset: 4 desc: Data transfer direction
        CIRC: bool, // bit offset: 5 desc: Circular mode
        PINC: bool, // bit offset: 6 desc: Peripheral increment mode
        MINC: bool, // bit offset: 7 desc: Memory increment mode
        PSIZE: u2, // bit offset: 8 desc: Peripheral size
        MSIZE: u2, // bit offset: 10 desc: Memory size
        PL: u2, // bit offset: 12 desc: Channel Priority level
        MEM2MEM: bool, // bit offset: 14 desc: Memory to memory mode
        padding: u17 = 0,
    });
    // byte offset: 32 DMA channel 2 number of data register
    pub const CNDTR2 = MMIO(Address + 0x00000020, u32, packed struct {
        NDT: u16, // bit offset: 0 desc: Number of data to transfer
        padding: u16 = 0,
    });
    // byte offset: 36 DMA channel 2 peripheral address register
    pub const CPAR2 = MMIO(Address + 0x00000024, u32, packed struct {
        PA: u32, // bit offset: 0 desc: Peripheral address
    });
    // byte offset: 40 DMA channel 2 memory address register
    pub const CMAR2 = MMIO(Address + 0x00000028, u32, packed struct {
        MA: u32, // bit offset: 0 desc: Memory address
    });
    // byte offset: 48 DMA channel configuration register (DMA_CCR)
    pub const CCR3 = MMIO(Address + 0x00000030, u32, packed struct {
        EN: bool, // bit offset: 0 desc: Channel enable
        TCIE: bool, // bit offset: 1 desc: Transfer complete interrupt enable
        HTIE: bool, // bit offset: 2 desc: Half Transfer interrupt enable
        TEIE: bool, // bit offset: 3 desc: Transfer error interrupt enable
        DIR: bool, // bit offset: 4 desc: Data transfer direction
        CIRC: bool, // bit offset: 5 desc: Circular mode
        PINC: bool, // bit offset: 6 desc: Peripheral increment mode
        MINC: bool, // bit offset: 7 desc: Memory increment mode
        PSIZE: u2, // bit offset: 8 desc: Peripheral size
        MSIZE: u2, // bit offset: 10 desc: Memory size
        PL: u2, // bit offset: 12 desc: Channel Priority level
        MEM2MEM: bool, // bit offset: 14 desc: Memory to memory mode
        padding: u17 = 0,
    });
    // byte offset: 52 DMA channel 3 number of data register
    pub const CNDTR3 = MMIO(Address + 0x00000034, u32, packed struct {
        NDT: u16, // bit offset: 0 desc: Number of data to transfer
        padding: u16 = 0,
    });
    // byte offset: 56 DMA channel 3 peripheral address register
    pub const CPAR3 = MMIO(Address + 0x00000038, u32, packed struct {
        PA: u32, // bit offset: 0 desc: Peripheral address
    });
    // byte offset: 60 DMA channel 3 memory address register
    pub const CMAR3 = MMIO(Address + 0x0000003c, u32, packed struct {
        MA: u32, // bit offset: 0 desc: Memory address
    });
    // byte offset: 68 DMA channel configuration register (DMA_CCR)
    pub const CCR4 = MMIO(Address + 0x00000044, u32, packed struct {
        EN: bool, // bit offset: 0 desc: Channel enable
        TCIE: bool, // bit offset: 1 desc: Transfer complete interrupt enable
        HTIE: bool, // bit offset: 2 desc: Half Transfer interrupt enable
        TEIE: bool, // bit offset: 3 desc: Transfer error interrupt enable
        DIR: bool, // bit offset: 4 desc: Data transfer direction
        CIRC: bool, // bit offset: 5 desc: Circular mode
        PINC: bool, // bit offset: 6 desc: Peripheral increment mode
        MINC: bool, // bit offset: 7 desc: Memory increment mode
        PSIZE: u2, // bit offset: 8 desc: Peripheral size
        MSIZE: u2, // bit offset: 10 desc: Memory size
        PL: u2, // bit offset: 12 desc: Channel Priority level
        MEM2MEM: bool, // bit offset: 14 desc: Memory to memory mode
        padding: u17 = 0,
    });
    // byte offset: 72 DMA channel 4 number of data register
    pub const CNDTR4 = MMIO(Address + 0x00000048, u32, packed struct {
        NDT: u16, // bit offset: 0 desc: Number of data to transfer
        padding: u16 = 0,
    });
    // byte offset: 76 DMA channel 4 peripheral address register
    pub const CPAR4 = MMIO(Address + 0x0000004c, u32, packed struct {
        PA: u32, // bit offset: 0 desc: Peripheral address
    });
    // byte offset: 80 DMA channel 4 memory address register
    pub const CMAR4 = MMIO(Address + 0x00000050, u32, packed struct {
        MA: u32, // bit offset: 0 desc: Memory address
    });
    // byte offset: 88 DMA channel configuration register (DMA_CCR)
    pub const CCR5 = MMIO(Address + 0x00000058, u32, packed struct {
        EN: bool, // bit offset: 0 desc: Channel enable
        TCIE: bool, // bit offset: 1 desc: Transfer complete interrupt enable
        HTIE: bool, // bit offset: 2 desc: Half Transfer interrupt enable
        TEIE: bool, // bit offset: 3 desc: Transfer error interrupt enable
        DIR: bool, // bit offset: 4 desc: Data transfer direction
        CIRC: bool, // bit offset: 5 desc: Circular mode
        PINC: bool, // bit offset: 6 desc: Peripheral increment mode
        MINC: bool, // bit offset: 7 desc: Memory increment mode
        PSIZE: u2, // bit offset: 8 desc: Peripheral size
        MSIZE: u2, // bit offset: 10 desc: Memory size
        PL: u2, // bit offset: 12 desc: Channel Priority level
        MEM2MEM: bool, // bit offset: 14 desc: Memory to memory mode
        padding: u17 = 0,
    });
    // byte offset: 92 DMA channel 5 number of data register
    pub const CNDTR5 = MMIO(Address + 0x0000005c, u32, packed struct {
        NDT: u16, // bit offset: 0 desc: Number of data to transfer
        padding: u16 = 0,
    });
    // byte offset: 96 DMA channel 5 peripheral address register
    pub const CPAR5 = MMIO(Address + 0x00000060, u32, packed struct {
        PA: u32, // bit offset: 0 desc: Peripheral address
    });
    // byte offset: 100 DMA channel 5 memory address register
    pub const CMAR5 = MMIO(Address + 0x00000064, u32, packed struct {
        MA: u32, // bit offset: 0 desc: Memory address
    });
    // byte offset: 108 DMA channel configuration register (DMA_CCR)
    pub const CCR6 = MMIO(Address + 0x0000006c, u32, packed struct {
        EN: bool, // bit offset: 0 desc: Channel enable
        TCIE: bool, // bit offset: 1 desc: Transfer complete interrupt enable
        HTIE: bool, // bit offset: 2 desc: Half Transfer interrupt enable
        TEIE: bool, // bit offset: 3 desc: Transfer error interrupt enable
        DIR: bool, // bit offset: 4 desc: Data transfer direction
        CIRC: bool, // bit offset: 5 desc: Circular mode
        PINC: bool, // bit offset: 6 desc: Peripheral increment mode
        MINC: bool, // bit offset: 7 desc: Memory increment mode
        PSIZE: u2, // bit offset: 8 desc: Peripheral size
        MSIZE: u2, // bit offset: 10 desc: Memory size
        PL: u2, // bit offset: 12 desc: Channel Priority level
        MEM2MEM: bool, // bit offset: 14 desc: Memory to memory mode
        padding: u17 = 0,
    });
    // byte offset: 112 DMA channel 6 number of data register
    pub const CNDTR6 = MMIO(Address + 0x00000070, u32, packed struct {
        NDT: u16, // bit offset: 0 desc: Number of data to transfer
        padding: u16 = 0,
    });
    // byte offset: 116 DMA channel 6 peripheral address register
    pub const CPAR6 = MMIO(Address + 0x00000074, u32, packed struct {
        PA: u32, // bit offset: 0 desc: Peripheral address
    });
    // byte offset: 120 DMA channel 6 memory address register
    pub const CMAR6 = MMIO(Address + 0x00000078, u32, packed struct {
        MA: u32, // bit offset: 0 desc: Memory address
    });
    // byte offset: 128 DMA channel configuration register (DMA_CCR)
    pub const CCR7 = MMIO(Address + 0x00000080, u32, packed struct {
        EN: bool, // bit offset: 0 desc: Channel enable
        TCIE: bool, // bit offset: 1 desc: Transfer complete interrupt enable
        HTIE: bool, // bit offset: 2 desc: Half Transfer interrupt enable
        TEIE: bool, // bit offset: 3 desc: Transfer error interrupt enable
        DIR: bool, // bit offset: 4 desc: Data transfer direction
        CIRC: bool, // bit offset: 5 desc: Circular mode
        PINC: bool, // bit offset: 6 desc: Peripheral increment mode
        MINC: bool, // bit offset: 7 desc: Memory increment mode
        PSIZE: u2, // bit offset: 8 desc: Peripheral size
        MSIZE: u2, // bit offset: 10 desc: Memory size
        PL: u2, // bit offset: 12 desc: Channel Priority level
        MEM2MEM: bool, // bit offset: 14 desc: Memory to memory mode
        padding: u17 = 0,
    });
    // byte offset: 132 DMA channel 7 number of data register
    pub const CNDTR7 = MMIO(Address + 0x00000084, u32, packed struct {
        NDT: u16, // bit offset: 0 desc: Number of data to transfer
        padding: u16 = 0,
    });
    // byte offset: 136 DMA channel 7 peripheral address register
    pub const CPAR7 = MMIO(Address + 0x00000088, u32, packed struct {
        PA: u32, // bit offset: 0 desc: Peripheral address
    });
    // byte offset: 140 DMA channel 7 memory address register
    pub const CMAR7 = MMIO(Address + 0x0000008c, u32, packed struct {
        MA: u32, // bit offset: 0 desc: Memory address
    });
};
pub const DMA2 = extern struct {
    pub const Address: u32 = 0x40020400;
    // byte offset: 0 DMA interrupt status register (DMA_ISR)
    pub const ISR = MMIO(Address + 0x00000000, u32, packed struct {
        GIF1: bool, // bit offset: 0 desc: Channel 1 Global interrupt flag
        TCIF1: bool, // bit offset: 1 desc: Channel 1 Transfer Complete flag
        HTIF1: bool, // bit offset: 2 desc: Channel 1 Half Transfer Complete flag
        TEIF1: bool, // bit offset: 3 desc: Channel 1 Transfer Error flag
        GIF2: bool, // bit offset: 4 desc: Channel 2 Global interrupt flag
        TCIF2: bool, // bit offset: 5 desc: Channel 2 Transfer Complete flag
        HTIF2: bool, // bit offset: 6 desc: Channel 2 Half Transfer Complete flag
        TEIF2: bool, // bit offset: 7 desc: Channel 2 Transfer Error flag
        GIF3: bool, // bit offset: 8 desc: Channel 3 Global interrupt flag
        TCIF3: bool, // bit offset: 9 desc: Channel 3 Transfer Complete flag
        HTIF3: bool, // bit offset: 10 desc: Channel 3 Half Transfer Complete flag
        TEIF3: bool, // bit offset: 11 desc: Channel 3 Transfer Error flag
        GIF4: bool, // bit offset: 12 desc: Channel 4 Global interrupt flag
        TCIF4: bool, // bit offset: 13 desc: Channel 4 Transfer Complete flag
        HTIF4: bool, // bit offset: 14 desc: Channel 4 Half Transfer Complete flag
        TEIF4: bool, // bit offset: 15 desc: Channel 4 Transfer Error flag
        GIF5: bool, // bit offset: 16 desc: Channel 5 Global interrupt flag
        TCIF5: bool, // bit offset: 17 desc: Channel 5 Transfer Complete flag
        HTIF5: bool, // bit offset: 18 desc: Channel 5 Half Transfer Complete flag
        TEIF5: bool, // bit offset: 19 desc: Channel 5 Transfer Error flag
        GIF6: bool, // bit offset: 20 desc: Channel 6 Global interrupt flag
        TCIF6: bool, // bit offset: 21 desc: Channel 6 Transfer Complete flag
        HTIF6: bool, // bit offset: 22 desc: Channel 6 Half Transfer Complete flag
        TEIF6: bool, // bit offset: 23 desc: Channel 6 Transfer Error flag
        GIF7: bool, // bit offset: 24 desc: Channel 7 Global interrupt flag
        TCIF7: bool, // bit offset: 25 desc: Channel 7 Transfer Complete flag
        HTIF7: bool, // bit offset: 26 desc: Channel 7 Half Transfer Complete flag
        TEIF7: bool, // bit offset: 27 desc: Channel 7 Transfer Error flag
        padding: u4 = 0,
    });
    // byte offset: 4 DMA interrupt flag clear register (DMA_IFCR)
    pub const IFCR = MMIO(Address + 0x00000004, u32, packed struct {
        CGIF1: bool, // bit offset: 0 desc: Channel 1 Global interrupt clear
        CTCIF1: bool, // bit offset: 1 desc: Channel 1 Transfer Complete clear
        CHTIF1: bool, // bit offset: 2 desc: Channel 1 Half Transfer clear
        CTEIF1: bool, // bit offset: 3 desc: Channel 1 Transfer Error clear
        CGIF2: bool, // bit offset: 4 desc: Channel 2 Global interrupt clear
        CTCIF2: bool, // bit offset: 5 desc: Channel 2 Transfer Complete clear
        CHTIF2: bool, // bit offset: 6 desc: Channel 2 Half Transfer clear
        CTEIF2: bool, // bit offset: 7 desc: Channel 2 Transfer Error clear
        CGIF3: bool, // bit offset: 8 desc: Channel 3 Global interrupt clear
        CTCIF3: bool, // bit offset: 9 desc: Channel 3 Transfer Complete clear
        CHTIF3: bool, // bit offset: 10 desc: Channel 3 Half Transfer clear
        CTEIF3: bool, // bit offset: 11 desc: Channel 3 Transfer Error clear
        CGIF4: bool, // bit offset: 12 desc: Channel 4 Global interrupt clear
        CTCIF4: bool, // bit offset: 13 desc: Channel 4 Transfer Complete clear
        CHTIF4: bool, // bit offset: 14 desc: Channel 4 Half Transfer clear
        CTEIF4: bool, // bit offset: 15 desc: Channel 4 Transfer Error clear
        CGIF5: bool, // bit offset: 16 desc: Channel 5 Global interrupt clear
        CTCIF5: bool, // bit offset: 17 desc: Channel 5 Transfer Complete clear
        CHTIF5: bool, // bit offset: 18 desc: Channel 5 Half Transfer clear
        CTEIF5: bool, // bit offset: 19 desc: Channel 5 Transfer Error clear
        CGIF6: bool, // bit offset: 20 desc: Channel 6 Global interrupt clear
        CTCIF6: bool, // bit offset: 21 desc: Channel 6 Transfer Complete clear
        CHTIF6: bool, // bit offset: 22 desc: Channel 6 Half Transfer clear
        CTEIF6: bool, // bit offset: 23 desc: Channel 6 Transfer Error clear
        CGIF7: bool, // bit offset: 24 desc: Channel 7 Global interrupt clear
        CTCIF7: bool, // bit offset: 25 desc: Channel 7 Transfer Complete clear
        CHTIF7: bool, // bit offset: 26 desc: Channel 7 Half Transfer clear
        CTEIF7: bool, // bit offset: 27 desc: Channel 7 Transfer Error clear
        padding: u4 = 0,
    });
    // byte offset: 8 DMA channel configuration register (DMA_CCR)
    pub const CCR1 = MMIO(Address + 0x00000008, u32, packed struct {
        EN: bool, // bit offset: 0 desc: Channel enable
        TCIE: bool, // bit offset: 1 desc: Transfer complete interrupt enable
        HTIE: bool, // bit offset: 2 desc: Half Transfer interrupt enable
        TEIE: bool, // bit offset: 3 desc: Transfer error interrupt enable
        DIR: bool, // bit offset: 4 desc: Data transfer direction
        CIRC: bool, // bit offset: 5 desc: Circular mode
        PINC: bool, // bit offset: 6 desc: Peripheral increment mode
        MINC: bool, // bit offset: 7 desc: Memory increment mode
        PSIZE: u2, // bit offset: 8 desc: Peripheral size
        MSIZE: u2, // bit offset: 10 desc: Memory size
        PL: u2, // bit offset: 12 desc: Channel Priority level
        MEM2MEM: bool, // bit offset: 14 desc: Memory to memory mode
        padding: u17 = 0,
    });
    // byte offset: 12 DMA channel 1 number of data register
    pub const CNDTR1 = MMIO(Address + 0x0000000c, u32, packed struct {
        NDT: u16, // bit offset: 0 desc: Number of data to transfer
        padding: u16 = 0,
    });
    // byte offset: 16 DMA channel 1 peripheral address register
    pub const CPAR1 = MMIO(Address + 0x00000010, u32, packed struct {
        PA: u32, // bit offset: 0 desc: Peripheral address
    });
    // byte offset: 20 DMA channel 1 memory address register
    pub const CMAR1 = MMIO(Address + 0x00000014, u32, packed struct {
        MA: u32, // bit offset: 0 desc: Memory address
    });
    // byte offset: 28 DMA channel configuration register (DMA_CCR)
    pub const CCR2 = MMIO(Address + 0x0000001c, u32, packed struct {
        EN: bool, // bit offset: 0 desc: Channel enable
        TCIE: bool, // bit offset: 1 desc: Transfer complete interrupt enable
        HTIE: bool, // bit offset: 2 desc: Half Transfer interrupt enable
        TEIE: bool, // bit offset: 3 desc: Transfer error interrupt enable
        DIR: bool, // bit offset: 4 desc: Data transfer direction
        CIRC: bool, // bit offset: 5 desc: Circular mode
        PINC: bool, // bit offset: 6 desc: Peripheral increment mode
        MINC: bool, // bit offset: 7 desc: Memory increment mode
        PSIZE: u2, // bit offset: 8 desc: Peripheral size
        MSIZE: u2, // bit offset: 10 desc: Memory size
        PL: u2, // bit offset: 12 desc: Channel Priority level
        MEM2MEM: bool, // bit offset: 14 desc: Memory to memory mode
        padding: u17 = 0,
    });
    // byte offset: 32 DMA channel 2 number of data register
    pub const CNDTR2 = MMIO(Address + 0x00000020, u32, packed struct {
        NDT: u16, // bit offset: 0 desc: Number of data to transfer
        padding: u16 = 0,
    });
    // byte offset: 36 DMA channel 2 peripheral address register
    pub const CPAR2 = MMIO(Address + 0x00000024, u32, packed struct {
        PA: u32, // bit offset: 0 desc: Peripheral address
    });
    // byte offset: 40 DMA channel 2 memory address register
    pub const CMAR2 = MMIO(Address + 0x00000028, u32, packed struct {
        MA: u32, // bit offset: 0 desc: Memory address
    });
    // byte offset: 48 DMA channel configuration register (DMA_CCR)
    pub const CCR3 = MMIO(Address + 0x00000030, u32, packed struct {
        EN: bool, // bit offset: 0 desc: Channel enable
        TCIE: bool, // bit offset: 1 desc: Transfer complete interrupt enable
        HTIE: bool, // bit offset: 2 desc: Half Transfer interrupt enable
        TEIE: bool, // bit offset: 3 desc: Transfer error interrupt enable
        DIR: bool, // bit offset: 4 desc: Data transfer direction
        CIRC: bool, // bit offset: 5 desc: Circular mode
        PINC: bool, // bit offset: 6 desc: Peripheral increment mode
        MINC: bool, // bit offset: 7 desc: Memory increment mode
        PSIZE: u2, // bit offset: 8 desc: Peripheral size
        MSIZE: u2, // bit offset: 10 desc: Memory size
        PL: u2, // bit offset: 12 desc: Channel Priority level
        MEM2MEM: bool, // bit offset: 14 desc: Memory to memory mode
        padding: u17 = 0,
    });
    // byte offset: 52 DMA channel 3 number of data register
    pub const CNDTR3 = MMIO(Address + 0x00000034, u32, packed struct {
        NDT: u16, // bit offset: 0 desc: Number of data to transfer
        padding: u16 = 0,
    });
    // byte offset: 56 DMA channel 3 peripheral address register
    pub const CPAR3 = MMIO(Address + 0x00000038, u32, packed struct {
        PA: u32, // bit offset: 0 desc: Peripheral address
    });
    // byte offset: 60 DMA channel 3 memory address register
    pub const CMAR3 = MMIO(Address + 0x0000003c, u32, packed struct {
        MA: u32, // bit offset: 0 desc: Memory address
    });
    // byte offset: 68 DMA channel configuration register (DMA_CCR)
    pub const CCR4 = MMIO(Address + 0x00000044, u32, packed struct {
        EN: bool, // bit offset: 0 desc: Channel enable
        TCIE: bool, // bit offset: 1 desc: Transfer complete interrupt enable
        HTIE: bool, // bit offset: 2 desc: Half Transfer interrupt enable
        TEIE: bool, // bit offset: 3 desc: Transfer error interrupt enable
        DIR: bool, // bit offset: 4 desc: Data transfer direction
        CIRC: bool, // bit offset: 5 desc: Circular mode
        PINC: bool, // bit offset: 6 desc: Peripheral increment mode
        MINC: bool, // bit offset: 7 desc: Memory increment mode
        PSIZE: u2, // bit offset: 8 desc: Peripheral size
        MSIZE: u2, // bit offset: 10 desc: Memory size
        PL: u2, // bit offset: 12 desc: Channel Priority level
        MEM2MEM: bool, // bit offset: 14 desc: Memory to memory mode
        padding: u17 = 0,
    });
    // byte offset: 72 DMA channel 4 number of data register
    pub const CNDTR4 = MMIO(Address + 0x00000048, u32, packed struct {
        NDT: u16, // bit offset: 0 desc: Number of data to transfer
        padding: u16 = 0,
    });
    // byte offset: 76 DMA channel 4 peripheral address register
    pub const CPAR4 = MMIO(Address + 0x0000004c, u32, packed struct {
        PA: u32, // bit offset: 0 desc: Peripheral address
    });
    // byte offset: 80 DMA channel 4 memory address register
    pub const CMAR4 = MMIO(Address + 0x00000050, u32, packed struct {
        MA: u32, // bit offset: 0 desc: Memory address
    });
    // byte offset: 88 DMA channel configuration register (DMA_CCR)
    pub const CCR5 = MMIO(Address + 0x00000058, u32, packed struct {
        EN: bool, // bit offset: 0 desc: Channel enable
        TCIE: bool, // bit offset: 1 desc: Transfer complete interrupt enable
        HTIE: bool, // bit offset: 2 desc: Half Transfer interrupt enable
        TEIE: bool, // bit offset: 3 desc: Transfer error interrupt enable
        DIR: bool, // bit offset: 4 desc: Data transfer direction
        CIRC: bool, // bit offset: 5 desc: Circular mode
        PINC: bool, // bit offset: 6 desc: Peripheral increment mode
        MINC: bool, // bit offset: 7 desc: Memory increment mode
        PSIZE: u2, // bit offset: 8 desc: Peripheral size
        MSIZE: u2, // bit offset: 10 desc: Memory size
        PL: u2, // bit offset: 12 desc: Channel Priority level
        MEM2MEM: bool, // bit offset: 14 desc: Memory to memory mode
        padding: u17 = 0,
    });
    // byte offset: 92 DMA channel 5 number of data register
    pub const CNDTR5 = MMIO(Address + 0x0000005c, u32, packed struct {
        NDT: u16, // bit offset: 0 desc: Number of data to transfer
        padding: u16 = 0,
    });
    // byte offset: 96 DMA channel 5 peripheral address register
    pub const CPAR5 = MMIO(Address + 0x00000060, u32, packed struct {
        PA: u32, // bit offset: 0 desc: Peripheral address
    });
    // byte offset: 100 DMA channel 5 memory address register
    pub const CMAR5 = MMIO(Address + 0x00000064, u32, packed struct {
        MA: u32, // bit offset: 0 desc: Memory address
    });
    // byte offset: 108 DMA channel configuration register (DMA_CCR)
    pub const CCR6 = MMIO(Address + 0x0000006c, u32, packed struct {
        EN: bool, // bit offset: 0 desc: Channel enable
        TCIE: bool, // bit offset: 1 desc: Transfer complete interrupt enable
        HTIE: bool, // bit offset: 2 desc: Half Transfer interrupt enable
        TEIE: bool, // bit offset: 3 desc: Transfer error interrupt enable
        DIR: bool, // bit offset: 4 desc: Data transfer direction
        CIRC: bool, // bit offset: 5 desc: Circular mode
        PINC: bool, // bit offset: 6 desc: Peripheral increment mode
        MINC: bool, // bit offset: 7 desc: Memory increment mode
        PSIZE: u2, // bit offset: 8 desc: Peripheral size
        MSIZE: u2, // bit offset: 10 desc: Memory size
        PL: u2, // bit offset: 12 desc: Channel Priority level
        MEM2MEM: bool, // bit offset: 14 desc: Memory to memory mode
        padding: u17 = 0,
    });
    // byte offset: 112 DMA channel 6 number of data register
    pub const CNDTR6 = MMIO(Address + 0x00000070, u32, packed struct {
        NDT: u16, // bit offset: 0 desc: Number of data to transfer
        padding: u16 = 0,
    });
    // byte offset: 116 DMA channel 6 peripheral address register
    pub const CPAR6 = MMIO(Address + 0x00000074, u32, packed struct {
        PA: u32, // bit offset: 0 desc: Peripheral address
    });
    // byte offset: 120 DMA channel 6 memory address register
    pub const CMAR6 = MMIO(Address + 0x00000078, u32, packed struct {
        MA: u32, // bit offset: 0 desc: Memory address
    });
    // byte offset: 128 DMA channel configuration register (DMA_CCR)
    pub const CCR7 = MMIO(Address + 0x00000080, u32, packed struct {
        EN: bool, // bit offset: 0 desc: Channel enable
        TCIE: bool, // bit offset: 1 desc: Transfer complete interrupt enable
        HTIE: bool, // bit offset: 2 desc: Half Transfer interrupt enable
        TEIE: bool, // bit offset: 3 desc: Transfer error interrupt enable
        DIR: bool, // bit offset: 4 desc: Data transfer direction
        CIRC: bool, // bit offset: 5 desc: Circular mode
        PINC: bool, // bit offset: 6 desc: Peripheral increment mode
        MINC: bool, // bit offset: 7 desc: Memory increment mode
        PSIZE: u2, // bit offset: 8 desc: Peripheral size
        MSIZE: u2, // bit offset: 10 desc: Memory size
        PL: u2, // bit offset: 12 desc: Channel Priority level
        MEM2MEM: bool, // bit offset: 14 desc: Memory to memory mode
        padding: u17 = 0,
    });
    // byte offset: 132 DMA channel 7 number of data register
    pub const CNDTR7 = MMIO(Address + 0x00000084, u32, packed struct {
        NDT: u16, // bit offset: 0 desc: Number of data to transfer
        padding: u16 = 0,
    });
    // byte offset: 136 DMA channel 7 peripheral address register
    pub const CPAR7 = MMIO(Address + 0x00000088, u32, packed struct {
        PA: u32, // bit offset: 0 desc: Peripheral address
    });
    // byte offset: 140 DMA channel 7 memory address register
    pub const CMAR7 = MMIO(Address + 0x0000008c, u32, packed struct {
        MA: u32, // bit offset: 0 desc: Memory address
    });
};
pub const TIM2 = extern struct {
    pub const Address: u32 = 0x40000000;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CEN: bool, // bit offset: 0 desc: Counter enable
        UDIS: bool, // bit offset: 1 desc: Update disable
        URS: bool, // bit offset: 2 desc: Update request source
        OPM: bool, // bit offset: 3 desc: One-pulse mode
        DIR: bool, // bit offset: 4 desc: Direction
        CMS: u2, // bit offset: 5 desc: Center-aligned mode selection
        ARPE: bool, // bit offset: 7 desc: Auto-reload preload enable
        CKD: u2, // bit offset: 8 desc: Clock division
        reserved0: u1 = 0,
        UIFREMAP: bool, // bit offset: 11 desc: UIF status bit remapping
        padding: u20 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        reserved0: u3 = 0,
        CCDS: bool, // bit offset: 3 desc: Capture/compare DMA selection
        MMS: u3, // bit offset: 4 desc: Master mode selection
        TI1S: bool, // bit offset: 7 desc: TI1 selection
        padding: u24 = 0,
    });
    // byte offset: 8 slave mode control register
    pub const SMCR = MMIO(Address + 0x00000008, u32, packed struct {
        SMS: u3, // bit offset: 0 desc: Slave mode selection
        OCCS: bool, // bit offset: 3 desc: OCREF clear selection
        TS: u3, // bit offset: 4 desc: Trigger selection
        MSM: bool, // bit offset: 7 desc: Master/Slave mode
        ETF: u4, // bit offset: 8 desc: External trigger filter
        ETPS: u2, // bit offset: 12 desc: External trigger prescaler
        ECE: bool, // bit offset: 14 desc: External clock enable
        ETP: bool, // bit offset: 15 desc: External trigger polarity
        SMS_3: bool, // bit offset: 16 desc: Slave mode selection bit3
        padding: u15 = 0,
    });
    // byte offset: 12 DMA/Interrupt enable register
    pub const DIER = MMIO(Address + 0x0000000c, u32, packed struct {
        UIE: bool, // bit offset: 0 desc: Update interrupt enable
        CC1IE: bool, // bit offset: 1 desc: Capture/Compare 1 interrupt enable
        CC2IE: bool, // bit offset: 2 desc: Capture/Compare 2 interrupt enable
        CC3IE: bool, // bit offset: 3 desc: Capture/Compare 3 interrupt enable
        CC4IE: bool, // bit offset: 4 desc: Capture/Compare 4 interrupt enable
        reserved0: u1 = 0,
        TIE: bool, // bit offset: 6 desc: Trigger interrupt enable
        reserved1: u1 = 0,
        UDE: bool, // bit offset: 8 desc: Update DMA request enable
        CC1DE: bool, // bit offset: 9 desc: Capture/Compare 1 DMA request enable
        CC2DE: bool, // bit offset: 10 desc: Capture/Compare 2 DMA request enable
        CC3DE: bool, // bit offset: 11 desc: Capture/Compare 3 DMA request enable
        CC4DE: bool, // bit offset: 12 desc: Capture/Compare 4 DMA request enable
        reserved2: u1 = 0,
        TDE: bool, // bit offset: 14 desc: Trigger DMA request enable
        padding: u17 = 0,
    });
    // byte offset: 16 status register
    pub const SR = MMIO(Address + 0x00000010, u32, packed struct {
        UIF: bool, // bit offset: 0 desc: Update interrupt flag
        CC1IF: bool, // bit offset: 1 desc: Capture/compare 1 interrupt flag
        CC2IF: bool, // bit offset: 2 desc: Capture/Compare 2 interrupt flag
        CC3IF: bool, // bit offset: 3 desc: Capture/Compare 3 interrupt flag
        CC4IF: bool, // bit offset: 4 desc: Capture/Compare 4 interrupt flag
        reserved0: u1 = 0,
        TIF: bool, // bit offset: 6 desc: Trigger interrupt flag
        reserved1: u2 = 0,
        CC1OF: bool, // bit offset: 9 desc: Capture/Compare 1 overcapture flag
        CC2OF: bool, // bit offset: 10 desc: Capture/compare 2 overcapture flag
        CC3OF: bool, // bit offset: 11 desc: Capture/Compare 3 overcapture flag
        CC4OF: bool, // bit offset: 12 desc: Capture/Compare 4 overcapture flag
        padding: u19 = 0,
    });
    // byte offset: 20 event generation register
    pub const EGR = MMIO(Address + 0x00000014, u32, packed struct {
        UG: bool, // bit offset: 0 desc: Update generation
        CC1G: bool, // bit offset: 1 desc: Capture/compare 1 generation
        CC2G: bool, // bit offset: 2 desc: Capture/compare 2 generation
        CC3G: bool, // bit offset: 3 desc: Capture/compare 3 generation
        CC4G: bool, // bit offset: 4 desc: Capture/compare 4 generation
        reserved0: u1 = 0,
        TG: bool, // bit offset: 6 desc: Trigger generation
        padding: u25 = 0,
    });
    // byte offset: 24 capture/compare mode register 1 (output mode)
    pub const CCMR1_Output = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        OC1FE: bool, // bit offset: 2 desc: Output compare 1 fast enable
        OC1PE: bool, // bit offset: 3 desc: Output compare 1 preload enable
        OC1M: u3, // bit offset: 4 desc: Output compare 1 mode
        OC1CE: bool, // bit offset: 7 desc: Output compare 1 clear enable
        CC2S: u2, // bit offset: 8 desc: Capture/Compare 2 selection
        OC2FE: bool, // bit offset: 10 desc: Output compare 2 fast enable
        OC2PE: bool, // bit offset: 11 desc: Output compare 2 preload enable
        OC2M: u3, // bit offset: 12 desc: Output compare 2 mode
        OC2CE: bool, // bit offset: 15 desc: Output compare 2 clear enable
        OC1M_3: bool, // bit offset: 16 desc: Output compare 1 mode bit 3
        reserved0: u7 = 0,
        OC2M_3: bool, // bit offset: 24 desc: Output compare 2 mode bit 3
        padding: u7 = 0,
    });
    // byte offset: 24 capture/compare mode register 1 (input mode)
    pub const CCMR1_Input = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        IC1PSC: u2, // bit offset: 2 desc: Input capture 1 prescaler
        IC1F: u4, // bit offset: 4 desc: Input capture 1 filter
        CC2S: u2, // bit offset: 8 desc: Capture/compare 2 selection
        IC2PSC: u2, // bit offset: 10 desc: Input capture 2 prescaler
        IC2F: u4, // bit offset: 12 desc: Input capture 2 filter
        padding: u16 = 0,
    });
    // byte offset: 28 capture/compare mode register 2 (output mode)
    pub const CCMR2_Output = MMIO(Address + 0x0000001c, u32, packed struct {
        CC3S: u2, // bit offset: 0 desc: Capture/Compare 3 selection
        OC3FE: bool, // bit offset: 2 desc: Output compare 3 fast enable
        OC3PE: bool, // bit offset: 3 desc: Output compare 3 preload enable
        OC3M: u3, // bit offset: 4 desc: Output compare 3 mode
        OC3CE: bool, // bit offset: 7 desc: Output compare 3 clear enable
        CC4S: u2, // bit offset: 8 desc: Capture/Compare 4 selection
        OC4FE: bool, // bit offset: 10 desc: Output compare 4 fast enable
        OC4PE: bool, // bit offset: 11 desc: Output compare 4 preload enable
        OC4M: u3, // bit offset: 12 desc: Output compare 4 mode
        O24CE: bool, // bit offset: 15 desc: Output compare 4 clear enable
        OC3M_3: bool, // bit offset: 16 desc: Output compare 3 mode bit3
        reserved0: u7 = 0,
        OC4M_3: bool, // bit offset: 24 desc: Output compare 4 mode bit3
        padding: u7 = 0,
    });
    // byte offset: 28 capture/compare mode register 2 (input mode)
    pub const CCMR2_Input = MMIO(Address + 0x0000001c, u32, packed struct {
        CC3S: u2, // bit offset: 0 desc: Capture/Compare 3 selection
        IC3PSC: u2, // bit offset: 2 desc: Input capture 3 prescaler
        IC3F: u4, // bit offset: 4 desc: Input capture 3 filter
        CC4S: u2, // bit offset: 8 desc: Capture/Compare 4 selection
        IC4PSC: u2, // bit offset: 10 desc: Input capture 4 prescaler
        IC4F: u4, // bit offset: 12 desc: Input capture 4 filter
        padding: u16 = 0,
    });
    // byte offset: 32 capture/compare enable register
    pub const CCER = MMIO(Address + 0x00000020, u32, packed struct {
        CC1E: bool, // bit offset: 0 desc: Capture/Compare 1 output enable
        CC1P: bool, // bit offset: 1 desc: Capture/Compare 1 output Polarity
        reserved0: u1 = 0,
        CC1NP: bool, // bit offset: 3 desc: Capture/Compare 1 output Polarity
        CC2E: bool, // bit offset: 4 desc: Capture/Compare 2 output enable
        CC2P: bool, // bit offset: 5 desc: Capture/Compare 2 output Polarity
        reserved1: u1 = 0,
        CC2NP: bool, // bit offset: 7 desc: Capture/Compare 2 output Polarity
        CC3E: bool, // bit offset: 8 desc: Capture/Compare 3 output enable
        CC3P: bool, // bit offset: 9 desc: Capture/Compare 3 output Polarity
        reserved2: u1 = 0,
        CC3NP: bool, // bit offset: 11 desc: Capture/Compare 3 output Polarity
        CC4E: bool, // bit offset: 12 desc: Capture/Compare 4 output enable
        CC4P: bool, // bit offset: 13 desc: Capture/Compare 3 output Polarity
        reserved3: u1 = 0,
        CC4NP: bool, // bit offset: 15 desc: Capture/Compare 3 output Polarity
        padding: u16 = 0,
    });
    // byte offset: 36 counter
    pub const CNT = MMIO(Address + 0x00000024, u32, packed struct {
        CNTL: u16, // bit offset: 0 desc: Low counter value
        CNTH: u15, // bit offset: 16 desc: High counter value
        CNT_or_UIFCPY: bool, // bit offset: 31 desc: if IUFREMAP=0 than CNT with read write access else UIFCPY with read only access
    });
    // byte offset: 40 prescaler
    pub const PSC = MMIO(Address + 0x00000028, u32, packed struct {
        PSC: u16, // bit offset: 0 desc: Prescaler value
        padding: u16 = 0,
    });
    // byte offset: 44 auto-reload register
    pub const ARR = MMIO(Address + 0x0000002c, u32, packed struct {
        ARRL: u16, // bit offset: 0 desc: Low Auto-reload value
        ARRH: u16, // bit offset: 16 desc: High Auto-reload value
    });
    // byte offset: 52 capture/compare register 1
    pub const CCR1 = MMIO(Address + 0x00000034, u32, packed struct {
        CCR1L: u16, // bit offset: 0 desc: Low Capture/Compare 1 value
        CCR1H: u16, // bit offset: 16 desc: High Capture/Compare 1 value (on TIM2)
    });
    // byte offset: 56 capture/compare register 2
    pub const CCR2 = MMIO(Address + 0x00000038, u32, packed struct {
        CCR2L: u16, // bit offset: 0 desc: Low Capture/Compare 2 value
        CCR2H: u16, // bit offset: 16 desc: High Capture/Compare 2 value (on TIM2)
    });
    // byte offset: 60 capture/compare register 3
    pub const CCR3 = MMIO(Address + 0x0000003c, u32, packed struct {
        CCR3L: u16, // bit offset: 0 desc: Low Capture/Compare value
        CCR3H: u16, // bit offset: 16 desc: High Capture/Compare value (on TIM2)
    });
    // byte offset: 64 capture/compare register 4
    pub const CCR4 = MMIO(Address + 0x00000040, u32, packed struct {
        CCR4L: u16, // bit offset: 0 desc: Low Capture/Compare value
        CCR4H: u16, // bit offset: 16 desc: High Capture/Compare value (on TIM2)
    });
    // byte offset: 72 DMA control register
    pub const DCR = MMIO(Address + 0x00000048, u32, packed struct {
        DBA: u5, // bit offset: 0 desc: DMA base address
        reserved0: u3 = 0,
        DBL: u5, // bit offset: 8 desc: DMA burst length
        padding: u19 = 0,
    });
    // byte offset: 76 DMA address for full transfer
    pub const DMAR = MMIO(Address + 0x0000004c, u32, packed struct {
        DMAB: u16, // bit offset: 0 desc: DMA register for burst accesses
        padding: u16 = 0,
    });
};
pub const TIM3 = extern struct {
    pub const Address: u32 = 0x40000400;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CEN: bool, // bit offset: 0 desc: Counter enable
        UDIS: bool, // bit offset: 1 desc: Update disable
        URS: bool, // bit offset: 2 desc: Update request source
        OPM: bool, // bit offset: 3 desc: One-pulse mode
        DIR: bool, // bit offset: 4 desc: Direction
        CMS: u2, // bit offset: 5 desc: Center-aligned mode selection
        ARPE: bool, // bit offset: 7 desc: Auto-reload preload enable
        CKD: u2, // bit offset: 8 desc: Clock division
        reserved0: u1 = 0,
        UIFREMAP: bool, // bit offset: 11 desc: UIF status bit remapping
        padding: u20 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        reserved0: u3 = 0,
        CCDS: bool, // bit offset: 3 desc: Capture/compare DMA selection
        MMS: u3, // bit offset: 4 desc: Master mode selection
        TI1S: bool, // bit offset: 7 desc: TI1 selection
        padding: u24 = 0,
    });
    // byte offset: 8 slave mode control register
    pub const SMCR = MMIO(Address + 0x00000008, u32, packed struct {
        SMS: u3, // bit offset: 0 desc: Slave mode selection
        OCCS: bool, // bit offset: 3 desc: OCREF clear selection
        TS: u3, // bit offset: 4 desc: Trigger selection
        MSM: bool, // bit offset: 7 desc: Master/Slave mode
        ETF: u4, // bit offset: 8 desc: External trigger filter
        ETPS: u2, // bit offset: 12 desc: External trigger prescaler
        ECE: bool, // bit offset: 14 desc: External clock enable
        ETP: bool, // bit offset: 15 desc: External trigger polarity
        SMS_3: bool, // bit offset: 16 desc: Slave mode selection bit3
        padding: u15 = 0,
    });
    // byte offset: 12 DMA/Interrupt enable register
    pub const DIER = MMIO(Address + 0x0000000c, u32, packed struct {
        UIE: bool, // bit offset: 0 desc: Update interrupt enable
        CC1IE: bool, // bit offset: 1 desc: Capture/Compare 1 interrupt enable
        CC2IE: bool, // bit offset: 2 desc: Capture/Compare 2 interrupt enable
        CC3IE: bool, // bit offset: 3 desc: Capture/Compare 3 interrupt enable
        CC4IE: bool, // bit offset: 4 desc: Capture/Compare 4 interrupt enable
        reserved0: u1 = 0,
        TIE: bool, // bit offset: 6 desc: Trigger interrupt enable
        reserved1: u1 = 0,
        UDE: bool, // bit offset: 8 desc: Update DMA request enable
        CC1DE: bool, // bit offset: 9 desc: Capture/Compare 1 DMA request enable
        CC2DE: bool, // bit offset: 10 desc: Capture/Compare 2 DMA request enable
        CC3DE: bool, // bit offset: 11 desc: Capture/Compare 3 DMA request enable
        CC4DE: bool, // bit offset: 12 desc: Capture/Compare 4 DMA request enable
        reserved2: u1 = 0,
        TDE: bool, // bit offset: 14 desc: Trigger DMA request enable
        padding: u17 = 0,
    });
    // byte offset: 16 status register
    pub const SR = MMIO(Address + 0x00000010, u32, packed struct {
        UIF: bool, // bit offset: 0 desc: Update interrupt flag
        CC1IF: bool, // bit offset: 1 desc: Capture/compare 1 interrupt flag
        CC2IF: bool, // bit offset: 2 desc: Capture/Compare 2 interrupt flag
        CC3IF: bool, // bit offset: 3 desc: Capture/Compare 3 interrupt flag
        CC4IF: bool, // bit offset: 4 desc: Capture/Compare 4 interrupt flag
        reserved0: u1 = 0,
        TIF: bool, // bit offset: 6 desc: Trigger interrupt flag
        reserved1: u2 = 0,
        CC1OF: bool, // bit offset: 9 desc: Capture/Compare 1 overcapture flag
        CC2OF: bool, // bit offset: 10 desc: Capture/compare 2 overcapture flag
        CC3OF: bool, // bit offset: 11 desc: Capture/Compare 3 overcapture flag
        CC4OF: bool, // bit offset: 12 desc: Capture/Compare 4 overcapture flag
        padding: u19 = 0,
    });
    // byte offset: 20 event generation register
    pub const EGR = MMIO(Address + 0x00000014, u32, packed struct {
        UG: bool, // bit offset: 0 desc: Update generation
        CC1G: bool, // bit offset: 1 desc: Capture/compare 1 generation
        CC2G: bool, // bit offset: 2 desc: Capture/compare 2 generation
        CC3G: bool, // bit offset: 3 desc: Capture/compare 3 generation
        CC4G: bool, // bit offset: 4 desc: Capture/compare 4 generation
        reserved0: u1 = 0,
        TG: bool, // bit offset: 6 desc: Trigger generation
        padding: u25 = 0,
    });
    // byte offset: 24 capture/compare mode register 1 (output mode)
    pub const CCMR1_Output = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        OC1FE: bool, // bit offset: 2 desc: Output compare 1 fast enable
        OC1PE: bool, // bit offset: 3 desc: Output compare 1 preload enable
        OC1M: u3, // bit offset: 4 desc: Output compare 1 mode
        OC1CE: bool, // bit offset: 7 desc: Output compare 1 clear enable
        CC2S: u2, // bit offset: 8 desc: Capture/Compare 2 selection
        OC2FE: bool, // bit offset: 10 desc: Output compare 2 fast enable
        OC2PE: bool, // bit offset: 11 desc: Output compare 2 preload enable
        OC2M: u3, // bit offset: 12 desc: Output compare 2 mode
        OC2CE: bool, // bit offset: 15 desc: Output compare 2 clear enable
        OC1M_3: bool, // bit offset: 16 desc: Output compare 1 mode bit 3
        reserved0: u7 = 0,
        OC2M_3: bool, // bit offset: 24 desc: Output compare 2 mode bit 3
        padding: u7 = 0,
    });
    // byte offset: 24 capture/compare mode register 1 (input mode)
    pub const CCMR1_Input = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        IC1PSC: u2, // bit offset: 2 desc: Input capture 1 prescaler
        IC1F: u4, // bit offset: 4 desc: Input capture 1 filter
        CC2S: u2, // bit offset: 8 desc: Capture/compare 2 selection
        IC2PSC: u2, // bit offset: 10 desc: Input capture 2 prescaler
        IC2F: u4, // bit offset: 12 desc: Input capture 2 filter
        padding: u16 = 0,
    });
    // byte offset: 28 capture/compare mode register 2 (output mode)
    pub const CCMR2_Output = MMIO(Address + 0x0000001c, u32, packed struct {
        CC3S: u2, // bit offset: 0 desc: Capture/Compare 3 selection
        OC3FE: bool, // bit offset: 2 desc: Output compare 3 fast enable
        OC3PE: bool, // bit offset: 3 desc: Output compare 3 preload enable
        OC3M: u3, // bit offset: 4 desc: Output compare 3 mode
        OC3CE: bool, // bit offset: 7 desc: Output compare 3 clear enable
        CC4S: u2, // bit offset: 8 desc: Capture/Compare 4 selection
        OC4FE: bool, // bit offset: 10 desc: Output compare 4 fast enable
        OC4PE: bool, // bit offset: 11 desc: Output compare 4 preload enable
        OC4M: u3, // bit offset: 12 desc: Output compare 4 mode
        O24CE: bool, // bit offset: 15 desc: Output compare 4 clear enable
        OC3M_3: bool, // bit offset: 16 desc: Output compare 3 mode bit3
        reserved0: u7 = 0,
        OC4M_3: bool, // bit offset: 24 desc: Output compare 4 mode bit3
        padding: u7 = 0,
    });
    // byte offset: 28 capture/compare mode register 2 (input mode)
    pub const CCMR2_Input = MMIO(Address + 0x0000001c, u32, packed struct {
        CC3S: u2, // bit offset: 0 desc: Capture/Compare 3 selection
        IC3PSC: u2, // bit offset: 2 desc: Input capture 3 prescaler
        IC3F: u4, // bit offset: 4 desc: Input capture 3 filter
        CC4S: u2, // bit offset: 8 desc: Capture/Compare 4 selection
        IC4PSC: u2, // bit offset: 10 desc: Input capture 4 prescaler
        IC4F: u4, // bit offset: 12 desc: Input capture 4 filter
        padding: u16 = 0,
    });
    // byte offset: 32 capture/compare enable register
    pub const CCER = MMIO(Address + 0x00000020, u32, packed struct {
        CC1E: bool, // bit offset: 0 desc: Capture/Compare 1 output enable
        CC1P: bool, // bit offset: 1 desc: Capture/Compare 1 output Polarity
        reserved0: u1 = 0,
        CC1NP: bool, // bit offset: 3 desc: Capture/Compare 1 output Polarity
        CC2E: bool, // bit offset: 4 desc: Capture/Compare 2 output enable
        CC2P: bool, // bit offset: 5 desc: Capture/Compare 2 output Polarity
        reserved1: u1 = 0,
        CC2NP: bool, // bit offset: 7 desc: Capture/Compare 2 output Polarity
        CC3E: bool, // bit offset: 8 desc: Capture/Compare 3 output enable
        CC3P: bool, // bit offset: 9 desc: Capture/Compare 3 output Polarity
        reserved2: u1 = 0,
        CC3NP: bool, // bit offset: 11 desc: Capture/Compare 3 output Polarity
        CC4E: bool, // bit offset: 12 desc: Capture/Compare 4 output enable
        CC4P: bool, // bit offset: 13 desc: Capture/Compare 3 output Polarity
        reserved3: u1 = 0,
        CC4NP: bool, // bit offset: 15 desc: Capture/Compare 3 output Polarity
        padding: u16 = 0,
    });
    // byte offset: 36 counter
    pub const CNT = MMIO(Address + 0x00000024, u32, packed struct {
        CNTL: u16, // bit offset: 0 desc: Low counter value
        CNTH: u15, // bit offset: 16 desc: High counter value
        CNT_or_UIFCPY: bool, // bit offset: 31 desc: if IUFREMAP=0 than CNT with read write access else UIFCPY with read only access
    });
    // byte offset: 40 prescaler
    pub const PSC = MMIO(Address + 0x00000028, u32, packed struct {
        PSC: u16, // bit offset: 0 desc: Prescaler value
        padding: u16 = 0,
    });
    // byte offset: 44 auto-reload register
    pub const ARR = MMIO(Address + 0x0000002c, u32, packed struct {
        ARRL: u16, // bit offset: 0 desc: Low Auto-reload value
        ARRH: u16, // bit offset: 16 desc: High Auto-reload value
    });
    // byte offset: 52 capture/compare register 1
    pub const CCR1 = MMIO(Address + 0x00000034, u32, packed struct {
        CCR1L: u16, // bit offset: 0 desc: Low Capture/Compare 1 value
        CCR1H: u16, // bit offset: 16 desc: High Capture/Compare 1 value (on TIM2)
    });
    // byte offset: 56 capture/compare register 2
    pub const CCR2 = MMIO(Address + 0x00000038, u32, packed struct {
        CCR2L: u16, // bit offset: 0 desc: Low Capture/Compare 2 value
        CCR2H: u16, // bit offset: 16 desc: High Capture/Compare 2 value (on TIM2)
    });
    // byte offset: 60 capture/compare register 3
    pub const CCR3 = MMIO(Address + 0x0000003c, u32, packed struct {
        CCR3L: u16, // bit offset: 0 desc: Low Capture/Compare value
        CCR3H: u16, // bit offset: 16 desc: High Capture/Compare value (on TIM2)
    });
    // byte offset: 64 capture/compare register 4
    pub const CCR4 = MMIO(Address + 0x00000040, u32, packed struct {
        CCR4L: u16, // bit offset: 0 desc: Low Capture/Compare value
        CCR4H: u16, // bit offset: 16 desc: High Capture/Compare value (on TIM2)
    });
    // byte offset: 72 DMA control register
    pub const DCR = MMIO(Address + 0x00000048, u32, packed struct {
        DBA: u5, // bit offset: 0 desc: DMA base address
        reserved0: u3 = 0,
        DBL: u5, // bit offset: 8 desc: DMA burst length
        padding: u19 = 0,
    });
    // byte offset: 76 DMA address for full transfer
    pub const DMAR = MMIO(Address + 0x0000004c, u32, packed struct {
        DMAB: u16, // bit offset: 0 desc: DMA register for burst accesses
        padding: u16 = 0,
    });
};
pub const TIM4 = extern struct {
    pub const Address: u32 = 0x40000800;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CEN: bool, // bit offset: 0 desc: Counter enable
        UDIS: bool, // bit offset: 1 desc: Update disable
        URS: bool, // bit offset: 2 desc: Update request source
        OPM: bool, // bit offset: 3 desc: One-pulse mode
        DIR: bool, // bit offset: 4 desc: Direction
        CMS: u2, // bit offset: 5 desc: Center-aligned mode selection
        ARPE: bool, // bit offset: 7 desc: Auto-reload preload enable
        CKD: u2, // bit offset: 8 desc: Clock division
        reserved0: u1 = 0,
        UIFREMAP: bool, // bit offset: 11 desc: UIF status bit remapping
        padding: u20 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        reserved0: u3 = 0,
        CCDS: bool, // bit offset: 3 desc: Capture/compare DMA selection
        MMS: u3, // bit offset: 4 desc: Master mode selection
        TI1S: bool, // bit offset: 7 desc: TI1 selection
        padding: u24 = 0,
    });
    // byte offset: 8 slave mode control register
    pub const SMCR = MMIO(Address + 0x00000008, u32, packed struct {
        SMS: u3, // bit offset: 0 desc: Slave mode selection
        OCCS: bool, // bit offset: 3 desc: OCREF clear selection
        TS: u3, // bit offset: 4 desc: Trigger selection
        MSM: bool, // bit offset: 7 desc: Master/Slave mode
        ETF: u4, // bit offset: 8 desc: External trigger filter
        ETPS: u2, // bit offset: 12 desc: External trigger prescaler
        ECE: bool, // bit offset: 14 desc: External clock enable
        ETP: bool, // bit offset: 15 desc: External trigger polarity
        SMS_3: bool, // bit offset: 16 desc: Slave mode selection bit3
        padding: u15 = 0,
    });
    // byte offset: 12 DMA/Interrupt enable register
    pub const DIER = MMIO(Address + 0x0000000c, u32, packed struct {
        UIE: bool, // bit offset: 0 desc: Update interrupt enable
        CC1IE: bool, // bit offset: 1 desc: Capture/Compare 1 interrupt enable
        CC2IE: bool, // bit offset: 2 desc: Capture/Compare 2 interrupt enable
        CC3IE: bool, // bit offset: 3 desc: Capture/Compare 3 interrupt enable
        CC4IE: bool, // bit offset: 4 desc: Capture/Compare 4 interrupt enable
        reserved0: u1 = 0,
        TIE: bool, // bit offset: 6 desc: Trigger interrupt enable
        reserved1: u1 = 0,
        UDE: bool, // bit offset: 8 desc: Update DMA request enable
        CC1DE: bool, // bit offset: 9 desc: Capture/Compare 1 DMA request enable
        CC2DE: bool, // bit offset: 10 desc: Capture/Compare 2 DMA request enable
        CC3DE: bool, // bit offset: 11 desc: Capture/Compare 3 DMA request enable
        CC4DE: bool, // bit offset: 12 desc: Capture/Compare 4 DMA request enable
        reserved2: u1 = 0,
        TDE: bool, // bit offset: 14 desc: Trigger DMA request enable
        padding: u17 = 0,
    });
    // byte offset: 16 status register
    pub const SR = MMIO(Address + 0x00000010, u32, packed struct {
        UIF: bool, // bit offset: 0 desc: Update interrupt flag
        CC1IF: bool, // bit offset: 1 desc: Capture/compare 1 interrupt flag
        CC2IF: bool, // bit offset: 2 desc: Capture/Compare 2 interrupt flag
        CC3IF: bool, // bit offset: 3 desc: Capture/Compare 3 interrupt flag
        CC4IF: bool, // bit offset: 4 desc: Capture/Compare 4 interrupt flag
        reserved0: u1 = 0,
        TIF: bool, // bit offset: 6 desc: Trigger interrupt flag
        reserved1: u2 = 0,
        CC1OF: bool, // bit offset: 9 desc: Capture/Compare 1 overcapture flag
        CC2OF: bool, // bit offset: 10 desc: Capture/compare 2 overcapture flag
        CC3OF: bool, // bit offset: 11 desc: Capture/Compare 3 overcapture flag
        CC4OF: bool, // bit offset: 12 desc: Capture/Compare 4 overcapture flag
        padding: u19 = 0,
    });
    // byte offset: 20 event generation register
    pub const EGR = MMIO(Address + 0x00000014, u32, packed struct {
        UG: bool, // bit offset: 0 desc: Update generation
        CC1G: bool, // bit offset: 1 desc: Capture/compare 1 generation
        CC2G: bool, // bit offset: 2 desc: Capture/compare 2 generation
        CC3G: bool, // bit offset: 3 desc: Capture/compare 3 generation
        CC4G: bool, // bit offset: 4 desc: Capture/compare 4 generation
        reserved0: u1 = 0,
        TG: bool, // bit offset: 6 desc: Trigger generation
        padding: u25 = 0,
    });
    // byte offset: 24 capture/compare mode register 1 (output mode)
    pub const CCMR1_Output = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        OC1FE: bool, // bit offset: 2 desc: Output compare 1 fast enable
        OC1PE: bool, // bit offset: 3 desc: Output compare 1 preload enable
        OC1M: u3, // bit offset: 4 desc: Output compare 1 mode
        OC1CE: bool, // bit offset: 7 desc: Output compare 1 clear enable
        CC2S: u2, // bit offset: 8 desc: Capture/Compare 2 selection
        OC2FE: bool, // bit offset: 10 desc: Output compare 2 fast enable
        OC2PE: bool, // bit offset: 11 desc: Output compare 2 preload enable
        OC2M: u3, // bit offset: 12 desc: Output compare 2 mode
        OC2CE: bool, // bit offset: 15 desc: Output compare 2 clear enable
        OC1M_3: bool, // bit offset: 16 desc: Output compare 1 mode bit 3
        reserved0: u7 = 0,
        OC2M_3: bool, // bit offset: 24 desc: Output compare 2 mode bit 3
        padding: u7 = 0,
    });
    // byte offset: 24 capture/compare mode register 1 (input mode)
    pub const CCMR1_Input = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        IC1PSC: u2, // bit offset: 2 desc: Input capture 1 prescaler
        IC1F: u4, // bit offset: 4 desc: Input capture 1 filter
        CC2S: u2, // bit offset: 8 desc: Capture/compare 2 selection
        IC2PSC: u2, // bit offset: 10 desc: Input capture 2 prescaler
        IC2F: u4, // bit offset: 12 desc: Input capture 2 filter
        padding: u16 = 0,
    });
    // byte offset: 28 capture/compare mode register 2 (output mode)
    pub const CCMR2_Output = MMIO(Address + 0x0000001c, u32, packed struct {
        CC3S: u2, // bit offset: 0 desc: Capture/Compare 3 selection
        OC3FE: bool, // bit offset: 2 desc: Output compare 3 fast enable
        OC3PE: bool, // bit offset: 3 desc: Output compare 3 preload enable
        OC3M: u3, // bit offset: 4 desc: Output compare 3 mode
        OC3CE: bool, // bit offset: 7 desc: Output compare 3 clear enable
        CC4S: u2, // bit offset: 8 desc: Capture/Compare 4 selection
        OC4FE: bool, // bit offset: 10 desc: Output compare 4 fast enable
        OC4PE: bool, // bit offset: 11 desc: Output compare 4 preload enable
        OC4M: u3, // bit offset: 12 desc: Output compare 4 mode
        O24CE: bool, // bit offset: 15 desc: Output compare 4 clear enable
        OC3M_3: bool, // bit offset: 16 desc: Output compare 3 mode bit3
        reserved0: u7 = 0,
        OC4M_3: bool, // bit offset: 24 desc: Output compare 4 mode bit3
        padding: u7 = 0,
    });
    // byte offset: 28 capture/compare mode register 2 (input mode)
    pub const CCMR2_Input = MMIO(Address + 0x0000001c, u32, packed struct {
        CC3S: u2, // bit offset: 0 desc: Capture/Compare 3 selection
        IC3PSC: u2, // bit offset: 2 desc: Input capture 3 prescaler
        IC3F: u4, // bit offset: 4 desc: Input capture 3 filter
        CC4S: u2, // bit offset: 8 desc: Capture/Compare 4 selection
        IC4PSC: u2, // bit offset: 10 desc: Input capture 4 prescaler
        IC4F: u4, // bit offset: 12 desc: Input capture 4 filter
        padding: u16 = 0,
    });
    // byte offset: 32 capture/compare enable register
    pub const CCER = MMIO(Address + 0x00000020, u32, packed struct {
        CC1E: bool, // bit offset: 0 desc: Capture/Compare 1 output enable
        CC1P: bool, // bit offset: 1 desc: Capture/Compare 1 output Polarity
        reserved0: u1 = 0,
        CC1NP: bool, // bit offset: 3 desc: Capture/Compare 1 output Polarity
        CC2E: bool, // bit offset: 4 desc: Capture/Compare 2 output enable
        CC2P: bool, // bit offset: 5 desc: Capture/Compare 2 output Polarity
        reserved1: u1 = 0,
        CC2NP: bool, // bit offset: 7 desc: Capture/Compare 2 output Polarity
        CC3E: bool, // bit offset: 8 desc: Capture/Compare 3 output enable
        CC3P: bool, // bit offset: 9 desc: Capture/Compare 3 output Polarity
        reserved2: u1 = 0,
        CC3NP: bool, // bit offset: 11 desc: Capture/Compare 3 output Polarity
        CC4E: bool, // bit offset: 12 desc: Capture/Compare 4 output enable
        CC4P: bool, // bit offset: 13 desc: Capture/Compare 3 output Polarity
        reserved3: u1 = 0,
        CC4NP: bool, // bit offset: 15 desc: Capture/Compare 3 output Polarity
        padding: u16 = 0,
    });
    // byte offset: 36 counter
    pub const CNT = MMIO(Address + 0x00000024, u32, packed struct {
        CNTL: u16, // bit offset: 0 desc: Low counter value
        CNTH: u15, // bit offset: 16 desc: High counter value
        CNT_or_UIFCPY: bool, // bit offset: 31 desc: if IUFREMAP=0 than CNT with read write access else UIFCPY with read only access
    });
    // byte offset: 40 prescaler
    pub const PSC = MMIO(Address + 0x00000028, u32, packed struct {
        PSC: u16, // bit offset: 0 desc: Prescaler value
        padding: u16 = 0,
    });
    // byte offset: 44 auto-reload register
    pub const ARR = MMIO(Address + 0x0000002c, u32, packed struct {
        ARRL: u16, // bit offset: 0 desc: Low Auto-reload value
        ARRH: u16, // bit offset: 16 desc: High Auto-reload value
    });
    // byte offset: 52 capture/compare register 1
    pub const CCR1 = MMIO(Address + 0x00000034, u32, packed struct {
        CCR1L: u16, // bit offset: 0 desc: Low Capture/Compare 1 value
        CCR1H: u16, // bit offset: 16 desc: High Capture/Compare 1 value (on TIM2)
    });
    // byte offset: 56 capture/compare register 2
    pub const CCR2 = MMIO(Address + 0x00000038, u32, packed struct {
        CCR2L: u16, // bit offset: 0 desc: Low Capture/Compare 2 value
        CCR2H: u16, // bit offset: 16 desc: High Capture/Compare 2 value (on TIM2)
    });
    // byte offset: 60 capture/compare register 3
    pub const CCR3 = MMIO(Address + 0x0000003c, u32, packed struct {
        CCR3L: u16, // bit offset: 0 desc: Low Capture/Compare value
        CCR3H: u16, // bit offset: 16 desc: High Capture/Compare value (on TIM2)
    });
    // byte offset: 64 capture/compare register 4
    pub const CCR4 = MMIO(Address + 0x00000040, u32, packed struct {
        CCR4L: u16, // bit offset: 0 desc: Low Capture/Compare value
        CCR4H: u16, // bit offset: 16 desc: High Capture/Compare value (on TIM2)
    });
    // byte offset: 72 DMA control register
    pub const DCR = MMIO(Address + 0x00000048, u32, packed struct {
        DBA: u5, // bit offset: 0 desc: DMA base address
        reserved0: u3 = 0,
        DBL: u5, // bit offset: 8 desc: DMA burst length
        padding: u19 = 0,
    });
    // byte offset: 76 DMA address for full transfer
    pub const DMAR = MMIO(Address + 0x0000004c, u32, packed struct {
        DMAB: u16, // bit offset: 0 desc: DMA register for burst accesses
        padding: u16 = 0,
    });
};
pub const TIM15 = extern struct {
    pub const Address: u32 = 0x40014000;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CEN: bool, // bit offset: 0 desc: Counter enable
        UDIS: bool, // bit offset: 1 desc: Update disable
        URS: bool, // bit offset: 2 desc: Update request source
        OPM: bool, // bit offset: 3 desc: One-pulse mode
        reserved0: u3 = 0,
        ARPE: bool, // bit offset: 7 desc: Auto-reload preload enable
        CKD: u2, // bit offset: 8 desc: Clock division
        reserved1: u1 = 0,
        UIFREMAP: bool, // bit offset: 11 desc: UIF status bit remapping
        padding: u20 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        CCPC: bool, // bit offset: 0 desc: Capture/compare preloaded control
        reserved0: u1 = 0,
        CCUS: bool, // bit offset: 2 desc: Capture/compare control update selection
        CCDS: bool, // bit offset: 3 desc: Capture/compare DMA selection
        MMS: u3, // bit offset: 4 desc: Master mode selection
        TI1S: bool, // bit offset: 7 desc: TI1 selection
        OIS1: bool, // bit offset: 8 desc: Output Idle state 1
        OIS1N: bool, // bit offset: 9 desc: Output Idle state 1
        OIS2: bool, // bit offset: 10 desc: Output Idle state 2
        padding: u21 = 0,
    });
    // byte offset: 8 slave mode control register
    pub const SMCR = MMIO(Address + 0x00000008, u32, packed struct {
        SMS: u3, // bit offset: 0 desc: Slave mode selection
        reserved0: u1 = 0,
        TS: u3, // bit offset: 4 desc: Trigger selection
        MSM: bool, // bit offset: 7 desc: Master/Slave mode
        reserved1: u8 = 0,
        SMS_3: bool, // bit offset: 16 desc: Slave mode selection bit 3
        padding: u15 = 0,
    });
    // byte offset: 12 DMA/Interrupt enable register
    pub const DIER = MMIO(Address + 0x0000000c, u32, packed struct {
        UIE: bool, // bit offset: 0 desc: Update interrupt enable
        CC1IE: bool, // bit offset: 1 desc: Capture/Compare 1 interrupt enable
        CC2IE: bool, // bit offset: 2 desc: Capture/Compare 2 interrupt enable
        reserved0: u2 = 0,
        COMIE: bool, // bit offset: 5 desc: COM interrupt enable
        TIE: bool, // bit offset: 6 desc: Trigger interrupt enable
        BIE: bool, // bit offset: 7 desc: Break interrupt enable
        UDE: bool, // bit offset: 8 desc: Update DMA request enable
        CC1DE: bool, // bit offset: 9 desc: Capture/Compare 1 DMA request enable
        CC2DE: bool, // bit offset: 10 desc: Capture/Compare 2 DMA request enable
        reserved1: u2 = 0,
        COMDE: bool, // bit offset: 13 desc: COM DMA request enable
        TDE: bool, // bit offset: 14 desc: Trigger DMA request enable
        padding: u17 = 0,
    });
    // byte offset: 16 status register
    pub const SR = MMIO(Address + 0x00000010, u32, packed struct {
        UIF: bool, // bit offset: 0 desc: Update interrupt flag
        CC1IF: bool, // bit offset: 1 desc: Capture/compare 1 interrupt flag
        CC2IF: bool, // bit offset: 2 desc: Capture/Compare 2 interrupt flag
        reserved0: u2 = 0,
        COMIF: bool, // bit offset: 5 desc: COM interrupt flag
        TIF: bool, // bit offset: 6 desc: Trigger interrupt flag
        BIF: bool, // bit offset: 7 desc: Break interrupt flag
        reserved1: u1 = 0,
        CC1OF: bool, // bit offset: 9 desc: Capture/Compare 1 overcapture flag
        CC2OF: bool, // bit offset: 10 desc: Capture/compare 2 overcapture flag
        padding: u21 = 0,
    });
    // byte offset: 20 event generation register
    pub const EGR = MMIO(Address + 0x00000014, u32, packed struct {
        UG: bool, // bit offset: 0 desc: Update generation
        CC1G: bool, // bit offset: 1 desc: Capture/compare 1 generation
        CC2G: bool, // bit offset: 2 desc: Capture/compare 2 generation
        reserved0: u2 = 0,
        COMG: bool, // bit offset: 5 desc: Capture/Compare control update generation
        TG: bool, // bit offset: 6 desc: Trigger generation
        BG: bool, // bit offset: 7 desc: Break generation
        padding: u24 = 0,
    });
    // byte offset: 24 capture/compare mode register (output mode)
    pub const CCMR1_Output = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        OC1FE: bool, // bit offset: 2 desc: Output Compare 1 fast enable
        OC1PE: bool, // bit offset: 3 desc: Output Compare 1 preload enable
        OC1M: u3, // bit offset: 4 desc: Output Compare 1 mode
        reserved0: u1 = 0,
        CC2S: u2, // bit offset: 8 desc: Capture/Compare 2 selection
        OC2FE: bool, // bit offset: 10 desc: Output Compare 2 fast enable
        OC2PE: bool, // bit offset: 11 desc: Output Compare 2 preload enable
        OC2M: u3, // bit offset: 12 desc: Output Compare 2 mode
        reserved1: u1 = 0,
        OC1M_3: bool, // bit offset: 16 desc: Output Compare 1 mode bit 3
        reserved2: u7 = 0,
        OC2M_3: bool, // bit offset: 24 desc: Output Compare 2 mode bit 3
        padding: u7 = 0,
    });
    // byte offset: 24 capture/compare mode register 1 (input mode)
    pub const CCMR1_Input = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        IC1PSC: u2, // bit offset: 2 desc: Input capture 1 prescaler
        IC1F: u4, // bit offset: 4 desc: Input capture 1 filter
        CC2S: u2, // bit offset: 8 desc: Capture/Compare 2 selection
        IC2PSC: u2, // bit offset: 10 desc: Input capture 2 prescaler
        IC2F: u4, // bit offset: 12 desc: Input capture 2 filter
        padding: u16 = 0,
    });
    // byte offset: 32 capture/compare enable register
    pub const CCER = MMIO(Address + 0x00000020, u32, packed struct {
        CC1E: bool, // bit offset: 0 desc: Capture/Compare 1 output enable
        CC1P: bool, // bit offset: 1 desc: Capture/Compare 1 output Polarity
        CC1NE: bool, // bit offset: 2 desc: Capture/Compare 1 complementary output enable
        CC1NP: bool, // bit offset: 3 desc: Capture/Compare 1 output Polarity
        CC2E: bool, // bit offset: 4 desc: Capture/Compare 2 output enable
        CC2P: bool, // bit offset: 5 desc: Capture/Compare 2 output Polarity
        reserved0: u1 = 0,
        CC2NP: bool, // bit offset: 7 desc: Capture/Compare 2 output Polarity
        padding: u24 = 0,
    });
    // byte offset: 36 counter
    pub const CNT = MMIO(Address + 0x00000024, u32, packed struct {
        CNT: u16, // bit offset: 0 desc: counter value
        reserved0: u15 = 0,
        UIFCPY: bool, // bit offset: 31 desc: UIF copy
    });
    // byte offset: 40 prescaler
    pub const PSC = MMIO(Address + 0x00000028, u32, packed struct {
        PSC: u16, // bit offset: 0 desc: Prescaler value
        padding: u16 = 0,
    });
    // byte offset: 44 auto-reload register
    pub const ARR = MMIO(Address + 0x0000002c, u32, packed struct {
        ARR: u16, // bit offset: 0 desc: Auto-reload value
        padding: u16 = 0,
    });
    // byte offset: 48 repetition counter register
    pub const RCR = MMIO(Address + 0x00000030, u32, packed struct {
        REP: u8, // bit offset: 0 desc: Repetition counter value
        padding: u24 = 0,
    });
    // byte offset: 52 capture/compare register 1
    pub const CCR1 = MMIO(Address + 0x00000034, u32, packed struct {
        CCR1: u16, // bit offset: 0 desc: Capture/Compare 1 value
        padding: u16 = 0,
    });
    // byte offset: 56 capture/compare register 2
    pub const CCR2 = MMIO(Address + 0x00000038, u32, packed struct {
        CCR2: u16, // bit offset: 0 desc: Capture/Compare 2 value
        padding: u16 = 0,
    });
    // byte offset: 68 break and dead-time register
    pub const BDTR = MMIO(Address + 0x00000044, u32, packed struct {
        DTG: u8, // bit offset: 0 desc: Dead-time generator setup
        LOCK: u2, // bit offset: 8 desc: Lock configuration
        OSSI: bool, // bit offset: 10 desc: Off-state selection for Idle mode
        OSSR: bool, // bit offset: 11 desc: Off-state selection for Run mode
        BKE: bool, // bit offset: 12 desc: Break enable
        BKP: bool, // bit offset: 13 desc: Break polarity
        AOE: bool, // bit offset: 14 desc: Automatic output enable
        MOE: bool, // bit offset: 15 desc: Main output enable
        BKF: u4, // bit offset: 16 desc: Break filter
        padding: u12 = 0,
    });
    // byte offset: 72 DMA control register
    pub const DCR = MMIO(Address + 0x00000048, u32, packed struct {
        DBA: u5, // bit offset: 0 desc: DMA base address
        reserved0: u3 = 0,
        DBL: u5, // bit offset: 8 desc: DMA burst length
        padding: u19 = 0,
    });
    // byte offset: 76 DMA address for full transfer
    pub const DMAR = MMIO(Address + 0x0000004c, u32, packed struct {
        DMAB: u16, // bit offset: 0 desc: DMA register for burst accesses
        padding: u16 = 0,
    });
};
pub const TIM16 = extern struct {
    pub const Address: u32 = 0x40014400;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CEN: bool, // bit offset: 0 desc: Counter enable
        UDIS: bool, // bit offset: 1 desc: Update disable
        URS: bool, // bit offset: 2 desc: Update request source
        OPM: bool, // bit offset: 3 desc: One-pulse mode
        reserved0: u3 = 0,
        ARPE: bool, // bit offset: 7 desc: Auto-reload preload enable
        CKD: u2, // bit offset: 8 desc: Clock division
        reserved1: u1 = 0,
        UIFREMAP: bool, // bit offset: 11 desc: UIF status bit remapping
        padding: u20 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        CCPC: bool, // bit offset: 0 desc: Capture/compare preloaded control
        reserved0: u1 = 0,
        CCUS: bool, // bit offset: 2 desc: Capture/compare control update selection
        CCDS: bool, // bit offset: 3 desc: Capture/compare DMA selection
        reserved1: u4 = 0,
        OIS1: bool, // bit offset: 8 desc: Output Idle state 1
        OIS1N: bool, // bit offset: 9 desc: Output Idle state 1
        padding: u22 = 0,
    });
    // byte offset: 12 DMA/Interrupt enable register
    pub const DIER = MMIO(Address + 0x0000000c, u32, packed struct {
        UIE: bool, // bit offset: 0 desc: Update interrupt enable
        CC1IE: bool, // bit offset: 1 desc: Capture/Compare 1 interrupt enable
        reserved0: u3 = 0,
        COMIE: bool, // bit offset: 5 desc: COM interrupt enable
        TIE: bool, // bit offset: 6 desc: Trigger interrupt enable
        BIE: bool, // bit offset: 7 desc: Break interrupt enable
        UDE: bool, // bit offset: 8 desc: Update DMA request enable
        CC1DE: bool, // bit offset: 9 desc: Capture/Compare 1 DMA request enable
        reserved1: u3 = 0,
        COMDE: bool, // bit offset: 13 desc: COM DMA request enable
        TDE: bool, // bit offset: 14 desc: Trigger DMA request enable
        padding: u17 = 0,
    });
    // byte offset: 16 status register
    pub const SR = MMIO(Address + 0x00000010, u32, packed struct {
        UIF: bool, // bit offset: 0 desc: Update interrupt flag
        CC1IF: bool, // bit offset: 1 desc: Capture/compare 1 interrupt flag
        reserved0: u3 = 0,
        COMIF: bool, // bit offset: 5 desc: COM interrupt flag
        TIF: bool, // bit offset: 6 desc: Trigger interrupt flag
        BIF: bool, // bit offset: 7 desc: Break interrupt flag
        reserved1: u1 = 0,
        CC1OF: bool, // bit offset: 9 desc: Capture/Compare 1 overcapture flag
        padding: u22 = 0,
    });
    // byte offset: 20 event generation register
    pub const EGR = MMIO(Address + 0x00000014, u32, packed struct {
        UG: bool, // bit offset: 0 desc: Update generation
        CC1G: bool, // bit offset: 1 desc: Capture/compare 1 generation
        reserved0: u3 = 0,
        COMG: bool, // bit offset: 5 desc: Capture/Compare control update generation
        TG: bool, // bit offset: 6 desc: Trigger generation
        BG: bool, // bit offset: 7 desc: Break generation
        padding: u24 = 0,
    });
    // byte offset: 24 capture/compare mode register (output mode)
    pub const CCMR1_Output = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        OC1FE: bool, // bit offset: 2 desc: Output Compare 1 fast enable
        OC1PE: bool, // bit offset: 3 desc: Output Compare 1 preload enable
        OC1M: u3, // bit offset: 4 desc: Output Compare 1 mode
        reserved0: u9 = 0,
        OC1M_3: bool, // bit offset: 16 desc: Output Compare 1 mode
        padding: u15 = 0,
    });
    // byte offset: 24 capture/compare mode register 1 (input mode)
    pub const CCMR1_Input = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        IC1PSC: u2, // bit offset: 2 desc: Input capture 1 prescaler
        IC1F: u4, // bit offset: 4 desc: Input capture 1 filter
        padding: u24 = 0,
    });
    // byte offset: 32 capture/compare enable register
    pub const CCER = MMIO(Address + 0x00000020, u32, packed struct {
        CC1E: bool, // bit offset: 0 desc: Capture/Compare 1 output enable
        CC1P: bool, // bit offset: 1 desc: Capture/Compare 1 output Polarity
        CC1NE: bool, // bit offset: 2 desc: Capture/Compare 1 complementary output enable
        CC1NP: bool, // bit offset: 3 desc: Capture/Compare 1 output Polarity
        padding: u28 = 0,
    });
    // byte offset: 36 counter
    pub const CNT = MMIO(Address + 0x00000024, u32, packed struct {
        CNT: u16, // bit offset: 0 desc: counter value
        reserved0: u15 = 0,
        UIFCPY: bool, // bit offset: 31 desc: UIF Copy
    });
    // byte offset: 40 prescaler
    pub const PSC = MMIO(Address + 0x00000028, u32, packed struct {
        PSC: u16, // bit offset: 0 desc: Prescaler value
        padding: u16 = 0,
    });
    // byte offset: 44 auto-reload register
    pub const ARR = MMIO(Address + 0x0000002c, u32, packed struct {
        ARR: u16, // bit offset: 0 desc: Auto-reload value
        padding: u16 = 0,
    });
    // byte offset: 48 repetition counter register
    pub const RCR = MMIO(Address + 0x00000030, u32, packed struct {
        REP: u8, // bit offset: 0 desc: Repetition counter value
        padding: u24 = 0,
    });
    // byte offset: 52 capture/compare register 1
    pub const CCR1 = MMIO(Address + 0x00000034, u32, packed struct {
        CCR1: u16, // bit offset: 0 desc: Capture/Compare 1 value
        padding: u16 = 0,
    });
    // byte offset: 68 break and dead-time register
    pub const BDTR = MMIO(Address + 0x00000044, u32, packed struct {
        DTG: u8, // bit offset: 0 desc: Dead-time generator setup
        LOCK: u2, // bit offset: 8 desc: Lock configuration
        OSSI: bool, // bit offset: 10 desc: Off-state selection for Idle mode
        OSSR: bool, // bit offset: 11 desc: Off-state selection for Run mode
        BKE: bool, // bit offset: 12 desc: Break enable
        BKP: bool, // bit offset: 13 desc: Break polarity
        AOE: bool, // bit offset: 14 desc: Automatic output enable
        MOE: bool, // bit offset: 15 desc: Main output enable
        BKF: u4, // bit offset: 16 desc: Break filter
        padding: u12 = 0,
    });
    // byte offset: 72 DMA control register
    pub const DCR = MMIO(Address + 0x00000048, u32, packed struct {
        DBA: u5, // bit offset: 0 desc: DMA base address
        reserved0: u3 = 0,
        DBL: u5, // bit offset: 8 desc: DMA burst length
        padding: u19 = 0,
    });
    // byte offset: 76 DMA address for full transfer
    pub const DMAR = MMIO(Address + 0x0000004c, u32, packed struct {
        DMAB: u16, // bit offset: 0 desc: DMA register for burst accesses
        padding: u16 = 0,
    });
    // byte offset: 80 option register
    pub const OR = MMIO(Address + 0x00000050, u32, packed struct {
        padding: u32 = 0,
    });
};
pub const TIM17 = extern struct {
    pub const Address: u32 = 0x40014800;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CEN: bool, // bit offset: 0 desc: Counter enable
        UDIS: bool, // bit offset: 1 desc: Update disable
        URS: bool, // bit offset: 2 desc: Update request source
        OPM: bool, // bit offset: 3 desc: One-pulse mode
        reserved0: u3 = 0,
        ARPE: bool, // bit offset: 7 desc: Auto-reload preload enable
        CKD: u2, // bit offset: 8 desc: Clock division
        reserved1: u1 = 0,
        UIFREMAP: bool, // bit offset: 11 desc: UIF status bit remapping
        padding: u20 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        CCPC: bool, // bit offset: 0 desc: Capture/compare preloaded control
        reserved0: u1 = 0,
        CCUS: bool, // bit offset: 2 desc: Capture/compare control update selection
        CCDS: bool, // bit offset: 3 desc: Capture/compare DMA selection
        reserved1: u4 = 0,
        OIS1: bool, // bit offset: 8 desc: Output Idle state 1
        OIS1N: bool, // bit offset: 9 desc: Output Idle state 1
        padding: u22 = 0,
    });
    // byte offset: 12 DMA/Interrupt enable register
    pub const DIER = MMIO(Address + 0x0000000c, u32, packed struct {
        UIE: bool, // bit offset: 0 desc: Update interrupt enable
        CC1IE: bool, // bit offset: 1 desc: Capture/Compare 1 interrupt enable
        reserved0: u3 = 0,
        COMIE: bool, // bit offset: 5 desc: COM interrupt enable
        TIE: bool, // bit offset: 6 desc: Trigger interrupt enable
        BIE: bool, // bit offset: 7 desc: Break interrupt enable
        UDE: bool, // bit offset: 8 desc: Update DMA request enable
        CC1DE: bool, // bit offset: 9 desc: Capture/Compare 1 DMA request enable
        reserved1: u3 = 0,
        COMDE: bool, // bit offset: 13 desc: COM DMA request enable
        TDE: bool, // bit offset: 14 desc: Trigger DMA request enable
        padding: u17 = 0,
    });
    // byte offset: 16 status register
    pub const SR = MMIO(Address + 0x00000010, u32, packed struct {
        UIF: bool, // bit offset: 0 desc: Update interrupt flag
        CC1IF: bool, // bit offset: 1 desc: Capture/compare 1 interrupt flag
        reserved0: u3 = 0,
        COMIF: bool, // bit offset: 5 desc: COM interrupt flag
        TIF: bool, // bit offset: 6 desc: Trigger interrupt flag
        BIF: bool, // bit offset: 7 desc: Break interrupt flag
        reserved1: u1 = 0,
        CC1OF: bool, // bit offset: 9 desc: Capture/Compare 1 overcapture flag
        padding: u22 = 0,
    });
    // byte offset: 20 event generation register
    pub const EGR = MMIO(Address + 0x00000014, u32, packed struct {
        UG: bool, // bit offset: 0 desc: Update generation
        CC1G: bool, // bit offset: 1 desc: Capture/compare 1 generation
        reserved0: u3 = 0,
        COMG: bool, // bit offset: 5 desc: Capture/Compare control update generation
        TG: bool, // bit offset: 6 desc: Trigger generation
        BG: bool, // bit offset: 7 desc: Break generation
        padding: u24 = 0,
    });
    // byte offset: 24 capture/compare mode register (output mode)
    pub const CCMR1_Output = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        OC1FE: bool, // bit offset: 2 desc: Output Compare 1 fast enable
        OC1PE: bool, // bit offset: 3 desc: Output Compare 1 preload enable
        OC1M: u3, // bit offset: 4 desc: Output Compare 1 mode
        reserved0: u9 = 0,
        OC1M_3: bool, // bit offset: 16 desc: Output Compare 1 mode
        padding: u15 = 0,
    });
    // byte offset: 24 capture/compare mode register 1 (input mode)
    pub const CCMR1_Input = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        IC1PSC: u2, // bit offset: 2 desc: Input capture 1 prescaler
        IC1F: u4, // bit offset: 4 desc: Input capture 1 filter
        padding: u24 = 0,
    });
    // byte offset: 32 capture/compare enable register
    pub const CCER = MMIO(Address + 0x00000020, u32, packed struct {
        CC1E: bool, // bit offset: 0 desc: Capture/Compare 1 output enable
        CC1P: bool, // bit offset: 1 desc: Capture/Compare 1 output Polarity
        CC1NE: bool, // bit offset: 2 desc: Capture/Compare 1 complementary output enable
        CC1NP: bool, // bit offset: 3 desc: Capture/Compare 1 output Polarity
        padding: u28 = 0,
    });
    // byte offset: 36 counter
    pub const CNT = MMIO(Address + 0x00000024, u32, packed struct {
        CNT: u16, // bit offset: 0 desc: counter value
        reserved0: u15 = 0,
        UIFCPY: bool, // bit offset: 31 desc: UIF Copy
    });
    // byte offset: 40 prescaler
    pub const PSC = MMIO(Address + 0x00000028, u32, packed struct {
        PSC: u16, // bit offset: 0 desc: Prescaler value
        padding: u16 = 0,
    });
    // byte offset: 44 auto-reload register
    pub const ARR = MMIO(Address + 0x0000002c, u32, packed struct {
        ARR: u16, // bit offset: 0 desc: Auto-reload value
        padding: u16 = 0,
    });
    // byte offset: 48 repetition counter register
    pub const RCR = MMIO(Address + 0x00000030, u32, packed struct {
        REP: u8, // bit offset: 0 desc: Repetition counter value
        padding: u24 = 0,
    });
    // byte offset: 52 capture/compare register 1
    pub const CCR1 = MMIO(Address + 0x00000034, u32, packed struct {
        CCR1: u16, // bit offset: 0 desc: Capture/Compare 1 value
        padding: u16 = 0,
    });
    // byte offset: 68 break and dead-time register
    pub const BDTR = MMIO(Address + 0x00000044, u32, packed struct {
        DTG: u8, // bit offset: 0 desc: Dead-time generator setup
        LOCK: u2, // bit offset: 8 desc: Lock configuration
        OSSI: bool, // bit offset: 10 desc: Off-state selection for Idle mode
        OSSR: bool, // bit offset: 11 desc: Off-state selection for Run mode
        BKE: bool, // bit offset: 12 desc: Break enable
        BKP: bool, // bit offset: 13 desc: Break polarity
        AOE: bool, // bit offset: 14 desc: Automatic output enable
        MOE: bool, // bit offset: 15 desc: Main output enable
        BKF: u4, // bit offset: 16 desc: Break filter
        padding: u12 = 0,
    });
    // byte offset: 72 DMA control register
    pub const DCR = MMIO(Address + 0x00000048, u32, packed struct {
        DBA: u5, // bit offset: 0 desc: DMA base address
        reserved0: u3 = 0,
        DBL: u5, // bit offset: 8 desc: DMA burst length
        padding: u19 = 0,
    });
    // byte offset: 76 DMA address for full transfer
    pub const DMAR = MMIO(Address + 0x0000004c, u32, packed struct {
        DMAB: u16, // bit offset: 0 desc: DMA register for burst accesses
        padding: u16 = 0,
    });
};
pub const USART1 = extern struct {
    pub const Address: u32 = 0x40013800;
    // byte offset: 0 Control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        UE: bool, // bit offset: 0 desc: USART enable
        UESM: bool, // bit offset: 1 desc: USART enable in Stop mode
        RE: bool, // bit offset: 2 desc: Receiver enable
        TE: bool, // bit offset: 3 desc: Transmitter enable
        IDLEIE: bool, // bit offset: 4 desc: IDLE interrupt enable
        RXNEIE: bool, // bit offset: 5 desc: RXNE interrupt enable
        TCIE: bool, // bit offset: 6 desc: Transmission complete interrupt enable
        TXEIE: bool, // bit offset: 7 desc: interrupt enable
        PEIE: bool, // bit offset: 8 desc: PE interrupt enable
        PS: bool, // bit offset: 9 desc: Parity selection
        PCE: bool, // bit offset: 10 desc: Parity control enable
        WAKE: bool, // bit offset: 11 desc: Receiver wakeup method
        M: bool, // bit offset: 12 desc: Word length
        MME: bool, // bit offset: 13 desc: Mute mode enable
        CMIE: bool, // bit offset: 14 desc: Character match interrupt enable
        OVER8: bool, // bit offset: 15 desc: Oversampling mode
        DEDT: u5, // bit offset: 16 desc: Driver Enable deassertion time
        DEAT: u5, // bit offset: 21 desc: Driver Enable assertion time
        RTOIE: bool, // bit offset: 26 desc: Receiver timeout interrupt enable
        EOBIE: bool, // bit offset: 27 desc: End of Block interrupt enable
        padding: u4 = 0,
    });
    // byte offset: 4 Control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        reserved0: u4 = 0,
        ADDM7: bool, // bit offset: 4 desc: 7-bit Address Detection/4-bit Address Detection
        LBDL: bool, // bit offset: 5 desc: LIN break detection length
        LBDIE: bool, // bit offset: 6 desc: LIN break detection interrupt enable
        reserved1: u1 = 0,
        LBCL: bool, // bit offset: 8 desc: Last bit clock pulse
        CPHA: bool, // bit offset: 9 desc: Clock phase
        CPOL: bool, // bit offset: 10 desc: Clock polarity
        CLKEN: bool, // bit offset: 11 desc: Clock enable
        STOP: u2, // bit offset: 12 desc: STOP bits
        LINEN: bool, // bit offset: 14 desc: LIN mode enable
        SWAP: bool, // bit offset: 15 desc: Swap TX/RX pins
        RXINV: bool, // bit offset: 16 desc: RX pin active level inversion
        TXINV: bool, // bit offset: 17 desc: TX pin active level inversion
        DATAINV: bool, // bit offset: 18 desc: Binary data inversion
        MSBFIRST: bool, // bit offset: 19 desc: Most significant bit first
        ABREN: bool, // bit offset: 20 desc: Auto baud rate enable
        ABRMOD: u2, // bit offset: 21 desc: Auto baud rate mode
        RTOEN: bool, // bit offset: 23 desc: Receiver timeout enable
        ADD0: u4, // bit offset: 24 desc: Address of the USART node
        ADD4: u4, // bit offset: 28 desc: Address of the USART node
    });
    // byte offset: 8 Control register 3
    pub const CR3 = MMIO(Address + 0x00000008, u32, packed struct {
        EIE: bool, // bit offset: 0 desc: Error interrupt enable
        IREN: bool, // bit offset: 1 desc: IrDA mode enable
        IRLP: bool, // bit offset: 2 desc: IrDA low-power
        HDSEL: bool, // bit offset: 3 desc: Half-duplex selection
        NACK: bool, // bit offset: 4 desc: Smartcard NACK enable
        SCEN: bool, // bit offset: 5 desc: Smartcard mode enable
        DMAR: bool, // bit offset: 6 desc: DMA enable receiver
        DMAT: bool, // bit offset: 7 desc: DMA enable transmitter
        RTSE: bool, // bit offset: 8 desc: RTS enable
        CTSE: bool, // bit offset: 9 desc: CTS enable
        CTSIE: bool, // bit offset: 10 desc: CTS interrupt enable
        ONEBIT: bool, // bit offset: 11 desc: One sample bit method enable
        OVRDIS: bool, // bit offset: 12 desc: Overrun Disable
        DDRE: bool, // bit offset: 13 desc: DMA Disable on Reception Error
        DEM: bool, // bit offset: 14 desc: Driver enable mode
        DEP: bool, // bit offset: 15 desc: Driver enable polarity selection
        reserved0: u1 = 0,
        SCARCNT: u3, // bit offset: 17 desc: Smartcard auto-retry count
        WUS: u2, // bit offset: 20 desc: Wakeup from Stop mode interrupt flag selection
        WUFIE: bool, // bit offset: 22 desc: Wakeup from Stop mode interrupt enable
        padding: u9 = 0,
    });
    // byte offset: 12 Baud rate register
    pub const BRR = MMIO(Address + 0x0000000c, u32, packed struct {
        DIV_Fraction: u4, // bit offset: 0 desc: fraction of USARTDIV
        DIV_Mantissa: u12, // bit offset: 4 desc: mantissa of USARTDIV
        padding: u16 = 0,
    });
    // byte offset: 16 Guard time and prescaler register
    pub const GTPR = MMIO(Address + 0x00000010, u32, packed struct {
        PSC: u8, // bit offset: 0 desc: Prescaler value
        GT: u8, // bit offset: 8 desc: Guard time value
        padding: u16 = 0,
    });
    // byte offset: 20 Receiver timeout register
    pub const RTOR = MMIO(Address + 0x00000014, u32, packed struct {
        RTO: u24, // bit offset: 0 desc: Receiver timeout value
        BLEN: u8, // bit offset: 24 desc: Block Length
    });
    // byte offset: 24 Request register
    pub const RQR = MMIO(Address + 0x00000018, u32, packed struct {
        ABRRQ: bool, // bit offset: 0 desc: Auto baud rate request
        SBKRQ: bool, // bit offset: 1 desc: Send break request
        MMRQ: bool, // bit offset: 2 desc: Mute mode request
        RXFRQ: bool, // bit offset: 3 desc: Receive data flush request
        TXFRQ: bool, // bit offset: 4 desc: Transmit data flush request
        padding: u27 = 0,
    });
    // byte offset: 28 Interrupt & status register
    pub const ISR = MMIO(Address + 0x0000001c, u32, packed struct {
        PE: bool, // bit offset: 0 desc: Parity error
        FE: bool, // bit offset: 1 desc: Framing error
        NF: bool, // bit offset: 2 desc: Noise detected flag
        ORE: bool, // bit offset: 3 desc: Overrun error
        IDLE: bool, // bit offset: 4 desc: Idle line detected
        RXNE: bool, // bit offset: 5 desc: Read data register not empty
        TC: bool, // bit offset: 6 desc: Transmission complete
        TXE: bool, // bit offset: 7 desc: Transmit data register empty
        LBDF: bool, // bit offset: 8 desc: LIN break detection flag
        CTSIF: bool, // bit offset: 9 desc: CTS interrupt flag
        CTS: bool, // bit offset: 10 desc: CTS flag
        RTOF: bool, // bit offset: 11 desc: Receiver timeout
        EOBF: bool, // bit offset: 12 desc: End of block flag
        reserved0: u1 = 0,
        ABRE: bool, // bit offset: 14 desc: Auto baud rate error
        ABRF: bool, // bit offset: 15 desc: Auto baud rate flag
        BUSY: bool, // bit offset: 16 desc: Busy flag
        CMF: bool, // bit offset: 17 desc: character match flag
        SBKF: bool, // bit offset: 18 desc: Send break flag
        RWU: bool, // bit offset: 19 desc: Receiver wakeup from Mute mode
        WUF: bool, // bit offset: 20 desc: Wakeup from Stop mode flag
        TEACK: bool, // bit offset: 21 desc: Transmit enable acknowledge flag
        REACK: bool, // bit offset: 22 desc: Receive enable acknowledge flag
        padding: u9 = 0,
    });
    // byte offset: 32 Interrupt flag clear register
    pub const ICR = MMIO(Address + 0x00000020, u32, packed struct {
        PECF: bool, // bit offset: 0 desc: Parity error clear flag
        FECF: bool, // bit offset: 1 desc: Framing error clear flag
        NCF: bool, // bit offset: 2 desc: Noise detected clear flag
        ORECF: bool, // bit offset: 3 desc: Overrun error clear flag
        IDLECF: bool, // bit offset: 4 desc: Idle line detected clear flag
        reserved0: u1 = 0,
        TCCF: bool, // bit offset: 6 desc: Transmission complete clear flag
        reserved1: u1 = 0,
        LBDCF: bool, // bit offset: 8 desc: LIN break detection clear flag
        CTSCF: bool, // bit offset: 9 desc: CTS clear flag
        reserved2: u1 = 0,
        RTOCF: bool, // bit offset: 11 desc: Receiver timeout clear flag
        EOBCF: bool, // bit offset: 12 desc: End of timeout clear flag
        reserved3: u4 = 0,
        CMCF: bool, // bit offset: 17 desc: Character match clear flag
        reserved4: u2 = 0,
        WUCF: bool, // bit offset: 20 desc: Wakeup from Stop mode clear flag
        padding: u11 = 0,
    });
    // byte offset: 36 Receive data register
    pub const RDR = MMIO(Address + 0x00000024, u32, packed struct {
        RDR: u9, // bit offset: 0 desc: Receive data value
        padding: u23 = 0,
    });
    // byte offset: 40 Transmit data register
    pub const TDR = MMIO(Address + 0x00000028, u32, packed struct {
        TDR: u9, // bit offset: 0 desc: Transmit data value
        padding: u23 = 0,
    });
};
pub const USART2 = extern struct {
    pub const Address: u32 = 0x40004400;
    // byte offset: 0 Control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        UE: bool, // bit offset: 0 desc: USART enable
        UESM: bool, // bit offset: 1 desc: USART enable in Stop mode
        RE: bool, // bit offset: 2 desc: Receiver enable
        TE: bool, // bit offset: 3 desc: Transmitter enable
        IDLEIE: bool, // bit offset: 4 desc: IDLE interrupt enable
        RXNEIE: bool, // bit offset: 5 desc: RXNE interrupt enable
        TCIE: bool, // bit offset: 6 desc: Transmission complete interrupt enable
        TXEIE: bool, // bit offset: 7 desc: interrupt enable
        PEIE: bool, // bit offset: 8 desc: PE interrupt enable
        PS: bool, // bit offset: 9 desc: Parity selection
        PCE: bool, // bit offset: 10 desc: Parity control enable
        WAKE: bool, // bit offset: 11 desc: Receiver wakeup method
        M: bool, // bit offset: 12 desc: Word length
        MME: bool, // bit offset: 13 desc: Mute mode enable
        CMIE: bool, // bit offset: 14 desc: Character match interrupt enable
        OVER8: bool, // bit offset: 15 desc: Oversampling mode
        DEDT: u5, // bit offset: 16 desc: Driver Enable deassertion time
        DEAT: u5, // bit offset: 21 desc: Driver Enable assertion time
        RTOIE: bool, // bit offset: 26 desc: Receiver timeout interrupt enable
        EOBIE: bool, // bit offset: 27 desc: End of Block interrupt enable
        padding: u4 = 0,
    });
    // byte offset: 4 Control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        reserved0: u4 = 0,
        ADDM7: bool, // bit offset: 4 desc: 7-bit Address Detection/4-bit Address Detection
        LBDL: bool, // bit offset: 5 desc: LIN break detection length
        LBDIE: bool, // bit offset: 6 desc: LIN break detection interrupt enable
        reserved1: u1 = 0,
        LBCL: bool, // bit offset: 8 desc: Last bit clock pulse
        CPHA: bool, // bit offset: 9 desc: Clock phase
        CPOL: bool, // bit offset: 10 desc: Clock polarity
        CLKEN: bool, // bit offset: 11 desc: Clock enable
        STOP: u2, // bit offset: 12 desc: STOP bits
        LINEN: bool, // bit offset: 14 desc: LIN mode enable
        SWAP: bool, // bit offset: 15 desc: Swap TX/RX pins
        RXINV: bool, // bit offset: 16 desc: RX pin active level inversion
        TXINV: bool, // bit offset: 17 desc: TX pin active level inversion
        DATAINV: bool, // bit offset: 18 desc: Binary data inversion
        MSBFIRST: bool, // bit offset: 19 desc: Most significant bit first
        ABREN: bool, // bit offset: 20 desc: Auto baud rate enable
        ABRMOD: u2, // bit offset: 21 desc: Auto baud rate mode
        RTOEN: bool, // bit offset: 23 desc: Receiver timeout enable
        ADD0: u4, // bit offset: 24 desc: Address of the USART node
        ADD4: u4, // bit offset: 28 desc: Address of the USART node
    });
    // byte offset: 8 Control register 3
    pub const CR3 = MMIO(Address + 0x00000008, u32, packed struct {
        EIE: bool, // bit offset: 0 desc: Error interrupt enable
        IREN: bool, // bit offset: 1 desc: IrDA mode enable
        IRLP: bool, // bit offset: 2 desc: IrDA low-power
        HDSEL: bool, // bit offset: 3 desc: Half-duplex selection
        NACK: bool, // bit offset: 4 desc: Smartcard NACK enable
        SCEN: bool, // bit offset: 5 desc: Smartcard mode enable
        DMAR: bool, // bit offset: 6 desc: DMA enable receiver
        DMAT: bool, // bit offset: 7 desc: DMA enable transmitter
        RTSE: bool, // bit offset: 8 desc: RTS enable
        CTSE: bool, // bit offset: 9 desc: CTS enable
        CTSIE: bool, // bit offset: 10 desc: CTS interrupt enable
        ONEBIT: bool, // bit offset: 11 desc: One sample bit method enable
        OVRDIS: bool, // bit offset: 12 desc: Overrun Disable
        DDRE: bool, // bit offset: 13 desc: DMA Disable on Reception Error
        DEM: bool, // bit offset: 14 desc: Driver enable mode
        DEP: bool, // bit offset: 15 desc: Driver enable polarity selection
        reserved0: u1 = 0,
        SCARCNT: u3, // bit offset: 17 desc: Smartcard auto-retry count
        WUS: u2, // bit offset: 20 desc: Wakeup from Stop mode interrupt flag selection
        WUFIE: bool, // bit offset: 22 desc: Wakeup from Stop mode interrupt enable
        padding: u9 = 0,
    });
    // byte offset: 12 Baud rate register
    pub const BRR = MMIO(Address + 0x0000000c, u32, packed struct {
        DIV_Fraction: u4, // bit offset: 0 desc: fraction of USARTDIV
        DIV_Mantissa: u12, // bit offset: 4 desc: mantissa of USARTDIV
        padding: u16 = 0,
    });
    // byte offset: 16 Guard time and prescaler register
    pub const GTPR = MMIO(Address + 0x00000010, u32, packed struct {
        PSC: u8, // bit offset: 0 desc: Prescaler value
        GT: u8, // bit offset: 8 desc: Guard time value
        padding: u16 = 0,
    });
    // byte offset: 20 Receiver timeout register
    pub const RTOR = MMIO(Address + 0x00000014, u32, packed struct {
        RTO: u24, // bit offset: 0 desc: Receiver timeout value
        BLEN: u8, // bit offset: 24 desc: Block Length
    });
    // byte offset: 24 Request register
    pub const RQR = MMIO(Address + 0x00000018, u32, packed struct {
        ABRRQ: bool, // bit offset: 0 desc: Auto baud rate request
        SBKRQ: bool, // bit offset: 1 desc: Send break request
        MMRQ: bool, // bit offset: 2 desc: Mute mode request
        RXFRQ: bool, // bit offset: 3 desc: Receive data flush request
        TXFRQ: bool, // bit offset: 4 desc: Transmit data flush request
        padding: u27 = 0,
    });
    // byte offset: 28 Interrupt & status register
    pub const ISR = MMIO(Address + 0x0000001c, u32, packed struct {
        PE: bool, // bit offset: 0 desc: Parity error
        FE: bool, // bit offset: 1 desc: Framing error
        NF: bool, // bit offset: 2 desc: Noise detected flag
        ORE: bool, // bit offset: 3 desc: Overrun error
        IDLE: bool, // bit offset: 4 desc: Idle line detected
        RXNE: bool, // bit offset: 5 desc: Read data register not empty
        TC: bool, // bit offset: 6 desc: Transmission complete
        TXE: bool, // bit offset: 7 desc: Transmit data register empty
        LBDF: bool, // bit offset: 8 desc: LIN break detection flag
        CTSIF: bool, // bit offset: 9 desc: CTS interrupt flag
        CTS: bool, // bit offset: 10 desc: CTS flag
        RTOF: bool, // bit offset: 11 desc: Receiver timeout
        EOBF: bool, // bit offset: 12 desc: End of block flag
        reserved0: u1 = 0,
        ABRE: bool, // bit offset: 14 desc: Auto baud rate error
        ABRF: bool, // bit offset: 15 desc: Auto baud rate flag
        BUSY: bool, // bit offset: 16 desc: Busy flag
        CMF: bool, // bit offset: 17 desc: character match flag
        SBKF: bool, // bit offset: 18 desc: Send break flag
        RWU: bool, // bit offset: 19 desc: Receiver wakeup from Mute mode
        WUF: bool, // bit offset: 20 desc: Wakeup from Stop mode flag
        TEACK: bool, // bit offset: 21 desc: Transmit enable acknowledge flag
        REACK: bool, // bit offset: 22 desc: Receive enable acknowledge flag
        padding: u9 = 0,
    });
    // byte offset: 32 Interrupt flag clear register
    pub const ICR = MMIO(Address + 0x00000020, u32, packed struct {
        PECF: bool, // bit offset: 0 desc: Parity error clear flag
        FECF: bool, // bit offset: 1 desc: Framing error clear flag
        NCF: bool, // bit offset: 2 desc: Noise detected clear flag
        ORECF: bool, // bit offset: 3 desc: Overrun error clear flag
        IDLECF: bool, // bit offset: 4 desc: Idle line detected clear flag
        reserved0: u1 = 0,
        TCCF: bool, // bit offset: 6 desc: Transmission complete clear flag
        reserved1: u1 = 0,
        LBDCF: bool, // bit offset: 8 desc: LIN break detection clear flag
        CTSCF: bool, // bit offset: 9 desc: CTS clear flag
        reserved2: u1 = 0,
        RTOCF: bool, // bit offset: 11 desc: Receiver timeout clear flag
        EOBCF: bool, // bit offset: 12 desc: End of timeout clear flag
        reserved3: u4 = 0,
        CMCF: bool, // bit offset: 17 desc: Character match clear flag
        reserved4: u2 = 0,
        WUCF: bool, // bit offset: 20 desc: Wakeup from Stop mode clear flag
        padding: u11 = 0,
    });
    // byte offset: 36 Receive data register
    pub const RDR = MMIO(Address + 0x00000024, u32, packed struct {
        RDR: u9, // bit offset: 0 desc: Receive data value
        padding: u23 = 0,
    });
    // byte offset: 40 Transmit data register
    pub const TDR = MMIO(Address + 0x00000028, u32, packed struct {
        TDR: u9, // bit offset: 0 desc: Transmit data value
        padding: u23 = 0,
    });
};
pub const USART3 = extern struct {
    pub const Address: u32 = 0x40004800;
    // byte offset: 0 Control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        UE: bool, // bit offset: 0 desc: USART enable
        UESM: bool, // bit offset: 1 desc: USART enable in Stop mode
        RE: bool, // bit offset: 2 desc: Receiver enable
        TE: bool, // bit offset: 3 desc: Transmitter enable
        IDLEIE: bool, // bit offset: 4 desc: IDLE interrupt enable
        RXNEIE: bool, // bit offset: 5 desc: RXNE interrupt enable
        TCIE: bool, // bit offset: 6 desc: Transmission complete interrupt enable
        TXEIE: bool, // bit offset: 7 desc: interrupt enable
        PEIE: bool, // bit offset: 8 desc: PE interrupt enable
        PS: bool, // bit offset: 9 desc: Parity selection
        PCE: bool, // bit offset: 10 desc: Parity control enable
        WAKE: bool, // bit offset: 11 desc: Receiver wakeup method
        M: bool, // bit offset: 12 desc: Word length
        MME: bool, // bit offset: 13 desc: Mute mode enable
        CMIE: bool, // bit offset: 14 desc: Character match interrupt enable
        OVER8: bool, // bit offset: 15 desc: Oversampling mode
        DEDT: u5, // bit offset: 16 desc: Driver Enable deassertion time
        DEAT: u5, // bit offset: 21 desc: Driver Enable assertion time
        RTOIE: bool, // bit offset: 26 desc: Receiver timeout interrupt enable
        EOBIE: bool, // bit offset: 27 desc: End of Block interrupt enable
        padding: u4 = 0,
    });
    // byte offset: 4 Control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        reserved0: u4 = 0,
        ADDM7: bool, // bit offset: 4 desc: 7-bit Address Detection/4-bit Address Detection
        LBDL: bool, // bit offset: 5 desc: LIN break detection length
        LBDIE: bool, // bit offset: 6 desc: LIN break detection interrupt enable
        reserved1: u1 = 0,
        LBCL: bool, // bit offset: 8 desc: Last bit clock pulse
        CPHA: bool, // bit offset: 9 desc: Clock phase
        CPOL: bool, // bit offset: 10 desc: Clock polarity
        CLKEN: bool, // bit offset: 11 desc: Clock enable
        STOP: u2, // bit offset: 12 desc: STOP bits
        LINEN: bool, // bit offset: 14 desc: LIN mode enable
        SWAP: bool, // bit offset: 15 desc: Swap TX/RX pins
        RXINV: bool, // bit offset: 16 desc: RX pin active level inversion
        TXINV: bool, // bit offset: 17 desc: TX pin active level inversion
        DATAINV: bool, // bit offset: 18 desc: Binary data inversion
        MSBFIRST: bool, // bit offset: 19 desc: Most significant bit first
        ABREN: bool, // bit offset: 20 desc: Auto baud rate enable
        ABRMOD: u2, // bit offset: 21 desc: Auto baud rate mode
        RTOEN: bool, // bit offset: 23 desc: Receiver timeout enable
        ADD0: u4, // bit offset: 24 desc: Address of the USART node
        ADD4: u4, // bit offset: 28 desc: Address of the USART node
    });
    // byte offset: 8 Control register 3
    pub const CR3 = MMIO(Address + 0x00000008, u32, packed struct {
        EIE: bool, // bit offset: 0 desc: Error interrupt enable
        IREN: bool, // bit offset: 1 desc: IrDA mode enable
        IRLP: bool, // bit offset: 2 desc: IrDA low-power
        HDSEL: bool, // bit offset: 3 desc: Half-duplex selection
        NACK: bool, // bit offset: 4 desc: Smartcard NACK enable
        SCEN: bool, // bit offset: 5 desc: Smartcard mode enable
        DMAR: bool, // bit offset: 6 desc: DMA enable receiver
        DMAT: bool, // bit offset: 7 desc: DMA enable transmitter
        RTSE: bool, // bit offset: 8 desc: RTS enable
        CTSE: bool, // bit offset: 9 desc: CTS enable
        CTSIE: bool, // bit offset: 10 desc: CTS interrupt enable
        ONEBIT: bool, // bit offset: 11 desc: One sample bit method enable
        OVRDIS: bool, // bit offset: 12 desc: Overrun Disable
        DDRE: bool, // bit offset: 13 desc: DMA Disable on Reception Error
        DEM: bool, // bit offset: 14 desc: Driver enable mode
        DEP: bool, // bit offset: 15 desc: Driver enable polarity selection
        reserved0: u1 = 0,
        SCARCNT: u3, // bit offset: 17 desc: Smartcard auto-retry count
        WUS: u2, // bit offset: 20 desc: Wakeup from Stop mode interrupt flag selection
        WUFIE: bool, // bit offset: 22 desc: Wakeup from Stop mode interrupt enable
        padding: u9 = 0,
    });
    // byte offset: 12 Baud rate register
    pub const BRR = MMIO(Address + 0x0000000c, u32, packed struct {
        DIV_Fraction: u4, // bit offset: 0 desc: fraction of USARTDIV
        DIV_Mantissa: u12, // bit offset: 4 desc: mantissa of USARTDIV
        padding: u16 = 0,
    });
    // byte offset: 16 Guard time and prescaler register
    pub const GTPR = MMIO(Address + 0x00000010, u32, packed struct {
        PSC: u8, // bit offset: 0 desc: Prescaler value
        GT: u8, // bit offset: 8 desc: Guard time value
        padding: u16 = 0,
    });
    // byte offset: 20 Receiver timeout register
    pub const RTOR = MMIO(Address + 0x00000014, u32, packed struct {
        RTO: u24, // bit offset: 0 desc: Receiver timeout value
        BLEN: u8, // bit offset: 24 desc: Block Length
    });
    // byte offset: 24 Request register
    pub const RQR = MMIO(Address + 0x00000018, u32, packed struct {
        ABRRQ: bool, // bit offset: 0 desc: Auto baud rate request
        SBKRQ: bool, // bit offset: 1 desc: Send break request
        MMRQ: bool, // bit offset: 2 desc: Mute mode request
        RXFRQ: bool, // bit offset: 3 desc: Receive data flush request
        TXFRQ: bool, // bit offset: 4 desc: Transmit data flush request
        padding: u27 = 0,
    });
    // byte offset: 28 Interrupt & status register
    pub const ISR = MMIO(Address + 0x0000001c, u32, packed struct {
        PE: bool, // bit offset: 0 desc: Parity error
        FE: bool, // bit offset: 1 desc: Framing error
        NF: bool, // bit offset: 2 desc: Noise detected flag
        ORE: bool, // bit offset: 3 desc: Overrun error
        IDLE: bool, // bit offset: 4 desc: Idle line detected
        RXNE: bool, // bit offset: 5 desc: Read data register not empty
        TC: bool, // bit offset: 6 desc: Transmission complete
        TXE: bool, // bit offset: 7 desc: Transmit data register empty
        LBDF: bool, // bit offset: 8 desc: LIN break detection flag
        CTSIF: bool, // bit offset: 9 desc: CTS interrupt flag
        CTS: bool, // bit offset: 10 desc: CTS flag
        RTOF: bool, // bit offset: 11 desc: Receiver timeout
        EOBF: bool, // bit offset: 12 desc: End of block flag
        reserved0: u1 = 0,
        ABRE: bool, // bit offset: 14 desc: Auto baud rate error
        ABRF: bool, // bit offset: 15 desc: Auto baud rate flag
        BUSY: bool, // bit offset: 16 desc: Busy flag
        CMF: bool, // bit offset: 17 desc: character match flag
        SBKF: bool, // bit offset: 18 desc: Send break flag
        RWU: bool, // bit offset: 19 desc: Receiver wakeup from Mute mode
        WUF: bool, // bit offset: 20 desc: Wakeup from Stop mode flag
        TEACK: bool, // bit offset: 21 desc: Transmit enable acknowledge flag
        REACK: bool, // bit offset: 22 desc: Receive enable acknowledge flag
        padding: u9 = 0,
    });
    // byte offset: 32 Interrupt flag clear register
    pub const ICR = MMIO(Address + 0x00000020, u32, packed struct {
        PECF: bool, // bit offset: 0 desc: Parity error clear flag
        FECF: bool, // bit offset: 1 desc: Framing error clear flag
        NCF: bool, // bit offset: 2 desc: Noise detected clear flag
        ORECF: bool, // bit offset: 3 desc: Overrun error clear flag
        IDLECF: bool, // bit offset: 4 desc: Idle line detected clear flag
        reserved0: u1 = 0,
        TCCF: bool, // bit offset: 6 desc: Transmission complete clear flag
        reserved1: u1 = 0,
        LBDCF: bool, // bit offset: 8 desc: LIN break detection clear flag
        CTSCF: bool, // bit offset: 9 desc: CTS clear flag
        reserved2: u1 = 0,
        RTOCF: bool, // bit offset: 11 desc: Receiver timeout clear flag
        EOBCF: bool, // bit offset: 12 desc: End of timeout clear flag
        reserved3: u4 = 0,
        CMCF: bool, // bit offset: 17 desc: Character match clear flag
        reserved4: u2 = 0,
        WUCF: bool, // bit offset: 20 desc: Wakeup from Stop mode clear flag
        padding: u11 = 0,
    });
    // byte offset: 36 Receive data register
    pub const RDR = MMIO(Address + 0x00000024, u32, packed struct {
        RDR: u9, // bit offset: 0 desc: Receive data value
        padding: u23 = 0,
    });
    // byte offset: 40 Transmit data register
    pub const TDR = MMIO(Address + 0x00000028, u32, packed struct {
        TDR: u9, // bit offset: 0 desc: Transmit data value
        padding: u23 = 0,
    });
};
pub const UART4 = extern struct {
    pub const Address: u32 = 0x40004c00;
    // byte offset: 0 Control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        UE: bool, // bit offset: 0 desc: USART enable
        UESM: bool, // bit offset: 1 desc: USART enable in Stop mode
        RE: bool, // bit offset: 2 desc: Receiver enable
        TE: bool, // bit offset: 3 desc: Transmitter enable
        IDLEIE: bool, // bit offset: 4 desc: IDLE interrupt enable
        RXNEIE: bool, // bit offset: 5 desc: RXNE interrupt enable
        TCIE: bool, // bit offset: 6 desc: Transmission complete interrupt enable
        TXEIE: bool, // bit offset: 7 desc: interrupt enable
        PEIE: bool, // bit offset: 8 desc: PE interrupt enable
        PS: bool, // bit offset: 9 desc: Parity selection
        PCE: bool, // bit offset: 10 desc: Parity control enable
        WAKE: bool, // bit offset: 11 desc: Receiver wakeup method
        M: bool, // bit offset: 12 desc: Word length
        MME: bool, // bit offset: 13 desc: Mute mode enable
        CMIE: bool, // bit offset: 14 desc: Character match interrupt enable
        OVER8: bool, // bit offset: 15 desc: Oversampling mode
        DEDT: u5, // bit offset: 16 desc: Driver Enable deassertion time
        DEAT: u5, // bit offset: 21 desc: Driver Enable assertion time
        RTOIE: bool, // bit offset: 26 desc: Receiver timeout interrupt enable
        EOBIE: bool, // bit offset: 27 desc: End of Block interrupt enable
        padding: u4 = 0,
    });
    // byte offset: 4 Control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        reserved0: u4 = 0,
        ADDM7: bool, // bit offset: 4 desc: 7-bit Address Detection/4-bit Address Detection
        LBDL: bool, // bit offset: 5 desc: LIN break detection length
        LBDIE: bool, // bit offset: 6 desc: LIN break detection interrupt enable
        reserved1: u1 = 0,
        LBCL: bool, // bit offset: 8 desc: Last bit clock pulse
        CPHA: bool, // bit offset: 9 desc: Clock phase
        CPOL: bool, // bit offset: 10 desc: Clock polarity
        CLKEN: bool, // bit offset: 11 desc: Clock enable
        STOP: u2, // bit offset: 12 desc: STOP bits
        LINEN: bool, // bit offset: 14 desc: LIN mode enable
        SWAP: bool, // bit offset: 15 desc: Swap TX/RX pins
        RXINV: bool, // bit offset: 16 desc: RX pin active level inversion
        TXINV: bool, // bit offset: 17 desc: TX pin active level inversion
        DATAINV: bool, // bit offset: 18 desc: Binary data inversion
        MSBFIRST: bool, // bit offset: 19 desc: Most significant bit first
        ABREN: bool, // bit offset: 20 desc: Auto baud rate enable
        ABRMOD: u2, // bit offset: 21 desc: Auto baud rate mode
        RTOEN: bool, // bit offset: 23 desc: Receiver timeout enable
        ADD0: u4, // bit offset: 24 desc: Address of the USART node
        ADD4: u4, // bit offset: 28 desc: Address of the USART node
    });
    // byte offset: 8 Control register 3
    pub const CR3 = MMIO(Address + 0x00000008, u32, packed struct {
        EIE: bool, // bit offset: 0 desc: Error interrupt enable
        IREN: bool, // bit offset: 1 desc: IrDA mode enable
        IRLP: bool, // bit offset: 2 desc: IrDA low-power
        HDSEL: bool, // bit offset: 3 desc: Half-duplex selection
        NACK: bool, // bit offset: 4 desc: Smartcard NACK enable
        SCEN: bool, // bit offset: 5 desc: Smartcard mode enable
        DMAR: bool, // bit offset: 6 desc: DMA enable receiver
        DMAT: bool, // bit offset: 7 desc: DMA enable transmitter
        RTSE: bool, // bit offset: 8 desc: RTS enable
        CTSE: bool, // bit offset: 9 desc: CTS enable
        CTSIE: bool, // bit offset: 10 desc: CTS interrupt enable
        ONEBIT: bool, // bit offset: 11 desc: One sample bit method enable
        OVRDIS: bool, // bit offset: 12 desc: Overrun Disable
        DDRE: bool, // bit offset: 13 desc: DMA Disable on Reception Error
        DEM: bool, // bit offset: 14 desc: Driver enable mode
        DEP: bool, // bit offset: 15 desc: Driver enable polarity selection
        reserved0: u1 = 0,
        SCARCNT: u3, // bit offset: 17 desc: Smartcard auto-retry count
        WUS: u2, // bit offset: 20 desc: Wakeup from Stop mode interrupt flag selection
        WUFIE: bool, // bit offset: 22 desc: Wakeup from Stop mode interrupt enable
        padding: u9 = 0,
    });
    // byte offset: 12 Baud rate register
    pub const BRR = MMIO(Address + 0x0000000c, u32, packed struct {
        DIV_Fraction: u4, // bit offset: 0 desc: fraction of USARTDIV
        DIV_Mantissa: u12, // bit offset: 4 desc: mantissa of USARTDIV
        padding: u16 = 0,
    });
    // byte offset: 16 Guard time and prescaler register
    pub const GTPR = MMIO(Address + 0x00000010, u32, packed struct {
        PSC: u8, // bit offset: 0 desc: Prescaler value
        GT: u8, // bit offset: 8 desc: Guard time value
        padding: u16 = 0,
    });
    // byte offset: 20 Receiver timeout register
    pub const RTOR = MMIO(Address + 0x00000014, u32, packed struct {
        RTO: u24, // bit offset: 0 desc: Receiver timeout value
        BLEN: u8, // bit offset: 24 desc: Block Length
    });
    // byte offset: 24 Request register
    pub const RQR = MMIO(Address + 0x00000018, u32, packed struct {
        ABRRQ: bool, // bit offset: 0 desc: Auto baud rate request
        SBKRQ: bool, // bit offset: 1 desc: Send break request
        MMRQ: bool, // bit offset: 2 desc: Mute mode request
        RXFRQ: bool, // bit offset: 3 desc: Receive data flush request
        TXFRQ: bool, // bit offset: 4 desc: Transmit data flush request
        padding: u27 = 0,
    });
    // byte offset: 28 Interrupt & status register
    pub const ISR = MMIO(Address + 0x0000001c, u32, packed struct {
        PE: bool, // bit offset: 0 desc: Parity error
        FE: bool, // bit offset: 1 desc: Framing error
        NF: bool, // bit offset: 2 desc: Noise detected flag
        ORE: bool, // bit offset: 3 desc: Overrun error
        IDLE: bool, // bit offset: 4 desc: Idle line detected
        RXNE: bool, // bit offset: 5 desc: Read data register not empty
        TC: bool, // bit offset: 6 desc: Transmission complete
        TXE: bool, // bit offset: 7 desc: Transmit data register empty
        LBDF: bool, // bit offset: 8 desc: LIN break detection flag
        CTSIF: bool, // bit offset: 9 desc: CTS interrupt flag
        CTS: bool, // bit offset: 10 desc: CTS flag
        RTOF: bool, // bit offset: 11 desc: Receiver timeout
        EOBF: bool, // bit offset: 12 desc: End of block flag
        reserved0: u1 = 0,
        ABRE: bool, // bit offset: 14 desc: Auto baud rate error
        ABRF: bool, // bit offset: 15 desc: Auto baud rate flag
        BUSY: bool, // bit offset: 16 desc: Busy flag
        CMF: bool, // bit offset: 17 desc: character match flag
        SBKF: bool, // bit offset: 18 desc: Send break flag
        RWU: bool, // bit offset: 19 desc: Receiver wakeup from Mute mode
        WUF: bool, // bit offset: 20 desc: Wakeup from Stop mode flag
        TEACK: bool, // bit offset: 21 desc: Transmit enable acknowledge flag
        REACK: bool, // bit offset: 22 desc: Receive enable acknowledge flag
        padding: u9 = 0,
    });
    // byte offset: 32 Interrupt flag clear register
    pub const ICR = MMIO(Address + 0x00000020, u32, packed struct {
        PECF: bool, // bit offset: 0 desc: Parity error clear flag
        FECF: bool, // bit offset: 1 desc: Framing error clear flag
        NCF: bool, // bit offset: 2 desc: Noise detected clear flag
        ORECF: bool, // bit offset: 3 desc: Overrun error clear flag
        IDLECF: bool, // bit offset: 4 desc: Idle line detected clear flag
        reserved0: u1 = 0,
        TCCF: bool, // bit offset: 6 desc: Transmission complete clear flag
        reserved1: u1 = 0,
        LBDCF: bool, // bit offset: 8 desc: LIN break detection clear flag
        CTSCF: bool, // bit offset: 9 desc: CTS clear flag
        reserved2: u1 = 0,
        RTOCF: bool, // bit offset: 11 desc: Receiver timeout clear flag
        EOBCF: bool, // bit offset: 12 desc: End of timeout clear flag
        reserved3: u4 = 0,
        CMCF: bool, // bit offset: 17 desc: Character match clear flag
        reserved4: u2 = 0,
        WUCF: bool, // bit offset: 20 desc: Wakeup from Stop mode clear flag
        padding: u11 = 0,
    });
    // byte offset: 36 Receive data register
    pub const RDR = MMIO(Address + 0x00000024, u32, packed struct {
        RDR: u9, // bit offset: 0 desc: Receive data value
        padding: u23 = 0,
    });
    // byte offset: 40 Transmit data register
    pub const TDR = MMIO(Address + 0x00000028, u32, packed struct {
        TDR: u9, // bit offset: 0 desc: Transmit data value
        padding: u23 = 0,
    });
};
pub const UART5 = extern struct {
    pub const Address: u32 = 0x40005000;
    // byte offset: 0 Control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        UE: bool, // bit offset: 0 desc: USART enable
        UESM: bool, // bit offset: 1 desc: USART enable in Stop mode
        RE: bool, // bit offset: 2 desc: Receiver enable
        TE: bool, // bit offset: 3 desc: Transmitter enable
        IDLEIE: bool, // bit offset: 4 desc: IDLE interrupt enable
        RXNEIE: bool, // bit offset: 5 desc: RXNE interrupt enable
        TCIE: bool, // bit offset: 6 desc: Transmission complete interrupt enable
        TXEIE: bool, // bit offset: 7 desc: interrupt enable
        PEIE: bool, // bit offset: 8 desc: PE interrupt enable
        PS: bool, // bit offset: 9 desc: Parity selection
        PCE: bool, // bit offset: 10 desc: Parity control enable
        WAKE: bool, // bit offset: 11 desc: Receiver wakeup method
        M: bool, // bit offset: 12 desc: Word length
        MME: bool, // bit offset: 13 desc: Mute mode enable
        CMIE: bool, // bit offset: 14 desc: Character match interrupt enable
        OVER8: bool, // bit offset: 15 desc: Oversampling mode
        DEDT: u5, // bit offset: 16 desc: Driver Enable deassertion time
        DEAT: u5, // bit offset: 21 desc: Driver Enable assertion time
        RTOIE: bool, // bit offset: 26 desc: Receiver timeout interrupt enable
        EOBIE: bool, // bit offset: 27 desc: End of Block interrupt enable
        padding: u4 = 0,
    });
    // byte offset: 4 Control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        reserved0: u4 = 0,
        ADDM7: bool, // bit offset: 4 desc: 7-bit Address Detection/4-bit Address Detection
        LBDL: bool, // bit offset: 5 desc: LIN break detection length
        LBDIE: bool, // bit offset: 6 desc: LIN break detection interrupt enable
        reserved1: u1 = 0,
        LBCL: bool, // bit offset: 8 desc: Last bit clock pulse
        CPHA: bool, // bit offset: 9 desc: Clock phase
        CPOL: bool, // bit offset: 10 desc: Clock polarity
        CLKEN: bool, // bit offset: 11 desc: Clock enable
        STOP: u2, // bit offset: 12 desc: STOP bits
        LINEN: bool, // bit offset: 14 desc: LIN mode enable
        SWAP: bool, // bit offset: 15 desc: Swap TX/RX pins
        RXINV: bool, // bit offset: 16 desc: RX pin active level inversion
        TXINV: bool, // bit offset: 17 desc: TX pin active level inversion
        DATAINV: bool, // bit offset: 18 desc: Binary data inversion
        MSBFIRST: bool, // bit offset: 19 desc: Most significant bit first
        ABREN: bool, // bit offset: 20 desc: Auto baud rate enable
        ABRMOD: u2, // bit offset: 21 desc: Auto baud rate mode
        RTOEN: bool, // bit offset: 23 desc: Receiver timeout enable
        ADD0: u4, // bit offset: 24 desc: Address of the USART node
        ADD4: u4, // bit offset: 28 desc: Address of the USART node
    });
    // byte offset: 8 Control register 3
    pub const CR3 = MMIO(Address + 0x00000008, u32, packed struct {
        EIE: bool, // bit offset: 0 desc: Error interrupt enable
        IREN: bool, // bit offset: 1 desc: IrDA mode enable
        IRLP: bool, // bit offset: 2 desc: IrDA low-power
        HDSEL: bool, // bit offset: 3 desc: Half-duplex selection
        NACK: bool, // bit offset: 4 desc: Smartcard NACK enable
        SCEN: bool, // bit offset: 5 desc: Smartcard mode enable
        DMAR: bool, // bit offset: 6 desc: DMA enable receiver
        DMAT: bool, // bit offset: 7 desc: DMA enable transmitter
        RTSE: bool, // bit offset: 8 desc: RTS enable
        CTSE: bool, // bit offset: 9 desc: CTS enable
        CTSIE: bool, // bit offset: 10 desc: CTS interrupt enable
        ONEBIT: bool, // bit offset: 11 desc: One sample bit method enable
        OVRDIS: bool, // bit offset: 12 desc: Overrun Disable
        DDRE: bool, // bit offset: 13 desc: DMA Disable on Reception Error
        DEM: bool, // bit offset: 14 desc: Driver enable mode
        DEP: bool, // bit offset: 15 desc: Driver enable polarity selection
        reserved0: u1 = 0,
        SCARCNT: u3, // bit offset: 17 desc: Smartcard auto-retry count
        WUS: u2, // bit offset: 20 desc: Wakeup from Stop mode interrupt flag selection
        WUFIE: bool, // bit offset: 22 desc: Wakeup from Stop mode interrupt enable
        padding: u9 = 0,
    });
    // byte offset: 12 Baud rate register
    pub const BRR = MMIO(Address + 0x0000000c, u32, packed struct {
        DIV_Fraction: u4, // bit offset: 0 desc: fraction of USARTDIV
        DIV_Mantissa: u12, // bit offset: 4 desc: mantissa of USARTDIV
        padding: u16 = 0,
    });
    // byte offset: 16 Guard time and prescaler register
    pub const GTPR = MMIO(Address + 0x00000010, u32, packed struct {
        PSC: u8, // bit offset: 0 desc: Prescaler value
        GT: u8, // bit offset: 8 desc: Guard time value
        padding: u16 = 0,
    });
    // byte offset: 20 Receiver timeout register
    pub const RTOR = MMIO(Address + 0x00000014, u32, packed struct {
        RTO: u24, // bit offset: 0 desc: Receiver timeout value
        BLEN: u8, // bit offset: 24 desc: Block Length
    });
    // byte offset: 24 Request register
    pub const RQR = MMIO(Address + 0x00000018, u32, packed struct {
        ABRRQ: bool, // bit offset: 0 desc: Auto baud rate request
        SBKRQ: bool, // bit offset: 1 desc: Send break request
        MMRQ: bool, // bit offset: 2 desc: Mute mode request
        RXFRQ: bool, // bit offset: 3 desc: Receive data flush request
        TXFRQ: bool, // bit offset: 4 desc: Transmit data flush request
        padding: u27 = 0,
    });
    // byte offset: 28 Interrupt & status register
    pub const ISR = MMIO(Address + 0x0000001c, u32, packed struct {
        PE: bool, // bit offset: 0 desc: Parity error
        FE: bool, // bit offset: 1 desc: Framing error
        NF: bool, // bit offset: 2 desc: Noise detected flag
        ORE: bool, // bit offset: 3 desc: Overrun error
        IDLE: bool, // bit offset: 4 desc: Idle line detected
        RXNE: bool, // bit offset: 5 desc: Read data register not empty
        TC: bool, // bit offset: 6 desc: Transmission complete
        TXE: bool, // bit offset: 7 desc: Transmit data register empty
        LBDF: bool, // bit offset: 8 desc: LIN break detection flag
        CTSIF: bool, // bit offset: 9 desc: CTS interrupt flag
        CTS: bool, // bit offset: 10 desc: CTS flag
        RTOF: bool, // bit offset: 11 desc: Receiver timeout
        EOBF: bool, // bit offset: 12 desc: End of block flag
        reserved0: u1 = 0,
        ABRE: bool, // bit offset: 14 desc: Auto baud rate error
        ABRF: bool, // bit offset: 15 desc: Auto baud rate flag
        BUSY: bool, // bit offset: 16 desc: Busy flag
        CMF: bool, // bit offset: 17 desc: character match flag
        SBKF: bool, // bit offset: 18 desc: Send break flag
        RWU: bool, // bit offset: 19 desc: Receiver wakeup from Mute mode
        WUF: bool, // bit offset: 20 desc: Wakeup from Stop mode flag
        TEACK: bool, // bit offset: 21 desc: Transmit enable acknowledge flag
        REACK: bool, // bit offset: 22 desc: Receive enable acknowledge flag
        padding: u9 = 0,
    });
    // byte offset: 32 Interrupt flag clear register
    pub const ICR = MMIO(Address + 0x00000020, u32, packed struct {
        PECF: bool, // bit offset: 0 desc: Parity error clear flag
        FECF: bool, // bit offset: 1 desc: Framing error clear flag
        NCF: bool, // bit offset: 2 desc: Noise detected clear flag
        ORECF: bool, // bit offset: 3 desc: Overrun error clear flag
        IDLECF: bool, // bit offset: 4 desc: Idle line detected clear flag
        reserved0: u1 = 0,
        TCCF: bool, // bit offset: 6 desc: Transmission complete clear flag
        reserved1: u1 = 0,
        LBDCF: bool, // bit offset: 8 desc: LIN break detection clear flag
        CTSCF: bool, // bit offset: 9 desc: CTS clear flag
        reserved2: u1 = 0,
        RTOCF: bool, // bit offset: 11 desc: Receiver timeout clear flag
        EOBCF: bool, // bit offset: 12 desc: End of timeout clear flag
        reserved3: u4 = 0,
        CMCF: bool, // bit offset: 17 desc: Character match clear flag
        reserved4: u2 = 0,
        WUCF: bool, // bit offset: 20 desc: Wakeup from Stop mode clear flag
        padding: u11 = 0,
    });
    // byte offset: 36 Receive data register
    pub const RDR = MMIO(Address + 0x00000024, u32, packed struct {
        RDR: u9, // bit offset: 0 desc: Receive data value
        padding: u23 = 0,
    });
    // byte offset: 40 Transmit data register
    pub const TDR = MMIO(Address + 0x00000028, u32, packed struct {
        TDR: u9, // bit offset: 0 desc: Transmit data value
        padding: u23 = 0,
    });
};
pub const SPI1 = extern struct {
    pub const Address: u32 = 0x40013000;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CPHA: bool, // bit offset: 0 desc: Clock phase
        CPOL: bool, // bit offset: 1 desc: Clock polarity
        MSTR: bool, // bit offset: 2 desc: Master selection
        BR: u3, // bit offset: 3 desc: Baud rate control
        SPE: bool, // bit offset: 6 desc: SPI enable
        LSBFIRST: bool, // bit offset: 7 desc: Frame format
        SSI: bool, // bit offset: 8 desc: Internal slave select
        SSM: bool, // bit offset: 9 desc: Software slave management
        RXONLY: bool, // bit offset: 10 desc: Receive only
        CRCL: bool, // bit offset: 11 desc: CRC length
        CRCNEXT: bool, // bit offset: 12 desc: CRC transfer next
        CRCEN: bool, // bit offset: 13 desc: Hardware CRC calculation enable
        BIDIOE: bool, // bit offset: 14 desc: Output enable in bidirectional mode
        BIDIMODE: bool, // bit offset: 15 desc: Bidirectional data mode enable
        padding: u16 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        RXDMAEN: bool, // bit offset: 0 desc: Rx buffer DMA enable
        TXDMAEN: bool, // bit offset: 1 desc: Tx buffer DMA enable
        SSOE: bool, // bit offset: 2 desc: SS output enable
        NSSP: bool, // bit offset: 3 desc: NSS pulse management
        FRF: bool, // bit offset: 4 desc: Frame format
        ERRIE: bool, // bit offset: 5 desc: Error interrupt enable
        RXNEIE: bool, // bit offset: 6 desc: RX buffer not empty interrupt enable
        TXEIE: bool, // bit offset: 7 desc: Tx buffer empty interrupt enable
        DS: u4, // bit offset: 8 desc: Data size
        FRXTH: bool, // bit offset: 12 desc: FIFO reception threshold
        LDMA_RX: bool, // bit offset: 13 desc: Last DMA transfer for reception
        LDMA_TX: bool, // bit offset: 14 desc: Last DMA transfer for transmission
        padding: u17 = 0,
    });
    // byte offset: 8 status register
    pub const SR = MMIO(Address + 0x00000008, u32, packed struct {
        RXNE: bool, // bit offset: 0 desc: Receive buffer not empty
        TXE: bool, // bit offset: 1 desc: Transmit buffer empty
        CHSIDE: bool, // bit offset: 2 desc: Channel side
        UDR: bool, // bit offset: 3 desc: Underrun flag
        CRCERR: bool, // bit offset: 4 desc: CRC error flag
        MODF: bool, // bit offset: 5 desc: Mode fault
        OVR: bool, // bit offset: 6 desc: Overrun flag
        BSY: bool, // bit offset: 7 desc: Busy flag
        TIFRFE: bool, // bit offset: 8 desc: TI frame format error
        FRLVL: u2, // bit offset: 9 desc: FIFO reception level
        FTLVL: u2, // bit offset: 11 desc: FIFO transmission level
        padding: u19 = 0,
    });
    // byte offset: 12 data register
    pub const DR = MMIO(Address + 0x0000000c, u32, packed struct {
        DR: u16, // bit offset: 0 desc: Data register
        padding: u16 = 0,
    });
    // byte offset: 16 CRC polynomial register
    pub const CRCPR = MMIO(Address + 0x00000010, u32, packed struct {
        CRCPOLY: u16, // bit offset: 0 desc: CRC polynomial register
        padding: u16 = 0,
    });
    // byte offset: 20 RX CRC register
    pub const RXCRCR = MMIO(Address + 0x00000014, u32, packed struct {
        RxCRC: u16, // bit offset: 0 desc: Rx CRC register
        padding: u16 = 0,
    });
    // byte offset: 24 TX CRC register
    pub const TXCRCR = MMIO(Address + 0x00000018, u32, packed struct {
        TxCRC: u16, // bit offset: 0 desc: Tx CRC register
        padding: u16 = 0,
    });
    // byte offset: 28 I2S configuration register
    pub const I2SCFGR = MMIO(Address + 0x0000001c, u32, packed struct {
        CHLEN: bool, // bit offset: 0 desc: Channel length (number of bits per audio channel)
        DATLEN: u2, // bit offset: 1 desc: Data length to be transferred
        CKPOL: bool, // bit offset: 3 desc: Steady state clock polarity
        I2SSTD: u2, // bit offset: 4 desc: I2S standard selection
        reserved0: u1 = 0,
        PCMSYNC: bool, // bit offset: 7 desc: PCM frame synchronization
        I2SCFG: u2, // bit offset: 8 desc: I2S configuration mode
        I2SE: bool, // bit offset: 10 desc: I2S Enable
        I2SMOD: bool, // bit offset: 11 desc: I2S mode selection
        padding: u20 = 0,
    });
    // byte offset: 32 I2S prescaler register
    pub const I2SPR = MMIO(Address + 0x00000020, u32, packed struct {
        I2SDIV: u8, // bit offset: 0 desc: I2S Linear prescaler
        ODD: bool, // bit offset: 8 desc: Odd factor for the prescaler
        MCKOE: bool, // bit offset: 9 desc: Master clock output enable
        padding: u22 = 0,
    });
};
pub const SPI2 = extern struct {
    pub const Address: u32 = 0x40003800;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CPHA: bool, // bit offset: 0 desc: Clock phase
        CPOL: bool, // bit offset: 1 desc: Clock polarity
        MSTR: bool, // bit offset: 2 desc: Master selection
        BR: u3, // bit offset: 3 desc: Baud rate control
        SPE: bool, // bit offset: 6 desc: SPI enable
        LSBFIRST: bool, // bit offset: 7 desc: Frame format
        SSI: bool, // bit offset: 8 desc: Internal slave select
        SSM: bool, // bit offset: 9 desc: Software slave management
        RXONLY: bool, // bit offset: 10 desc: Receive only
        CRCL: bool, // bit offset: 11 desc: CRC length
        CRCNEXT: bool, // bit offset: 12 desc: CRC transfer next
        CRCEN: bool, // bit offset: 13 desc: Hardware CRC calculation enable
        BIDIOE: bool, // bit offset: 14 desc: Output enable in bidirectional mode
        BIDIMODE: bool, // bit offset: 15 desc: Bidirectional data mode enable
        padding: u16 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        RXDMAEN: bool, // bit offset: 0 desc: Rx buffer DMA enable
        TXDMAEN: bool, // bit offset: 1 desc: Tx buffer DMA enable
        SSOE: bool, // bit offset: 2 desc: SS output enable
        NSSP: bool, // bit offset: 3 desc: NSS pulse management
        FRF: bool, // bit offset: 4 desc: Frame format
        ERRIE: bool, // bit offset: 5 desc: Error interrupt enable
        RXNEIE: bool, // bit offset: 6 desc: RX buffer not empty interrupt enable
        TXEIE: bool, // bit offset: 7 desc: Tx buffer empty interrupt enable
        DS: u4, // bit offset: 8 desc: Data size
        FRXTH: bool, // bit offset: 12 desc: FIFO reception threshold
        LDMA_RX: bool, // bit offset: 13 desc: Last DMA transfer for reception
        LDMA_TX: bool, // bit offset: 14 desc: Last DMA transfer for transmission
        padding: u17 = 0,
    });
    // byte offset: 8 status register
    pub const SR = MMIO(Address + 0x00000008, u32, packed struct {
        RXNE: bool, // bit offset: 0 desc: Receive buffer not empty
        TXE: bool, // bit offset: 1 desc: Transmit buffer empty
        CHSIDE: bool, // bit offset: 2 desc: Channel side
        UDR: bool, // bit offset: 3 desc: Underrun flag
        CRCERR: bool, // bit offset: 4 desc: CRC error flag
        MODF: bool, // bit offset: 5 desc: Mode fault
        OVR: bool, // bit offset: 6 desc: Overrun flag
        BSY: bool, // bit offset: 7 desc: Busy flag
        TIFRFE: bool, // bit offset: 8 desc: TI frame format error
        FRLVL: u2, // bit offset: 9 desc: FIFO reception level
        FTLVL: u2, // bit offset: 11 desc: FIFO transmission level
        padding: u19 = 0,
    });
    // byte offset: 12 data register
    pub const DR = MMIO(Address + 0x0000000c, u32, packed struct {
        DR: u16, // bit offset: 0 desc: Data register
        padding: u16 = 0,
    });
    // byte offset: 16 CRC polynomial register
    pub const CRCPR = MMIO(Address + 0x00000010, u32, packed struct {
        CRCPOLY: u16, // bit offset: 0 desc: CRC polynomial register
        padding: u16 = 0,
    });
    // byte offset: 20 RX CRC register
    pub const RXCRCR = MMIO(Address + 0x00000014, u32, packed struct {
        RxCRC: u16, // bit offset: 0 desc: Rx CRC register
        padding: u16 = 0,
    });
    // byte offset: 24 TX CRC register
    pub const TXCRCR = MMIO(Address + 0x00000018, u32, packed struct {
        TxCRC: u16, // bit offset: 0 desc: Tx CRC register
        padding: u16 = 0,
    });
    // byte offset: 28 I2S configuration register
    pub const I2SCFGR = MMIO(Address + 0x0000001c, u32, packed struct {
        CHLEN: bool, // bit offset: 0 desc: Channel length (number of bits per audio channel)
        DATLEN: u2, // bit offset: 1 desc: Data length to be transferred
        CKPOL: bool, // bit offset: 3 desc: Steady state clock polarity
        I2SSTD: u2, // bit offset: 4 desc: I2S standard selection
        reserved0: u1 = 0,
        PCMSYNC: bool, // bit offset: 7 desc: PCM frame synchronization
        I2SCFG: u2, // bit offset: 8 desc: I2S configuration mode
        I2SE: bool, // bit offset: 10 desc: I2S Enable
        I2SMOD: bool, // bit offset: 11 desc: I2S mode selection
        padding: u20 = 0,
    });
    // byte offset: 32 I2S prescaler register
    pub const I2SPR = MMIO(Address + 0x00000020, u32, packed struct {
        I2SDIV: u8, // bit offset: 0 desc: I2S Linear prescaler
        ODD: bool, // bit offset: 8 desc: Odd factor for the prescaler
        MCKOE: bool, // bit offset: 9 desc: Master clock output enable
        padding: u22 = 0,
    });
};
pub const SPI3 = extern struct {
    pub const Address: u32 = 0x40003c00;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CPHA: bool, // bit offset: 0 desc: Clock phase
        CPOL: bool, // bit offset: 1 desc: Clock polarity
        MSTR: bool, // bit offset: 2 desc: Master selection
        BR: u3, // bit offset: 3 desc: Baud rate control
        SPE: bool, // bit offset: 6 desc: SPI enable
        LSBFIRST: bool, // bit offset: 7 desc: Frame format
        SSI: bool, // bit offset: 8 desc: Internal slave select
        SSM: bool, // bit offset: 9 desc: Software slave management
        RXONLY: bool, // bit offset: 10 desc: Receive only
        CRCL: bool, // bit offset: 11 desc: CRC length
        CRCNEXT: bool, // bit offset: 12 desc: CRC transfer next
        CRCEN: bool, // bit offset: 13 desc: Hardware CRC calculation enable
        BIDIOE: bool, // bit offset: 14 desc: Output enable in bidirectional mode
        BIDIMODE: bool, // bit offset: 15 desc: Bidirectional data mode enable
        padding: u16 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        RXDMAEN: bool, // bit offset: 0 desc: Rx buffer DMA enable
        TXDMAEN: bool, // bit offset: 1 desc: Tx buffer DMA enable
        SSOE: bool, // bit offset: 2 desc: SS output enable
        NSSP: bool, // bit offset: 3 desc: NSS pulse management
        FRF: bool, // bit offset: 4 desc: Frame format
        ERRIE: bool, // bit offset: 5 desc: Error interrupt enable
        RXNEIE: bool, // bit offset: 6 desc: RX buffer not empty interrupt enable
        TXEIE: bool, // bit offset: 7 desc: Tx buffer empty interrupt enable
        DS: u4, // bit offset: 8 desc: Data size
        FRXTH: bool, // bit offset: 12 desc: FIFO reception threshold
        LDMA_RX: bool, // bit offset: 13 desc: Last DMA transfer for reception
        LDMA_TX: bool, // bit offset: 14 desc: Last DMA transfer for transmission
        padding: u17 = 0,
    });
    // byte offset: 8 status register
    pub const SR = MMIO(Address + 0x00000008, u32, packed struct {
        RXNE: bool, // bit offset: 0 desc: Receive buffer not empty
        TXE: bool, // bit offset: 1 desc: Transmit buffer empty
        CHSIDE: bool, // bit offset: 2 desc: Channel side
        UDR: bool, // bit offset: 3 desc: Underrun flag
        CRCERR: bool, // bit offset: 4 desc: CRC error flag
        MODF: bool, // bit offset: 5 desc: Mode fault
        OVR: bool, // bit offset: 6 desc: Overrun flag
        BSY: bool, // bit offset: 7 desc: Busy flag
        TIFRFE: bool, // bit offset: 8 desc: TI frame format error
        FRLVL: u2, // bit offset: 9 desc: FIFO reception level
        FTLVL: u2, // bit offset: 11 desc: FIFO transmission level
        padding: u19 = 0,
    });
    // byte offset: 12 data register
    pub const DR = MMIO(Address + 0x0000000c, u32, packed struct {
        DR: u16, // bit offset: 0 desc: Data register
        padding: u16 = 0,
    });
    // byte offset: 16 CRC polynomial register
    pub const CRCPR = MMIO(Address + 0x00000010, u32, packed struct {
        CRCPOLY: u16, // bit offset: 0 desc: CRC polynomial register
        padding: u16 = 0,
    });
    // byte offset: 20 RX CRC register
    pub const RXCRCR = MMIO(Address + 0x00000014, u32, packed struct {
        RxCRC: u16, // bit offset: 0 desc: Rx CRC register
        padding: u16 = 0,
    });
    // byte offset: 24 TX CRC register
    pub const TXCRCR = MMIO(Address + 0x00000018, u32, packed struct {
        TxCRC: u16, // bit offset: 0 desc: Tx CRC register
        padding: u16 = 0,
    });
    // byte offset: 28 I2S configuration register
    pub const I2SCFGR = MMIO(Address + 0x0000001c, u32, packed struct {
        CHLEN: bool, // bit offset: 0 desc: Channel length (number of bits per audio channel)
        DATLEN: u2, // bit offset: 1 desc: Data length to be transferred
        CKPOL: bool, // bit offset: 3 desc: Steady state clock polarity
        I2SSTD: u2, // bit offset: 4 desc: I2S standard selection
        reserved0: u1 = 0,
        PCMSYNC: bool, // bit offset: 7 desc: PCM frame synchronization
        I2SCFG: u2, // bit offset: 8 desc: I2S configuration mode
        I2SE: bool, // bit offset: 10 desc: I2S Enable
        I2SMOD: bool, // bit offset: 11 desc: I2S mode selection
        padding: u20 = 0,
    });
    // byte offset: 32 I2S prescaler register
    pub const I2SPR = MMIO(Address + 0x00000020, u32, packed struct {
        I2SDIV: u8, // bit offset: 0 desc: I2S Linear prescaler
        ODD: bool, // bit offset: 8 desc: Odd factor for the prescaler
        MCKOE: bool, // bit offset: 9 desc: Master clock output enable
        padding: u22 = 0,
    });
};
pub const I2S2ext = extern struct {
    pub const Address: u32 = 0x40003400;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CPHA: bool, // bit offset: 0 desc: Clock phase
        CPOL: bool, // bit offset: 1 desc: Clock polarity
        MSTR: bool, // bit offset: 2 desc: Master selection
        BR: u3, // bit offset: 3 desc: Baud rate control
        SPE: bool, // bit offset: 6 desc: SPI enable
        LSBFIRST: bool, // bit offset: 7 desc: Frame format
        SSI: bool, // bit offset: 8 desc: Internal slave select
        SSM: bool, // bit offset: 9 desc: Software slave management
        RXONLY: bool, // bit offset: 10 desc: Receive only
        CRCL: bool, // bit offset: 11 desc: CRC length
        CRCNEXT: bool, // bit offset: 12 desc: CRC transfer next
        CRCEN: bool, // bit offset: 13 desc: Hardware CRC calculation enable
        BIDIOE: bool, // bit offset: 14 desc: Output enable in bidirectional mode
        BIDIMODE: bool, // bit offset: 15 desc: Bidirectional data mode enable
        padding: u16 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        RXDMAEN: bool, // bit offset: 0 desc: Rx buffer DMA enable
        TXDMAEN: bool, // bit offset: 1 desc: Tx buffer DMA enable
        SSOE: bool, // bit offset: 2 desc: SS output enable
        NSSP: bool, // bit offset: 3 desc: NSS pulse management
        FRF: bool, // bit offset: 4 desc: Frame format
        ERRIE: bool, // bit offset: 5 desc: Error interrupt enable
        RXNEIE: bool, // bit offset: 6 desc: RX buffer not empty interrupt enable
        TXEIE: bool, // bit offset: 7 desc: Tx buffer empty interrupt enable
        DS: u4, // bit offset: 8 desc: Data size
        FRXTH: bool, // bit offset: 12 desc: FIFO reception threshold
        LDMA_RX: bool, // bit offset: 13 desc: Last DMA transfer for reception
        LDMA_TX: bool, // bit offset: 14 desc: Last DMA transfer for transmission
        padding: u17 = 0,
    });
    // byte offset: 8 status register
    pub const SR = MMIO(Address + 0x00000008, u32, packed struct {
        RXNE: bool, // bit offset: 0 desc: Receive buffer not empty
        TXE: bool, // bit offset: 1 desc: Transmit buffer empty
        CHSIDE: bool, // bit offset: 2 desc: Channel side
        UDR: bool, // bit offset: 3 desc: Underrun flag
        CRCERR: bool, // bit offset: 4 desc: CRC error flag
        MODF: bool, // bit offset: 5 desc: Mode fault
        OVR: bool, // bit offset: 6 desc: Overrun flag
        BSY: bool, // bit offset: 7 desc: Busy flag
        TIFRFE: bool, // bit offset: 8 desc: TI frame format error
        FRLVL: u2, // bit offset: 9 desc: FIFO reception level
        FTLVL: u2, // bit offset: 11 desc: FIFO transmission level
        padding: u19 = 0,
    });
    // byte offset: 12 data register
    pub const DR = MMIO(Address + 0x0000000c, u32, packed struct {
        DR: u16, // bit offset: 0 desc: Data register
        padding: u16 = 0,
    });
    // byte offset: 16 CRC polynomial register
    pub const CRCPR = MMIO(Address + 0x00000010, u32, packed struct {
        CRCPOLY: u16, // bit offset: 0 desc: CRC polynomial register
        padding: u16 = 0,
    });
    // byte offset: 20 RX CRC register
    pub const RXCRCR = MMIO(Address + 0x00000014, u32, packed struct {
        RxCRC: u16, // bit offset: 0 desc: Rx CRC register
        padding: u16 = 0,
    });
    // byte offset: 24 TX CRC register
    pub const TXCRCR = MMIO(Address + 0x00000018, u32, packed struct {
        TxCRC: u16, // bit offset: 0 desc: Tx CRC register
        padding: u16 = 0,
    });
    // byte offset: 28 I2S configuration register
    pub const I2SCFGR = MMIO(Address + 0x0000001c, u32, packed struct {
        CHLEN: bool, // bit offset: 0 desc: Channel length (number of bits per audio channel)
        DATLEN: u2, // bit offset: 1 desc: Data length to be transferred
        CKPOL: bool, // bit offset: 3 desc: Steady state clock polarity
        I2SSTD: u2, // bit offset: 4 desc: I2S standard selection
        reserved0: u1 = 0,
        PCMSYNC: bool, // bit offset: 7 desc: PCM frame synchronization
        I2SCFG: u2, // bit offset: 8 desc: I2S configuration mode
        I2SE: bool, // bit offset: 10 desc: I2S Enable
        I2SMOD: bool, // bit offset: 11 desc: I2S mode selection
        padding: u20 = 0,
    });
    // byte offset: 32 I2S prescaler register
    pub const I2SPR = MMIO(Address + 0x00000020, u32, packed struct {
        I2SDIV: u8, // bit offset: 0 desc: I2S Linear prescaler
        ODD: bool, // bit offset: 8 desc: Odd factor for the prescaler
        MCKOE: bool, // bit offset: 9 desc: Master clock output enable
        padding: u22 = 0,
    });
};
pub const I2S3ext = extern struct {
    pub const Address: u32 = 0x40004000;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CPHA: bool, // bit offset: 0 desc: Clock phase
        CPOL: bool, // bit offset: 1 desc: Clock polarity
        MSTR: bool, // bit offset: 2 desc: Master selection
        BR: u3, // bit offset: 3 desc: Baud rate control
        SPE: bool, // bit offset: 6 desc: SPI enable
        LSBFIRST: bool, // bit offset: 7 desc: Frame format
        SSI: bool, // bit offset: 8 desc: Internal slave select
        SSM: bool, // bit offset: 9 desc: Software slave management
        RXONLY: bool, // bit offset: 10 desc: Receive only
        CRCL: bool, // bit offset: 11 desc: CRC length
        CRCNEXT: bool, // bit offset: 12 desc: CRC transfer next
        CRCEN: bool, // bit offset: 13 desc: Hardware CRC calculation enable
        BIDIOE: bool, // bit offset: 14 desc: Output enable in bidirectional mode
        BIDIMODE: bool, // bit offset: 15 desc: Bidirectional data mode enable
        padding: u16 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        RXDMAEN: bool, // bit offset: 0 desc: Rx buffer DMA enable
        TXDMAEN: bool, // bit offset: 1 desc: Tx buffer DMA enable
        SSOE: bool, // bit offset: 2 desc: SS output enable
        NSSP: bool, // bit offset: 3 desc: NSS pulse management
        FRF: bool, // bit offset: 4 desc: Frame format
        ERRIE: bool, // bit offset: 5 desc: Error interrupt enable
        RXNEIE: bool, // bit offset: 6 desc: RX buffer not empty interrupt enable
        TXEIE: bool, // bit offset: 7 desc: Tx buffer empty interrupt enable
        DS: u4, // bit offset: 8 desc: Data size
        FRXTH: bool, // bit offset: 12 desc: FIFO reception threshold
        LDMA_RX: bool, // bit offset: 13 desc: Last DMA transfer for reception
        LDMA_TX: bool, // bit offset: 14 desc: Last DMA transfer for transmission
        padding: u17 = 0,
    });
    // byte offset: 8 status register
    pub const SR = MMIO(Address + 0x00000008, u32, packed struct {
        RXNE: bool, // bit offset: 0 desc: Receive buffer not empty
        TXE: bool, // bit offset: 1 desc: Transmit buffer empty
        CHSIDE: bool, // bit offset: 2 desc: Channel side
        UDR: bool, // bit offset: 3 desc: Underrun flag
        CRCERR: bool, // bit offset: 4 desc: CRC error flag
        MODF: bool, // bit offset: 5 desc: Mode fault
        OVR: bool, // bit offset: 6 desc: Overrun flag
        BSY: bool, // bit offset: 7 desc: Busy flag
        TIFRFE: bool, // bit offset: 8 desc: TI frame format error
        FRLVL: u2, // bit offset: 9 desc: FIFO reception level
        FTLVL: u2, // bit offset: 11 desc: FIFO transmission level
        padding: u19 = 0,
    });
    // byte offset: 12 data register
    pub const DR = MMIO(Address + 0x0000000c, u32, packed struct {
        DR: u16, // bit offset: 0 desc: Data register
        padding: u16 = 0,
    });
    // byte offset: 16 CRC polynomial register
    pub const CRCPR = MMIO(Address + 0x00000010, u32, packed struct {
        CRCPOLY: u16, // bit offset: 0 desc: CRC polynomial register
        padding: u16 = 0,
    });
    // byte offset: 20 RX CRC register
    pub const RXCRCR = MMIO(Address + 0x00000014, u32, packed struct {
        RxCRC: u16, // bit offset: 0 desc: Rx CRC register
        padding: u16 = 0,
    });
    // byte offset: 24 TX CRC register
    pub const TXCRCR = MMIO(Address + 0x00000018, u32, packed struct {
        TxCRC: u16, // bit offset: 0 desc: Tx CRC register
        padding: u16 = 0,
    });
    // byte offset: 28 I2S configuration register
    pub const I2SCFGR = MMIO(Address + 0x0000001c, u32, packed struct {
        CHLEN: bool, // bit offset: 0 desc: Channel length (number of bits per audio channel)
        DATLEN: u2, // bit offset: 1 desc: Data length to be transferred
        CKPOL: bool, // bit offset: 3 desc: Steady state clock polarity
        I2SSTD: u2, // bit offset: 4 desc: I2S standard selection
        reserved0: u1 = 0,
        PCMSYNC: bool, // bit offset: 7 desc: PCM frame synchronization
        I2SCFG: u2, // bit offset: 8 desc: I2S configuration mode
        I2SE: bool, // bit offset: 10 desc: I2S Enable
        I2SMOD: bool, // bit offset: 11 desc: I2S mode selection
        padding: u20 = 0,
    });
    // byte offset: 32 I2S prescaler register
    pub const I2SPR = MMIO(Address + 0x00000020, u32, packed struct {
        I2SDIV: u8, // bit offset: 0 desc: I2S Linear prescaler
        ODD: bool, // bit offset: 8 desc: Odd factor for the prescaler
        MCKOE: bool, // bit offset: 9 desc: Master clock output enable
        padding: u22 = 0,
    });
};
pub const SPI4 = extern struct {
    pub const Address: u32 = 0x40013c00;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CPHA: bool, // bit offset: 0 desc: Clock phase
        CPOL: bool, // bit offset: 1 desc: Clock polarity
        MSTR: bool, // bit offset: 2 desc: Master selection
        BR: u3, // bit offset: 3 desc: Baud rate control
        SPE: bool, // bit offset: 6 desc: SPI enable
        LSBFIRST: bool, // bit offset: 7 desc: Frame format
        SSI: bool, // bit offset: 8 desc: Internal slave select
        SSM: bool, // bit offset: 9 desc: Software slave management
        RXONLY: bool, // bit offset: 10 desc: Receive only
        CRCL: bool, // bit offset: 11 desc: CRC length
        CRCNEXT: bool, // bit offset: 12 desc: CRC transfer next
        CRCEN: bool, // bit offset: 13 desc: Hardware CRC calculation enable
        BIDIOE: bool, // bit offset: 14 desc: Output enable in bidirectional mode
        BIDIMODE: bool, // bit offset: 15 desc: Bidirectional data mode enable
        padding: u16 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        RXDMAEN: bool, // bit offset: 0 desc: Rx buffer DMA enable
        TXDMAEN: bool, // bit offset: 1 desc: Tx buffer DMA enable
        SSOE: bool, // bit offset: 2 desc: SS output enable
        NSSP: bool, // bit offset: 3 desc: NSS pulse management
        FRF: bool, // bit offset: 4 desc: Frame format
        ERRIE: bool, // bit offset: 5 desc: Error interrupt enable
        RXNEIE: bool, // bit offset: 6 desc: RX buffer not empty interrupt enable
        TXEIE: bool, // bit offset: 7 desc: Tx buffer empty interrupt enable
        DS: u4, // bit offset: 8 desc: Data size
        FRXTH: bool, // bit offset: 12 desc: FIFO reception threshold
        LDMA_RX: bool, // bit offset: 13 desc: Last DMA transfer for reception
        LDMA_TX: bool, // bit offset: 14 desc: Last DMA transfer for transmission
        padding: u17 = 0,
    });
    // byte offset: 8 status register
    pub const SR = MMIO(Address + 0x00000008, u32, packed struct {
        RXNE: bool, // bit offset: 0 desc: Receive buffer not empty
        TXE: bool, // bit offset: 1 desc: Transmit buffer empty
        CHSIDE: bool, // bit offset: 2 desc: Channel side
        UDR: bool, // bit offset: 3 desc: Underrun flag
        CRCERR: bool, // bit offset: 4 desc: CRC error flag
        MODF: bool, // bit offset: 5 desc: Mode fault
        OVR: bool, // bit offset: 6 desc: Overrun flag
        BSY: bool, // bit offset: 7 desc: Busy flag
        TIFRFE: bool, // bit offset: 8 desc: TI frame format error
        FRLVL: u2, // bit offset: 9 desc: FIFO reception level
        FTLVL: u2, // bit offset: 11 desc: FIFO transmission level
        padding: u19 = 0,
    });
    // byte offset: 12 data register
    pub const DR = MMIO(Address + 0x0000000c, u32, packed struct {
        DR: u16, // bit offset: 0 desc: Data register
        padding: u16 = 0,
    });
    // byte offset: 16 CRC polynomial register
    pub const CRCPR = MMIO(Address + 0x00000010, u32, packed struct {
        CRCPOLY: u16, // bit offset: 0 desc: CRC polynomial register
        padding: u16 = 0,
    });
    // byte offset: 20 RX CRC register
    pub const RXCRCR = MMIO(Address + 0x00000014, u32, packed struct {
        RxCRC: u16, // bit offset: 0 desc: Rx CRC register
        padding: u16 = 0,
    });
    // byte offset: 24 TX CRC register
    pub const TXCRCR = MMIO(Address + 0x00000018, u32, packed struct {
        TxCRC: u16, // bit offset: 0 desc: Tx CRC register
        padding: u16 = 0,
    });
    // byte offset: 28 I2S configuration register
    pub const I2SCFGR = MMIO(Address + 0x0000001c, u32, packed struct {
        CHLEN: bool, // bit offset: 0 desc: Channel length (number of bits per audio channel)
        DATLEN: u2, // bit offset: 1 desc: Data length to be transferred
        CKPOL: bool, // bit offset: 3 desc: Steady state clock polarity
        I2SSTD: u2, // bit offset: 4 desc: I2S standard selection
        reserved0: u1 = 0,
        PCMSYNC: bool, // bit offset: 7 desc: PCM frame synchronization
        I2SCFG: u2, // bit offset: 8 desc: I2S configuration mode
        I2SE: bool, // bit offset: 10 desc: I2S Enable
        I2SMOD: bool, // bit offset: 11 desc: I2S mode selection
        padding: u20 = 0,
    });
    // byte offset: 32 I2S prescaler register
    pub const I2SPR = MMIO(Address + 0x00000020, u32, packed struct {
        I2SDIV: u8, // bit offset: 0 desc: I2S Linear prescaler
        ODD: bool, // bit offset: 8 desc: Odd factor for the prescaler
        MCKOE: bool, // bit offset: 9 desc: Master clock output enable
        padding: u22 = 0,
    });
};
pub const EXTI = extern struct {
    pub const Address: u32 = 0x40010400;
    // byte offset: 0 Interrupt mask register
    pub const IMR1 = MMIO(Address + 0x00000000, u32, packed struct {
        MR0: bool, // bit offset: 0 desc: Interrupt Mask on line 0
        MR1: bool, // bit offset: 1 desc: Interrupt Mask on line 1
        MR2: bool, // bit offset: 2 desc: Interrupt Mask on line 2
        MR3: bool, // bit offset: 3 desc: Interrupt Mask on line 3
        MR4: bool, // bit offset: 4 desc: Interrupt Mask on line 4
        MR5: bool, // bit offset: 5 desc: Interrupt Mask on line 5
        MR6: bool, // bit offset: 6 desc: Interrupt Mask on line 6
        MR7: bool, // bit offset: 7 desc: Interrupt Mask on line 7
        MR8: bool, // bit offset: 8 desc: Interrupt Mask on line 8
        MR9: bool, // bit offset: 9 desc: Interrupt Mask on line 9
        MR10: bool, // bit offset: 10 desc: Interrupt Mask on line 10
        MR11: bool, // bit offset: 11 desc: Interrupt Mask on line 11
        MR12: bool, // bit offset: 12 desc: Interrupt Mask on line 12
        MR13: bool, // bit offset: 13 desc: Interrupt Mask on line 13
        MR14: bool, // bit offset: 14 desc: Interrupt Mask on line 14
        MR15: bool, // bit offset: 15 desc: Interrupt Mask on line 15
        MR16: bool, // bit offset: 16 desc: Interrupt Mask on line 16
        MR17: bool, // bit offset: 17 desc: Interrupt Mask on line 17
        MR18: bool, // bit offset: 18 desc: Interrupt Mask on line 18
        MR19: bool, // bit offset: 19 desc: Interrupt Mask on line 19
        MR20: bool, // bit offset: 20 desc: Interrupt Mask on line 20
        MR21: bool, // bit offset: 21 desc: Interrupt Mask on line 21
        MR22: bool, // bit offset: 22 desc: Interrupt Mask on line 22
        MR23: bool, // bit offset: 23 desc: Interrupt Mask on line 23
        MR24: bool, // bit offset: 24 desc: Interrupt Mask on line 24
        MR25: bool, // bit offset: 25 desc: Interrupt Mask on line 25
        MR26: bool, // bit offset: 26 desc: Interrupt Mask on line 26
        MR27: bool, // bit offset: 27 desc: Interrupt Mask on line 27
        MR28: bool, // bit offset: 28 desc: Interrupt Mask on line 28
        MR29: bool, // bit offset: 29 desc: Interrupt Mask on line 29
        MR30: bool, // bit offset: 30 desc: Interrupt Mask on line 30
        MR31: bool, // bit offset: 31 desc: Interrupt Mask on line 31
    });
    // byte offset: 4 Event mask register
    pub const EMR1 = MMIO(Address + 0x00000004, u32, packed struct {
        MR0: bool, // bit offset: 0 desc: Event Mask on line 0
        MR1: bool, // bit offset: 1 desc: Event Mask on line 1
        MR2: bool, // bit offset: 2 desc: Event Mask on line 2
        MR3: bool, // bit offset: 3 desc: Event Mask on line 3
        MR4: bool, // bit offset: 4 desc: Event Mask on line 4
        MR5: bool, // bit offset: 5 desc: Event Mask on line 5
        MR6: bool, // bit offset: 6 desc: Event Mask on line 6
        MR7: bool, // bit offset: 7 desc: Event Mask on line 7
        MR8: bool, // bit offset: 8 desc: Event Mask on line 8
        MR9: bool, // bit offset: 9 desc: Event Mask on line 9
        MR10: bool, // bit offset: 10 desc: Event Mask on line 10
        MR11: bool, // bit offset: 11 desc: Event Mask on line 11
        MR12: bool, // bit offset: 12 desc: Event Mask on line 12
        MR13: bool, // bit offset: 13 desc: Event Mask on line 13
        MR14: bool, // bit offset: 14 desc: Event Mask on line 14
        MR15: bool, // bit offset: 15 desc: Event Mask on line 15
        MR16: bool, // bit offset: 16 desc: Event Mask on line 16
        MR17: bool, // bit offset: 17 desc: Event Mask on line 17
        MR18: bool, // bit offset: 18 desc: Event Mask on line 18
        MR19: bool, // bit offset: 19 desc: Event Mask on line 19
        MR20: bool, // bit offset: 20 desc: Event Mask on line 20
        MR21: bool, // bit offset: 21 desc: Event Mask on line 21
        MR22: bool, // bit offset: 22 desc: Event Mask on line 22
        MR23: bool, // bit offset: 23 desc: Event Mask on line 23
        MR24: bool, // bit offset: 24 desc: Event Mask on line 24
        MR25: bool, // bit offset: 25 desc: Event Mask on line 25
        MR26: bool, // bit offset: 26 desc: Event Mask on line 26
        MR27: bool, // bit offset: 27 desc: Event Mask on line 27
        MR28: bool, // bit offset: 28 desc: Event Mask on line 28
        MR29: bool, // bit offset: 29 desc: Event Mask on line 29
        MR30: bool, // bit offset: 30 desc: Event Mask on line 30
        MR31: bool, // bit offset: 31 desc: Event Mask on line 31
    });
    // byte offset: 8 Rising Trigger selection register
    pub const RTSR1 = MMIO(Address + 0x00000008, u32, packed struct {
        TR0: bool, // bit offset: 0 desc: Rising trigger event configuration of line 0
        TR1: bool, // bit offset: 1 desc: Rising trigger event configuration of line 1
        TR2: bool, // bit offset: 2 desc: Rising trigger event configuration of line 2
        TR3: bool, // bit offset: 3 desc: Rising trigger event configuration of line 3
        TR4: bool, // bit offset: 4 desc: Rising trigger event configuration of line 4
        TR5: bool, // bit offset: 5 desc: Rising trigger event configuration of line 5
        TR6: bool, // bit offset: 6 desc: Rising trigger event configuration of line 6
        TR7: bool, // bit offset: 7 desc: Rising trigger event configuration of line 7
        TR8: bool, // bit offset: 8 desc: Rising trigger event configuration of line 8
        TR9: bool, // bit offset: 9 desc: Rising trigger event configuration of line 9
        TR10: bool, // bit offset: 10 desc: Rising trigger event configuration of line 10
        TR11: bool, // bit offset: 11 desc: Rising trigger event configuration of line 11
        TR12: bool, // bit offset: 12 desc: Rising trigger event configuration of line 12
        TR13: bool, // bit offset: 13 desc: Rising trigger event configuration of line 13
        TR14: bool, // bit offset: 14 desc: Rising trigger event configuration of line 14
        TR15: bool, // bit offset: 15 desc: Rising trigger event configuration of line 15
        TR16: bool, // bit offset: 16 desc: Rising trigger event configuration of line 16
        TR17: bool, // bit offset: 17 desc: Rising trigger event configuration of line 17
        TR18: bool, // bit offset: 18 desc: Rising trigger event configuration of line 18
        TR19: bool, // bit offset: 19 desc: Rising trigger event configuration of line 19
        TR20: bool, // bit offset: 20 desc: Rising trigger event configuration of line 20
        TR21: bool, // bit offset: 21 desc: Rising trigger event configuration of line 21
        TR22: bool, // bit offset: 22 desc: Rising trigger event configuration of line 22
        reserved0: u6 = 0,
        TR29: bool, // bit offset: 29 desc: Rising trigger event configuration of line 29
        TR30: bool, // bit offset: 30 desc: Rising trigger event configuration of line 30
        TR31: bool, // bit offset: 31 desc: Rising trigger event configuration of line 31
    });
    // byte offset: 12 Falling Trigger selection register
    pub const FTSR1 = MMIO(Address + 0x0000000c, u32, packed struct {
        TR0: bool, // bit offset: 0 desc: Falling trigger event configuration of line 0
        TR1: bool, // bit offset: 1 desc: Falling trigger event configuration of line 1
        TR2: bool, // bit offset: 2 desc: Falling trigger event configuration of line 2
        TR3: bool, // bit offset: 3 desc: Falling trigger event configuration of line 3
        TR4: bool, // bit offset: 4 desc: Falling trigger event configuration of line 4
        TR5: bool, // bit offset: 5 desc: Falling trigger event configuration of line 5
        TR6: bool, // bit offset: 6 desc: Falling trigger event configuration of line 6
        TR7: bool, // bit offset: 7 desc: Falling trigger event configuration of line 7
        TR8: bool, // bit offset: 8 desc: Falling trigger event configuration of line 8
        TR9: bool, // bit offset: 9 desc: Falling trigger event configuration of line 9
        TR10: bool, // bit offset: 10 desc: Falling trigger event configuration of line 10
        TR11: bool, // bit offset: 11 desc: Falling trigger event configuration of line 11
        TR12: bool, // bit offset: 12 desc: Falling trigger event configuration of line 12
        TR13: bool, // bit offset: 13 desc: Falling trigger event configuration of line 13
        TR14: bool, // bit offset: 14 desc: Falling trigger event configuration of line 14
        TR15: bool, // bit offset: 15 desc: Falling trigger event configuration of line 15
        TR16: bool, // bit offset: 16 desc: Falling trigger event configuration of line 16
        TR17: bool, // bit offset: 17 desc: Falling trigger event configuration of line 17
        TR18: bool, // bit offset: 18 desc: Falling trigger event configuration of line 18
        TR19: bool, // bit offset: 19 desc: Falling trigger event configuration of line 19
        TR20: bool, // bit offset: 20 desc: Falling trigger event configuration of line 20
        TR21: bool, // bit offset: 21 desc: Falling trigger event configuration of line 21
        TR22: bool, // bit offset: 22 desc: Falling trigger event configuration of line 22
        reserved0: u6 = 0,
        TR29: bool, // bit offset: 29 desc: Falling trigger event configuration of line 29
        TR30: bool, // bit offset: 30 desc: Falling trigger event configuration of line 30.
        TR31: bool, // bit offset: 31 desc: Falling trigger event configuration of line 31
    });
    // byte offset: 16 Software interrupt event register
    pub const SWIER1 = MMIO(Address + 0x00000010, u32, packed struct {
        SWIER0: bool, // bit offset: 0 desc: Software Interrupt on line 0
        SWIER1: bool, // bit offset: 1 desc: Software Interrupt on line 1
        SWIER2: bool, // bit offset: 2 desc: Software Interrupt on line 2
        SWIER3: bool, // bit offset: 3 desc: Software Interrupt on line 3
        SWIER4: bool, // bit offset: 4 desc: Software Interrupt on line 4
        SWIER5: bool, // bit offset: 5 desc: Software Interrupt on line 5
        SWIER6: bool, // bit offset: 6 desc: Software Interrupt on line 6
        SWIER7: bool, // bit offset: 7 desc: Software Interrupt on line 7
        SWIER8: bool, // bit offset: 8 desc: Software Interrupt on line 8
        SWIER9: bool, // bit offset: 9 desc: Software Interrupt on line 9
        SWIER10: bool, // bit offset: 10 desc: Software Interrupt on line 10
        SWIER11: bool, // bit offset: 11 desc: Software Interrupt on line 11
        SWIER12: bool, // bit offset: 12 desc: Software Interrupt on line 12
        SWIER13: bool, // bit offset: 13 desc: Software Interrupt on line 13
        SWIER14: bool, // bit offset: 14 desc: Software Interrupt on line 14
        SWIER15: bool, // bit offset: 15 desc: Software Interrupt on line 15
        SWIER16: bool, // bit offset: 16 desc: Software Interrupt on line 16
        SWIER17: bool, // bit offset: 17 desc: Software Interrupt on line 17
        SWIER18: bool, // bit offset: 18 desc: Software Interrupt on line 18
        SWIER19: bool, // bit offset: 19 desc: Software Interrupt on line 19
        SWIER20: bool, // bit offset: 20 desc: Software Interrupt on line 20
        SWIER21: bool, // bit offset: 21 desc: Software Interrupt on line 21
        SWIER22: bool, // bit offset: 22 desc: Software Interrupt on line 22
        reserved0: u6 = 0,
        SWIER29: bool, // bit offset: 29 desc: Software Interrupt on line 29
        SWIER30: bool, // bit offset: 30 desc: Software Interrupt on line 309
        SWIER31: bool, // bit offset: 31 desc: Software Interrupt on line 319
    });
    // byte offset: 20 Pending register
    pub const PR1 = MMIO(Address + 0x00000014, u32, packed struct {
        PR0: bool, // bit offset: 0 desc: Pending bit 0
        PR1: bool, // bit offset: 1 desc: Pending bit 1
        PR2: bool, // bit offset: 2 desc: Pending bit 2
        PR3: bool, // bit offset: 3 desc: Pending bit 3
        PR4: bool, // bit offset: 4 desc: Pending bit 4
        PR5: bool, // bit offset: 5 desc: Pending bit 5
        PR6: bool, // bit offset: 6 desc: Pending bit 6
        PR7: bool, // bit offset: 7 desc: Pending bit 7
        PR8: bool, // bit offset: 8 desc: Pending bit 8
        PR9: bool, // bit offset: 9 desc: Pending bit 9
        PR10: bool, // bit offset: 10 desc: Pending bit 10
        PR11: bool, // bit offset: 11 desc: Pending bit 11
        PR12: bool, // bit offset: 12 desc: Pending bit 12
        PR13: bool, // bit offset: 13 desc: Pending bit 13
        PR14: bool, // bit offset: 14 desc: Pending bit 14
        PR15: bool, // bit offset: 15 desc: Pending bit 15
        PR16: bool, // bit offset: 16 desc: Pending bit 16
        PR17: bool, // bit offset: 17 desc: Pending bit 17
        PR18: bool, // bit offset: 18 desc: Pending bit 18
        PR19: bool, // bit offset: 19 desc: Pending bit 19
        PR20: bool, // bit offset: 20 desc: Pending bit 20
        PR21: bool, // bit offset: 21 desc: Pending bit 21
        PR22: bool, // bit offset: 22 desc: Pending bit 22
        reserved0: u6 = 0,
        PR29: bool, // bit offset: 29 desc: Pending bit 29
        PR30: bool, // bit offset: 30 desc: Pending bit 30
        PR31: bool, // bit offset: 31 desc: Pending bit 31
    });
    // byte offset: 24 Interrupt mask register
    pub const IMR2 = MMIO(Address + 0x00000018, u32, packed struct {
        MR32: bool, // bit offset: 0 desc: Interrupt Mask on external/internal line 32
        MR33: bool, // bit offset: 1 desc: Interrupt Mask on external/internal line 33
        MR34: bool, // bit offset: 2 desc: Interrupt Mask on external/internal line 34
        MR35: bool, // bit offset: 3 desc: Interrupt Mask on external/internal line 35
        padding: u28 = 0,
    });
    // byte offset: 28 Event mask register
    pub const EMR2 = MMIO(Address + 0x0000001c, u32, packed struct {
        MR32: bool, // bit offset: 0 desc: Event mask on external/internal line 32
        MR33: bool, // bit offset: 1 desc: Event mask on external/internal line 33
        MR34: bool, // bit offset: 2 desc: Event mask on external/internal line 34
        MR35: bool, // bit offset: 3 desc: Event mask on external/internal line 35
        padding: u28 = 0,
    });
    // byte offset: 32 Rising Trigger selection register
    pub const RTSR2 = MMIO(Address + 0x00000020, u32, packed struct {
        TR32: bool, // bit offset: 0 desc: Rising trigger event configuration bit of line 32
        TR33: bool, // bit offset: 1 desc: Rising trigger event configuration bit of line 33
        padding: u30 = 0,
    });
    // byte offset: 36 Falling Trigger selection register
    pub const FTSR2 = MMIO(Address + 0x00000024, u32, packed struct {
        TR32: bool, // bit offset: 0 desc: Falling trigger event configuration bit of line 32
        TR33: bool, // bit offset: 1 desc: Falling trigger event configuration bit of line 33
        padding: u30 = 0,
    });
    // byte offset: 40 Software interrupt event register
    pub const SWIER2 = MMIO(Address + 0x00000028, u32, packed struct {
        SWIER32: bool, // bit offset: 0 desc: Software interrupt on line 32
        SWIER33: bool, // bit offset: 1 desc: Software interrupt on line 33
        padding: u30 = 0,
    });
    // byte offset: 44 Pending register
    pub const PR2 = MMIO(Address + 0x0000002c, u32, packed struct {
        PR32: bool, // bit offset: 0 desc: Pending bit on line 32
        PR33: bool, // bit offset: 1 desc: Pending bit on line 33
        padding: u30 = 0,
    });
};
pub const PWR = extern struct {
    pub const Address: u32 = 0x40007000;
    // byte offset: 0 power control register
    pub const CR = MMIO(Address + 0x00000000, u32, packed struct {
        LPDS: bool, // bit offset: 0 desc: Low-power deep sleep
        PDDS: bool, // bit offset: 1 desc: Power down deepsleep
        CWUF: bool, // bit offset: 2 desc: Clear wakeup flag
        CSBF: bool, // bit offset: 3 desc: Clear standby flag
        PVDE: bool, // bit offset: 4 desc: Power voltage detector enable
        PLS: u3, // bit offset: 5 desc: PVD level selection
        DBP: bool, // bit offset: 8 desc: Disable backup domain write protection
        padding: u23 = 0,
    });
    // byte offset: 4 power control/status register
    pub const CSR = MMIO(Address + 0x00000004, u32, packed struct {
        WUF: bool, // bit offset: 0 desc: Wakeup flag
        SBF: bool, // bit offset: 1 desc: Standby flag
        PVDO: bool, // bit offset: 2 desc: PVD output
        reserved0: u5 = 0,
        EWUP1: bool, // bit offset: 8 desc: Enable WKUP1 pin
        EWUP2: bool, // bit offset: 9 desc: Enable WKUP2 pin
        padding: u22 = 0,
    });
};
pub const CAN = extern struct {
    pub const Address: u32 = 0x40006400;
    // byte offset: 0 master control register
    pub const MCR = MMIO(Address + 0x00000000, u32, packed struct {
        INRQ: bool, // bit offset: 0 desc: INRQ
        SLEEP: bool, // bit offset: 1 desc: SLEEP
        TXFP: bool, // bit offset: 2 desc: TXFP
        RFLM: bool, // bit offset: 3 desc: RFLM
        NART: bool, // bit offset: 4 desc: NART
        AWUM: bool, // bit offset: 5 desc: AWUM
        ABOM: bool, // bit offset: 6 desc: ABOM
        TTCM: bool, // bit offset: 7 desc: TTCM
        reserved0: u7 = 0,
        RESET: bool, // bit offset: 15 desc: RESET
        DBF: bool, // bit offset: 16 desc: DBF
        padding: u15 = 0,
    });
    // byte offset: 4 master status register
    pub const MSR = MMIO(Address + 0x00000004, u32, packed struct {
        INAK: bool, // bit offset: 0 desc: INAK
        SLAK: bool, // bit offset: 1 desc: SLAK
        ERRI: bool, // bit offset: 2 desc: ERRI
        WKUI: bool, // bit offset: 3 desc: WKUI
        SLAKI: bool, // bit offset: 4 desc: SLAKI
        reserved0: u3 = 0,
        TXM: bool, // bit offset: 8 desc: TXM
        RXM: bool, // bit offset: 9 desc: RXM
        SAMP: bool, // bit offset: 10 desc: SAMP
        RX: bool, // bit offset: 11 desc: RX
        padding: u20 = 0,
    });
    // byte offset: 8 transmit status register
    pub const TSR = MMIO(Address + 0x00000008, u32, packed struct {
        RQCP0: bool, // bit offset: 0 desc: RQCP0
        TXOK0: bool, // bit offset: 1 desc: TXOK0
        ALST0: bool, // bit offset: 2 desc: ALST0
        TERR0: bool, // bit offset: 3 desc: TERR0
        reserved0: u3 = 0,
        ABRQ0: bool, // bit offset: 7 desc: ABRQ0
        RQCP1: bool, // bit offset: 8 desc: RQCP1
        TXOK1: bool, // bit offset: 9 desc: TXOK1
        ALST1: bool, // bit offset: 10 desc: ALST1
        TERR1: bool, // bit offset: 11 desc: TERR1
        reserved1: u3 = 0,
        ABRQ1: bool, // bit offset: 15 desc: ABRQ1
        RQCP2: bool, // bit offset: 16 desc: RQCP2
        TXOK2: bool, // bit offset: 17 desc: TXOK2
        ALST2: bool, // bit offset: 18 desc: ALST2
        TERR2: bool, // bit offset: 19 desc: TERR2
        reserved2: u3 = 0,
        ABRQ2: bool, // bit offset: 23 desc: ABRQ2
        CODE: u2, // bit offset: 24 desc: CODE
        TME0: bool, // bit offset: 26 desc: Lowest priority flag for mailbox 0
        TME1: bool, // bit offset: 27 desc: Lowest priority flag for mailbox 1
        TME2: bool, // bit offset: 28 desc: Lowest priority flag for mailbox 2
        LOW0: bool, // bit offset: 29 desc: Lowest priority flag for mailbox 0
        LOW1: bool, // bit offset: 30 desc: Lowest priority flag for mailbox 1
        LOW2: bool, // bit offset: 31 desc: Lowest priority flag for mailbox 2
    });
    // byte offset: 12 receive FIFO 0 register
    pub const RF0R = MMIO(Address + 0x0000000c, u32, packed struct {
        FMP0: u2, // bit offset: 0 desc: FMP0
        reserved0: u1 = 0,
        FULL0: bool, // bit offset: 3 desc: FULL0
        FOVR0: bool, // bit offset: 4 desc: FOVR0
        RFOM0: bool, // bit offset: 5 desc: RFOM0
        padding: u26 = 0,
    });
    // byte offset: 16 receive FIFO 1 register
    pub const RF1R = MMIO(Address + 0x00000010, u32, packed struct {
        FMP1: u2, // bit offset: 0 desc: FMP1
        reserved0: u1 = 0,
        FULL1: bool, // bit offset: 3 desc: FULL1
        FOVR1: bool, // bit offset: 4 desc: FOVR1
        RFOM1: bool, // bit offset: 5 desc: RFOM1
        padding: u26 = 0,
    });
    // byte offset: 20 interrupt enable register
    pub const IER = MMIO(Address + 0x00000014, u32, packed struct {
        TMEIE: bool, // bit offset: 0 desc: TMEIE
        FMPIE0: bool, // bit offset: 1 desc: FMPIE0
        FFIE0: bool, // bit offset: 2 desc: FFIE0
        FOVIE0: bool, // bit offset: 3 desc: FOVIE0
        FMPIE1: bool, // bit offset: 4 desc: FMPIE1
        FFIE1: bool, // bit offset: 5 desc: FFIE1
        FOVIE1: bool, // bit offset: 6 desc: FOVIE1
        reserved0: u1 = 0,
        EWGIE: bool, // bit offset: 8 desc: EWGIE
        EPVIE: bool, // bit offset: 9 desc: EPVIE
        BOFIE: bool, // bit offset: 10 desc: BOFIE
        LECIE: bool, // bit offset: 11 desc: LECIE
        reserved1: u3 = 0,
        ERRIE: bool, // bit offset: 15 desc: ERRIE
        WKUIE: bool, // bit offset: 16 desc: WKUIE
        SLKIE: bool, // bit offset: 17 desc: SLKIE
        padding: u14 = 0,
    });
    // byte offset: 24 error status register
    pub const ESR = MMIO(Address + 0x00000018, u32, packed struct {
        EWGF: bool, // bit offset: 0 desc: EWGF
        EPVF: bool, // bit offset: 1 desc: EPVF
        BOFF: bool, // bit offset: 2 desc: BOFF
        reserved0: u1 = 0,
        LEC: u3, // bit offset: 4 desc: LEC
        reserved1: u9 = 0,
        TEC: u8, // bit offset: 16 desc: TEC
        REC: u8, // bit offset: 24 desc: REC
    });
    // byte offset: 28 bit timing register
    pub const BTR = MMIO(Address + 0x0000001c, u32, packed struct {
        BRP: u10, // bit offset: 0 desc: BRP
        reserved0: u6 = 0,
        TS1: u4, // bit offset: 16 desc: TS1
        TS2: u3, // bit offset: 20 desc: TS2
        reserved1: u1 = 0,
        SJW: u2, // bit offset: 24 desc: SJW
        reserved2: u4 = 0,
        LBKM: bool, // bit offset: 30 desc: LBKM
        SILM: bool, // bit offset: 31 desc: SILM
    });
    // byte offset: 384 TX mailbox identifier register
    pub const TI0R = MMIO(Address + 0x00000180, u32, packed struct {
        TXRQ: bool, // bit offset: 0 desc: TXRQ
        RTR: bool, // bit offset: 1 desc: RTR
        IDE: bool, // bit offset: 2 desc: IDE
        EXID: u18, // bit offset: 3 desc: EXID
        STID: u11, // bit offset: 21 desc: STID
    });
    // byte offset: 388 mailbox data length control and time stamp register
    pub const TDT0R = MMIO(Address + 0x00000184, u32, packed struct {
        DLC: u4, // bit offset: 0 desc: DLC
        reserved0: u4 = 0,
        TGT: bool, // bit offset: 8 desc: TGT
        reserved1: u7 = 0,
        TIME: u16, // bit offset: 16 desc: TIME
    });
    // byte offset: 392 mailbox data low register
    pub const TDL0R = MMIO(Address + 0x00000188, u32, packed struct {
        DATA0: u8, // bit offset: 0 desc: DATA0
        DATA1: u8, // bit offset: 8 desc: DATA1
        DATA2: u8, // bit offset: 16 desc: DATA2
        DATA3: u8, // bit offset: 24 desc: DATA3
    });
    // byte offset: 396 mailbox data high register
    pub const TDH0R = MMIO(Address + 0x0000018c, u32, packed struct {
        DATA4: u8, // bit offset: 0 desc: DATA4
        DATA5: u8, // bit offset: 8 desc: DATA5
        DATA6: u8, // bit offset: 16 desc: DATA6
        DATA7: u8, // bit offset: 24 desc: DATA7
    });
    // byte offset: 400 TX mailbox identifier register
    pub const TI1R = MMIO(Address + 0x00000190, u32, packed struct {
        TXRQ: bool, // bit offset: 0 desc: TXRQ
        RTR: bool, // bit offset: 1 desc: RTR
        IDE: bool, // bit offset: 2 desc: IDE
        EXID: u18, // bit offset: 3 desc: EXID
        STID: u11, // bit offset: 21 desc: STID
    });
    // byte offset: 404 mailbox data length control and time stamp register
    pub const TDT1R = MMIO(Address + 0x00000194, u32, packed struct {
        DLC: u4, // bit offset: 0 desc: DLC
        reserved0: u4 = 0,
        TGT: bool, // bit offset: 8 desc: TGT
        reserved1: u7 = 0,
        TIME: u16, // bit offset: 16 desc: TIME
    });
    // byte offset: 408 mailbox data low register
    pub const TDL1R = MMIO(Address + 0x00000198, u32, packed struct {
        DATA0: u8, // bit offset: 0 desc: DATA0
        DATA1: u8, // bit offset: 8 desc: DATA1
        DATA2: u8, // bit offset: 16 desc: DATA2
        DATA3: u8, // bit offset: 24 desc: DATA3
    });
    // byte offset: 412 mailbox data high register
    pub const TDH1R = MMIO(Address + 0x0000019c, u32, packed struct {
        DATA4: u8, // bit offset: 0 desc: DATA4
        DATA5: u8, // bit offset: 8 desc: DATA5
        DATA6: u8, // bit offset: 16 desc: DATA6
        DATA7: u8, // bit offset: 24 desc: DATA7
    });
    // byte offset: 416 TX mailbox identifier register
    pub const TI2R = MMIO(Address + 0x000001a0, u32, packed struct {
        TXRQ: bool, // bit offset: 0 desc: TXRQ
        RTR: bool, // bit offset: 1 desc: RTR
        IDE: bool, // bit offset: 2 desc: IDE
        EXID: u18, // bit offset: 3 desc: EXID
        STID: u11, // bit offset: 21 desc: STID
    });
    // byte offset: 420 mailbox data length control and time stamp register
    pub const TDT2R = MMIO(Address + 0x000001a4, u32, packed struct {
        DLC: u4, // bit offset: 0 desc: DLC
        reserved0: u4 = 0,
        TGT: bool, // bit offset: 8 desc: TGT
        reserved1: u7 = 0,
        TIME: u16, // bit offset: 16 desc: TIME
    });
    // byte offset: 424 mailbox data low register
    pub const TDL2R = MMIO(Address + 0x000001a8, u32, packed struct {
        DATA0: u8, // bit offset: 0 desc: DATA0
        DATA1: u8, // bit offset: 8 desc: DATA1
        DATA2: u8, // bit offset: 16 desc: DATA2
        DATA3: u8, // bit offset: 24 desc: DATA3
    });
    // byte offset: 428 mailbox data high register
    pub const TDH2R = MMIO(Address + 0x000001ac, u32, packed struct {
        DATA4: u8, // bit offset: 0 desc: DATA4
        DATA5: u8, // bit offset: 8 desc: DATA5
        DATA6: u8, // bit offset: 16 desc: DATA6
        DATA7: u8, // bit offset: 24 desc: DATA7
    });
    // byte offset: 432 receive FIFO mailbox identifier register
    pub const RI0R = MMIO(Address + 0x000001b0, u32, packed struct {
        reserved0: u1 = 0,
        RTR: bool, // bit offset: 1 desc: RTR
        IDE: bool, // bit offset: 2 desc: IDE
        EXID: u18, // bit offset: 3 desc: EXID
        STID: u11, // bit offset: 21 desc: STID
    });
    // byte offset: 436 receive FIFO mailbox data length control and time stamp register
    pub const RDT0R = MMIO(Address + 0x000001b4, u32, packed struct {
        DLC: u4, // bit offset: 0 desc: DLC
        reserved0: u4 = 0,
        FMI: u8, // bit offset: 8 desc: FMI
        TIME: u16, // bit offset: 16 desc: TIME
    });
    // byte offset: 440 receive FIFO mailbox data low register
    pub const RDL0R = MMIO(Address + 0x000001b8, u32, packed struct {
        DATA0: u8, // bit offset: 0 desc: DATA0
        DATA1: u8, // bit offset: 8 desc: DATA1
        DATA2: u8, // bit offset: 16 desc: DATA2
        DATA3: u8, // bit offset: 24 desc: DATA3
    });
    // byte offset: 444 receive FIFO mailbox data high register
    pub const RDH0R = MMIO(Address + 0x000001bc, u32, packed struct {
        DATA4: u8, // bit offset: 0 desc: DATA4
        DATA5: u8, // bit offset: 8 desc: DATA5
        DATA6: u8, // bit offset: 16 desc: DATA6
        DATA7: u8, // bit offset: 24 desc: DATA7
    });
    // byte offset: 448 receive FIFO mailbox identifier register
    pub const RI1R = MMIO(Address + 0x000001c0, u32, packed struct {
        reserved0: u1 = 0,
        RTR: bool, // bit offset: 1 desc: RTR
        IDE: bool, // bit offset: 2 desc: IDE
        EXID: u18, // bit offset: 3 desc: EXID
        STID: u11, // bit offset: 21 desc: STID
    });
    // byte offset: 452 receive FIFO mailbox data length control and time stamp register
    pub const RDT1R = MMIO(Address + 0x000001c4, u32, packed struct {
        DLC: u4, // bit offset: 0 desc: DLC
        reserved0: u4 = 0,
        FMI: u8, // bit offset: 8 desc: FMI
        TIME: u16, // bit offset: 16 desc: TIME
    });
    // byte offset: 456 receive FIFO mailbox data low register
    pub const RDL1R = MMIO(Address + 0x000001c8, u32, packed struct {
        DATA0: u8, // bit offset: 0 desc: DATA0
        DATA1: u8, // bit offset: 8 desc: DATA1
        DATA2: u8, // bit offset: 16 desc: DATA2
        DATA3: u8, // bit offset: 24 desc: DATA3
    });
    // byte offset: 460 receive FIFO mailbox data high register
    pub const RDH1R = MMIO(Address + 0x000001cc, u32, packed struct {
        DATA4: u8, // bit offset: 0 desc: DATA4
        DATA5: u8, // bit offset: 8 desc: DATA5
        DATA6: u8, // bit offset: 16 desc: DATA6
        DATA7: u8, // bit offset: 24 desc: DATA7
    });
    // byte offset: 512 filter master register
    pub const FMR = MMIO(Address + 0x00000200, u32, packed struct {
        FINIT: bool, // bit offset: 0 desc: Filter init mode
        reserved0: u7 = 0,
        CAN2SB: u6, // bit offset: 8 desc: CAN2 start bank
        padding: u18 = 0,
    });
    // byte offset: 516 filter mode register
    pub const FM1R = MMIO(Address + 0x00000204, u32, packed struct {
        FBM0: bool, // bit offset: 0 desc: Filter mode
        FBM1: bool, // bit offset: 1 desc: Filter mode
        FBM2: bool, // bit offset: 2 desc: Filter mode
        FBM3: bool, // bit offset: 3 desc: Filter mode
        FBM4: bool, // bit offset: 4 desc: Filter mode
        FBM5: bool, // bit offset: 5 desc: Filter mode
        FBM6: bool, // bit offset: 6 desc: Filter mode
        FBM7: bool, // bit offset: 7 desc: Filter mode
        FBM8: bool, // bit offset: 8 desc: Filter mode
        FBM9: bool, // bit offset: 9 desc: Filter mode
        FBM10: bool, // bit offset: 10 desc: Filter mode
        FBM11: bool, // bit offset: 11 desc: Filter mode
        FBM12: bool, // bit offset: 12 desc: Filter mode
        FBM13: bool, // bit offset: 13 desc: Filter mode
        FBM14: bool, // bit offset: 14 desc: Filter mode
        FBM15: bool, // bit offset: 15 desc: Filter mode
        FBM16: bool, // bit offset: 16 desc: Filter mode
        FBM17: bool, // bit offset: 17 desc: Filter mode
        FBM18: bool, // bit offset: 18 desc: Filter mode
        FBM19: bool, // bit offset: 19 desc: Filter mode
        FBM20: bool, // bit offset: 20 desc: Filter mode
        FBM21: bool, // bit offset: 21 desc: Filter mode
        FBM22: bool, // bit offset: 22 desc: Filter mode
        FBM23: bool, // bit offset: 23 desc: Filter mode
        FBM24: bool, // bit offset: 24 desc: Filter mode
        FBM25: bool, // bit offset: 25 desc: Filter mode
        FBM26: bool, // bit offset: 26 desc: Filter mode
        FBM27: bool, // bit offset: 27 desc: Filter mode
        padding: u4 = 0,
    });
    // byte offset: 524 filter scale register
    pub const FS1R = MMIO(Address + 0x0000020c, u32, packed struct {
        FSC0: bool, // bit offset: 0 desc: Filter scale configuration
        FSC1: bool, // bit offset: 1 desc: Filter scale configuration
        FSC2: bool, // bit offset: 2 desc: Filter scale configuration
        FSC3: bool, // bit offset: 3 desc: Filter scale configuration
        FSC4: bool, // bit offset: 4 desc: Filter scale configuration
        FSC5: bool, // bit offset: 5 desc: Filter scale configuration
        FSC6: bool, // bit offset: 6 desc: Filter scale configuration
        FSC7: bool, // bit offset: 7 desc: Filter scale configuration
        FSC8: bool, // bit offset: 8 desc: Filter scale configuration
        FSC9: bool, // bit offset: 9 desc: Filter scale configuration
        FSC10: bool, // bit offset: 10 desc: Filter scale configuration
        FSC11: bool, // bit offset: 11 desc: Filter scale configuration
        FSC12: bool, // bit offset: 12 desc: Filter scale configuration
        FSC13: bool, // bit offset: 13 desc: Filter scale configuration
        FSC14: bool, // bit offset: 14 desc: Filter scale configuration
        FSC15: bool, // bit offset: 15 desc: Filter scale configuration
        FSC16: bool, // bit offset: 16 desc: Filter scale configuration
        FSC17: bool, // bit offset: 17 desc: Filter scale configuration
        FSC18: bool, // bit offset: 18 desc: Filter scale configuration
        FSC19: bool, // bit offset: 19 desc: Filter scale configuration
        FSC20: bool, // bit offset: 20 desc: Filter scale configuration
        FSC21: bool, // bit offset: 21 desc: Filter scale configuration
        FSC22: bool, // bit offset: 22 desc: Filter scale configuration
        FSC23: bool, // bit offset: 23 desc: Filter scale configuration
        FSC24: bool, // bit offset: 24 desc: Filter scale configuration
        FSC25: bool, // bit offset: 25 desc: Filter scale configuration
        FSC26: bool, // bit offset: 26 desc: Filter scale configuration
        FSC27: bool, // bit offset: 27 desc: Filter scale configuration
        padding: u4 = 0,
    });
    // byte offset: 532 filter FIFO assignment register
    pub const FFA1R = MMIO(Address + 0x00000214, u32, packed struct {
        FFA0: bool, // bit offset: 0 desc: Filter FIFO assignment for filter 0
        FFA1: bool, // bit offset: 1 desc: Filter FIFO assignment for filter 1
        FFA2: bool, // bit offset: 2 desc: Filter FIFO assignment for filter 2
        FFA3: bool, // bit offset: 3 desc: Filter FIFO assignment for filter 3
        FFA4: bool, // bit offset: 4 desc: Filter FIFO assignment for filter 4
        FFA5: bool, // bit offset: 5 desc: Filter FIFO assignment for filter 5
        FFA6: bool, // bit offset: 6 desc: Filter FIFO assignment for filter 6
        FFA7: bool, // bit offset: 7 desc: Filter FIFO assignment for filter 7
        FFA8: bool, // bit offset: 8 desc: Filter FIFO assignment for filter 8
        FFA9: bool, // bit offset: 9 desc: Filter FIFO assignment for filter 9
        FFA10: bool, // bit offset: 10 desc: Filter FIFO assignment for filter 10
        FFA11: bool, // bit offset: 11 desc: Filter FIFO assignment for filter 11
        FFA12: bool, // bit offset: 12 desc: Filter FIFO assignment for filter 12
        FFA13: bool, // bit offset: 13 desc: Filter FIFO assignment for filter 13
        FFA14: bool, // bit offset: 14 desc: Filter FIFO assignment for filter 14
        FFA15: bool, // bit offset: 15 desc: Filter FIFO assignment for filter 15
        FFA16: bool, // bit offset: 16 desc: Filter FIFO assignment for filter 16
        FFA17: bool, // bit offset: 17 desc: Filter FIFO assignment for filter 17
        FFA18: bool, // bit offset: 18 desc: Filter FIFO assignment for filter 18
        FFA19: bool, // bit offset: 19 desc: Filter FIFO assignment for filter 19
        FFA20: bool, // bit offset: 20 desc: Filter FIFO assignment for filter 20
        FFA21: bool, // bit offset: 21 desc: Filter FIFO assignment for filter 21
        FFA22: bool, // bit offset: 22 desc: Filter FIFO assignment for filter 22
        FFA23: bool, // bit offset: 23 desc: Filter FIFO assignment for filter 23
        FFA24: bool, // bit offset: 24 desc: Filter FIFO assignment for filter 24
        FFA25: bool, // bit offset: 25 desc: Filter FIFO assignment for filter 25
        FFA26: bool, // bit offset: 26 desc: Filter FIFO assignment for filter 26
        FFA27: bool, // bit offset: 27 desc: Filter FIFO assignment for filter 27
        padding: u4 = 0,
    });
    // byte offset: 540 CAN filter activation register
    pub const FA1R = MMIO(Address + 0x0000021c, u32, packed struct {
        FACT0: bool, // bit offset: 0 desc: Filter active
        FACT1: bool, // bit offset: 1 desc: Filter active
        FACT2: bool, // bit offset: 2 desc: Filter active
        FACT3: bool, // bit offset: 3 desc: Filter active
        FACT4: bool, // bit offset: 4 desc: Filter active
        FACT5: bool, // bit offset: 5 desc: Filter active
        FACT6: bool, // bit offset: 6 desc: Filter active
        FACT7: bool, // bit offset: 7 desc: Filter active
        FACT8: bool, // bit offset: 8 desc: Filter active
        FACT9: bool, // bit offset: 9 desc: Filter active
        FACT10: bool, // bit offset: 10 desc: Filter active
        FACT11: bool, // bit offset: 11 desc: Filter active
        FACT12: bool, // bit offset: 12 desc: Filter active
        FACT13: bool, // bit offset: 13 desc: Filter active
        FACT14: bool, // bit offset: 14 desc: Filter active
        FACT15: bool, // bit offset: 15 desc: Filter active
        FACT16: bool, // bit offset: 16 desc: Filter active
        FACT17: bool, // bit offset: 17 desc: Filter active
        FACT18: bool, // bit offset: 18 desc: Filter active
        FACT19: bool, // bit offset: 19 desc: Filter active
        FACT20: bool, // bit offset: 20 desc: Filter active
        FACT21: bool, // bit offset: 21 desc: Filter active
        FACT22: bool, // bit offset: 22 desc: Filter active
        FACT23: bool, // bit offset: 23 desc: Filter active
        FACT24: bool, // bit offset: 24 desc: Filter active
        FACT25: bool, // bit offset: 25 desc: Filter active
        FACT26: bool, // bit offset: 26 desc: Filter active
        FACT27: bool, // bit offset: 27 desc: Filter active
        padding: u4 = 0,
    });
    // byte offset: 576 Filter bank 0 register 1
    pub const F0R1 = MMIO(Address + 0x00000240, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 580 Filter bank 0 register 2
    pub const F0R2 = MMIO(Address + 0x00000244, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 584 Filter bank 1 register 1
    pub const F1R1 = MMIO(Address + 0x00000248, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 588 Filter bank 1 register 2
    pub const F1R2 = MMIO(Address + 0x0000024c, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 592 Filter bank 2 register 1
    pub const F2R1 = MMIO(Address + 0x00000250, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 596 Filter bank 2 register 2
    pub const F2R2 = MMIO(Address + 0x00000254, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 600 Filter bank 3 register 1
    pub const F3R1 = MMIO(Address + 0x00000258, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 604 Filter bank 3 register 2
    pub const F3R2 = MMIO(Address + 0x0000025c, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 608 Filter bank 4 register 1
    pub const F4R1 = MMIO(Address + 0x00000260, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 612 Filter bank 4 register 2
    pub const F4R2 = MMIO(Address + 0x00000264, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 616 Filter bank 5 register 1
    pub const F5R1 = MMIO(Address + 0x00000268, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 620 Filter bank 5 register 2
    pub const F5R2 = MMIO(Address + 0x0000026c, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 624 Filter bank 6 register 1
    pub const F6R1 = MMIO(Address + 0x00000270, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 628 Filter bank 6 register 2
    pub const F6R2 = MMIO(Address + 0x00000274, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 632 Filter bank 7 register 1
    pub const F7R1 = MMIO(Address + 0x00000278, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 636 Filter bank 7 register 2
    pub const F7R2 = MMIO(Address + 0x0000027c, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 640 Filter bank 8 register 1
    pub const F8R1 = MMIO(Address + 0x00000280, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 644 Filter bank 8 register 2
    pub const F8R2 = MMIO(Address + 0x00000284, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 648 Filter bank 9 register 1
    pub const F9R1 = MMIO(Address + 0x00000288, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 652 Filter bank 9 register 2
    pub const F9R2 = MMIO(Address + 0x0000028c, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 656 Filter bank 10 register 1
    pub const F10R1 = MMIO(Address + 0x00000290, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 660 Filter bank 10 register 2
    pub const F10R2 = MMIO(Address + 0x00000294, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 664 Filter bank 11 register 1
    pub const F11R1 = MMIO(Address + 0x00000298, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 668 Filter bank 11 register 2
    pub const F11R2 = MMIO(Address + 0x0000029c, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 672 Filter bank 4 register 1
    pub const F12R1 = MMIO(Address + 0x000002a0, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 676 Filter bank 12 register 2
    pub const F12R2 = MMIO(Address + 0x000002a4, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 680 Filter bank 13 register 1
    pub const F13R1 = MMIO(Address + 0x000002a8, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 684 Filter bank 13 register 2
    pub const F13R2 = MMIO(Address + 0x000002ac, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 688 Filter bank 14 register 1
    pub const F14R1 = MMIO(Address + 0x000002b0, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 692 Filter bank 14 register 2
    pub const F14R2 = MMIO(Address + 0x000002b4, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 696 Filter bank 15 register 1
    pub const F15R1 = MMIO(Address + 0x000002b8, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 700 Filter bank 15 register 2
    pub const F15R2 = MMIO(Address + 0x000002bc, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 704 Filter bank 16 register 1
    pub const F16R1 = MMIO(Address + 0x000002c0, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 708 Filter bank 16 register 2
    pub const F16R2 = MMIO(Address + 0x000002c4, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 712 Filter bank 17 register 1
    pub const F17R1 = MMIO(Address + 0x000002c8, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 716 Filter bank 17 register 2
    pub const F17R2 = MMIO(Address + 0x000002cc, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 720 Filter bank 18 register 1
    pub const F18R1 = MMIO(Address + 0x000002d0, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 724 Filter bank 18 register 2
    pub const F18R2 = MMIO(Address + 0x000002d4, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 728 Filter bank 19 register 1
    pub const F19R1 = MMIO(Address + 0x000002d8, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 732 Filter bank 19 register 2
    pub const F19R2 = MMIO(Address + 0x000002dc, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 736 Filter bank 20 register 1
    pub const F20R1 = MMIO(Address + 0x000002e0, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 740 Filter bank 20 register 2
    pub const F20R2 = MMIO(Address + 0x000002e4, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 744 Filter bank 21 register 1
    pub const F21R1 = MMIO(Address + 0x000002e8, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 748 Filter bank 21 register 2
    pub const F21R2 = MMIO(Address + 0x000002ec, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 752 Filter bank 22 register 1
    pub const F22R1 = MMIO(Address + 0x000002f0, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 756 Filter bank 22 register 2
    pub const F22R2 = MMIO(Address + 0x000002f4, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 760 Filter bank 23 register 1
    pub const F23R1 = MMIO(Address + 0x000002f8, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 764 Filter bank 23 register 2
    pub const F23R2 = MMIO(Address + 0x000002fc, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 768 Filter bank 24 register 1
    pub const F24R1 = MMIO(Address + 0x00000300, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 772 Filter bank 24 register 2
    pub const F24R2 = MMIO(Address + 0x00000304, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 776 Filter bank 25 register 1
    pub const F25R1 = MMIO(Address + 0x00000308, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 780 Filter bank 25 register 2
    pub const F25R2 = MMIO(Address + 0x0000030c, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 784 Filter bank 26 register 1
    pub const F26R1 = MMIO(Address + 0x00000310, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 788 Filter bank 26 register 2
    pub const F26R2 = MMIO(Address + 0x00000314, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 792 Filter bank 27 register 1
    pub const F27R1 = MMIO(Address + 0x00000318, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
    // byte offset: 796 Filter bank 27 register 2
    pub const F27R2 = MMIO(Address + 0x0000031c, u32, packed struct {
        FB0: bool, // bit offset: 0 desc: Filter bits
        FB1: bool, // bit offset: 1 desc: Filter bits
        FB2: bool, // bit offset: 2 desc: Filter bits
        FB3: bool, // bit offset: 3 desc: Filter bits
        FB4: bool, // bit offset: 4 desc: Filter bits
        FB5: bool, // bit offset: 5 desc: Filter bits
        FB6: bool, // bit offset: 6 desc: Filter bits
        FB7: bool, // bit offset: 7 desc: Filter bits
        FB8: bool, // bit offset: 8 desc: Filter bits
        FB9: bool, // bit offset: 9 desc: Filter bits
        FB10: bool, // bit offset: 10 desc: Filter bits
        FB11: bool, // bit offset: 11 desc: Filter bits
        FB12: bool, // bit offset: 12 desc: Filter bits
        FB13: bool, // bit offset: 13 desc: Filter bits
        FB14: bool, // bit offset: 14 desc: Filter bits
        FB15: bool, // bit offset: 15 desc: Filter bits
        FB16: bool, // bit offset: 16 desc: Filter bits
        FB17: bool, // bit offset: 17 desc: Filter bits
        FB18: bool, // bit offset: 18 desc: Filter bits
        FB19: bool, // bit offset: 19 desc: Filter bits
        FB20: bool, // bit offset: 20 desc: Filter bits
        FB21: bool, // bit offset: 21 desc: Filter bits
        FB22: bool, // bit offset: 22 desc: Filter bits
        FB23: bool, // bit offset: 23 desc: Filter bits
        FB24: bool, // bit offset: 24 desc: Filter bits
        FB25: bool, // bit offset: 25 desc: Filter bits
        FB26: bool, // bit offset: 26 desc: Filter bits
        FB27: bool, // bit offset: 27 desc: Filter bits
        FB28: bool, // bit offset: 28 desc: Filter bits
        FB29: bool, // bit offset: 29 desc: Filter bits
        FB30: bool, // bit offset: 30 desc: Filter bits
        FB31: bool, // bit offset: 31 desc: Filter bits
    });
};
pub const USB_FS = extern struct {
    pub const Address: u32 = 0x40005c00;
    // byte offset: 0 endpoint 0 register
    pub const USB_EP0R = MMIO(Address + 0x00000000, u32, packed struct {
        EA: u4, // bit offset: 0 desc: Endpoint address
        STAT_TX: u2, // bit offset: 4 desc: Status bits, for transmission transfers
        DTOG_TX: bool, // bit offset: 6 desc: Data Toggle, for transmission transfers
        CTR_TX: bool, // bit offset: 7 desc: Correct Transfer for transmission
        EP_KIND: bool, // bit offset: 8 desc: Endpoint kind
        EP_TYPE: u2, // bit offset: 9 desc: Endpoint type
        SETUP: bool, // bit offset: 11 desc: Setup transaction completed
        STAT_RX: u2, // bit offset: 12 desc: Status bits, for reception transfers
        DTOG_RX: bool, // bit offset: 14 desc: Data Toggle, for reception transfers
        CTR_RX: bool, // bit offset: 15 desc: Correct transfer for reception
        padding: u16 = 0,
    });
    // byte offset: 4 endpoint 1 register
    pub const USB_EP1R = MMIO(Address + 0x00000004, u32, packed struct {
        EA: u4, // bit offset: 0 desc: Endpoint address
        STAT_TX: u2, // bit offset: 4 desc: Status bits, for transmission transfers
        DTOG_TX: bool, // bit offset: 6 desc: Data Toggle, for transmission transfers
        CTR_TX: bool, // bit offset: 7 desc: Correct Transfer for transmission
        EP_KIND: bool, // bit offset: 8 desc: Endpoint kind
        EP_TYPE: u2, // bit offset: 9 desc: Endpoint type
        SETUP: bool, // bit offset: 11 desc: Setup transaction completed
        STAT_RX: u2, // bit offset: 12 desc: Status bits, for reception transfers
        DTOG_RX: bool, // bit offset: 14 desc: Data Toggle, for reception transfers
        CTR_RX: bool, // bit offset: 15 desc: Correct transfer for reception
        padding: u16 = 0,
    });
    // byte offset: 8 endpoint 2 register
    pub const USB_EP2R = MMIO(Address + 0x00000008, u32, packed struct {
        EA: u4, // bit offset: 0 desc: Endpoint address
        STAT_TX: u2, // bit offset: 4 desc: Status bits, for transmission transfers
        DTOG_TX: bool, // bit offset: 6 desc: Data Toggle, for transmission transfers
        CTR_TX: bool, // bit offset: 7 desc: Correct Transfer for transmission
        EP_KIND: bool, // bit offset: 8 desc: Endpoint kind
        EP_TYPE: u2, // bit offset: 9 desc: Endpoint type
        SETUP: bool, // bit offset: 11 desc: Setup transaction completed
        STAT_RX: u2, // bit offset: 12 desc: Status bits, for reception transfers
        DTOG_RX: bool, // bit offset: 14 desc: Data Toggle, for reception transfers
        CTR_RX: bool, // bit offset: 15 desc: Correct transfer for reception
        padding: u16 = 0,
    });
    // byte offset: 12 endpoint 3 register
    pub const USB_EP3R = MMIO(Address + 0x0000000c, u32, packed struct {
        EA: u4, // bit offset: 0 desc: Endpoint address
        STAT_TX: u2, // bit offset: 4 desc: Status bits, for transmission transfers
        DTOG_TX: bool, // bit offset: 6 desc: Data Toggle, for transmission transfers
        CTR_TX: bool, // bit offset: 7 desc: Correct Transfer for transmission
        EP_KIND: bool, // bit offset: 8 desc: Endpoint kind
        EP_TYPE: u2, // bit offset: 9 desc: Endpoint type
        SETUP: bool, // bit offset: 11 desc: Setup transaction completed
        STAT_RX: u2, // bit offset: 12 desc: Status bits, for reception transfers
        DTOG_RX: bool, // bit offset: 14 desc: Data Toggle, for reception transfers
        CTR_RX: bool, // bit offset: 15 desc: Correct transfer for reception
        padding: u16 = 0,
    });
    // byte offset: 16 endpoint 4 register
    pub const USB_EP4R = MMIO(Address + 0x00000010, u32, packed struct {
        EA: u4, // bit offset: 0 desc: Endpoint address
        STAT_TX: u2, // bit offset: 4 desc: Status bits, for transmission transfers
        DTOG_TX: bool, // bit offset: 6 desc: Data Toggle, for transmission transfers
        CTR_TX: bool, // bit offset: 7 desc: Correct Transfer for transmission
        EP_KIND: bool, // bit offset: 8 desc: Endpoint kind
        EP_TYPE: u2, // bit offset: 9 desc: Endpoint type
        SETUP: bool, // bit offset: 11 desc: Setup transaction completed
        STAT_RX: u2, // bit offset: 12 desc: Status bits, for reception transfers
        DTOG_RX: bool, // bit offset: 14 desc: Data Toggle, for reception transfers
        CTR_RX: bool, // bit offset: 15 desc: Correct transfer for reception
        padding: u16 = 0,
    });
    // byte offset: 20 endpoint 5 register
    pub const USB_EP5R = MMIO(Address + 0x00000014, u32, packed struct {
        EA: u4, // bit offset: 0 desc: Endpoint address
        STAT_TX: u2, // bit offset: 4 desc: Status bits, for transmission transfers
        DTOG_TX: bool, // bit offset: 6 desc: Data Toggle, for transmission transfers
        CTR_TX: bool, // bit offset: 7 desc: Correct Transfer for transmission
        EP_KIND: bool, // bit offset: 8 desc: Endpoint kind
        EP_TYPE: u2, // bit offset: 9 desc: Endpoint type
        SETUP: bool, // bit offset: 11 desc: Setup transaction completed
        STAT_RX: u2, // bit offset: 12 desc: Status bits, for reception transfers
        DTOG_RX: bool, // bit offset: 14 desc: Data Toggle, for reception transfers
        CTR_RX: bool, // bit offset: 15 desc: Correct transfer for reception
        padding: u16 = 0,
    });
    // byte offset: 24 endpoint 6 register
    pub const USB_EP6R = MMIO(Address + 0x00000018, u32, packed struct {
        EA: u4, // bit offset: 0 desc: Endpoint address
        STAT_TX: u2, // bit offset: 4 desc: Status bits, for transmission transfers
        DTOG_TX: bool, // bit offset: 6 desc: Data Toggle, for transmission transfers
        CTR_TX: bool, // bit offset: 7 desc: Correct Transfer for transmission
        EP_KIND: bool, // bit offset: 8 desc: Endpoint kind
        EP_TYPE: u2, // bit offset: 9 desc: Endpoint type
        SETUP: bool, // bit offset: 11 desc: Setup transaction completed
        STAT_RX: u2, // bit offset: 12 desc: Status bits, for reception transfers
        DTOG_RX: bool, // bit offset: 14 desc: Data Toggle, for reception transfers
        CTR_RX: bool, // bit offset: 15 desc: Correct transfer for reception
        padding: u16 = 0,
    });
    // byte offset: 28 endpoint 7 register
    pub const USB_EP7R = MMIO(Address + 0x0000001c, u32, packed struct {
        EA: u4, // bit offset: 0 desc: Endpoint address
        STAT_TX: u2, // bit offset: 4 desc: Status bits, for transmission transfers
        DTOG_TX: bool, // bit offset: 6 desc: Data Toggle, for transmission transfers
        CTR_TX: bool, // bit offset: 7 desc: Correct Transfer for transmission
        EP_KIND: bool, // bit offset: 8 desc: Endpoint kind
        EP_TYPE: u2, // bit offset: 9 desc: Endpoint type
        SETUP: bool, // bit offset: 11 desc: Setup transaction completed
        STAT_RX: u2, // bit offset: 12 desc: Status bits, for reception transfers
        DTOG_RX: bool, // bit offset: 14 desc: Data Toggle, for reception transfers
        CTR_RX: bool, // bit offset: 15 desc: Correct transfer for reception
        padding: u16 = 0,
    });
    // byte offset: 64 control register
    pub const USB_CNTR = MMIO(Address + 0x00000040, u32, packed struct {
        FRES: bool, // bit offset: 0 desc: Force USB Reset
        PDWN: bool, // bit offset: 1 desc: Power down
        LPMODE: bool, // bit offset: 2 desc: Low-power mode
        FSUSP: bool, // bit offset: 3 desc: Force suspend
        RESUME: bool, // bit offset: 4 desc: Resume request
        reserved0: u3 = 0,
        ESOFM: bool, // bit offset: 8 desc: Expected start of frame interrupt mask
        SOFM: bool, // bit offset: 9 desc: Start of frame interrupt mask
        RESETM: bool, // bit offset: 10 desc: USB reset interrupt mask
        SUSPM: bool, // bit offset: 11 desc: Suspend mode interrupt mask
        WKUPM: bool, // bit offset: 12 desc: Wakeup interrupt mask
        ERRM: bool, // bit offset: 13 desc: Error interrupt mask
        PMAOVRM: bool, // bit offset: 14 desc: Packet memory area over / underrun interrupt mask
        CTRM: bool, // bit offset: 15 desc: Correct transfer interrupt mask
        padding: u16 = 0,
    });
    // byte offset: 68 interrupt status register
    pub const ISTR = MMIO(Address + 0x00000044, u32, packed struct {
        EP_ID: u4, // bit offset: 0 desc: Endpoint Identifier
        DIR: bool, // bit offset: 4 desc: Direction of transaction
        reserved0: u3 = 0,
        ESOF: bool, // bit offset: 8 desc: Expected start frame
        SOF: bool, // bit offset: 9 desc: start of frame
        RESET: bool, // bit offset: 10 desc: reset request
        SUSP: bool, // bit offset: 11 desc: Suspend mode request
        WKUP: bool, // bit offset: 12 desc: Wakeup
        ERR: bool, // bit offset: 13 desc: Error
        PMAOVR: bool, // bit offset: 14 desc: Packet memory area over / underrun
        CTR: bool, // bit offset: 15 desc: Correct transfer
        padding: u16 = 0,
    });
    // byte offset: 72 frame number register
    pub const FNR = MMIO(Address + 0x00000048, u32, packed struct {
        FN: u11, // bit offset: 0 desc: Frame number
        LSOF: u2, // bit offset: 11 desc: Lost SOF
        LCK: bool, // bit offset: 13 desc: Locked
        RXDM: bool, // bit offset: 14 desc: Receive data - line status
        RXDP: bool, // bit offset: 15 desc: Receive data + line status
        padding: u16 = 0,
    });
    // byte offset: 76 device address
    pub const DADDR = MMIO(Address + 0x0000004c, u32, packed struct {
        ADD: bool, // bit offset: 0 desc: Device address
        ADD1: bool, // bit offset: 1 desc: Device address
        ADD2: bool, // bit offset: 2 desc: Device address
        ADD3: bool, // bit offset: 3 desc: Device address
        ADD4: bool, // bit offset: 4 desc: Device address
        ADD5: bool, // bit offset: 5 desc: Device address
        ADD6: bool, // bit offset: 6 desc: Device address
        EF: bool, // bit offset: 7 desc: Enable function
        padding: u24 = 0,
    });
    // byte offset: 80 Buffer table address
    pub const BTABLE = MMIO(Address + 0x00000050, u32, packed struct {
        reserved0: u3 = 0,
        BTABLE: u13, // bit offset: 3 desc: Buffer table
        padding: u16 = 0,
    });
};
pub const I2C1 = extern struct {
    pub const Address: u32 = 0x40005400;
    // byte offset: 0 Control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        PE: bool, // bit offset: 0 desc: Peripheral enable
        TXIE: bool, // bit offset: 1 desc: TX Interrupt enable
        RXIE: bool, // bit offset: 2 desc: RX Interrupt enable
        ADDRIE: bool, // bit offset: 3 desc: Address match interrupt enable (slave only)
        NACKIE: bool, // bit offset: 4 desc: Not acknowledge received interrupt enable
        STOPIE: bool, // bit offset: 5 desc: STOP detection Interrupt enable
        TCIE: bool, // bit offset: 6 desc: Transfer Complete interrupt enable
        ERRIE: bool, // bit offset: 7 desc: Error interrupts enable
        DNF: u4, // bit offset: 8 desc: Digital noise filter
        ANFOFF: bool, // bit offset: 12 desc: Analog noise filter OFF
        SWRST: bool, // bit offset: 13 desc: Software reset
        TXDMAEN: bool, // bit offset: 14 desc: DMA transmission requests enable
        RXDMAEN: bool, // bit offset: 15 desc: DMA reception requests enable
        SBC: bool, // bit offset: 16 desc: Slave byte control
        NOSTRETCH: bool, // bit offset: 17 desc: Clock stretching disable
        WUPEN: bool, // bit offset: 18 desc: Wakeup from STOP enable
        GCEN: bool, // bit offset: 19 desc: General call enable
        SMBHEN: bool, // bit offset: 20 desc: SMBus Host address enable
        SMBDEN: bool, // bit offset: 21 desc: SMBus Device Default address enable
        ALERTEN: bool, // bit offset: 22 desc: SMBUS alert enable
        PECEN: bool, // bit offset: 23 desc: PEC enable
        padding: u8 = 0,
    });
    // byte offset: 4 Control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        SADD0: bool, // bit offset: 0 desc: Slave address bit 0 (master mode)
        SADD1: u7, // bit offset: 1 desc: Slave address bit 7:1 (master mode)
        SADD8: u2, // bit offset: 8 desc: Slave address bit 9:8 (master mode)
        RD_WRN: bool, // bit offset: 10 desc: Transfer direction (master mode)
        ADD10: bool, // bit offset: 11 desc: 10-bit addressing mode (master mode)
        HEAD10R: bool, // bit offset: 12 desc: 10-bit address header only read direction (master receiver mode)
        START: bool, // bit offset: 13 desc: Start generation
        STOP: bool, // bit offset: 14 desc: Stop generation (master mode)
        NACK: bool, // bit offset: 15 desc: NACK generation (slave mode)
        NBYTES: u8, // bit offset: 16 desc: Number of bytes
        RELOAD: bool, // bit offset: 24 desc: NBYTES reload mode
        AUTOEND: bool, // bit offset: 25 desc: Automatic end mode (master mode)
        PECBYTE: bool, // bit offset: 26 desc: Packet error checking byte
        padding: u5 = 0,
    });
    // byte offset: 8 Own address register 1
    pub const OAR1 = MMIO(Address + 0x00000008, u32, packed struct {
        OA1_0: bool, // bit offset: 0 desc: Interface address
        OA1_1: u7, // bit offset: 1 desc: Interface address
        OA1_8: u2, // bit offset: 8 desc: Interface address
        OA1MODE: bool, // bit offset: 10 desc: Own Address 1 10-bit mode
        reserved0: u4 = 0,
        OA1EN: bool, // bit offset: 15 desc: Own Address 1 enable
        padding: u16 = 0,
    });
    // byte offset: 12 Own address register 2
    pub const OAR2 = MMIO(Address + 0x0000000c, u32, packed struct {
        reserved0: u1 = 0,
        OA2: u7, // bit offset: 1 desc: Interface address
        OA2MSK: u3, // bit offset: 8 desc: Own Address 2 masks
        reserved1: u4 = 0,
        OA2EN: bool, // bit offset: 15 desc: Own Address 2 enable
        padding: u16 = 0,
    });
    // byte offset: 16 Timing register
    pub const TIMINGR = MMIO(Address + 0x00000010, u32, packed struct {
        SCLL: u8, // bit offset: 0 desc: SCL low period (master mode)
        SCLH: u8, // bit offset: 8 desc: SCL high period (master mode)
        SDADEL: u4, // bit offset: 16 desc: Data hold time
        SCLDEL: u4, // bit offset: 20 desc: Data setup time
        reserved0: u4 = 0,
        PRESC: u4, // bit offset: 28 desc: Timing prescaler
    });
    // byte offset: 20 Status register 1
    pub const TIMEOUTR = MMIO(Address + 0x00000014, u32, packed struct {
        TIMEOUTA: u12, // bit offset: 0 desc: Bus timeout A
        TIDLE: bool, // bit offset: 12 desc: Idle clock timeout detection
        reserved0: u2 = 0,
        TIMOUTEN: bool, // bit offset: 15 desc: Clock timeout enable
        TIMEOUTB: u12, // bit offset: 16 desc: Bus timeout B
        reserved1: u3 = 0,
        TEXTEN: bool, // bit offset: 31 desc: Extended clock timeout enable
    });
    // byte offset: 24 Interrupt and Status register
    pub const ISR = MMIO(Address + 0x00000018, u32, packed struct {
        TXE: bool, // bit offset: 0 desc: Transmit data register empty (transmitters)
        TXIS: bool, // bit offset: 1 desc: Transmit interrupt status (transmitters)
        RXNE: bool, // bit offset: 2 desc: Receive data register not empty (receivers)
        ADDR: bool, // bit offset: 3 desc: Address matched (slave mode)
        NACKF: bool, // bit offset: 4 desc: Not acknowledge received flag
        STOPF: bool, // bit offset: 5 desc: Stop detection flag
        TC: bool, // bit offset: 6 desc: Transfer Complete (master mode)
        TCR: bool, // bit offset: 7 desc: Transfer Complete Reload
        BERR: bool, // bit offset: 8 desc: Bus error
        ARLO: bool, // bit offset: 9 desc: Arbitration lost
        OVR: bool, // bit offset: 10 desc: Overrun/Underrun (slave mode)
        PECERR: bool, // bit offset: 11 desc: PEC Error in reception
        TIMEOUT: bool, // bit offset: 12 desc: Timeout or t_low detection flag
        ALERT: bool, // bit offset: 13 desc: SMBus alert
        reserved0: u1 = 0,
        BUSY: bool, // bit offset: 15 desc: Bus busy
        DIR: bool, // bit offset: 16 desc: Transfer direction (Slave mode)
        ADDCODE: u7, // bit offset: 17 desc: Address match code (Slave mode)
        padding: u8 = 0,
    });
    // byte offset: 28 Interrupt clear register
    pub const ICR = MMIO(Address + 0x0000001c, u32, packed struct {
        reserved0: u3 = 0,
        ADDRCF: bool, // bit offset: 3 desc: Address Matched flag clear
        NACKCF: bool, // bit offset: 4 desc: Not Acknowledge flag clear
        STOPCF: bool, // bit offset: 5 desc: Stop detection flag clear
        reserved1: u2 = 0,
        BERRCF: bool, // bit offset: 8 desc: Bus error flag clear
        ARLOCF: bool, // bit offset: 9 desc: Arbitration lost flag clear
        OVRCF: bool, // bit offset: 10 desc: Overrun/Underrun flag clear
        PECCF: bool, // bit offset: 11 desc: PEC Error flag clear
        TIMOUTCF: bool, // bit offset: 12 desc: Timeout detection flag clear
        ALERTCF: bool, // bit offset: 13 desc: Alert flag clear
        padding: u18 = 0,
    });
    // byte offset: 32 PEC register
    pub const PECR = MMIO(Address + 0x00000020, u32, packed struct {
        PEC: u8, // bit offset: 0 desc: Packet error checking register
        padding: u24 = 0,
    });
    // byte offset: 36 Receive data register
    pub const RXDR = MMIO(Address + 0x00000024, u32, packed struct {
        RXDATA: u8, // bit offset: 0 desc: 8-bit receive data
        padding: u24 = 0,
    });
    // byte offset: 40 Transmit data register
    pub const TXDR = MMIO(Address + 0x00000028, u32, packed struct {
        TXDATA: u8, // bit offset: 0 desc: 8-bit transmit data
        padding: u24 = 0,
    });
};
pub const I2C2 = extern struct {
    pub const Address: u32 = 0x40005800;
    // byte offset: 0 Control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        PE: bool, // bit offset: 0 desc: Peripheral enable
        TXIE: bool, // bit offset: 1 desc: TX Interrupt enable
        RXIE: bool, // bit offset: 2 desc: RX Interrupt enable
        ADDRIE: bool, // bit offset: 3 desc: Address match interrupt enable (slave only)
        NACKIE: bool, // bit offset: 4 desc: Not acknowledge received interrupt enable
        STOPIE: bool, // bit offset: 5 desc: STOP detection Interrupt enable
        TCIE: bool, // bit offset: 6 desc: Transfer Complete interrupt enable
        ERRIE: bool, // bit offset: 7 desc: Error interrupts enable
        DNF: u4, // bit offset: 8 desc: Digital noise filter
        ANFOFF: bool, // bit offset: 12 desc: Analog noise filter OFF
        SWRST: bool, // bit offset: 13 desc: Software reset
        TXDMAEN: bool, // bit offset: 14 desc: DMA transmission requests enable
        RXDMAEN: bool, // bit offset: 15 desc: DMA reception requests enable
        SBC: bool, // bit offset: 16 desc: Slave byte control
        NOSTRETCH: bool, // bit offset: 17 desc: Clock stretching disable
        WUPEN: bool, // bit offset: 18 desc: Wakeup from STOP enable
        GCEN: bool, // bit offset: 19 desc: General call enable
        SMBHEN: bool, // bit offset: 20 desc: SMBus Host address enable
        SMBDEN: bool, // bit offset: 21 desc: SMBus Device Default address enable
        ALERTEN: bool, // bit offset: 22 desc: SMBUS alert enable
        PECEN: bool, // bit offset: 23 desc: PEC enable
        padding: u8 = 0,
    });
    // byte offset: 4 Control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        SADD0: bool, // bit offset: 0 desc: Slave address bit 0 (master mode)
        SADD1: u7, // bit offset: 1 desc: Slave address bit 7:1 (master mode)
        SADD8: u2, // bit offset: 8 desc: Slave address bit 9:8 (master mode)
        RD_WRN: bool, // bit offset: 10 desc: Transfer direction (master mode)
        ADD10: bool, // bit offset: 11 desc: 10-bit addressing mode (master mode)
        HEAD10R: bool, // bit offset: 12 desc: 10-bit address header only read direction (master receiver mode)
        START: bool, // bit offset: 13 desc: Start generation
        STOP: bool, // bit offset: 14 desc: Stop generation (master mode)
        NACK: bool, // bit offset: 15 desc: NACK generation (slave mode)
        NBYTES: u8, // bit offset: 16 desc: Number of bytes
        RELOAD: bool, // bit offset: 24 desc: NBYTES reload mode
        AUTOEND: bool, // bit offset: 25 desc: Automatic end mode (master mode)
        PECBYTE: bool, // bit offset: 26 desc: Packet error checking byte
        padding: u5 = 0,
    });
    // byte offset: 8 Own address register 1
    pub const OAR1 = MMIO(Address + 0x00000008, u32, packed struct {
        OA1_0: bool, // bit offset: 0 desc: Interface address
        OA1_1: u7, // bit offset: 1 desc: Interface address
        OA1_8: u2, // bit offset: 8 desc: Interface address
        OA1MODE: bool, // bit offset: 10 desc: Own Address 1 10-bit mode
        reserved0: u4 = 0,
        OA1EN: bool, // bit offset: 15 desc: Own Address 1 enable
        padding: u16 = 0,
    });
    // byte offset: 12 Own address register 2
    pub const OAR2 = MMIO(Address + 0x0000000c, u32, packed struct {
        reserved0: u1 = 0,
        OA2: u7, // bit offset: 1 desc: Interface address
        OA2MSK: u3, // bit offset: 8 desc: Own Address 2 masks
        reserved1: u4 = 0,
        OA2EN: bool, // bit offset: 15 desc: Own Address 2 enable
        padding: u16 = 0,
    });
    // byte offset: 16 Timing register
    pub const TIMINGR = MMIO(Address + 0x00000010, u32, packed struct {
        SCLL: u8, // bit offset: 0 desc: SCL low period (master mode)
        SCLH: u8, // bit offset: 8 desc: SCL high period (master mode)
        SDADEL: u4, // bit offset: 16 desc: Data hold time
        SCLDEL: u4, // bit offset: 20 desc: Data setup time
        reserved0: u4 = 0,
        PRESC: u4, // bit offset: 28 desc: Timing prescaler
    });
    // byte offset: 20 Status register 1
    pub const TIMEOUTR = MMIO(Address + 0x00000014, u32, packed struct {
        TIMEOUTA: u12, // bit offset: 0 desc: Bus timeout A
        TIDLE: bool, // bit offset: 12 desc: Idle clock timeout detection
        reserved0: u2 = 0,
        TIMOUTEN: bool, // bit offset: 15 desc: Clock timeout enable
        TIMEOUTB: u12, // bit offset: 16 desc: Bus timeout B
        reserved1: u3 = 0,
        TEXTEN: bool, // bit offset: 31 desc: Extended clock timeout enable
    });
    // byte offset: 24 Interrupt and Status register
    pub const ISR = MMIO(Address + 0x00000018, u32, packed struct {
        TXE: bool, // bit offset: 0 desc: Transmit data register empty (transmitters)
        TXIS: bool, // bit offset: 1 desc: Transmit interrupt status (transmitters)
        RXNE: bool, // bit offset: 2 desc: Receive data register not empty (receivers)
        ADDR: bool, // bit offset: 3 desc: Address matched (slave mode)
        NACKF: bool, // bit offset: 4 desc: Not acknowledge received flag
        STOPF: bool, // bit offset: 5 desc: Stop detection flag
        TC: bool, // bit offset: 6 desc: Transfer Complete (master mode)
        TCR: bool, // bit offset: 7 desc: Transfer Complete Reload
        BERR: bool, // bit offset: 8 desc: Bus error
        ARLO: bool, // bit offset: 9 desc: Arbitration lost
        OVR: bool, // bit offset: 10 desc: Overrun/Underrun (slave mode)
        PECERR: bool, // bit offset: 11 desc: PEC Error in reception
        TIMEOUT: bool, // bit offset: 12 desc: Timeout or t_low detection flag
        ALERT: bool, // bit offset: 13 desc: SMBus alert
        reserved0: u1 = 0,
        BUSY: bool, // bit offset: 15 desc: Bus busy
        DIR: bool, // bit offset: 16 desc: Transfer direction (Slave mode)
        ADDCODE: u7, // bit offset: 17 desc: Address match code (Slave mode)
        padding: u8 = 0,
    });
    // byte offset: 28 Interrupt clear register
    pub const ICR = MMIO(Address + 0x0000001c, u32, packed struct {
        reserved0: u3 = 0,
        ADDRCF: bool, // bit offset: 3 desc: Address Matched flag clear
        NACKCF: bool, // bit offset: 4 desc: Not Acknowledge flag clear
        STOPCF: bool, // bit offset: 5 desc: Stop detection flag clear
        reserved1: u2 = 0,
        BERRCF: bool, // bit offset: 8 desc: Bus error flag clear
        ARLOCF: bool, // bit offset: 9 desc: Arbitration lost flag clear
        OVRCF: bool, // bit offset: 10 desc: Overrun/Underrun flag clear
        PECCF: bool, // bit offset: 11 desc: PEC Error flag clear
        TIMOUTCF: bool, // bit offset: 12 desc: Timeout detection flag clear
        ALERTCF: bool, // bit offset: 13 desc: Alert flag clear
        padding: u18 = 0,
    });
    // byte offset: 32 PEC register
    pub const PECR = MMIO(Address + 0x00000020, u32, packed struct {
        PEC: u8, // bit offset: 0 desc: Packet error checking register
        padding: u24 = 0,
    });
    // byte offset: 36 Receive data register
    pub const RXDR = MMIO(Address + 0x00000024, u32, packed struct {
        RXDATA: u8, // bit offset: 0 desc: 8-bit receive data
        padding: u24 = 0,
    });
    // byte offset: 40 Transmit data register
    pub const TXDR = MMIO(Address + 0x00000028, u32, packed struct {
        TXDATA: u8, // bit offset: 0 desc: 8-bit transmit data
        padding: u24 = 0,
    });
};
pub const I2C3 = extern struct {
    pub const Address: u32 = 0x40007800;
    // byte offset: 0 Control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        PE: bool, // bit offset: 0 desc: Peripheral enable
        TXIE: bool, // bit offset: 1 desc: TX Interrupt enable
        RXIE: bool, // bit offset: 2 desc: RX Interrupt enable
        ADDRIE: bool, // bit offset: 3 desc: Address match interrupt enable (slave only)
        NACKIE: bool, // bit offset: 4 desc: Not acknowledge received interrupt enable
        STOPIE: bool, // bit offset: 5 desc: STOP detection Interrupt enable
        TCIE: bool, // bit offset: 6 desc: Transfer Complete interrupt enable
        ERRIE: bool, // bit offset: 7 desc: Error interrupts enable
        DNF: u4, // bit offset: 8 desc: Digital noise filter
        ANFOFF: bool, // bit offset: 12 desc: Analog noise filter OFF
        SWRST: bool, // bit offset: 13 desc: Software reset
        TXDMAEN: bool, // bit offset: 14 desc: DMA transmission requests enable
        RXDMAEN: bool, // bit offset: 15 desc: DMA reception requests enable
        SBC: bool, // bit offset: 16 desc: Slave byte control
        NOSTRETCH: bool, // bit offset: 17 desc: Clock stretching disable
        WUPEN: bool, // bit offset: 18 desc: Wakeup from STOP enable
        GCEN: bool, // bit offset: 19 desc: General call enable
        SMBHEN: bool, // bit offset: 20 desc: SMBus Host address enable
        SMBDEN: bool, // bit offset: 21 desc: SMBus Device Default address enable
        ALERTEN: bool, // bit offset: 22 desc: SMBUS alert enable
        PECEN: bool, // bit offset: 23 desc: PEC enable
        padding: u8 = 0,
    });
    // byte offset: 4 Control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        SADD0: bool, // bit offset: 0 desc: Slave address bit 0 (master mode)
        SADD1: u7, // bit offset: 1 desc: Slave address bit 7:1 (master mode)
        SADD8: u2, // bit offset: 8 desc: Slave address bit 9:8 (master mode)
        RD_WRN: bool, // bit offset: 10 desc: Transfer direction (master mode)
        ADD10: bool, // bit offset: 11 desc: 10-bit addressing mode (master mode)
        HEAD10R: bool, // bit offset: 12 desc: 10-bit address header only read direction (master receiver mode)
        START: bool, // bit offset: 13 desc: Start generation
        STOP: bool, // bit offset: 14 desc: Stop generation (master mode)
        NACK: bool, // bit offset: 15 desc: NACK generation (slave mode)
        NBYTES: u8, // bit offset: 16 desc: Number of bytes
        RELOAD: bool, // bit offset: 24 desc: NBYTES reload mode
        AUTOEND: bool, // bit offset: 25 desc: Automatic end mode (master mode)
        PECBYTE: bool, // bit offset: 26 desc: Packet error checking byte
        padding: u5 = 0,
    });
    // byte offset: 8 Own address register 1
    pub const OAR1 = MMIO(Address + 0x00000008, u32, packed struct {
        OA1_0: bool, // bit offset: 0 desc: Interface address
        OA1_1: u7, // bit offset: 1 desc: Interface address
        OA1_8: u2, // bit offset: 8 desc: Interface address
        OA1MODE: bool, // bit offset: 10 desc: Own Address 1 10-bit mode
        reserved0: u4 = 0,
        OA1EN: bool, // bit offset: 15 desc: Own Address 1 enable
        padding: u16 = 0,
    });
    // byte offset: 12 Own address register 2
    pub const OAR2 = MMIO(Address + 0x0000000c, u32, packed struct {
        reserved0: u1 = 0,
        OA2: u7, // bit offset: 1 desc: Interface address
        OA2MSK: u3, // bit offset: 8 desc: Own Address 2 masks
        reserved1: u4 = 0,
        OA2EN: bool, // bit offset: 15 desc: Own Address 2 enable
        padding: u16 = 0,
    });
    // byte offset: 16 Timing register
    pub const TIMINGR = MMIO(Address + 0x00000010, u32, packed struct {
        SCLL: u8, // bit offset: 0 desc: SCL low period (master mode)
        SCLH: u8, // bit offset: 8 desc: SCL high period (master mode)
        SDADEL: u4, // bit offset: 16 desc: Data hold time
        SCLDEL: u4, // bit offset: 20 desc: Data setup time
        reserved0: u4 = 0,
        PRESC: u4, // bit offset: 28 desc: Timing prescaler
    });
    // byte offset: 20 Status register 1
    pub const TIMEOUTR = MMIO(Address + 0x00000014, u32, packed struct {
        TIMEOUTA: u12, // bit offset: 0 desc: Bus timeout A
        TIDLE: bool, // bit offset: 12 desc: Idle clock timeout detection
        reserved0: u2 = 0,
        TIMOUTEN: bool, // bit offset: 15 desc: Clock timeout enable
        TIMEOUTB: u12, // bit offset: 16 desc: Bus timeout B
        reserved1: u3 = 0,
        TEXTEN: bool, // bit offset: 31 desc: Extended clock timeout enable
    });
    // byte offset: 24 Interrupt and Status register
    pub const ISR = MMIO(Address + 0x00000018, u32, packed struct {
        TXE: bool, // bit offset: 0 desc: Transmit data register empty (transmitters)
        TXIS: bool, // bit offset: 1 desc: Transmit interrupt status (transmitters)
        RXNE: bool, // bit offset: 2 desc: Receive data register not empty (receivers)
        ADDR: bool, // bit offset: 3 desc: Address matched (slave mode)
        NACKF: bool, // bit offset: 4 desc: Not acknowledge received flag
        STOPF: bool, // bit offset: 5 desc: Stop detection flag
        TC: bool, // bit offset: 6 desc: Transfer Complete (master mode)
        TCR: bool, // bit offset: 7 desc: Transfer Complete Reload
        BERR: bool, // bit offset: 8 desc: Bus error
        ARLO: bool, // bit offset: 9 desc: Arbitration lost
        OVR: bool, // bit offset: 10 desc: Overrun/Underrun (slave mode)
        PECERR: bool, // bit offset: 11 desc: PEC Error in reception
        TIMEOUT: bool, // bit offset: 12 desc: Timeout or t_low detection flag
        ALERT: bool, // bit offset: 13 desc: SMBus alert
        reserved0: u1 = 0,
        BUSY: bool, // bit offset: 15 desc: Bus busy
        DIR: bool, // bit offset: 16 desc: Transfer direction (Slave mode)
        ADDCODE: u7, // bit offset: 17 desc: Address match code (Slave mode)
        padding: u8 = 0,
    });
    // byte offset: 28 Interrupt clear register
    pub const ICR = MMIO(Address + 0x0000001c, u32, packed struct {
        reserved0: u3 = 0,
        ADDRCF: bool, // bit offset: 3 desc: Address Matched flag clear
        NACKCF: bool, // bit offset: 4 desc: Not Acknowledge flag clear
        STOPCF: bool, // bit offset: 5 desc: Stop detection flag clear
        reserved1: u2 = 0,
        BERRCF: bool, // bit offset: 8 desc: Bus error flag clear
        ARLOCF: bool, // bit offset: 9 desc: Arbitration lost flag clear
        OVRCF: bool, // bit offset: 10 desc: Overrun/Underrun flag clear
        PECCF: bool, // bit offset: 11 desc: PEC Error flag clear
        TIMOUTCF: bool, // bit offset: 12 desc: Timeout detection flag clear
        ALERTCF: bool, // bit offset: 13 desc: Alert flag clear
        padding: u18 = 0,
    });
    // byte offset: 32 PEC register
    pub const PECR = MMIO(Address + 0x00000020, u32, packed struct {
        PEC: u8, // bit offset: 0 desc: Packet error checking register
        padding: u24 = 0,
    });
    // byte offset: 36 Receive data register
    pub const RXDR = MMIO(Address + 0x00000024, u32, packed struct {
        RXDATA: u8, // bit offset: 0 desc: 8-bit receive data
        padding: u24 = 0,
    });
    // byte offset: 40 Transmit data register
    pub const TXDR = MMIO(Address + 0x00000028, u32, packed struct {
        TXDATA: u8, // bit offset: 0 desc: 8-bit transmit data
        padding: u24 = 0,
    });
};
pub const IWDG = extern struct {
    pub const Address: u32 = 0x40003000;
    // byte offset: 0 Key register
    pub const KR = MMIO(Address + 0x00000000, u32, packed struct {
        KEY: u16, // bit offset: 0 desc: Key value
        padding: u16 = 0,
    });
    // byte offset: 4 Prescaler register
    pub const PR = MMIO(Address + 0x00000004, u32, packed struct {
        PR: u3, // bit offset: 0 desc: Prescaler divider
        padding: u29 = 0,
    });
    // byte offset: 8 Reload register
    pub const RLR = MMIO(Address + 0x00000008, u32, packed struct {
        RL: u12, // bit offset: 0 desc: Watchdog counter reload value
        padding: u20 = 0,
    });
    // byte offset: 12 Status register
    pub const SR = MMIO(Address + 0x0000000c, u32, packed struct {
        PVU: bool, // bit offset: 0 desc: Watchdog prescaler value update
        RVU: bool, // bit offset: 1 desc: Watchdog counter reload value update
        WVU: bool, // bit offset: 2 desc: Watchdog counter window value update
        padding: u29 = 0,
    });
    // byte offset: 16 Window register
    pub const WINR = MMIO(Address + 0x00000010, u32, packed struct {
        WIN: u12, // bit offset: 0 desc: Watchdog counter window value
        padding: u20 = 0,
    });
};
pub const WWDG = extern struct {
    pub const Address: u32 = 0x40002c00;
    // byte offset: 0 Control register
    pub const CR = MMIO(Address + 0x00000000, u32, packed struct {
        T: u7, // bit offset: 0 desc: 7-bit counter
        WDGA: bool, // bit offset: 7 desc: Activation bit
        padding: u24 = 0,
    });
    // byte offset: 4 Configuration register
    pub const CFR = MMIO(Address + 0x00000004, u32, packed struct {
        W: u7, // bit offset: 0 desc: 7-bit window value
        WDGTB: u2, // bit offset: 7 desc: Timer base
        EWI: bool, // bit offset: 9 desc: Early wakeup interrupt
        padding: u22 = 0,
    });
    // byte offset: 8 Status register
    pub const SR = MMIO(Address + 0x00000008, u32, packed struct {
        EWIF: bool, // bit offset: 0 desc: Early wakeup interrupt flag
        padding: u31 = 0,
    });
};
pub const RTC = extern struct {
    pub const Address: u32 = 0x40002800;
    // byte offset: 0 time register
    pub const TR = MMIO(Address + 0x00000000, u32, packed struct {
        SU: u4, // bit offset: 0 desc: Second units in BCD format
        ST: u3, // bit offset: 4 desc: Second tens in BCD format
        reserved0: u1 = 0,
        MNU: u4, // bit offset: 8 desc: Minute units in BCD format
        MNT: u3, // bit offset: 12 desc: Minute tens in BCD format
        reserved1: u1 = 0,
        HU: u4, // bit offset: 16 desc: Hour units in BCD format
        HT: u2, // bit offset: 20 desc: Hour tens in BCD format
        PM: bool, // bit offset: 22 desc: AM/PM notation
        padding: u9 = 0,
    });
    // byte offset: 4 date register
    pub const DR = MMIO(Address + 0x00000004, u32, packed struct {
        DU: u4, // bit offset: 0 desc: Date units in BCD format
        DT: u2, // bit offset: 4 desc: Date tens in BCD format
        reserved0: u2 = 0,
        MU: u4, // bit offset: 8 desc: Month units in BCD format
        MT: bool, // bit offset: 12 desc: Month tens in BCD format
        WDU: u3, // bit offset: 13 desc: Week day units
        YU: u4, // bit offset: 16 desc: Year units in BCD format
        YT: u4, // bit offset: 20 desc: Year tens in BCD format
        padding: u8 = 0,
    });
    // byte offset: 8 control register
    pub const CR = MMIO(Address + 0x00000008, u32, packed struct {
        WCKSEL: u3, // bit offset: 0 desc: Wakeup clock selection
        TSEDGE: bool, // bit offset: 3 desc: Time-stamp event active edge
        REFCKON: bool, // bit offset: 4 desc: Reference clock detection enable (50 or 60 Hz)
        BYPSHAD: bool, // bit offset: 5 desc: Bypass the shadow registers
        FMT: bool, // bit offset: 6 desc: Hour format
        reserved0: u1 = 0,
        ALRAE: bool, // bit offset: 8 desc: Alarm A enable
        ALRBE: bool, // bit offset: 9 desc: Alarm B enable
        WUTE: bool, // bit offset: 10 desc: Wakeup timer enable
        TSE: bool, // bit offset: 11 desc: Time stamp enable
        ALRAIE: bool, // bit offset: 12 desc: Alarm A interrupt enable
        ALRBIE: bool, // bit offset: 13 desc: Alarm B interrupt enable
        WUTIE: bool, // bit offset: 14 desc: Wakeup timer interrupt enable
        TSIE: bool, // bit offset: 15 desc: Time-stamp interrupt enable
        ADD1H: bool, // bit offset: 16 desc: Add 1 hour (summer time change)
        SUB1H: bool, // bit offset: 17 desc: Subtract 1 hour (winter time change)
        BKP: bool, // bit offset: 18 desc: Backup
        COSEL: bool, // bit offset: 19 desc: Calibration output selection
        POL: bool, // bit offset: 20 desc: Output polarity
        OSEL: u2, // bit offset: 21 desc: Output selection
        COE: bool, // bit offset: 23 desc: Calibration output enable
        padding: u8 = 0,
    });
    // byte offset: 12 initialization and status register
    pub const ISR = MMIO(Address + 0x0000000c, u32, packed struct {
        ALRAWF: bool, // bit offset: 0 desc: Alarm A write flag
        ALRBWF: bool, // bit offset: 1 desc: Alarm B write flag
        WUTWF: bool, // bit offset: 2 desc: Wakeup timer write flag
        SHPF: bool, // bit offset: 3 desc: Shift operation pending
        INITS: bool, // bit offset: 4 desc: Initialization status flag
        RSF: bool, // bit offset: 5 desc: Registers synchronization flag
        INITF: bool, // bit offset: 6 desc: Initialization flag
        INIT: bool, // bit offset: 7 desc: Initialization mode
        ALRAF: bool, // bit offset: 8 desc: Alarm A flag
        ALRBF: bool, // bit offset: 9 desc: Alarm B flag
        WUTF: bool, // bit offset: 10 desc: Wakeup timer flag
        TSF: bool, // bit offset: 11 desc: Time-stamp flag
        TSOVF: bool, // bit offset: 12 desc: Time-stamp overflow flag
        TAMP1F: bool, // bit offset: 13 desc: Tamper detection flag
        TAMP2F: bool, // bit offset: 14 desc: RTC_TAMP2 detection flag
        TAMP3F: bool, // bit offset: 15 desc: RTC_TAMP3 detection flag
        RECALPF: bool, // bit offset: 16 desc: Recalibration pending Flag
        padding: u15 = 0,
    });
    // byte offset: 16 prescaler register
    pub const PRER = MMIO(Address + 0x00000010, u32, packed struct {
        PREDIV_S: u15, // bit offset: 0 desc: Synchronous prescaler factor
        reserved0: u1 = 0,
        PREDIV_A: u7, // bit offset: 16 desc: Asynchronous prescaler factor
        padding: u9 = 0,
    });
    // byte offset: 20 wakeup timer register
    pub const WUTR = MMIO(Address + 0x00000014, u32, packed struct {
        WUT: u16, // bit offset: 0 desc: Wakeup auto-reload value bits
        padding: u16 = 0,
    });
    // byte offset: 28 alarm A register
    pub const ALRMAR = MMIO(Address + 0x0000001c, u32, packed struct {
        SU: u4, // bit offset: 0 desc: Second units in BCD format
        ST: u3, // bit offset: 4 desc: Second tens in BCD format
        MSK1: bool, // bit offset: 7 desc: Alarm A seconds mask
        MNU: u4, // bit offset: 8 desc: Minute units in BCD format
        MNT: u3, // bit offset: 12 desc: Minute tens in BCD format
        MSK2: bool, // bit offset: 15 desc: Alarm A minutes mask
        HU: u4, // bit offset: 16 desc: Hour units in BCD format
        HT: u2, // bit offset: 20 desc: Hour tens in BCD format
        PM: bool, // bit offset: 22 desc: AM/PM notation
        MSK3: bool, // bit offset: 23 desc: Alarm A hours mask
        DU: u4, // bit offset: 24 desc: Date units or day in BCD format
        DT: u2, // bit offset: 28 desc: Date tens in BCD format
        WDSEL: bool, // bit offset: 30 desc: Week day selection
        MSK4: bool, // bit offset: 31 desc: Alarm A date mask
    });
    // byte offset: 32 alarm B register
    pub const ALRMBR = MMIO(Address + 0x00000020, u32, packed struct {
        SU: u4, // bit offset: 0 desc: Second units in BCD format
        ST: u3, // bit offset: 4 desc: Second tens in BCD format
        MSK1: bool, // bit offset: 7 desc: Alarm B seconds mask
        MNU: u4, // bit offset: 8 desc: Minute units in BCD format
        MNT: u3, // bit offset: 12 desc: Minute tens in BCD format
        MSK2: bool, // bit offset: 15 desc: Alarm B minutes mask
        HU: u4, // bit offset: 16 desc: Hour units in BCD format
        HT: u2, // bit offset: 20 desc: Hour tens in BCD format
        PM: bool, // bit offset: 22 desc: AM/PM notation
        MSK3: bool, // bit offset: 23 desc: Alarm B hours mask
        DU: u4, // bit offset: 24 desc: Date units or day in BCD format
        DT: u2, // bit offset: 28 desc: Date tens in BCD format
        WDSEL: bool, // bit offset: 30 desc: Week day selection
        MSK4: bool, // bit offset: 31 desc: Alarm B date mask
    });
    // byte offset: 36 write protection register
    pub const WPR = MMIO(Address + 0x00000024, u32, packed struct {
        KEY: u8, // bit offset: 0 desc: Write protection key
        padding: u24 = 0,
    });
    // byte offset: 40 sub second register
    pub const SSR = MMIO(Address + 0x00000028, u32, packed struct {
        SS: u16, // bit offset: 0 desc: Sub second value
        padding: u16 = 0,
    });
    // byte offset: 44 shift control register
    pub const SHIFTR = MMIO(Address + 0x0000002c, u32, packed struct {
        SUBFS: u15, // bit offset: 0 desc: Subtract a fraction of a second
        reserved0: u16 = 0,
        ADD1S: bool, // bit offset: 31 desc: Add one second
    });
    // byte offset: 48 time stamp time register
    pub const TSTR = MMIO(Address + 0x00000030, u32, packed struct {
        SU: u4, // bit offset: 0 desc: Second units in BCD format
        ST: u3, // bit offset: 4 desc: Second tens in BCD format
        reserved0: u1 = 0,
        MNU: u4, // bit offset: 8 desc: Minute units in BCD format
        MNT: u3, // bit offset: 12 desc: Minute tens in BCD format
        reserved1: u1 = 0,
        HU: u4, // bit offset: 16 desc: Hour units in BCD format
        HT: u2, // bit offset: 20 desc: Hour tens in BCD format
        PM: bool, // bit offset: 22 desc: AM/PM notation
        padding: u9 = 0,
    });
    // byte offset: 52 time stamp date register
    pub const TSDR = MMIO(Address + 0x00000034, u32, packed struct {
        DU: u4, // bit offset: 0 desc: Date units in BCD format
        DT: u2, // bit offset: 4 desc: Date tens in BCD format
        reserved0: u2 = 0,
        MU: u4, // bit offset: 8 desc: Month units in BCD format
        MT: bool, // bit offset: 12 desc: Month tens in BCD format
        WDU: u3, // bit offset: 13 desc: Week day units
        padding: u16 = 0,
    });
    // byte offset: 56 timestamp sub second register
    pub const TSSSR = MMIO(Address + 0x00000038, u32, packed struct {
        SS: u16, // bit offset: 0 desc: Sub second value
        padding: u16 = 0,
    });
    // byte offset: 60 calibration register
    pub const CALR = MMIO(Address + 0x0000003c, u32, packed struct {
        CALM: u9, // bit offset: 0 desc: Calibration minus
        reserved0: u4 = 0,
        CALW16: bool, // bit offset: 13 desc: Use a 16-second calibration cycle period
        CALW8: bool, // bit offset: 14 desc: Use an 8-second calibration cycle period
        CALP: bool, // bit offset: 15 desc: Increase frequency of RTC by 488.5 ppm
        padding: u16 = 0,
    });
    // byte offset: 64 tamper and alternate function configuration register
    pub const TAFCR = MMIO(Address + 0x00000040, u32, packed struct {
        TAMP1E: bool, // bit offset: 0 desc: Tamper 1 detection enable
        TAMP1TRG: bool, // bit offset: 1 desc: Active level for tamper 1
        TAMPIE: bool, // bit offset: 2 desc: Tamper interrupt enable
        TAMP2E: bool, // bit offset: 3 desc: Tamper 2 detection enable
        TAMP2TRG: bool, // bit offset: 4 desc: Active level for tamper 2
        TAMP3E: bool, // bit offset: 5 desc: Tamper 3 detection enable
        TAMP3TRG: bool, // bit offset: 6 desc: Active level for tamper 3
        TAMPTS: bool, // bit offset: 7 desc: Activate timestamp on tamper detection event
        TAMPFREQ: u3, // bit offset: 8 desc: Tamper sampling frequency
        TAMPFLT: u2, // bit offset: 11 desc: Tamper filter count
        TAMPPRCH: u2, // bit offset: 13 desc: Tamper precharge duration
        TAMPPUDIS: bool, // bit offset: 15 desc: TAMPER pull-up disable
        reserved0: u2 = 0,
        PC13VALUE: bool, // bit offset: 18 desc: PC13 value
        PC13MODE: bool, // bit offset: 19 desc: PC13 mode
        PC14VALUE: bool, // bit offset: 20 desc: PC14 value
        PC14MODE: bool, // bit offset: 21 desc: PC 14 mode
        PC15VALUE: bool, // bit offset: 22 desc: PC15 value
        PC15MODE: bool, // bit offset: 23 desc: PC15 mode
        padding: u8 = 0,
    });
    // byte offset: 68 alarm A sub second register
    pub const ALRMASSR = MMIO(Address + 0x00000044, u32, packed struct {
        SS: u15, // bit offset: 0 desc: Sub seconds value
        reserved0: u9 = 0,
        MASKSS: u4, // bit offset: 24 desc: Mask the most-significant bits starting at this bit
        padding: u4 = 0,
    });
    // byte offset: 72 alarm B sub second register
    pub const ALRMBSSR = MMIO(Address + 0x00000048, u32, packed struct {
        SS: u15, // bit offset: 0 desc: Sub seconds value
        reserved0: u9 = 0,
        MASKSS: u4, // bit offset: 24 desc: Mask the most-significant bits starting at this bit
        padding: u4 = 0,
    });
    // byte offset: 80 backup register
    pub const BKP0R = MMIO(Address + 0x00000050, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 84 backup register
    pub const BKP1R = MMIO(Address + 0x00000054, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 88 backup register
    pub const BKP2R = MMIO(Address + 0x00000058, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 92 backup register
    pub const BKP3R = MMIO(Address + 0x0000005c, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 96 backup register
    pub const BKP4R = MMIO(Address + 0x00000060, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 100 backup register
    pub const BKP5R = MMIO(Address + 0x00000064, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 104 backup register
    pub const BKP6R = MMIO(Address + 0x00000068, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 108 backup register
    pub const BKP7R = MMIO(Address + 0x0000006c, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 112 backup register
    pub const BKP8R = MMIO(Address + 0x00000070, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 116 backup register
    pub const BKP9R = MMIO(Address + 0x00000074, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 120 backup register
    pub const BKP10R = MMIO(Address + 0x00000078, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 124 backup register
    pub const BKP11R = MMIO(Address + 0x0000007c, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 128 backup register
    pub const BKP12R = MMIO(Address + 0x00000080, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 132 backup register
    pub const BKP13R = MMIO(Address + 0x00000084, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 136 backup register
    pub const BKP14R = MMIO(Address + 0x00000088, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 140 backup register
    pub const BKP15R = MMIO(Address + 0x0000008c, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 144 backup register
    pub const BKP16R = MMIO(Address + 0x00000090, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 148 backup register
    pub const BKP17R = MMIO(Address + 0x00000094, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 152 backup register
    pub const BKP18R = MMIO(Address + 0x00000098, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 156 backup register
    pub const BKP19R = MMIO(Address + 0x0000009c, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 160 backup register
    pub const BKP20R = MMIO(Address + 0x000000a0, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 164 backup register
    pub const BKP21R = MMIO(Address + 0x000000a4, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 168 backup register
    pub const BKP22R = MMIO(Address + 0x000000a8, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 172 backup register
    pub const BKP23R = MMIO(Address + 0x000000ac, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 176 backup register
    pub const BKP24R = MMIO(Address + 0x000000b0, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 180 backup register
    pub const BKP25R = MMIO(Address + 0x000000b4, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 184 backup register
    pub const BKP26R = MMIO(Address + 0x000000b8, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 188 backup register
    pub const BKP27R = MMIO(Address + 0x000000bc, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 192 backup register
    pub const BKP28R = MMIO(Address + 0x000000c0, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 196 backup register
    pub const BKP29R = MMIO(Address + 0x000000c4, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 200 backup register
    pub const BKP30R = MMIO(Address + 0x000000c8, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
    // byte offset: 204 backup register
    pub const BKP31R = MMIO(Address + 0x000000cc, u32, packed struct {
        BKP: u32, // bit offset: 0 desc: BKP
    });
};
pub const TIM6 = extern struct {
    pub const Address: u32 = 0x40001000;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CEN: bool, // bit offset: 0 desc: Counter enable
        UDIS: bool, // bit offset: 1 desc: Update disable
        URS: bool, // bit offset: 2 desc: Update request source
        OPM: bool, // bit offset: 3 desc: One-pulse mode
        reserved0: u3 = 0,
        ARPE: bool, // bit offset: 7 desc: Auto-reload preload enable
        reserved1: u3 = 0,
        UIFREMAP: bool, // bit offset: 11 desc: UIF status bit remapping
        padding: u20 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        reserved0: u4 = 0,
        MMS: u3, // bit offset: 4 desc: Master mode selection
        padding: u25 = 0,
    });
    // byte offset: 12 DMA/Interrupt enable register
    pub const DIER = MMIO(Address + 0x0000000c, u32, packed struct {
        UIE: bool, // bit offset: 0 desc: Update interrupt enable
        reserved0: u7 = 0,
        UDE: bool, // bit offset: 8 desc: Update DMA request enable
        padding: u23 = 0,
    });
    // byte offset: 16 status register
    pub const SR = MMIO(Address + 0x00000010, u32, packed struct {
        UIF: bool, // bit offset: 0 desc: Update interrupt flag
        padding: u31 = 0,
    });
    // byte offset: 20 event generation register
    pub const EGR = MMIO(Address + 0x00000014, u32, packed struct {
        UG: bool, // bit offset: 0 desc: Update generation
        padding: u31 = 0,
    });
    // byte offset: 36 counter
    pub const CNT = MMIO(Address + 0x00000024, u32, packed struct {
        CNT: u16, // bit offset: 0 desc: Low counter value
        reserved0: u15 = 0,
        UIFCPY: bool, // bit offset: 31 desc: UIF Copy
    });
    // byte offset: 40 prescaler
    pub const PSC = MMIO(Address + 0x00000028, u32, packed struct {
        PSC: u16, // bit offset: 0 desc: Prescaler value
        padding: u16 = 0,
    });
    // byte offset: 44 auto-reload register
    pub const ARR = MMIO(Address + 0x0000002c, u32, packed struct {
        ARR: u16, // bit offset: 0 desc: Low Auto-reload value
        padding: u16 = 0,
    });
};
pub const TIM7 = extern struct {
    pub const Address: u32 = 0x40001400;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CEN: bool, // bit offset: 0 desc: Counter enable
        UDIS: bool, // bit offset: 1 desc: Update disable
        URS: bool, // bit offset: 2 desc: Update request source
        OPM: bool, // bit offset: 3 desc: One-pulse mode
        reserved0: u3 = 0,
        ARPE: bool, // bit offset: 7 desc: Auto-reload preload enable
        reserved1: u3 = 0,
        UIFREMAP: bool, // bit offset: 11 desc: UIF status bit remapping
        padding: u20 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        reserved0: u4 = 0,
        MMS: u3, // bit offset: 4 desc: Master mode selection
        padding: u25 = 0,
    });
    // byte offset: 12 DMA/Interrupt enable register
    pub const DIER = MMIO(Address + 0x0000000c, u32, packed struct {
        UIE: bool, // bit offset: 0 desc: Update interrupt enable
        reserved0: u7 = 0,
        UDE: bool, // bit offset: 8 desc: Update DMA request enable
        padding: u23 = 0,
    });
    // byte offset: 16 status register
    pub const SR = MMIO(Address + 0x00000010, u32, packed struct {
        UIF: bool, // bit offset: 0 desc: Update interrupt flag
        padding: u31 = 0,
    });
    // byte offset: 20 event generation register
    pub const EGR = MMIO(Address + 0x00000014, u32, packed struct {
        UG: bool, // bit offset: 0 desc: Update generation
        padding: u31 = 0,
    });
    // byte offset: 36 counter
    pub const CNT = MMIO(Address + 0x00000024, u32, packed struct {
        CNT: u16, // bit offset: 0 desc: Low counter value
        reserved0: u15 = 0,
        UIFCPY: bool, // bit offset: 31 desc: UIF Copy
    });
    // byte offset: 40 prescaler
    pub const PSC = MMIO(Address + 0x00000028, u32, packed struct {
        PSC: u16, // bit offset: 0 desc: Prescaler value
        padding: u16 = 0,
    });
    // byte offset: 44 auto-reload register
    pub const ARR = MMIO(Address + 0x0000002c, u32, packed struct {
        ARR: u16, // bit offset: 0 desc: Low Auto-reload value
        padding: u16 = 0,
    });
};
pub const DAC = extern struct {
    pub const Address: u32 = 0x40007400;
    // byte offset: 0 control register
    pub const CR = MMIO(Address + 0x00000000, u32, packed struct {
        EN1: bool, // bit offset: 0 desc: DAC channel1 enable
        BOFF1: bool, // bit offset: 1 desc: DAC channel1 output buffer disable
        TEN1: bool, // bit offset: 2 desc: DAC channel1 trigger enable
        TSEL1: u3, // bit offset: 3 desc: DAC channel1 trigger selection
        WAVE1: u2, // bit offset: 6 desc: DAC channel1 noise/triangle wave generation enable
        MAMP1: u4, // bit offset: 8 desc: DAC channel1 mask/amplitude selector
        DMAEN1: bool, // bit offset: 12 desc: DAC channel1 DMA enable
        DMAUDRIE1: bool, // bit offset: 13 desc: DAC channel1 DMA Underrun Interrupt enable
        reserved0: u2 = 0,
        EN2: bool, // bit offset: 16 desc: DAC channel2 enable
        BOFF2: bool, // bit offset: 17 desc: DAC channel2 output buffer disable
        TEN2: bool, // bit offset: 18 desc: DAC channel2 trigger enable
        TSEL2: u3, // bit offset: 19 desc: DAC channel2 trigger selection
        WAVE2: u2, // bit offset: 22 desc: DAC channel2 noise/triangle wave generation enable
        MAMP2: u4, // bit offset: 24 desc: DAC channel2 mask/amplitude selector
        DMAEN2: bool, // bit offset: 28 desc: DAC channel2 DMA enable
        DMAUDRIE2: bool, // bit offset: 29 desc: DAC channel2 DMA underrun interrupt enable
        padding: u2 = 0,
    });
    // byte offset: 4 software trigger register
    pub const SWTRIGR = MMIO(Address + 0x00000004, u32, packed struct {
        SWTRIG1: bool, // bit offset: 0 desc: DAC channel1 software trigger
        SWTRIG2: bool, // bit offset: 1 desc: DAC channel2 software trigger
        padding: u30 = 0,
    });
    // byte offset: 8 channel1 12-bit right-aligned data holding register
    pub const DHR12R1 = MMIO(Address + 0x00000008, u32, packed struct {
        DACC1DHR: u12, // bit offset: 0 desc: DAC channel1 12-bit right-aligned data
        padding: u20 = 0,
    });
    // byte offset: 12 channel1 12-bit left aligned data holding register
    pub const DHR12L1 = MMIO(Address + 0x0000000c, u32, packed struct {
        reserved0: u4 = 0,
        DACC1DHR: u12, // bit offset: 4 desc: DAC channel1 12-bit left-aligned data
        padding: u16 = 0,
    });
    // byte offset: 16 channel1 8-bit right aligned data holding register
    pub const DHR8R1 = MMIO(Address + 0x00000010, u32, packed struct {
        DACC1DHR: u8, // bit offset: 0 desc: DAC channel1 8-bit right-aligned data
        padding: u24 = 0,
    });
    // byte offset: 20 channel2 12-bit right aligned data holding register
    pub const DHR12R2 = MMIO(Address + 0x00000014, u32, packed struct {
        DACC2DHR: u12, // bit offset: 0 desc: DAC channel2 12-bit right-aligned data
        padding: u20 = 0,
    });
    // byte offset: 24 channel2 12-bit left aligned data holding register
    pub const DHR12L2 = MMIO(Address + 0x00000018, u32, packed struct {
        reserved0: u4 = 0,
        DACC2DHR: u12, // bit offset: 4 desc: DAC channel2 12-bit left-aligned data
        padding: u16 = 0,
    });
    // byte offset: 28 channel2 8-bit right-aligned data holding register
    pub const DHR8R2 = MMIO(Address + 0x0000001c, u32, packed struct {
        DACC2DHR: u8, // bit offset: 0 desc: DAC channel2 8-bit right-aligned data
        padding: u24 = 0,
    });
    // byte offset: 32 Dual DAC 12-bit right-aligned data holding register
    pub const DHR12RD = MMIO(Address + 0x00000020, u32, packed struct {
        DACC1DHR: u12, // bit offset: 0 desc: DAC channel1 12-bit right-aligned data
        reserved0: u4 = 0,
        DACC2DHR: u12, // bit offset: 16 desc: DAC channel2 12-bit right-aligned data
        padding: u4 = 0,
    });
    // byte offset: 36 DUAL DAC 12-bit left aligned data holding register
    pub const DHR12LD = MMIO(Address + 0x00000024, u32, packed struct {
        reserved0: u4 = 0,
        DACC1DHR: u12, // bit offset: 4 desc: DAC channel1 12-bit left-aligned data
        reserved1: u4 = 0,
        DACC2DHR: u12, // bit offset: 20 desc: DAC channel2 12-bit left-aligned data
    });
    // byte offset: 40 DUAL DAC 8-bit right aligned data holding register
    pub const DHR8RD = MMIO(Address + 0x00000028, u32, packed struct {
        DACC1DHR: u8, // bit offset: 0 desc: DAC channel1 8-bit right-aligned data
        DACC2DHR: u8, // bit offset: 8 desc: DAC channel2 8-bit right-aligned data
        padding: u16 = 0,
    });
    // byte offset: 44 channel1 data output register
    pub const DOR1 = MMIO(Address + 0x0000002c, u32, packed struct {
        DACC1DOR: u12, // bit offset: 0 desc: DAC channel1 data output
        padding: u20 = 0,
    });
    // byte offset: 48 channel2 data output register
    pub const DOR2 = MMIO(Address + 0x00000030, u32, packed struct {
        DACC2DOR: u12, // bit offset: 0 desc: DAC channel2 data output
        padding: u20 = 0,
    });
    // byte offset: 52 status register
    pub const SR = MMIO(Address + 0x00000034, u32, packed struct {
        reserved0: u13 = 0,
        DMAUDR1: bool, // bit offset: 13 desc: DAC channel1 DMA underrun flag
        reserved1: u15 = 0,
        DMAUDR2: bool, // bit offset: 29 desc: DAC channel2 DMA underrun flag
        padding: u2 = 0,
    });
};
pub const DBGMCU = extern struct {
    pub const Address: u32 = 0xe0042000;
    // byte offset: 0 MCU Device ID Code Register
    pub const IDCODE = MMIO(Address + 0x00000000, u32, packed struct {
        DEV_ID: u12, // bit offset: 0 desc: Device Identifier
        reserved0: u4 = 0,
        REV_ID: u16, // bit offset: 16 desc: Revision Identifier
    });
    // byte offset: 4 Debug MCU Configuration Register
    pub const CR = MMIO(Address + 0x00000004, u32, packed struct {
        DBG_SLEEP: bool, // bit offset: 0 desc: Debug Sleep mode
        DBG_STOP: bool, // bit offset: 1 desc: Debug Stop Mode
        DBG_STANDBY: bool, // bit offset: 2 desc: Debug Standby Mode
        reserved0: u2 = 0,
        TRACE_IOEN: bool, // bit offset: 5 desc: Trace pin assignment control
        TRACE_MODE: u2, // bit offset: 6 desc: Trace pin assignment control
        padding: u24 = 0,
    });
    // byte offset: 8 APB Low Freeze Register
    pub const APB1FZ = MMIO(Address + 0x00000008, u32, packed struct {
        DBG_TIM2_STOP: bool, // bit offset: 0 desc: Debug Timer 2 stopped when Core is halted
        DBG_TIM3_STOP: bool, // bit offset: 1 desc: Debug Timer 3 stopped when Core is halted
        DBG_TIM4_STOP: bool, // bit offset: 2 desc: Debug Timer 4 stopped when Core is halted
        DBG_TIM5_STOP: bool, // bit offset: 3 desc: Debug Timer 5 stopped when Core is halted
        DBG_TIM6_STOP: bool, // bit offset: 4 desc: Debug Timer 6 stopped when Core is halted
        DBG_TIM7_STOP: bool, // bit offset: 5 desc: Debug Timer 7 stopped when Core is halted
        DBG_TIM12_STOP: bool, // bit offset: 6 desc: Debug Timer 12 stopped when Core is halted
        DBG_TIM13_STOP: bool, // bit offset: 7 desc: Debug Timer 13 stopped when Core is halted
        DBG_TIMER14_STOP: bool, // bit offset: 8 desc: Debug Timer 14 stopped when Core is halted
        DBG_TIM18_STOP: bool, // bit offset: 9 desc: Debug Timer 18 stopped when Core is halted
        DBG_RTC_STOP: bool, // bit offset: 10 desc: Debug RTC stopped when Core is halted
        DBG_WWDG_STOP: bool, // bit offset: 11 desc: Debug Window Wachdog stopped when Core is halted
        DBG_IWDG_STOP: bool, // bit offset: 12 desc: Debug Independent Wachdog stopped when Core is halted
        reserved0: u8 = 0,
        I2C1_SMBUS_TIMEOUT: bool, // bit offset: 21 desc: SMBUS timeout mode stopped when Core is halted
        I2C2_SMBUS_TIMEOUT: bool, // bit offset: 22 desc: SMBUS timeout mode stopped when Core is halted
        reserved1: u2 = 0,
        DBG_CAN_STOP: bool, // bit offset: 25 desc: Debug CAN stopped when core is halted
        padding: u6 = 0,
    });
    // byte offset: 12 APB High Freeze Register
    pub const APB2FZ = MMIO(Address + 0x0000000c, u32, packed struct {
        reserved0: u2 = 0,
        DBG_TIM15_STOP: bool, // bit offset: 2 desc: Debug Timer 15 stopped when Core is halted
        DBG_TIM16_STOP: bool, // bit offset: 3 desc: Debug Timer 16 stopped when Core is halted
        DBG_TIM17_STO: bool, // bit offset: 4 desc: Debug Timer 17 stopped when Core is halted
        DBG_TIM19_STOP: bool, // bit offset: 5 desc: Debug Timer 19 stopped when Core is halted
        padding: u26 = 0,
    });
};
pub const TIM1 = extern struct {
    pub const Address: u32 = 0x40012c00;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CEN: bool, // bit offset: 0 desc: Counter enable
        UDIS: bool, // bit offset: 1 desc: Update disable
        URS: bool, // bit offset: 2 desc: Update request source
        OPM: bool, // bit offset: 3 desc: One-pulse mode
        DIR: bool, // bit offset: 4 desc: Direction
        CMS: u2, // bit offset: 5 desc: Center-aligned mode selection
        ARPE: bool, // bit offset: 7 desc: Auto-reload preload enable
        CKD: u2, // bit offset: 8 desc: Clock division
        reserved0: u1 = 0,
        UIFREMAP: bool, // bit offset: 11 desc: UIF status bit remapping
        padding: u20 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        CCPC: bool, // bit offset: 0 desc: Capture/compare preloaded control
        reserved0: u1 = 0,
        CCUS: bool, // bit offset: 2 desc: Capture/compare control update selection
        CCDS: bool, // bit offset: 3 desc: Capture/compare DMA selection
        MMS: u3, // bit offset: 4 desc: Master mode selection
        TI1S: bool, // bit offset: 7 desc: TI1 selection
        OIS1: bool, // bit offset: 8 desc: Output Idle state 1
        OIS1N: bool, // bit offset: 9 desc: Output Idle state 1
        OIS2: bool, // bit offset: 10 desc: Output Idle state 2
        OIS2N: bool, // bit offset: 11 desc: Output Idle state 2
        OIS3: bool, // bit offset: 12 desc: Output Idle state 3
        OIS3N: bool, // bit offset: 13 desc: Output Idle state 3
        OIS4: bool, // bit offset: 14 desc: Output Idle state 4
        reserved1: u1 = 0,
        OIS5: bool, // bit offset: 16 desc: Output Idle state 5
        reserved2: u1 = 0,
        OIS6: bool, // bit offset: 18 desc: Output Idle state 6
        reserved3: u1 = 0,
        MMS2: u4, // bit offset: 20 desc: Master mode selection 2
        padding: u8 = 0,
    });
    // byte offset: 8 slave mode control register
    pub const SMCR = MMIO(Address + 0x00000008, u32, packed struct {
        SMS: u3, // bit offset: 0 desc: Slave mode selection
        OCCS: bool, // bit offset: 3 desc: OCREF clear selection
        TS: u3, // bit offset: 4 desc: Trigger selection
        MSM: bool, // bit offset: 7 desc: Master/Slave mode
        ETF: u4, // bit offset: 8 desc: External trigger filter
        ETPS: u2, // bit offset: 12 desc: External trigger prescaler
        ECE: bool, // bit offset: 14 desc: External clock enable
        ETP: bool, // bit offset: 15 desc: External trigger polarity
        SMS3: bool, // bit offset: 16 desc: Slave mode selection bit 3
        padding: u15 = 0,
    });
    // byte offset: 12 DMA/Interrupt enable register
    pub const DIER = MMIO(Address + 0x0000000c, u32, packed struct {
        UIE: bool, // bit offset: 0 desc: Update interrupt enable
        CC1IE: bool, // bit offset: 1 desc: Capture/Compare 1 interrupt enable
        CC2IE: bool, // bit offset: 2 desc: Capture/Compare 2 interrupt enable
        CC3IE: bool, // bit offset: 3 desc: Capture/Compare 3 interrupt enable
        CC4IE: bool, // bit offset: 4 desc: Capture/Compare 4 interrupt enable
        COMIE: bool, // bit offset: 5 desc: COM interrupt enable
        TIE: bool, // bit offset: 6 desc: Trigger interrupt enable
        BIE: bool, // bit offset: 7 desc: Break interrupt enable
        UDE: bool, // bit offset: 8 desc: Update DMA request enable
        CC1DE: bool, // bit offset: 9 desc: Capture/Compare 1 DMA request enable
        CC2DE: bool, // bit offset: 10 desc: Capture/Compare 2 DMA request enable
        CC3DE: bool, // bit offset: 11 desc: Capture/Compare 3 DMA request enable
        CC4DE: bool, // bit offset: 12 desc: Capture/Compare 4 DMA request enable
        COMDE: bool, // bit offset: 13 desc: COM DMA request enable
        TDE: bool, // bit offset: 14 desc: Trigger DMA request enable
        padding: u17 = 0,
    });
    // byte offset: 16 status register
    pub const SR = MMIO(Address + 0x00000010, u32, packed struct {
        UIF: bool, // bit offset: 0 desc: Update interrupt flag
        CC1IF: bool, // bit offset: 1 desc: Capture/compare 1 interrupt flag
        CC2IF: bool, // bit offset: 2 desc: Capture/Compare 2 interrupt flag
        CC3IF: bool, // bit offset: 3 desc: Capture/Compare 3 interrupt flag
        CC4IF: bool, // bit offset: 4 desc: Capture/Compare 4 interrupt flag
        COMIF: bool, // bit offset: 5 desc: COM interrupt flag
        TIF: bool, // bit offset: 6 desc: Trigger interrupt flag
        BIF: bool, // bit offset: 7 desc: Break interrupt flag
        B2IF: bool, // bit offset: 8 desc: Break 2 interrupt flag
        CC1OF: bool, // bit offset: 9 desc: Capture/Compare 1 overcapture flag
        CC2OF: bool, // bit offset: 10 desc: Capture/compare 2 overcapture flag
        CC3OF: bool, // bit offset: 11 desc: Capture/Compare 3 overcapture flag
        CC4OF: bool, // bit offset: 12 desc: Capture/Compare 4 overcapture flag
        reserved0: u3 = 0,
        C5IF: bool, // bit offset: 16 desc: Capture/Compare 5 interrupt flag
        C6IF: bool, // bit offset: 17 desc: Capture/Compare 6 interrupt flag
        padding: u14 = 0,
    });
    // byte offset: 20 event generation register
    pub const EGR = MMIO(Address + 0x00000014, u32, packed struct {
        UG: bool, // bit offset: 0 desc: Update generation
        CC1G: bool, // bit offset: 1 desc: Capture/compare 1 generation
        CC2G: bool, // bit offset: 2 desc: Capture/compare 2 generation
        CC3G: bool, // bit offset: 3 desc: Capture/compare 3 generation
        CC4G: bool, // bit offset: 4 desc: Capture/compare 4 generation
        COMG: bool, // bit offset: 5 desc: Capture/Compare control update generation
        TG: bool, // bit offset: 6 desc: Trigger generation
        BG: bool, // bit offset: 7 desc: Break generation
        B2G: bool, // bit offset: 8 desc: Break 2 generation
        padding: u23 = 0,
    });
    // byte offset: 24 capture/compare mode register (output mode)
    pub const CCMR1_Output = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        OC1FE: bool, // bit offset: 2 desc: Output Compare 1 fast enable
        OC1PE: bool, // bit offset: 3 desc: Output Compare 1 preload enable
        OC1M: u3, // bit offset: 4 desc: Output Compare 1 mode
        OC1CE: bool, // bit offset: 7 desc: Output Compare 1 clear enable
        CC2S: u2, // bit offset: 8 desc: Capture/Compare 2 selection
        OC2FE: bool, // bit offset: 10 desc: Output Compare 2 fast enable
        OC2PE: bool, // bit offset: 11 desc: Output Compare 2 preload enable
        OC2M: u3, // bit offset: 12 desc: Output Compare 2 mode
        OC2CE: bool, // bit offset: 15 desc: Output Compare 2 clear enable
        OC1M_3: bool, // bit offset: 16 desc: Output Compare 1 mode bit 3
        reserved0: u7 = 0,
        OC2M_3: bool, // bit offset: 24 desc: Output Compare 2 mode bit 3
        padding: u7 = 0,
    });
    // byte offset: 24 capture/compare mode register 1 (input mode)
    pub const CCMR1_Input = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        IC1PCS: u2, // bit offset: 2 desc: Input capture 1 prescaler
        IC1F: u4, // bit offset: 4 desc: Input capture 1 filter
        CC2S: u2, // bit offset: 8 desc: Capture/Compare 2 selection
        IC2PCS: u2, // bit offset: 10 desc: Input capture 2 prescaler
        IC2F: u4, // bit offset: 12 desc: Input capture 2 filter
        padding: u16 = 0,
    });
    // byte offset: 28 capture/compare mode register (output mode)
    pub const CCMR2_Output = MMIO(Address + 0x0000001c, u32, packed struct {
        CC3S: u2, // bit offset: 0 desc: Capture/Compare 3 selection
        OC3FE: bool, // bit offset: 2 desc: Output compare 3 fast enable
        OC3PE: bool, // bit offset: 3 desc: Output compare 3 preload enable
        OC3M: u3, // bit offset: 4 desc: Output compare 3 mode
        OC3CE: bool, // bit offset: 7 desc: Output compare 3 clear enable
        CC4S: u2, // bit offset: 8 desc: Capture/Compare 4 selection
        OC4FE: bool, // bit offset: 10 desc: Output compare 4 fast enable
        OC4PE: bool, // bit offset: 11 desc: Output compare 4 preload enable
        OC4M: u3, // bit offset: 12 desc: Output compare 4 mode
        OC4CE: bool, // bit offset: 15 desc: Output compare 4 clear enable
        OC3M_3: bool, // bit offset: 16 desc: Output Compare 3 mode bit 3
        reserved0: u7 = 0,
        OC4M_3: bool, // bit offset: 24 desc: Output Compare 4 mode bit 3
        padding: u7 = 0,
    });
    // byte offset: 28 capture/compare mode register 2 (input mode)
    pub const CCMR2_Input = MMIO(Address + 0x0000001c, u32, packed struct {
        CC3S: u2, // bit offset: 0 desc: Capture/compare 3 selection
        IC3PSC: u2, // bit offset: 2 desc: Input capture 3 prescaler
        IC3F: u4, // bit offset: 4 desc: Input capture 3 filter
        CC4S: u2, // bit offset: 8 desc: Capture/Compare 4 selection
        IC4PSC: u2, // bit offset: 10 desc: Input capture 4 prescaler
        IC4F: u4, // bit offset: 12 desc: Input capture 4 filter
        padding: u16 = 0,
    });
    // byte offset: 32 capture/compare enable register
    pub const CCER = MMIO(Address + 0x00000020, u32, packed struct {
        CC1E: bool, // bit offset: 0 desc: Capture/Compare 1 output enable
        CC1P: bool, // bit offset: 1 desc: Capture/Compare 1 output Polarity
        CC1NE: bool, // bit offset: 2 desc: Capture/Compare 1 complementary output enable
        CC1NP: bool, // bit offset: 3 desc: Capture/Compare 1 output Polarity
        CC2E: bool, // bit offset: 4 desc: Capture/Compare 2 output enable
        CC2P: bool, // bit offset: 5 desc: Capture/Compare 2 output Polarity
        CC2NE: bool, // bit offset: 6 desc: Capture/Compare 2 complementary output enable
        CC2NP: bool, // bit offset: 7 desc: Capture/Compare 2 output Polarity
        CC3E: bool, // bit offset: 8 desc: Capture/Compare 3 output enable
        CC3P: bool, // bit offset: 9 desc: Capture/Compare 3 output Polarity
        CC3NE: bool, // bit offset: 10 desc: Capture/Compare 3 complementary output enable
        CC3NP: bool, // bit offset: 11 desc: Capture/Compare 3 output Polarity
        CC4E: bool, // bit offset: 12 desc: Capture/Compare 4 output enable
        CC4P: bool, // bit offset: 13 desc: Capture/Compare 3 output Polarity
        reserved0: u1 = 0,
        CC4NP: bool, // bit offset: 15 desc: Capture/Compare 4 output Polarity
        CC5E: bool, // bit offset: 16 desc: Capture/Compare 5 output enable
        CC5P: bool, // bit offset: 17 desc: Capture/Compare 5 output Polarity
        reserved1: u2 = 0,
        CC6E: bool, // bit offset: 20 desc: Capture/Compare 6 output enable
        CC6P: bool, // bit offset: 21 desc: Capture/Compare 6 output Polarity
        padding: u10 = 0,
    });
    // byte offset: 36 counter
    pub const CNT = MMIO(Address + 0x00000024, u32, packed struct {
        CNT: u16, // bit offset: 0 desc: counter value
        reserved0: u15 = 0,
        UIFCPY: bool, // bit offset: 31 desc: UIF copy
    });
    // byte offset: 40 prescaler
    pub const PSC = MMIO(Address + 0x00000028, u32, packed struct {
        PSC: u16, // bit offset: 0 desc: Prescaler value
        padding: u16 = 0,
    });
    // byte offset: 44 auto-reload register
    pub const ARR = MMIO(Address + 0x0000002c, u32, packed struct {
        ARR: u16, // bit offset: 0 desc: Auto-reload value
        padding: u16 = 0,
    });
    // byte offset: 48 repetition counter register
    pub const RCR = MMIO(Address + 0x00000030, u32, packed struct {
        REP: u16, // bit offset: 0 desc: Repetition counter value
        padding: u16 = 0,
    });
    // byte offset: 52 capture/compare register 1
    pub const CCR1 = MMIO(Address + 0x00000034, u32, packed struct {
        CCR1: u16, // bit offset: 0 desc: Capture/Compare 1 value
        padding: u16 = 0,
    });
    // byte offset: 56 capture/compare register 2
    pub const CCR2 = MMIO(Address + 0x00000038, u32, packed struct {
        CCR2: u16, // bit offset: 0 desc: Capture/Compare 2 value
        padding: u16 = 0,
    });
    // byte offset: 60 capture/compare register 3
    pub const CCR3 = MMIO(Address + 0x0000003c, u32, packed struct {
        CCR3: u16, // bit offset: 0 desc: Capture/Compare 3 value
        padding: u16 = 0,
    });
    // byte offset: 64 capture/compare register 4
    pub const CCR4 = MMIO(Address + 0x00000040, u32, packed struct {
        CCR4: u16, // bit offset: 0 desc: Capture/Compare 3 value
        padding: u16 = 0,
    });
    // byte offset: 68 break and dead-time register
    pub const BDTR = MMIO(Address + 0x00000044, u32, packed struct {
        DTG: u8, // bit offset: 0 desc: Dead-time generator setup
        LOCK: u2, // bit offset: 8 desc: Lock configuration
        OSSI: bool, // bit offset: 10 desc: Off-state selection for Idle mode
        OSSR: bool, // bit offset: 11 desc: Off-state selection for Run mode
        BKE: bool, // bit offset: 12 desc: Break enable
        BKP: bool, // bit offset: 13 desc: Break polarity
        AOE: bool, // bit offset: 14 desc: Automatic output enable
        MOE: bool, // bit offset: 15 desc: Main output enable
        BKF: u4, // bit offset: 16 desc: Break filter
        BK2F: u4, // bit offset: 20 desc: Break 2 filter
        BK2E: bool, // bit offset: 24 desc: Break 2 enable
        BK2P: bool, // bit offset: 25 desc: Break 2 polarity
        padding: u6 = 0,
    });
    // byte offset: 72 DMA control register
    pub const DCR = MMIO(Address + 0x00000048, u32, packed struct {
        DBA: u5, // bit offset: 0 desc: DMA base address
        reserved0: u3 = 0,
        DBL: u5, // bit offset: 8 desc: DMA burst length
        padding: u19 = 0,
    });
    // byte offset: 76 DMA address for full transfer
    pub const DMAR = MMIO(Address + 0x0000004c, u32, packed struct {
        DMAB: u16, // bit offset: 0 desc: DMA register for burst accesses
        padding: u16 = 0,
    });
    // byte offset: 84 capture/compare mode register 3 (output mode)
    pub const CCMR3_Output = MMIO(Address + 0x00000054, u32, packed struct {
        reserved0: u2 = 0,
        OC5FE: bool, // bit offset: 2 desc: Output compare 5 fast enable
        OC5PE: bool, // bit offset: 3 desc: Output compare 5 preload enable
        OC5M: u3, // bit offset: 4 desc: Output compare 5 mode
        OC5CE: bool, // bit offset: 7 desc: Output compare 5 clear enable
        reserved1: u2 = 0,
        OC6FE: bool, // bit offset: 10 desc: Output compare 6 fast enable
        OC6PE: bool, // bit offset: 11 desc: Output compare 6 preload enable
        OC6M: u3, // bit offset: 12 desc: Output compare 6 mode
        OC6CE: bool, // bit offset: 15 desc: Output compare 6 clear enable
        OC5M_3: bool, // bit offset: 16 desc: Outout Compare 5 mode bit 3
        reserved2: u7 = 0,
        OC6M_3: bool, // bit offset: 24 desc: Outout Compare 6 mode bit 3
        padding: u7 = 0,
    });
    // byte offset: 88 capture/compare register 5
    pub const CCR5 = MMIO(Address + 0x00000058, u32, packed struct {
        CCR5: u16, // bit offset: 0 desc: Capture/Compare 5 value
        reserved0: u13 = 0,
        GC5C1: bool, // bit offset: 29 desc: Group Channel 5 and Channel 1
        GC5C2: bool, // bit offset: 30 desc: Group Channel 5 and Channel 2
        GC5C3: bool, // bit offset: 31 desc: Group Channel 5 and Channel 3
    });
    // byte offset: 92 capture/compare register 6
    pub const CCR6 = MMIO(Address + 0x0000005c, u32, packed struct {
        CCR6: u16, // bit offset: 0 desc: Capture/Compare 6 value
        padding: u16 = 0,
    });
    // byte offset: 96 option registers
    pub const OR = MMIO(Address + 0x00000060, u32, packed struct {
        TIM1_ETR_ADC1_RMP: u2, // bit offset: 0 desc: TIM1_ETR_ADC1 remapping capability
        TIM1_ETR_ADC4_RMP: u2, // bit offset: 2 desc: TIM1_ETR_ADC4 remapping capability
        padding: u28 = 0,
    });
};
pub const TIM20 = extern struct {
    pub const Address: u32 = 0x40015000;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CEN: bool, // bit offset: 0 desc: Counter enable
        UDIS: bool, // bit offset: 1 desc: Update disable
        URS: bool, // bit offset: 2 desc: Update request source
        OPM: bool, // bit offset: 3 desc: One-pulse mode
        DIR: bool, // bit offset: 4 desc: Direction
        CMS: u2, // bit offset: 5 desc: Center-aligned mode selection
        ARPE: bool, // bit offset: 7 desc: Auto-reload preload enable
        CKD: u2, // bit offset: 8 desc: Clock division
        reserved0: u1 = 0,
        UIFREMAP: bool, // bit offset: 11 desc: UIF status bit remapping
        padding: u20 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        CCPC: bool, // bit offset: 0 desc: Capture/compare preloaded control
        reserved0: u1 = 0,
        CCUS: bool, // bit offset: 2 desc: Capture/compare control update selection
        CCDS: bool, // bit offset: 3 desc: Capture/compare DMA selection
        MMS: u3, // bit offset: 4 desc: Master mode selection
        TI1S: bool, // bit offset: 7 desc: TI1 selection
        OIS1: bool, // bit offset: 8 desc: Output Idle state 1
        OIS1N: bool, // bit offset: 9 desc: Output Idle state 1
        OIS2: bool, // bit offset: 10 desc: Output Idle state 2
        OIS2N: bool, // bit offset: 11 desc: Output Idle state 2
        OIS3: bool, // bit offset: 12 desc: Output Idle state 3
        OIS3N: bool, // bit offset: 13 desc: Output Idle state 3
        OIS4: bool, // bit offset: 14 desc: Output Idle state 4
        reserved1: u1 = 0,
        OIS5: bool, // bit offset: 16 desc: Output Idle state 5
        reserved2: u1 = 0,
        OIS6: bool, // bit offset: 18 desc: Output Idle state 6
        reserved3: u1 = 0,
        MMS2: u4, // bit offset: 20 desc: Master mode selection 2
        padding: u8 = 0,
    });
    // byte offset: 8 slave mode control register
    pub const SMCR = MMIO(Address + 0x00000008, u32, packed struct {
        SMS: u3, // bit offset: 0 desc: Slave mode selection
        OCCS: bool, // bit offset: 3 desc: OCREF clear selection
        TS: u3, // bit offset: 4 desc: Trigger selection
        MSM: bool, // bit offset: 7 desc: Master/Slave mode
        ETF: u4, // bit offset: 8 desc: External trigger filter
        ETPS: u2, // bit offset: 12 desc: External trigger prescaler
        ECE: bool, // bit offset: 14 desc: External clock enable
        ETP: bool, // bit offset: 15 desc: External trigger polarity
        SMS3: bool, // bit offset: 16 desc: Slave mode selection bit 3
        padding: u15 = 0,
    });
    // byte offset: 12 DMA/Interrupt enable register
    pub const DIER = MMIO(Address + 0x0000000c, u32, packed struct {
        UIE: bool, // bit offset: 0 desc: Update interrupt enable
        CC1IE: bool, // bit offset: 1 desc: Capture/Compare 1 interrupt enable
        CC2IE: bool, // bit offset: 2 desc: Capture/Compare 2 interrupt enable
        CC3IE: bool, // bit offset: 3 desc: Capture/Compare 3 interrupt enable
        CC4IE: bool, // bit offset: 4 desc: Capture/Compare 4 interrupt enable
        COMIE: bool, // bit offset: 5 desc: COM interrupt enable
        TIE: bool, // bit offset: 6 desc: Trigger interrupt enable
        BIE: bool, // bit offset: 7 desc: Break interrupt enable
        UDE: bool, // bit offset: 8 desc: Update DMA request enable
        CC1DE: bool, // bit offset: 9 desc: Capture/Compare 1 DMA request enable
        CC2DE: bool, // bit offset: 10 desc: Capture/Compare 2 DMA request enable
        CC3DE: bool, // bit offset: 11 desc: Capture/Compare 3 DMA request enable
        CC4DE: bool, // bit offset: 12 desc: Capture/Compare 4 DMA request enable
        COMDE: bool, // bit offset: 13 desc: COM DMA request enable
        TDE: bool, // bit offset: 14 desc: Trigger DMA request enable
        padding: u17 = 0,
    });
    // byte offset: 16 status register
    pub const SR = MMIO(Address + 0x00000010, u32, packed struct {
        UIF: bool, // bit offset: 0 desc: Update interrupt flag
        CC1IF: bool, // bit offset: 1 desc: Capture/compare 1 interrupt flag
        CC2IF: bool, // bit offset: 2 desc: Capture/Compare 2 interrupt flag
        CC3IF: bool, // bit offset: 3 desc: Capture/Compare 3 interrupt flag
        CC4IF: bool, // bit offset: 4 desc: Capture/Compare 4 interrupt flag
        COMIF: bool, // bit offset: 5 desc: COM interrupt flag
        TIF: bool, // bit offset: 6 desc: Trigger interrupt flag
        BIF: bool, // bit offset: 7 desc: Break interrupt flag
        B2IF: bool, // bit offset: 8 desc: Break 2 interrupt flag
        CC1OF: bool, // bit offset: 9 desc: Capture/Compare 1 overcapture flag
        CC2OF: bool, // bit offset: 10 desc: Capture/compare 2 overcapture flag
        CC3OF: bool, // bit offset: 11 desc: Capture/Compare 3 overcapture flag
        CC4OF: bool, // bit offset: 12 desc: Capture/Compare 4 overcapture flag
        reserved0: u3 = 0,
        C5IF: bool, // bit offset: 16 desc: Capture/Compare 5 interrupt flag
        C6IF: bool, // bit offset: 17 desc: Capture/Compare 6 interrupt flag
        padding: u14 = 0,
    });
    // byte offset: 20 event generation register
    pub const EGR = MMIO(Address + 0x00000014, u32, packed struct {
        UG: bool, // bit offset: 0 desc: Update generation
        CC1G: bool, // bit offset: 1 desc: Capture/compare 1 generation
        CC2G: bool, // bit offset: 2 desc: Capture/compare 2 generation
        CC3G: bool, // bit offset: 3 desc: Capture/compare 3 generation
        CC4G: bool, // bit offset: 4 desc: Capture/compare 4 generation
        COMG: bool, // bit offset: 5 desc: Capture/Compare control update generation
        TG: bool, // bit offset: 6 desc: Trigger generation
        BG: bool, // bit offset: 7 desc: Break generation
        B2G: bool, // bit offset: 8 desc: Break 2 generation
        padding: u23 = 0,
    });
    // byte offset: 24 capture/compare mode register (output mode)
    pub const CCMR1_Output = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        OC1FE: bool, // bit offset: 2 desc: Output Compare 1 fast enable
        OC1PE: bool, // bit offset: 3 desc: Output Compare 1 preload enable
        OC1M: u3, // bit offset: 4 desc: Output Compare 1 mode
        OC1CE: bool, // bit offset: 7 desc: Output Compare 1 clear enable
        CC2S: u2, // bit offset: 8 desc: Capture/Compare 2 selection
        OC2FE: bool, // bit offset: 10 desc: Output Compare 2 fast enable
        OC2PE: bool, // bit offset: 11 desc: Output Compare 2 preload enable
        OC2M: u3, // bit offset: 12 desc: Output Compare 2 mode
        OC2CE: bool, // bit offset: 15 desc: Output Compare 2 clear enable
        OC1M_3: bool, // bit offset: 16 desc: Output Compare 1 mode bit 3
        reserved0: u7 = 0,
        OC2M_3: bool, // bit offset: 24 desc: Output Compare 2 mode bit 3
        padding: u7 = 0,
    });
    // byte offset: 24 capture/compare mode register 1 (input mode)
    pub const CCMR1_Input = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        IC1PCS: u2, // bit offset: 2 desc: Input capture 1 prescaler
        IC1F: u4, // bit offset: 4 desc: Input capture 1 filter
        CC2S: u2, // bit offset: 8 desc: Capture/Compare 2 selection
        IC2PCS: u2, // bit offset: 10 desc: Input capture 2 prescaler
        IC2F: u4, // bit offset: 12 desc: Input capture 2 filter
        padding: u16 = 0,
    });
    // byte offset: 28 capture/compare mode register (output mode)
    pub const CCMR2_Output = MMIO(Address + 0x0000001c, u32, packed struct {
        CC3S: u2, // bit offset: 0 desc: Capture/Compare 3 selection
        OC3FE: bool, // bit offset: 2 desc: Output compare 3 fast enable
        OC3PE: bool, // bit offset: 3 desc: Output compare 3 preload enable
        OC3M: u3, // bit offset: 4 desc: Output compare 3 mode
        OC3CE: bool, // bit offset: 7 desc: Output compare 3 clear enable
        CC4S: u2, // bit offset: 8 desc: Capture/Compare 4 selection
        OC4FE: bool, // bit offset: 10 desc: Output compare 4 fast enable
        OC4PE: bool, // bit offset: 11 desc: Output compare 4 preload enable
        OC4M: u3, // bit offset: 12 desc: Output compare 4 mode
        OC4CE: bool, // bit offset: 15 desc: Output compare 4 clear enable
        OC3M_3: bool, // bit offset: 16 desc: Output Compare 3 mode bit 3
        reserved0: u7 = 0,
        OC4M_3: bool, // bit offset: 24 desc: Output Compare 4 mode bit 3
        padding: u7 = 0,
    });
    // byte offset: 28 capture/compare mode register 2 (input mode)
    pub const CCMR2_Input = MMIO(Address + 0x0000001c, u32, packed struct {
        CC3S: u2, // bit offset: 0 desc: Capture/compare 3 selection
        IC3PSC: u2, // bit offset: 2 desc: Input capture 3 prescaler
        IC3F: u4, // bit offset: 4 desc: Input capture 3 filter
        CC4S: u2, // bit offset: 8 desc: Capture/Compare 4 selection
        IC4PSC: u2, // bit offset: 10 desc: Input capture 4 prescaler
        IC4F: u4, // bit offset: 12 desc: Input capture 4 filter
        padding: u16 = 0,
    });
    // byte offset: 32 capture/compare enable register
    pub const CCER = MMIO(Address + 0x00000020, u32, packed struct {
        CC1E: bool, // bit offset: 0 desc: Capture/Compare 1 output enable
        CC1P: bool, // bit offset: 1 desc: Capture/Compare 1 output Polarity
        CC1NE: bool, // bit offset: 2 desc: Capture/Compare 1 complementary output enable
        CC1NP: bool, // bit offset: 3 desc: Capture/Compare 1 output Polarity
        CC2E: bool, // bit offset: 4 desc: Capture/Compare 2 output enable
        CC2P: bool, // bit offset: 5 desc: Capture/Compare 2 output Polarity
        CC2NE: bool, // bit offset: 6 desc: Capture/Compare 2 complementary output enable
        CC2NP: bool, // bit offset: 7 desc: Capture/Compare 2 output Polarity
        CC3E: bool, // bit offset: 8 desc: Capture/Compare 3 output enable
        CC3P: bool, // bit offset: 9 desc: Capture/Compare 3 output Polarity
        CC3NE: bool, // bit offset: 10 desc: Capture/Compare 3 complementary output enable
        CC3NP: bool, // bit offset: 11 desc: Capture/Compare 3 output Polarity
        CC4E: bool, // bit offset: 12 desc: Capture/Compare 4 output enable
        CC4P: bool, // bit offset: 13 desc: Capture/Compare 3 output Polarity
        reserved0: u1 = 0,
        CC4NP: bool, // bit offset: 15 desc: Capture/Compare 4 output Polarity
        CC5E: bool, // bit offset: 16 desc: Capture/Compare 5 output enable
        CC5P: bool, // bit offset: 17 desc: Capture/Compare 5 output Polarity
        reserved1: u2 = 0,
        CC6E: bool, // bit offset: 20 desc: Capture/Compare 6 output enable
        CC6P: bool, // bit offset: 21 desc: Capture/Compare 6 output Polarity
        padding: u10 = 0,
    });
    // byte offset: 36 counter
    pub const CNT = MMIO(Address + 0x00000024, u32, packed struct {
        CNT: u16, // bit offset: 0 desc: counter value
        reserved0: u15 = 0,
        UIFCPY: bool, // bit offset: 31 desc: UIF copy
    });
    // byte offset: 40 prescaler
    pub const PSC = MMIO(Address + 0x00000028, u32, packed struct {
        PSC: u16, // bit offset: 0 desc: Prescaler value
        padding: u16 = 0,
    });
    // byte offset: 44 auto-reload register
    pub const ARR = MMIO(Address + 0x0000002c, u32, packed struct {
        ARR: u16, // bit offset: 0 desc: Auto-reload value
        padding: u16 = 0,
    });
    // byte offset: 48 repetition counter register
    pub const RCR = MMIO(Address + 0x00000030, u32, packed struct {
        REP: u16, // bit offset: 0 desc: Repetition counter value
        padding: u16 = 0,
    });
    // byte offset: 52 capture/compare register 1
    pub const CCR1 = MMIO(Address + 0x00000034, u32, packed struct {
        CCR1: u16, // bit offset: 0 desc: Capture/Compare 1 value
        padding: u16 = 0,
    });
    // byte offset: 56 capture/compare register 2
    pub const CCR2 = MMIO(Address + 0x00000038, u32, packed struct {
        CCR2: u16, // bit offset: 0 desc: Capture/Compare 2 value
        padding: u16 = 0,
    });
    // byte offset: 60 capture/compare register 3
    pub const CCR3 = MMIO(Address + 0x0000003c, u32, packed struct {
        CCR3: u16, // bit offset: 0 desc: Capture/Compare 3 value
        padding: u16 = 0,
    });
    // byte offset: 64 capture/compare register 4
    pub const CCR4 = MMIO(Address + 0x00000040, u32, packed struct {
        CCR4: u16, // bit offset: 0 desc: Capture/Compare 3 value
        padding: u16 = 0,
    });
    // byte offset: 68 break and dead-time register
    pub const BDTR = MMIO(Address + 0x00000044, u32, packed struct {
        DTG: u8, // bit offset: 0 desc: Dead-time generator setup
        LOCK: u2, // bit offset: 8 desc: Lock configuration
        OSSI: bool, // bit offset: 10 desc: Off-state selection for Idle mode
        OSSR: bool, // bit offset: 11 desc: Off-state selection for Run mode
        BKE: bool, // bit offset: 12 desc: Break enable
        BKP: bool, // bit offset: 13 desc: Break polarity
        AOE: bool, // bit offset: 14 desc: Automatic output enable
        MOE: bool, // bit offset: 15 desc: Main output enable
        BKF: u4, // bit offset: 16 desc: Break filter
        BK2F: u4, // bit offset: 20 desc: Break 2 filter
        BK2E: bool, // bit offset: 24 desc: Break 2 enable
        BK2P: bool, // bit offset: 25 desc: Break 2 polarity
        padding: u6 = 0,
    });
    // byte offset: 72 DMA control register
    pub const DCR = MMIO(Address + 0x00000048, u32, packed struct {
        DBA: u5, // bit offset: 0 desc: DMA base address
        reserved0: u3 = 0,
        DBL: u5, // bit offset: 8 desc: DMA burst length
        padding: u19 = 0,
    });
    // byte offset: 76 DMA address for full transfer
    pub const DMAR = MMIO(Address + 0x0000004c, u32, packed struct {
        DMAB: u16, // bit offset: 0 desc: DMA register for burst accesses
        padding: u16 = 0,
    });
    // byte offset: 84 capture/compare mode register 3 (output mode)
    pub const CCMR3_Output = MMIO(Address + 0x00000054, u32, packed struct {
        reserved0: u2 = 0,
        OC5FE: bool, // bit offset: 2 desc: Output compare 5 fast enable
        OC5PE: bool, // bit offset: 3 desc: Output compare 5 preload enable
        OC5M: u3, // bit offset: 4 desc: Output compare 5 mode
        OC5CE: bool, // bit offset: 7 desc: Output compare 5 clear enable
        reserved1: u2 = 0,
        OC6FE: bool, // bit offset: 10 desc: Output compare 6 fast enable
        OC6PE: bool, // bit offset: 11 desc: Output compare 6 preload enable
        OC6M: u3, // bit offset: 12 desc: Output compare 6 mode
        OC6CE: bool, // bit offset: 15 desc: Output compare 6 clear enable
        OC5M_3: bool, // bit offset: 16 desc: Outout Compare 5 mode bit 3
        reserved2: u7 = 0,
        OC6M_3: bool, // bit offset: 24 desc: Outout Compare 6 mode bit 3
        padding: u7 = 0,
    });
    // byte offset: 88 capture/compare register 5
    pub const CCR5 = MMIO(Address + 0x00000058, u32, packed struct {
        CCR5: u16, // bit offset: 0 desc: Capture/Compare 5 value
        reserved0: u13 = 0,
        GC5C1: bool, // bit offset: 29 desc: Group Channel 5 and Channel 1
        GC5C2: bool, // bit offset: 30 desc: Group Channel 5 and Channel 2
        GC5C3: bool, // bit offset: 31 desc: Group Channel 5 and Channel 3
    });
    // byte offset: 92 capture/compare register 6
    pub const CCR6 = MMIO(Address + 0x0000005c, u32, packed struct {
        CCR6: u16, // bit offset: 0 desc: Capture/Compare 6 value
        padding: u16 = 0,
    });
    // byte offset: 96 option registers
    pub const OR = MMIO(Address + 0x00000060, u32, packed struct {
        TIM1_ETR_ADC1_RMP: u2, // bit offset: 0 desc: TIM1_ETR_ADC1 remapping capability
        TIM1_ETR_ADC4_RMP: u2, // bit offset: 2 desc: TIM1_ETR_ADC4 remapping capability
        padding: u28 = 0,
    });
};
pub const TIM8 = extern struct {
    pub const Address: u32 = 0x40013400;
    // byte offset: 0 control register 1
    pub const CR1 = MMIO(Address + 0x00000000, u32, packed struct {
        CEN: bool, // bit offset: 0 desc: Counter enable
        UDIS: bool, // bit offset: 1 desc: Update disable
        URS: bool, // bit offset: 2 desc: Update request source
        OPM: bool, // bit offset: 3 desc: One-pulse mode
        DIR: bool, // bit offset: 4 desc: Direction
        CMS: u2, // bit offset: 5 desc: Center-aligned mode selection
        ARPE: bool, // bit offset: 7 desc: Auto-reload preload enable
        CKD: u2, // bit offset: 8 desc: Clock division
        reserved0: u1 = 0,
        UIFREMAP: bool, // bit offset: 11 desc: UIF status bit remapping
        padding: u20 = 0,
    });
    // byte offset: 4 control register 2
    pub const CR2 = MMIO(Address + 0x00000004, u32, packed struct {
        CCPC: bool, // bit offset: 0 desc: Capture/compare preloaded control
        reserved0: u1 = 0,
        CCUS: bool, // bit offset: 2 desc: Capture/compare control update selection
        CCDS: bool, // bit offset: 3 desc: Capture/compare DMA selection
        MMS: u3, // bit offset: 4 desc: Master mode selection
        TI1S: bool, // bit offset: 7 desc: TI1 selection
        OIS1: bool, // bit offset: 8 desc: Output Idle state 1
        OIS1N: bool, // bit offset: 9 desc: Output Idle state 1
        OIS2: bool, // bit offset: 10 desc: Output Idle state 2
        OIS2N: bool, // bit offset: 11 desc: Output Idle state 2
        OIS3: bool, // bit offset: 12 desc: Output Idle state 3
        OIS3N: bool, // bit offset: 13 desc: Output Idle state 3
        OIS4: bool, // bit offset: 14 desc: Output Idle state 4
        reserved1: u1 = 0,
        OIS5: bool, // bit offset: 16 desc: Output Idle state 5
        reserved2: u1 = 0,
        OIS6: bool, // bit offset: 18 desc: Output Idle state 6
        reserved3: u1 = 0,
        MMS2: u4, // bit offset: 20 desc: Master mode selection 2
        padding: u8 = 0,
    });
    // byte offset: 8 slave mode control register
    pub const SMCR = MMIO(Address + 0x00000008, u32, packed struct {
        SMS: u3, // bit offset: 0 desc: Slave mode selection
        OCCS: bool, // bit offset: 3 desc: OCREF clear selection
        TS: u3, // bit offset: 4 desc: Trigger selection
        MSM: bool, // bit offset: 7 desc: Master/Slave mode
        ETF: u4, // bit offset: 8 desc: External trigger filter
        ETPS: u2, // bit offset: 12 desc: External trigger prescaler
        ECE: bool, // bit offset: 14 desc: External clock enable
        ETP: bool, // bit offset: 15 desc: External trigger polarity
        SMS3: bool, // bit offset: 16 desc: Slave mode selection bit 3
        padding: u15 = 0,
    });
    // byte offset: 12 DMA/Interrupt enable register
    pub const DIER = MMIO(Address + 0x0000000c, u32, packed struct {
        UIE: bool, // bit offset: 0 desc: Update interrupt enable
        CC1IE: bool, // bit offset: 1 desc: Capture/Compare 1 interrupt enable
        CC2IE: bool, // bit offset: 2 desc: Capture/Compare 2 interrupt enable
        CC3IE: bool, // bit offset: 3 desc: Capture/Compare 3 interrupt enable
        CC4IE: bool, // bit offset: 4 desc: Capture/Compare 4 interrupt enable
        COMIE: bool, // bit offset: 5 desc: COM interrupt enable
        TIE: bool, // bit offset: 6 desc: Trigger interrupt enable
        BIE: bool, // bit offset: 7 desc: Break interrupt enable
        UDE: bool, // bit offset: 8 desc: Update DMA request enable
        CC1DE: bool, // bit offset: 9 desc: Capture/Compare 1 DMA request enable
        CC2DE: bool, // bit offset: 10 desc: Capture/Compare 2 DMA request enable
        CC3DE: bool, // bit offset: 11 desc: Capture/Compare 3 DMA request enable
        CC4DE: bool, // bit offset: 12 desc: Capture/Compare 4 DMA request enable
        COMDE: bool, // bit offset: 13 desc: COM DMA request enable
        TDE: bool, // bit offset: 14 desc: Trigger DMA request enable
        padding: u17 = 0,
    });
    // byte offset: 16 status register
    pub const SR = MMIO(Address + 0x00000010, u32, packed struct {
        UIF: bool, // bit offset: 0 desc: Update interrupt flag
        CC1IF: bool, // bit offset: 1 desc: Capture/compare 1 interrupt flag
        CC2IF: bool, // bit offset: 2 desc: Capture/Compare 2 interrupt flag
        CC3IF: bool, // bit offset: 3 desc: Capture/Compare 3 interrupt flag
        CC4IF: bool, // bit offset: 4 desc: Capture/Compare 4 interrupt flag
        COMIF: bool, // bit offset: 5 desc: COM interrupt flag
        TIF: bool, // bit offset: 6 desc: Trigger interrupt flag
        BIF: bool, // bit offset: 7 desc: Break interrupt flag
        B2IF: bool, // bit offset: 8 desc: Break 2 interrupt flag
        CC1OF: bool, // bit offset: 9 desc: Capture/Compare 1 overcapture flag
        CC2OF: bool, // bit offset: 10 desc: Capture/compare 2 overcapture flag
        CC3OF: bool, // bit offset: 11 desc: Capture/Compare 3 overcapture flag
        CC4OF: bool, // bit offset: 12 desc: Capture/Compare 4 overcapture flag
        reserved0: u3 = 0,
        C5IF: bool, // bit offset: 16 desc: Capture/Compare 5 interrupt flag
        C6IF: bool, // bit offset: 17 desc: Capture/Compare 6 interrupt flag
        padding: u14 = 0,
    });
    // byte offset: 20 event generation register
    pub const EGR = MMIO(Address + 0x00000014, u32, packed struct {
        UG: bool, // bit offset: 0 desc: Update generation
        CC1G: bool, // bit offset: 1 desc: Capture/compare 1 generation
        CC2G: bool, // bit offset: 2 desc: Capture/compare 2 generation
        CC3G: bool, // bit offset: 3 desc: Capture/compare 3 generation
        CC4G: bool, // bit offset: 4 desc: Capture/compare 4 generation
        COMG: bool, // bit offset: 5 desc: Capture/Compare control update generation
        TG: bool, // bit offset: 6 desc: Trigger generation
        BG: bool, // bit offset: 7 desc: Break generation
        B2G: bool, // bit offset: 8 desc: Break 2 generation
        padding: u23 = 0,
    });
    // byte offset: 24 capture/compare mode register (output mode)
    pub const CCMR1_Output = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        OC1FE: bool, // bit offset: 2 desc: Output Compare 1 fast enable
        OC1PE: bool, // bit offset: 3 desc: Output Compare 1 preload enable
        OC1M: u3, // bit offset: 4 desc: Output Compare 1 mode
        OC1CE: bool, // bit offset: 7 desc: Output Compare 1 clear enable
        CC2S: u2, // bit offset: 8 desc: Capture/Compare 2 selection
        OC2FE: bool, // bit offset: 10 desc: Output Compare 2 fast enable
        OC2PE: bool, // bit offset: 11 desc: Output Compare 2 preload enable
        OC2M: u3, // bit offset: 12 desc: Output Compare 2 mode
        OC2CE: bool, // bit offset: 15 desc: Output Compare 2 clear enable
        OC1M_3: bool, // bit offset: 16 desc: Output Compare 1 mode bit 3
        reserved0: u7 = 0,
        OC2M_3: bool, // bit offset: 24 desc: Output Compare 2 mode bit 3
        padding: u7 = 0,
    });
    // byte offset: 24 capture/compare mode register 1 (input mode)
    pub const CCMR1_Input = MMIO(Address + 0x00000018, u32, packed struct {
        CC1S: u2, // bit offset: 0 desc: Capture/Compare 1 selection
        IC1PCS: u2, // bit offset: 2 desc: Input capture 1 prescaler
        IC1F: u4, // bit offset: 4 desc: Input capture 1 filter
        CC2S: u2, // bit offset: 8 desc: Capture/Compare 2 selection
        IC2PCS: u2, // bit offset: 10 desc: Input capture 2 prescaler
        IC2F: u4, // bit offset: 12 desc: Input capture 2 filter
        padding: u16 = 0,
    });
    // byte offset: 28 capture/compare mode register (output mode)
    pub const CCMR2_Output = MMIO(Address + 0x0000001c, u32, packed struct {
        CC3S: u2, // bit offset: 0 desc: Capture/Compare 3 selection
        OC3FE: bool, // bit offset: 2 desc: Output compare 3 fast enable
        OC3PE: bool, // bit offset: 3 desc: Output compare 3 preload enable
        OC3M: u3, // bit offset: 4 desc: Output compare 3 mode
        OC3CE: bool, // bit offset: 7 desc: Output compare 3 clear enable
        CC4S: u2, // bit offset: 8 desc: Capture/Compare 4 selection
        OC4FE: bool, // bit offset: 10 desc: Output compare 4 fast enable
        OC4PE: bool, // bit offset: 11 desc: Output compare 4 preload enable
        OC4M: u3, // bit offset: 12 desc: Output compare 4 mode
        OC4CE: bool, // bit offset: 15 desc: Output compare 4 clear enable
        OC3M_3: bool, // bit offset: 16 desc: Output Compare 3 mode bit 3
        reserved0: u7 = 0,
        OC4M_3: bool, // bit offset: 24 desc: Output Compare 4 mode bit 3
        padding: u7 = 0,
    });
    // byte offset: 28 capture/compare mode register 2 (input mode)
    pub const CCMR2_Input = MMIO(Address + 0x0000001c, u32, packed struct {
        CC3S: u2, // bit offset: 0 desc: Capture/compare 3 selection
        IC3PSC: u2, // bit offset: 2 desc: Input capture 3 prescaler
        IC3F: u4, // bit offset: 4 desc: Input capture 3 filter
        CC4S: u2, // bit offset: 8 desc: Capture/Compare 4 selection
        IC4PSC: u2, // bit offset: 10 desc: Input capture 4 prescaler
        IC4F: u4, // bit offset: 12 desc: Input capture 4 filter
        padding: u16 = 0,
    });
    // byte offset: 32 capture/compare enable register
    pub const CCER = MMIO(Address + 0x00000020, u32, packed struct {
        CC1E: bool, // bit offset: 0 desc: Capture/Compare 1 output enable
        CC1P: bool, // bit offset: 1 desc: Capture/Compare 1 output Polarity
        CC1NE: bool, // bit offset: 2 desc: Capture/Compare 1 complementary output enable
        CC1NP: bool, // bit offset: 3 desc: Capture/Compare 1 output Polarity
        CC2E: bool, // bit offset: 4 desc: Capture/Compare 2 output enable
        CC2P: bool, // bit offset: 5 desc: Capture/Compare 2 output Polarity
        CC2NE: bool, // bit offset: 6 desc: Capture/Compare 2 complementary output enable
        CC2NP: bool, // bit offset: 7 desc: Capture/Compare 2 output Polarity
        CC3E: bool, // bit offset: 8 desc: Capture/Compare 3 output enable
        CC3P: bool, // bit offset: 9 desc: Capture/Compare 3 output Polarity
        CC3NE: bool, // bit offset: 10 desc: Capture/Compare 3 complementary output enable
        CC3NP: bool, // bit offset: 11 desc: Capture/Compare 3 output Polarity
        CC4E: bool, // bit offset: 12 desc: Capture/Compare 4 output enable
        CC4P: bool, // bit offset: 13 desc: Capture/Compare 3 output Polarity
        reserved0: u1 = 0,
        CC4NP: bool, // bit offset: 15 desc: Capture/Compare 4 output Polarity
        CC5E: bool, // bit offset: 16 desc: Capture/Compare 5 output enable
        CC5P: bool, // bit offset: 17 desc: Capture/Compare 5 output Polarity
        reserved1: u2 = 0,
        CC6E: bool, // bit offset: 20 desc: Capture/Compare 6 output enable
        CC6P: bool, // bit offset: 21 desc: Capture/Compare 6 output Polarity
        padding: u10 = 0,
    });
    // byte offset: 36 counter
    pub const CNT = MMIO(Address + 0x00000024, u32, packed struct {
        CNT: u16, // bit offset: 0 desc: counter value
        reserved0: u15 = 0,
        UIFCPY: bool, // bit offset: 31 desc: UIF copy
    });
    // byte offset: 40 prescaler
    pub const PSC = MMIO(Address + 0x00000028, u32, packed struct {
        PSC: u16, // bit offset: 0 desc: Prescaler value
        padding: u16 = 0,
    });
    // byte offset: 44 auto-reload register
    pub const ARR = MMIO(Address + 0x0000002c, u32, packed struct {
        ARR: u16, // bit offset: 0 desc: Auto-reload value
        padding: u16 = 0,
    });
    // byte offset: 48 repetition counter register
    pub const RCR = MMIO(Address + 0x00000030, u32, packed struct {
        REP: u16, // bit offset: 0 desc: Repetition counter value
        padding: u16 = 0,
    });
    // byte offset: 52 capture/compare register 1
    pub const CCR1 = MMIO(Address + 0x00000034, u32, packed struct {
        CCR1: u16, // bit offset: 0 desc: Capture/Compare 1 value
        padding: u16 = 0,
    });
    // byte offset: 56 capture/compare register 2
    pub const CCR2 = MMIO(Address + 0x00000038, u32, packed struct {
        CCR2: u16, // bit offset: 0 desc: Capture/Compare 2 value
        padding: u16 = 0,
    });
    // byte offset: 60 capture/compare register 3
    pub const CCR3 = MMIO(Address + 0x0000003c, u32, packed struct {
        CCR3: u16, // bit offset: 0 desc: Capture/Compare 3 value
        padding: u16 = 0,
    });
    // byte offset: 64 capture/compare register 4
    pub const CCR4 = MMIO(Address + 0x00000040, u32, packed struct {
        CCR4: u16, // bit offset: 0 desc: Capture/Compare 3 value
        padding: u16 = 0,
    });
    // byte offset: 68 break and dead-time register
    pub const BDTR = MMIO(Address + 0x00000044, u32, packed struct {
        DTG: u8, // bit offset: 0 desc: Dead-time generator setup
        LOCK: u2, // bit offset: 8 desc: Lock configuration
        OSSI: bool, // bit offset: 10 desc: Off-state selection for Idle mode
        OSSR: bool, // bit offset: 11 desc: Off-state selection for Run mode
        BKE: bool, // bit offset: 12 desc: Break enable
        BKP: bool, // bit offset: 13 desc: Break polarity
        AOE: bool, // bit offset: 14 desc: Automatic output enable
        MOE: bool, // bit offset: 15 desc: Main output enable
        BKF: u4, // bit offset: 16 desc: Break filter
        BK2F: u4, // bit offset: 20 desc: Break 2 filter
        BK2E: bool, // bit offset: 24 desc: Break 2 enable
        BK2P: bool, // bit offset: 25 desc: Break 2 polarity
        padding: u6 = 0,
    });
    // byte offset: 72 DMA control register
    pub const DCR = MMIO(Address + 0x00000048, u32, packed struct {
        DBA: u5, // bit offset: 0 desc: DMA base address
        reserved0: u3 = 0,
        DBL: u5, // bit offset: 8 desc: DMA burst length
        padding: u19 = 0,
    });
    // byte offset: 76 DMA address for full transfer
    pub const DMAR = MMIO(Address + 0x0000004c, u32, packed struct {
        DMAB: u16, // bit offset: 0 desc: DMA register for burst accesses
        padding: u16 = 0,
    });
    // byte offset: 84 capture/compare mode register 3 (output mode)
    pub const CCMR3_Output = MMIO(Address + 0x00000054, u32, packed struct {
        reserved0: u2 = 0,
        OC5FE: bool, // bit offset: 2 desc: Output compare 5 fast enable
        OC5PE: bool, // bit offset: 3 desc: Output compare 5 preload enable
        OC5M: u3, // bit offset: 4 desc: Output compare 5 mode
        OC5CE: bool, // bit offset: 7 desc: Output compare 5 clear enable
        reserved1: u2 = 0,
        OC6FE: bool, // bit offset: 10 desc: Output compare 6 fast enable
        OC6PE: bool, // bit offset: 11 desc: Output compare 6 preload enable
        OC6M: u3, // bit offset: 12 desc: Output compare 6 mode
        OC6CE: bool, // bit offset: 15 desc: Output compare 6 clear enable
        OC5M_3: bool, // bit offset: 16 desc: Outout Compare 5 mode bit 3
        reserved2: u7 = 0,
        OC6M_3: bool, // bit offset: 24 desc: Outout Compare 6 mode bit 3
        padding: u7 = 0,
    });
    // byte offset: 88 capture/compare register 5
    pub const CCR5 = MMIO(Address + 0x00000058, u32, packed struct {
        CCR5: u16, // bit offset: 0 desc: Capture/Compare 5 value
        reserved0: u13 = 0,
        GC5C1: bool, // bit offset: 29 desc: Group Channel 5 and Channel 1
        GC5C2: bool, // bit offset: 30 desc: Group Channel 5 and Channel 2
        GC5C3: bool, // bit offset: 31 desc: Group Channel 5 and Channel 3
    });
    // byte offset: 92 capture/compare register 6
    pub const CCR6 = MMIO(Address + 0x0000005c, u32, packed struct {
        CCR6: u16, // bit offset: 0 desc: Capture/Compare 6 value
        padding: u16 = 0,
    });
    // byte offset: 96 option registers
    pub const OR = MMIO(Address + 0x00000060, u32, packed struct {
        TIM8_ETR_ADC2_RMP: u2, // bit offset: 0 desc: TIM8_ETR_ADC2 remapping capability
        TIM8_ETR_ADC3_RMP: u2, // bit offset: 2 desc: TIM8_ETR_ADC3 remapping capability
        padding: u28 = 0,
    });
};
pub const ADC1 = extern struct {
    pub const Address: u32 = 0x50000000;
    // byte offset: 0 interrupt and status register
    pub const ISR = MMIO(Address + 0x00000000, u32, packed struct {
        ADRDY: bool, // bit offset: 0 desc: ADRDY
        EOSMP: bool, // bit offset: 1 desc: EOSMP
        EOC: bool, // bit offset: 2 desc: EOC
        EOS: bool, // bit offset: 3 desc: EOS
        OVR: bool, // bit offset: 4 desc: OVR
        JEOC: bool, // bit offset: 5 desc: JEOC
        JEOS: bool, // bit offset: 6 desc: JEOS
        AWD1: bool, // bit offset: 7 desc: AWD1
        AWD2: bool, // bit offset: 8 desc: AWD2
        AWD3: bool, // bit offset: 9 desc: AWD3
        JQOVF: bool, // bit offset: 10 desc: JQOVF
        padding: u21 = 0,
    });
    // byte offset: 4 interrupt enable register
    pub const IER = MMIO(Address + 0x00000004, u32, packed struct {
        ADRDYIE: bool, // bit offset: 0 desc: ADRDYIE
        EOSMPIE: bool, // bit offset: 1 desc: EOSMPIE
        EOCIE: bool, // bit offset: 2 desc: EOCIE
        EOSIE: bool, // bit offset: 3 desc: EOSIE
        OVRIE: bool, // bit offset: 4 desc: OVRIE
        JEOCIE: bool, // bit offset: 5 desc: JEOCIE
        JEOSIE: bool, // bit offset: 6 desc: JEOSIE
        AWD1IE: bool, // bit offset: 7 desc: AWD1IE
        AWD2IE: bool, // bit offset: 8 desc: AWD2IE
        AWD3IE: bool, // bit offset: 9 desc: AWD3IE
        JQOVFIE: bool, // bit offset: 10 desc: JQOVFIE
        padding: u21 = 0,
    });
    // byte offset: 8 control register
    pub const CR = MMIO(Address + 0x00000008, u32, packed struct {
        ADEN: bool, // bit offset: 0 desc: ADEN
        ADDIS: bool, // bit offset: 1 desc: ADDIS
        ADSTART: bool, // bit offset: 2 desc: ADSTART
        JADSTART: bool, // bit offset: 3 desc: JADSTART
        ADSTP: bool, // bit offset: 4 desc: ADSTP
        JADSTP: bool, // bit offset: 5 desc: JADSTP
        reserved0: u22 = 0,
        ADVREGEN: bool, // bit offset: 28 desc: ADVREGEN
        DEEPPWD: bool, // bit offset: 29 desc: DEEPPWD
        ADCALDIF: bool, // bit offset: 30 desc: ADCALDIF
        ADCAL: bool, // bit offset: 31 desc: ADCAL
    });
    // byte offset: 12 configuration register
    pub const CFGR = MMIO(Address + 0x0000000c, u32, packed struct {
        DMAEN: bool, // bit offset: 0 desc: DMAEN
        DMACFG: bool, // bit offset: 1 desc: DMACFG
        reserved0: u1 = 0,
        RES: u2, // bit offset: 3 desc: RES
        ALIGN: bool, // bit offset: 5 desc: ALIGN
        EXTSEL: u4, // bit offset: 6 desc: EXTSEL
        EXTEN: u2, // bit offset: 10 desc: EXTEN
        OVRMOD: bool, // bit offset: 12 desc: OVRMOD
        CONT: bool, // bit offset: 13 desc: CONT
        AUTDLY: bool, // bit offset: 14 desc: AUTDLY
        AUTOFF: bool, // bit offset: 15 desc: AUTOFF
        DISCEN: bool, // bit offset: 16 desc: DISCEN
        DISCNUM: u3, // bit offset: 17 desc: DISCNUM
        JDISCEN: bool, // bit offset: 20 desc: JDISCEN
        JQM: bool, // bit offset: 21 desc: JQM
        AWD1SGL: bool, // bit offset: 22 desc: AWD1SGL
        AWD1EN: bool, // bit offset: 23 desc: AWD1EN
        JAWD1EN: bool, // bit offset: 24 desc: JAWD1EN
        JAUTO: bool, // bit offset: 25 desc: JAUTO
        AWDCH1CH: u5, // bit offset: 26 desc: AWDCH1CH
        padding: u1 = 0,
    });
    // byte offset: 20 sample time register 1
    pub const SMPR1 = MMIO(Address + 0x00000014, u32, packed struct {
        reserved0: u3 = 0,
        SMP1: u3, // bit offset: 3 desc: SMP1
        SMP2: u3, // bit offset: 6 desc: SMP2
        SMP3: u3, // bit offset: 9 desc: SMP3
        SMP4: u3, // bit offset: 12 desc: SMP4
        SMP5: u3, // bit offset: 15 desc: SMP5
        SMP6: u3, // bit offset: 18 desc: SMP6
        SMP7: u3, // bit offset: 21 desc: SMP7
        SMP8: u3, // bit offset: 24 desc: SMP8
        SMP9: u3, // bit offset: 27 desc: SMP9
        padding: u2 = 0,
    });
    // byte offset: 24 sample time register 2
    pub const SMPR2 = MMIO(Address + 0x00000018, u32, packed struct {
        SMP10: u3, // bit offset: 0 desc: SMP10
        SMP11: u3, // bit offset: 3 desc: SMP11
        SMP12: u3, // bit offset: 6 desc: SMP12
        SMP13: u3, // bit offset: 9 desc: SMP13
        SMP14: u3, // bit offset: 12 desc: SMP14
        SMP15: u3, // bit offset: 15 desc: SMP15
        SMP16: u3, // bit offset: 18 desc: SMP16
        SMP17: u3, // bit offset: 21 desc: SMP17
        SMP18: u3, // bit offset: 24 desc: SMP18
        padding: u5 = 0,
    });
    // byte offset: 32 watchdog threshold register 1
    pub const TR1 = MMIO(Address + 0x00000020, u32, packed struct {
        LT1: u12, // bit offset: 0 desc: LT1
        reserved0: u4 = 0,
        HT1: u12, // bit offset: 16 desc: HT1
        padding: u4 = 0,
    });
    // byte offset: 36 watchdog threshold register
    pub const TR2 = MMIO(Address + 0x00000024, u32, packed struct {
        LT2: u8, // bit offset: 0 desc: LT2
        reserved0: u8 = 0,
        HT2: u8, // bit offset: 16 desc: HT2
        padding: u8 = 0,
    });
    // byte offset: 40 watchdog threshold register 3
    pub const TR3 = MMIO(Address + 0x00000028, u32, packed struct {
        LT3: u8, // bit offset: 0 desc: LT3
        reserved0: u8 = 0,
        HT3: u8, // bit offset: 16 desc: HT3
        padding: u8 = 0,
    });
    // byte offset: 48 regular sequence register 1
    pub const SQR1 = MMIO(Address + 0x00000030, u32, packed struct {
        L3: u4, // bit offset: 0 desc: L3
        reserved0: u2 = 0,
        SQ1: u5, // bit offset: 6 desc: SQ1
        reserved1: u1 = 0,
        SQ2: u5, // bit offset: 12 desc: SQ2
        reserved2: u1 = 0,
        SQ3: u5, // bit offset: 18 desc: SQ3
        reserved3: u1 = 0,
        SQ4: u5, // bit offset: 24 desc: SQ4
        padding: u3 = 0,
    });
    // byte offset: 52 regular sequence register 2
    pub const SQR2 = MMIO(Address + 0x00000034, u32, packed struct {
        SQ5: u5, // bit offset: 0 desc: SQ5
        reserved0: u1 = 0,
        SQ6: u5, // bit offset: 6 desc: SQ6
        reserved1: u1 = 0,
        SQ7: u5, // bit offset: 12 desc: SQ7
        reserved2: u1 = 0,
        SQ8: u5, // bit offset: 18 desc: SQ8
        reserved3: u1 = 0,
        SQ9: u5, // bit offset: 24 desc: SQ9
        padding: u3 = 0,
    });
    // byte offset: 56 regular sequence register 3
    pub const SQR3 = MMIO(Address + 0x00000038, u32, packed struct {
        SQ10: u5, // bit offset: 0 desc: SQ10
        reserved0: u1 = 0,
        SQ11: u5, // bit offset: 6 desc: SQ11
        reserved1: u1 = 0,
        SQ12: u5, // bit offset: 12 desc: SQ12
        reserved2: u1 = 0,
        SQ13: u5, // bit offset: 18 desc: SQ13
        reserved3: u1 = 0,
        SQ14: u5, // bit offset: 24 desc: SQ14
        padding: u3 = 0,
    });
    // byte offset: 60 regular sequence register 4
    pub const SQR4 = MMIO(Address + 0x0000003c, u32, packed struct {
        SQ15: u5, // bit offset: 0 desc: SQ15
        reserved0: u1 = 0,
        SQ16: u5, // bit offset: 6 desc: SQ16
        padding: u21 = 0,
    });
    // byte offset: 64 regular Data Register
    pub const DR = MMIO(Address + 0x00000040, u32, packed struct {
        regularDATA: u16, // bit offset: 0 desc: regularDATA
        padding: u16 = 0,
    });
    // byte offset: 76 injected sequence register
    pub const JSQR = MMIO(Address + 0x0000004c, u32, packed struct {
        JL: u2, // bit offset: 0 desc: JL
        JEXTSEL: u4, // bit offset: 2 desc: JEXTSEL
        JEXTEN: u2, // bit offset: 6 desc: JEXTEN
        JSQ1: u5, // bit offset: 8 desc: JSQ1
        reserved0: u1 = 0,
        JSQ2: u5, // bit offset: 14 desc: JSQ2
        reserved1: u1 = 0,
        JSQ3: u5, // bit offset: 20 desc: JSQ3
        reserved2: u1 = 0,
        JSQ4: u5, // bit offset: 26 desc: JSQ4
        padding: u1 = 0,
    });
    // byte offset: 96 offset register 1
    pub const OFR1 = MMIO(Address + 0x00000060, u32, packed struct {
        OFFSET1: u12, // bit offset: 0 desc: OFFSET1
        reserved0: u14 = 0,
        OFFSET1_CH: u5, // bit offset: 26 desc: OFFSET1_CH
        OFFSET1_EN: bool, // bit offset: 31 desc: OFFSET1_EN
    });
    // byte offset: 100 offset register 2
    pub const OFR2 = MMIO(Address + 0x00000064, u32, packed struct {
        OFFSET2: u12, // bit offset: 0 desc: OFFSET2
        reserved0: u14 = 0,
        OFFSET2_CH: u5, // bit offset: 26 desc: OFFSET2_CH
        OFFSET2_EN: bool, // bit offset: 31 desc: OFFSET2_EN
    });
    // byte offset: 104 offset register 3
    pub const OFR3 = MMIO(Address + 0x00000068, u32, packed struct {
        OFFSET3: u12, // bit offset: 0 desc: OFFSET3
        reserved0: u14 = 0,
        OFFSET3_CH: u5, // bit offset: 26 desc: OFFSET3_CH
        OFFSET3_EN: bool, // bit offset: 31 desc: OFFSET3_EN
    });
    // byte offset: 108 offset register 4
    pub const OFR4 = MMIO(Address + 0x0000006c, u32, packed struct {
        OFFSET4: u12, // bit offset: 0 desc: OFFSET4
        reserved0: u14 = 0,
        OFFSET4_CH: u5, // bit offset: 26 desc: OFFSET4_CH
        OFFSET4_EN: bool, // bit offset: 31 desc: OFFSET4_EN
    });
    // byte offset: 128 injected data register 1
    pub const JDR1 = MMIO(Address + 0x00000080, u32, packed struct {
        JDATA1: u16, // bit offset: 0 desc: JDATA1
        padding: u16 = 0,
    });
    // byte offset: 132 injected data register 2
    pub const JDR2 = MMIO(Address + 0x00000084, u32, packed struct {
        JDATA2: u16, // bit offset: 0 desc: JDATA2
        padding: u16 = 0,
    });
    // byte offset: 136 injected data register 3
    pub const JDR3 = MMIO(Address + 0x00000088, u32, packed struct {
        JDATA3: u16, // bit offset: 0 desc: JDATA3
        padding: u16 = 0,
    });
    // byte offset: 140 injected data register 4
    pub const JDR4 = MMIO(Address + 0x0000008c, u32, packed struct {
        JDATA4: u16, // bit offset: 0 desc: JDATA4
        padding: u16 = 0,
    });
    // byte offset: 160 Analog Watchdog 2 Configuration Register
    pub const AWD2CR = MMIO(Address + 0x000000a0, u32, packed struct {
        reserved0: u1 = 0,
        AWD2CH: u18, // bit offset: 1 desc: AWD2CH
        padding: u13 = 0,
    });
    // byte offset: 164 Analog Watchdog 3 Configuration Register
    pub const AWD3CR = MMIO(Address + 0x000000a4, u32, packed struct {
        reserved0: u1 = 0,
        AWD3CH: u18, // bit offset: 1 desc: AWD3CH
        padding: u13 = 0,
    });
    // byte offset: 176 Differential Mode Selection Register 2
    pub const DIFSEL = MMIO(Address + 0x000000b0, u32, packed struct {
        reserved0: u1 = 0,
        DIFSEL_1_15: u15, // bit offset: 1 desc: Differential mode for channels 15 to 1
        DIFSEL_16_18: u3, // bit offset: 16 desc: Differential mode for channels 18 to 16
        padding: u13 = 0,
    });
    // byte offset: 180 Calibration Factors
    pub const CALFACT = MMIO(Address + 0x000000b4, u32, packed struct {
        CALFACT_S: u7, // bit offset: 0 desc: CALFACT_S
        reserved0: u9 = 0,
        CALFACT_D: u7, // bit offset: 16 desc: CALFACT_D
        padding: u9 = 0,
    });
};
pub const ADC2 = extern struct {
    pub const Address: u32 = 0x50000100;
    // byte offset: 0 interrupt and status register
    pub const ISR = MMIO(Address + 0x00000000, u32, packed struct {
        ADRDY: bool, // bit offset: 0 desc: ADRDY
        EOSMP: bool, // bit offset: 1 desc: EOSMP
        EOC: bool, // bit offset: 2 desc: EOC
        EOS: bool, // bit offset: 3 desc: EOS
        OVR: bool, // bit offset: 4 desc: OVR
        JEOC: bool, // bit offset: 5 desc: JEOC
        JEOS: bool, // bit offset: 6 desc: JEOS
        AWD1: bool, // bit offset: 7 desc: AWD1
        AWD2: bool, // bit offset: 8 desc: AWD2
        AWD3: bool, // bit offset: 9 desc: AWD3
        JQOVF: bool, // bit offset: 10 desc: JQOVF
        padding: u21 = 0,
    });
    // byte offset: 4 interrupt enable register
    pub const IER = MMIO(Address + 0x00000004, u32, packed struct {
        ADRDYIE: bool, // bit offset: 0 desc: ADRDYIE
        EOSMPIE: bool, // bit offset: 1 desc: EOSMPIE
        EOCIE: bool, // bit offset: 2 desc: EOCIE
        EOSIE: bool, // bit offset: 3 desc: EOSIE
        OVRIE: bool, // bit offset: 4 desc: OVRIE
        JEOCIE: bool, // bit offset: 5 desc: JEOCIE
        JEOSIE: bool, // bit offset: 6 desc: JEOSIE
        AWD1IE: bool, // bit offset: 7 desc: AWD1IE
        AWD2IE: bool, // bit offset: 8 desc: AWD2IE
        AWD3IE: bool, // bit offset: 9 desc: AWD3IE
        JQOVFIE: bool, // bit offset: 10 desc: JQOVFIE
        padding: u21 = 0,
    });
    // byte offset: 8 control register
    pub const CR = MMIO(Address + 0x00000008, u32, packed struct {
        ADEN: bool, // bit offset: 0 desc: ADEN
        ADDIS: bool, // bit offset: 1 desc: ADDIS
        ADSTART: bool, // bit offset: 2 desc: ADSTART
        JADSTART: bool, // bit offset: 3 desc: JADSTART
        ADSTP: bool, // bit offset: 4 desc: ADSTP
        JADSTP: bool, // bit offset: 5 desc: JADSTP
        reserved0: u22 = 0,
        ADVREGEN: bool, // bit offset: 28 desc: ADVREGEN
        DEEPPWD: bool, // bit offset: 29 desc: DEEPPWD
        ADCALDIF: bool, // bit offset: 30 desc: ADCALDIF
        ADCAL: bool, // bit offset: 31 desc: ADCAL
    });
    // byte offset: 12 configuration register
    pub const CFGR = MMIO(Address + 0x0000000c, u32, packed struct {
        DMAEN: bool, // bit offset: 0 desc: DMAEN
        DMACFG: bool, // bit offset: 1 desc: DMACFG
        reserved0: u1 = 0,
        RES: u2, // bit offset: 3 desc: RES
        ALIGN: bool, // bit offset: 5 desc: ALIGN
        EXTSEL: u4, // bit offset: 6 desc: EXTSEL
        EXTEN: u2, // bit offset: 10 desc: EXTEN
        OVRMOD: bool, // bit offset: 12 desc: OVRMOD
        CONT: bool, // bit offset: 13 desc: CONT
        AUTDLY: bool, // bit offset: 14 desc: AUTDLY
        AUTOFF: bool, // bit offset: 15 desc: AUTOFF
        DISCEN: bool, // bit offset: 16 desc: DISCEN
        DISCNUM: u3, // bit offset: 17 desc: DISCNUM
        JDISCEN: bool, // bit offset: 20 desc: JDISCEN
        JQM: bool, // bit offset: 21 desc: JQM
        AWD1SGL: bool, // bit offset: 22 desc: AWD1SGL
        AWD1EN: bool, // bit offset: 23 desc: AWD1EN
        JAWD1EN: bool, // bit offset: 24 desc: JAWD1EN
        JAUTO: bool, // bit offset: 25 desc: JAUTO
        AWDCH1CH: u5, // bit offset: 26 desc: AWDCH1CH
        padding: u1 = 0,
    });
    // byte offset: 20 sample time register 1
    pub const SMPR1 = MMIO(Address + 0x00000014, u32, packed struct {
        reserved0: u3 = 0,
        SMP1: u3, // bit offset: 3 desc: SMP1
        SMP2: u3, // bit offset: 6 desc: SMP2
        SMP3: u3, // bit offset: 9 desc: SMP3
        SMP4: u3, // bit offset: 12 desc: SMP4
        SMP5: u3, // bit offset: 15 desc: SMP5
        SMP6: u3, // bit offset: 18 desc: SMP6
        SMP7: u3, // bit offset: 21 desc: SMP7
        SMP8: u3, // bit offset: 24 desc: SMP8
        SMP9: u3, // bit offset: 27 desc: SMP9
        padding: u2 = 0,
    });
    // byte offset: 24 sample time register 2
    pub const SMPR2 = MMIO(Address + 0x00000018, u32, packed struct {
        SMP10: u3, // bit offset: 0 desc: SMP10
        SMP11: u3, // bit offset: 3 desc: SMP11
        SMP12: u3, // bit offset: 6 desc: SMP12
        SMP13: u3, // bit offset: 9 desc: SMP13
        SMP14: u3, // bit offset: 12 desc: SMP14
        SMP15: u3, // bit offset: 15 desc: SMP15
        SMP16: u3, // bit offset: 18 desc: SMP16
        SMP17: u3, // bit offset: 21 desc: SMP17
        SMP18: u3, // bit offset: 24 desc: SMP18
        padding: u5 = 0,
    });
    // byte offset: 32 watchdog threshold register 1
    pub const TR1 = MMIO(Address + 0x00000020, u32, packed struct {
        LT1: u12, // bit offset: 0 desc: LT1
        reserved0: u4 = 0,
        HT1: u12, // bit offset: 16 desc: HT1
        padding: u4 = 0,
    });
    // byte offset: 36 watchdog threshold register
    pub const TR2 = MMIO(Address + 0x00000024, u32, packed struct {
        LT2: u8, // bit offset: 0 desc: LT2
        reserved0: u8 = 0,
        HT2: u8, // bit offset: 16 desc: HT2
        padding: u8 = 0,
    });
    // byte offset: 40 watchdog threshold register 3
    pub const TR3 = MMIO(Address + 0x00000028, u32, packed struct {
        LT3: u8, // bit offset: 0 desc: LT3
        reserved0: u8 = 0,
        HT3: u8, // bit offset: 16 desc: HT3
        padding: u8 = 0,
    });
    // byte offset: 48 regular sequence register 1
    pub const SQR1 = MMIO(Address + 0x00000030, u32, packed struct {
        L3: u4, // bit offset: 0 desc: L3
        reserved0: u2 = 0,
        SQ1: u5, // bit offset: 6 desc: SQ1
        reserved1: u1 = 0,
        SQ2: u5, // bit offset: 12 desc: SQ2
        reserved2: u1 = 0,
        SQ3: u5, // bit offset: 18 desc: SQ3
        reserved3: u1 = 0,
        SQ4: u5, // bit offset: 24 desc: SQ4
        padding: u3 = 0,
    });
    // byte offset: 52 regular sequence register 2
    pub const SQR2 = MMIO(Address + 0x00000034, u32, packed struct {
        SQ5: u5, // bit offset: 0 desc: SQ5
        reserved0: u1 = 0,
        SQ6: u5, // bit offset: 6 desc: SQ6
        reserved1: u1 = 0,
        SQ7: u5, // bit offset: 12 desc: SQ7
        reserved2: u1 = 0,
        SQ8: u5, // bit offset: 18 desc: SQ8
        reserved3: u1 = 0,
        SQ9: u5, // bit offset: 24 desc: SQ9
        padding: u3 = 0,
    });
    // byte offset: 56 regular sequence register 3
    pub const SQR3 = MMIO(Address + 0x00000038, u32, packed struct {
        SQ10: u5, // bit offset: 0 desc: SQ10
        reserved0: u1 = 0,
        SQ11: u5, // bit offset: 6 desc: SQ11
        reserved1: u1 = 0,
        SQ12: u5, // bit offset: 12 desc: SQ12
        reserved2: u1 = 0,
        SQ13: u5, // bit offset: 18 desc: SQ13
        reserved3: u1 = 0,
        SQ14: u5, // bit offset: 24 desc: SQ14
        padding: u3 = 0,
    });
    // byte offset: 60 regular sequence register 4
    pub const SQR4 = MMIO(Address + 0x0000003c, u32, packed struct {
        SQ15: u5, // bit offset: 0 desc: SQ15
        reserved0: u1 = 0,
        SQ16: u5, // bit offset: 6 desc: SQ16
        padding: u21 = 0,
    });
    // byte offset: 64 regular Data Register
    pub const DR = MMIO(Address + 0x00000040, u32, packed struct {
        regularDATA: u16, // bit offset: 0 desc: regularDATA
        padding: u16 = 0,
    });
    // byte offset: 76 injected sequence register
    pub const JSQR = MMIO(Address + 0x0000004c, u32, packed struct {
        JL: u2, // bit offset: 0 desc: JL
        JEXTSEL: u4, // bit offset: 2 desc: JEXTSEL
        JEXTEN: u2, // bit offset: 6 desc: JEXTEN
        JSQ1: u5, // bit offset: 8 desc: JSQ1
        reserved0: u1 = 0,
        JSQ2: u5, // bit offset: 14 desc: JSQ2
        reserved1: u1 = 0,
        JSQ3: u5, // bit offset: 20 desc: JSQ3
        reserved2: u1 = 0,
        JSQ4: u5, // bit offset: 26 desc: JSQ4
        padding: u1 = 0,
    });
    // byte offset: 96 offset register 1
    pub const OFR1 = MMIO(Address + 0x00000060, u32, packed struct {
        OFFSET1: u12, // bit offset: 0 desc: OFFSET1
        reserved0: u14 = 0,
        OFFSET1_CH: u5, // bit offset: 26 desc: OFFSET1_CH
        OFFSET1_EN: bool, // bit offset: 31 desc: OFFSET1_EN
    });
    // byte offset: 100 offset register 2
    pub const OFR2 = MMIO(Address + 0x00000064, u32, packed struct {
        OFFSET2: u12, // bit offset: 0 desc: OFFSET2
        reserved0: u14 = 0,
        OFFSET2_CH: u5, // bit offset: 26 desc: OFFSET2_CH
        OFFSET2_EN: bool, // bit offset: 31 desc: OFFSET2_EN
    });
    // byte offset: 104 offset register 3
    pub const OFR3 = MMIO(Address + 0x00000068, u32, packed struct {
        OFFSET3: u12, // bit offset: 0 desc: OFFSET3
        reserved0: u14 = 0,
        OFFSET3_CH: u5, // bit offset: 26 desc: OFFSET3_CH
        OFFSET3_EN: bool, // bit offset: 31 desc: OFFSET3_EN
    });
    // byte offset: 108 offset register 4
    pub const OFR4 = MMIO(Address + 0x0000006c, u32, packed struct {
        OFFSET4: u12, // bit offset: 0 desc: OFFSET4
        reserved0: u14 = 0,
        OFFSET4_CH: u5, // bit offset: 26 desc: OFFSET4_CH
        OFFSET4_EN: bool, // bit offset: 31 desc: OFFSET4_EN
    });
    // byte offset: 128 injected data register 1
    pub const JDR1 = MMIO(Address + 0x00000080, u32, packed struct {
        JDATA1: u16, // bit offset: 0 desc: JDATA1
        padding: u16 = 0,
    });
    // byte offset: 132 injected data register 2
    pub const JDR2 = MMIO(Address + 0x00000084, u32, packed struct {
        JDATA2: u16, // bit offset: 0 desc: JDATA2
        padding: u16 = 0,
    });
    // byte offset: 136 injected data register 3
    pub const JDR3 = MMIO(Address + 0x00000088, u32, packed struct {
        JDATA3: u16, // bit offset: 0 desc: JDATA3
        padding: u16 = 0,
    });
    // byte offset: 140 injected data register 4
    pub const JDR4 = MMIO(Address + 0x0000008c, u32, packed struct {
        JDATA4: u16, // bit offset: 0 desc: JDATA4
        padding: u16 = 0,
    });
    // byte offset: 160 Analog Watchdog 2 Configuration Register
    pub const AWD2CR = MMIO(Address + 0x000000a0, u32, packed struct {
        reserved0: u1 = 0,
        AWD2CH: u18, // bit offset: 1 desc: AWD2CH
        padding: u13 = 0,
    });
    // byte offset: 164 Analog Watchdog 3 Configuration Register
    pub const AWD3CR = MMIO(Address + 0x000000a4, u32, packed struct {
        reserved0: u1 = 0,
        AWD3CH: u18, // bit offset: 1 desc: AWD3CH
        padding: u13 = 0,
    });
    // byte offset: 176 Differential Mode Selection Register 2
    pub const DIFSEL = MMIO(Address + 0x000000b0, u32, packed struct {
        reserved0: u1 = 0,
        DIFSEL_1_15: u15, // bit offset: 1 desc: Differential mode for channels 15 to 1
        DIFSEL_16_18: u3, // bit offset: 16 desc: Differential mode for channels 18 to 16
        padding: u13 = 0,
    });
    // byte offset: 180 Calibration Factors
    pub const CALFACT = MMIO(Address + 0x000000b4, u32, packed struct {
        CALFACT_S: u7, // bit offset: 0 desc: CALFACT_S
        reserved0: u9 = 0,
        CALFACT_D: u7, // bit offset: 16 desc: CALFACT_D
        padding: u9 = 0,
    });
};
pub const ADC3 = extern struct {
    pub const Address: u32 = 0x50000400;
    // byte offset: 0 interrupt and status register
    pub const ISR = MMIO(Address + 0x00000000, u32, packed struct {
        ADRDY: bool, // bit offset: 0 desc: ADRDY
        EOSMP: bool, // bit offset: 1 desc: EOSMP
        EOC: bool, // bit offset: 2 desc: EOC
        EOS: bool, // bit offset: 3 desc: EOS
        OVR: bool, // bit offset: 4 desc: OVR
        JEOC: bool, // bit offset: 5 desc: JEOC
        JEOS: bool, // bit offset: 6 desc: JEOS
        AWD1: bool, // bit offset: 7 desc: AWD1
        AWD2: bool, // bit offset: 8 desc: AWD2
        AWD3: bool, // bit offset: 9 desc: AWD3
        JQOVF: bool, // bit offset: 10 desc: JQOVF
        padding: u21 = 0,
    });
    // byte offset: 4 interrupt enable register
    pub const IER = MMIO(Address + 0x00000004, u32, packed struct {
        ADRDYIE: bool, // bit offset: 0 desc: ADRDYIE
        EOSMPIE: bool, // bit offset: 1 desc: EOSMPIE
        EOCIE: bool, // bit offset: 2 desc: EOCIE
        EOSIE: bool, // bit offset: 3 desc: EOSIE
        OVRIE: bool, // bit offset: 4 desc: OVRIE
        JEOCIE: bool, // bit offset: 5 desc: JEOCIE
        JEOSIE: bool, // bit offset: 6 desc: JEOSIE
        AWD1IE: bool, // bit offset: 7 desc: AWD1IE
        AWD2IE: bool, // bit offset: 8 desc: AWD2IE
        AWD3IE: bool, // bit offset: 9 desc: AWD3IE
        JQOVFIE: bool, // bit offset: 10 desc: JQOVFIE
        padding: u21 = 0,
    });
    // byte offset: 8 control register
    pub const CR = MMIO(Address + 0x00000008, u32, packed struct {
        ADEN: bool, // bit offset: 0 desc: ADEN
        ADDIS: bool, // bit offset: 1 desc: ADDIS
        ADSTART: bool, // bit offset: 2 desc: ADSTART
        JADSTART: bool, // bit offset: 3 desc: JADSTART
        ADSTP: bool, // bit offset: 4 desc: ADSTP
        JADSTP: bool, // bit offset: 5 desc: JADSTP
        reserved0: u22 = 0,
        ADVREGEN: bool, // bit offset: 28 desc: ADVREGEN
        DEEPPWD: bool, // bit offset: 29 desc: DEEPPWD
        ADCALDIF: bool, // bit offset: 30 desc: ADCALDIF
        ADCAL: bool, // bit offset: 31 desc: ADCAL
    });
    // byte offset: 12 configuration register
    pub const CFGR = MMIO(Address + 0x0000000c, u32, packed struct {
        DMAEN: bool, // bit offset: 0 desc: DMAEN
        DMACFG: bool, // bit offset: 1 desc: DMACFG
        reserved0: u1 = 0,
        RES: u2, // bit offset: 3 desc: RES
        ALIGN: bool, // bit offset: 5 desc: ALIGN
        EXTSEL: u4, // bit offset: 6 desc: EXTSEL
        EXTEN: u2, // bit offset: 10 desc: EXTEN
        OVRMOD: bool, // bit offset: 12 desc: OVRMOD
        CONT: bool, // bit offset: 13 desc: CONT
        AUTDLY: bool, // bit offset: 14 desc: AUTDLY
        AUTOFF: bool, // bit offset: 15 desc: AUTOFF
        DISCEN: bool, // bit offset: 16 desc: DISCEN
        DISCNUM: u3, // bit offset: 17 desc: DISCNUM
        JDISCEN: bool, // bit offset: 20 desc: JDISCEN
        JQM: bool, // bit offset: 21 desc: JQM
        AWD1SGL: bool, // bit offset: 22 desc: AWD1SGL
        AWD1EN: bool, // bit offset: 23 desc: AWD1EN
        JAWD1EN: bool, // bit offset: 24 desc: JAWD1EN
        JAUTO: bool, // bit offset: 25 desc: JAUTO
        AWDCH1CH: u5, // bit offset: 26 desc: AWDCH1CH
        padding: u1 = 0,
    });
    // byte offset: 20 sample time register 1
    pub const SMPR1 = MMIO(Address + 0x00000014, u32, packed struct {
        reserved0: u3 = 0,
        SMP1: u3, // bit offset: 3 desc: SMP1
        SMP2: u3, // bit offset: 6 desc: SMP2
        SMP3: u3, // bit offset: 9 desc: SMP3
        SMP4: u3, // bit offset: 12 desc: SMP4
        SMP5: u3, // bit offset: 15 desc: SMP5
        SMP6: u3, // bit offset: 18 desc: SMP6
        SMP7: u3, // bit offset: 21 desc: SMP7
        SMP8: u3, // bit offset: 24 desc: SMP8
        SMP9: u3, // bit offset: 27 desc: SMP9
        padding: u2 = 0,
    });
    // byte offset: 24 sample time register 2
    pub const SMPR2 = MMIO(Address + 0x00000018, u32, packed struct {
        SMP10: u3, // bit offset: 0 desc: SMP10
        SMP11: u3, // bit offset: 3 desc: SMP11
        SMP12: u3, // bit offset: 6 desc: SMP12
        SMP13: u3, // bit offset: 9 desc: SMP13
        SMP14: u3, // bit offset: 12 desc: SMP14
        SMP15: u3, // bit offset: 15 desc: SMP15
        SMP16: u3, // bit offset: 18 desc: SMP16
        SMP17: u3, // bit offset: 21 desc: SMP17
        SMP18: u3, // bit offset: 24 desc: SMP18
        padding: u5 = 0,
    });
    // byte offset: 32 watchdog threshold register 1
    pub const TR1 = MMIO(Address + 0x00000020, u32, packed struct {
        LT1: u12, // bit offset: 0 desc: LT1
        reserved0: u4 = 0,
        HT1: u12, // bit offset: 16 desc: HT1
        padding: u4 = 0,
    });
    // byte offset: 36 watchdog threshold register
    pub const TR2 = MMIO(Address + 0x00000024, u32, packed struct {
        LT2: u8, // bit offset: 0 desc: LT2
        reserved0: u8 = 0,
        HT2: u8, // bit offset: 16 desc: HT2
        padding: u8 = 0,
    });
    // byte offset: 40 watchdog threshold register 3
    pub const TR3 = MMIO(Address + 0x00000028, u32, packed struct {
        LT3: u8, // bit offset: 0 desc: LT3
        reserved0: u8 = 0,
        HT3: u8, // bit offset: 16 desc: HT3
        padding: u8 = 0,
    });
    // byte offset: 48 regular sequence register 1
    pub const SQR1 = MMIO(Address + 0x00000030, u32, packed struct {
        L3: u4, // bit offset: 0 desc: L3
        reserved0: u2 = 0,
        SQ1: u5, // bit offset: 6 desc: SQ1
        reserved1: u1 = 0,
        SQ2: u5, // bit offset: 12 desc: SQ2
        reserved2: u1 = 0,
        SQ3: u5, // bit offset: 18 desc: SQ3
        reserved3: u1 = 0,
        SQ4: u5, // bit offset: 24 desc: SQ4
        padding: u3 = 0,
    });
    // byte offset: 52 regular sequence register 2
    pub const SQR2 = MMIO(Address + 0x00000034, u32, packed struct {
        SQ5: u5, // bit offset: 0 desc: SQ5
        reserved0: u1 = 0,
        SQ6: u5, // bit offset: 6 desc: SQ6
        reserved1: u1 = 0,
        SQ7: u5, // bit offset: 12 desc: SQ7
        reserved2: u1 = 0,
        SQ8: u5, // bit offset: 18 desc: SQ8
        reserved3: u1 = 0,
        SQ9: u5, // bit offset: 24 desc: SQ9
        padding: u3 = 0,
    });
    // byte offset: 56 regular sequence register 3
    pub const SQR3 = MMIO(Address + 0x00000038, u32, packed struct {
        SQ10: u5, // bit offset: 0 desc: SQ10
        reserved0: u1 = 0,
        SQ11: u5, // bit offset: 6 desc: SQ11
        reserved1: u1 = 0,
        SQ12: u5, // bit offset: 12 desc: SQ12
        reserved2: u1 = 0,
        SQ13: u5, // bit offset: 18 desc: SQ13
        reserved3: u1 = 0,
        SQ14: u5, // bit offset: 24 desc: SQ14
        padding: u3 = 0,
    });
    // byte offset: 60 regular sequence register 4
    pub const SQR4 = MMIO(Address + 0x0000003c, u32, packed struct {
        SQ15: u5, // bit offset: 0 desc: SQ15
        reserved0: u1 = 0,
        SQ16: u5, // bit offset: 6 desc: SQ16
        padding: u21 = 0,
    });
    // byte offset: 64 regular Data Register
    pub const DR = MMIO(Address + 0x00000040, u32, packed struct {
        regularDATA: u16, // bit offset: 0 desc: regularDATA
        padding: u16 = 0,
    });
    // byte offset: 76 injected sequence register
    pub const JSQR = MMIO(Address + 0x0000004c, u32, packed struct {
        JL: u2, // bit offset: 0 desc: JL
        JEXTSEL: u4, // bit offset: 2 desc: JEXTSEL
        JEXTEN: u2, // bit offset: 6 desc: JEXTEN
        JSQ1: u5, // bit offset: 8 desc: JSQ1
        reserved0: u1 = 0,
        JSQ2: u5, // bit offset: 14 desc: JSQ2
        reserved1: u1 = 0,
        JSQ3: u5, // bit offset: 20 desc: JSQ3
        reserved2: u1 = 0,
        JSQ4: u5, // bit offset: 26 desc: JSQ4
        padding: u1 = 0,
    });
    // byte offset: 96 offset register 1
    pub const OFR1 = MMIO(Address + 0x00000060, u32, packed struct {
        OFFSET1: u12, // bit offset: 0 desc: OFFSET1
        reserved0: u14 = 0,
        OFFSET1_CH: u5, // bit offset: 26 desc: OFFSET1_CH
        OFFSET1_EN: bool, // bit offset: 31 desc: OFFSET1_EN
    });
    // byte offset: 100 offset register 2
    pub const OFR2 = MMIO(Address + 0x00000064, u32, packed struct {
        OFFSET2: u12, // bit offset: 0 desc: OFFSET2
        reserved0: u14 = 0,
        OFFSET2_CH: u5, // bit offset: 26 desc: OFFSET2_CH
        OFFSET2_EN: bool, // bit offset: 31 desc: OFFSET2_EN
    });
    // byte offset: 104 offset register 3
    pub const OFR3 = MMIO(Address + 0x00000068, u32, packed struct {
        OFFSET3: u12, // bit offset: 0 desc: OFFSET3
        reserved0: u14 = 0,
        OFFSET3_CH: u5, // bit offset: 26 desc: OFFSET3_CH
        OFFSET3_EN: bool, // bit offset: 31 desc: OFFSET3_EN
    });
    // byte offset: 108 offset register 4
    pub const OFR4 = MMIO(Address + 0x0000006c, u32, packed struct {
        OFFSET4: u12, // bit offset: 0 desc: OFFSET4
        reserved0: u14 = 0,
        OFFSET4_CH: u5, // bit offset: 26 desc: OFFSET4_CH
        OFFSET4_EN: bool, // bit offset: 31 desc: OFFSET4_EN
    });
    // byte offset: 128 injected data register 1
    pub const JDR1 = MMIO(Address + 0x00000080, u32, packed struct {
        JDATA1: u16, // bit offset: 0 desc: JDATA1
        padding: u16 = 0,
    });
    // byte offset: 132 injected data register 2
    pub const JDR2 = MMIO(Address + 0x00000084, u32, packed struct {
        JDATA2: u16, // bit offset: 0 desc: JDATA2
        padding: u16 = 0,
    });
    // byte offset: 136 injected data register 3
    pub const JDR3 = MMIO(Address + 0x00000088, u32, packed struct {
        JDATA3: u16, // bit offset: 0 desc: JDATA3
        padding: u16 = 0,
    });
    // byte offset: 140 injected data register 4
    pub const JDR4 = MMIO(Address + 0x0000008c, u32, packed struct {
        JDATA4: u16, // bit offset: 0 desc: JDATA4
        padding: u16 = 0,
    });
    // byte offset: 160 Analog Watchdog 2 Configuration Register
    pub const AWD2CR = MMIO(Address + 0x000000a0, u32, packed struct {
        reserved0: u1 = 0,
        AWD2CH: u18, // bit offset: 1 desc: AWD2CH
        padding: u13 = 0,
    });
    // byte offset: 164 Analog Watchdog 3 Configuration Register
    pub const AWD3CR = MMIO(Address + 0x000000a4, u32, packed struct {
        reserved0: u1 = 0,
        AWD3CH: u18, // bit offset: 1 desc: AWD3CH
        padding: u13 = 0,
    });
    // byte offset: 176 Differential Mode Selection Register 2
    pub const DIFSEL = MMIO(Address + 0x000000b0, u32, packed struct {
        reserved0: u1 = 0,
        DIFSEL_1_15: u15, // bit offset: 1 desc: Differential mode for channels 15 to 1
        DIFSEL_16_18: u3, // bit offset: 16 desc: Differential mode for channels 18 to 16
        padding: u13 = 0,
    });
    // byte offset: 180 Calibration Factors
    pub const CALFACT = MMIO(Address + 0x000000b4, u32, packed struct {
        CALFACT_S: u7, // bit offset: 0 desc: CALFACT_S
        reserved0: u9 = 0,
        CALFACT_D: u7, // bit offset: 16 desc: CALFACT_D
        padding: u9 = 0,
    });
};
pub const ADC4 = extern struct {
    pub const Address: u32 = 0x50000500;
    // byte offset: 0 interrupt and status register
    pub const ISR = MMIO(Address + 0x00000000, u32, packed struct {
        ADRDY: bool, // bit offset: 0 desc: ADRDY
        EOSMP: bool, // bit offset: 1 desc: EOSMP
        EOC: bool, // bit offset: 2 desc: EOC
        EOS: bool, // bit offset: 3 desc: EOS
        OVR: bool, // bit offset: 4 desc: OVR
        JEOC: bool, // bit offset: 5 desc: JEOC
        JEOS: bool, // bit offset: 6 desc: JEOS
        AWD1: bool, // bit offset: 7 desc: AWD1
        AWD2: bool, // bit offset: 8 desc: AWD2
        AWD3: bool, // bit offset: 9 desc: AWD3
        JQOVF: bool, // bit offset: 10 desc: JQOVF
        padding: u21 = 0,
    });
    // byte offset: 4 interrupt enable register
    pub const IER = MMIO(Address + 0x00000004, u32, packed struct {
        ADRDYIE: bool, // bit offset: 0 desc: ADRDYIE
        EOSMPIE: bool, // bit offset: 1 desc: EOSMPIE
        EOCIE: bool, // bit offset: 2 desc: EOCIE
        EOSIE: bool, // bit offset: 3 desc: EOSIE
        OVRIE: bool, // bit offset: 4 desc: OVRIE
        JEOCIE: bool, // bit offset: 5 desc: JEOCIE
        JEOSIE: bool, // bit offset: 6 desc: JEOSIE
        AWD1IE: bool, // bit offset: 7 desc: AWD1IE
        AWD2IE: bool, // bit offset: 8 desc: AWD2IE
        AWD3IE: bool, // bit offset: 9 desc: AWD3IE
        JQOVFIE: bool, // bit offset: 10 desc: JQOVFIE
        padding: u21 = 0,
    });
    // byte offset: 8 control register
    pub const CR = MMIO(Address + 0x00000008, u32, packed struct {
        ADEN: bool, // bit offset: 0 desc: ADEN
        ADDIS: bool, // bit offset: 1 desc: ADDIS
        ADSTART: bool, // bit offset: 2 desc: ADSTART
        JADSTART: bool, // bit offset: 3 desc: JADSTART
        ADSTP: bool, // bit offset: 4 desc: ADSTP
        JADSTP: bool, // bit offset: 5 desc: JADSTP
        reserved0: u22 = 0,
        ADVREGEN: bool, // bit offset: 28 desc: ADVREGEN
        DEEPPWD: bool, // bit offset: 29 desc: DEEPPWD
        ADCALDIF: bool, // bit offset: 30 desc: ADCALDIF
        ADCAL: bool, // bit offset: 31 desc: ADCAL
    });
    // byte offset: 12 configuration register
    pub const CFGR = MMIO(Address + 0x0000000c, u32, packed struct {
        DMAEN: bool, // bit offset: 0 desc: DMAEN
        DMACFG: bool, // bit offset: 1 desc: DMACFG
        reserved0: u1 = 0,
        RES: u2, // bit offset: 3 desc: RES
        ALIGN: bool, // bit offset: 5 desc: ALIGN
        EXTSEL: u4, // bit offset: 6 desc: EXTSEL
        EXTEN: u2, // bit offset: 10 desc: EXTEN
        OVRMOD: bool, // bit offset: 12 desc: OVRMOD
        CONT: bool, // bit offset: 13 desc: CONT
        AUTDLY: bool, // bit offset: 14 desc: AUTDLY
        AUTOFF: bool, // bit offset: 15 desc: AUTOFF
        DISCEN: bool, // bit offset: 16 desc: DISCEN
        DISCNUM: u3, // bit offset: 17 desc: DISCNUM
        JDISCEN: bool, // bit offset: 20 desc: JDISCEN
        JQM: bool, // bit offset: 21 desc: JQM
        AWD1SGL: bool, // bit offset: 22 desc: AWD1SGL
        AWD1EN: bool, // bit offset: 23 desc: AWD1EN
        JAWD1EN: bool, // bit offset: 24 desc: JAWD1EN
        JAUTO: bool, // bit offset: 25 desc: JAUTO
        AWDCH1CH: u5, // bit offset: 26 desc: AWDCH1CH
        padding: u1 = 0,
    });
    // byte offset: 20 sample time register 1
    pub const SMPR1 = MMIO(Address + 0x00000014, u32, packed struct {
        reserved0: u3 = 0,
        SMP1: u3, // bit offset: 3 desc: SMP1
        SMP2: u3, // bit offset: 6 desc: SMP2
        SMP3: u3, // bit offset: 9 desc: SMP3
        SMP4: u3, // bit offset: 12 desc: SMP4
        SMP5: u3, // bit offset: 15 desc: SMP5
        SMP6: u3, // bit offset: 18 desc: SMP6
        SMP7: u3, // bit offset: 21 desc: SMP7
        SMP8: u3, // bit offset: 24 desc: SMP8
        SMP9: u3, // bit offset: 27 desc: SMP9
        padding: u2 = 0,
    });
    // byte offset: 24 sample time register 2
    pub const SMPR2 = MMIO(Address + 0x00000018, u32, packed struct {
        SMP10: u3, // bit offset: 0 desc: SMP10
        SMP11: u3, // bit offset: 3 desc: SMP11
        SMP12: u3, // bit offset: 6 desc: SMP12
        SMP13: u3, // bit offset: 9 desc: SMP13
        SMP14: u3, // bit offset: 12 desc: SMP14
        SMP15: u3, // bit offset: 15 desc: SMP15
        SMP16: u3, // bit offset: 18 desc: SMP16
        SMP17: u3, // bit offset: 21 desc: SMP17
        SMP18: u3, // bit offset: 24 desc: SMP18
        padding: u5 = 0,
    });
    // byte offset: 32 watchdog threshold register 1
    pub const TR1 = MMIO(Address + 0x00000020, u32, packed struct {
        LT1: u12, // bit offset: 0 desc: LT1
        reserved0: u4 = 0,
        HT1: u12, // bit offset: 16 desc: HT1
        padding: u4 = 0,
    });
    // byte offset: 36 watchdog threshold register
    pub const TR2 = MMIO(Address + 0x00000024, u32, packed struct {
        LT2: u8, // bit offset: 0 desc: LT2
        reserved0: u8 = 0,
        HT2: u8, // bit offset: 16 desc: HT2
        padding: u8 = 0,
    });
    // byte offset: 40 watchdog threshold register 3
    pub const TR3 = MMIO(Address + 0x00000028, u32, packed struct {
        LT3: u8, // bit offset: 0 desc: LT3
        reserved0: u8 = 0,
        HT3: u8, // bit offset: 16 desc: HT3
        padding: u8 = 0,
    });
    // byte offset: 48 regular sequence register 1
    pub const SQR1 = MMIO(Address + 0x00000030, u32, packed struct {
        L3: u4, // bit offset: 0 desc: L3
        reserved0: u2 = 0,
        SQ1: u5, // bit offset: 6 desc: SQ1
        reserved1: u1 = 0,
        SQ2: u5, // bit offset: 12 desc: SQ2
        reserved2: u1 = 0,
        SQ3: u5, // bit offset: 18 desc: SQ3
        reserved3: u1 = 0,
        SQ4: u5, // bit offset: 24 desc: SQ4
        padding: u3 = 0,
    });
    // byte offset: 52 regular sequence register 2
    pub const SQR2 = MMIO(Address + 0x00000034, u32, packed struct {
        SQ5: u5, // bit offset: 0 desc: SQ5
        reserved0: u1 = 0,
        SQ6: u5, // bit offset: 6 desc: SQ6
        reserved1: u1 = 0,
        SQ7: u5, // bit offset: 12 desc: SQ7
        reserved2: u1 = 0,
        SQ8: u5, // bit offset: 18 desc: SQ8
        reserved3: u1 = 0,
        SQ9: u5, // bit offset: 24 desc: SQ9
        padding: u3 = 0,
    });
    // byte offset: 56 regular sequence register 3
    pub const SQR3 = MMIO(Address + 0x00000038, u32, packed struct {
        SQ10: u5, // bit offset: 0 desc: SQ10
        reserved0: u1 = 0,
        SQ11: u5, // bit offset: 6 desc: SQ11
        reserved1: u1 = 0,
        SQ12: u5, // bit offset: 12 desc: SQ12
        reserved2: u1 = 0,
        SQ13: u5, // bit offset: 18 desc: SQ13
        reserved3: u1 = 0,
        SQ14: u5, // bit offset: 24 desc: SQ14
        padding: u3 = 0,
    });
    // byte offset: 60 regular sequence register 4
    pub const SQR4 = MMIO(Address + 0x0000003c, u32, packed struct {
        SQ15: u5, // bit offset: 0 desc: SQ15
        reserved0: u1 = 0,
        SQ16: u5, // bit offset: 6 desc: SQ16
        padding: u21 = 0,
    });
    // byte offset: 64 regular Data Register
    pub const DR = MMIO(Address + 0x00000040, u32, packed struct {
        regularDATA: u16, // bit offset: 0 desc: regularDATA
        padding: u16 = 0,
    });
    // byte offset: 76 injected sequence register
    pub const JSQR = MMIO(Address + 0x0000004c, u32, packed struct {
        JL: u2, // bit offset: 0 desc: JL
        JEXTSEL: u4, // bit offset: 2 desc: JEXTSEL
        JEXTEN: u2, // bit offset: 6 desc: JEXTEN
        JSQ1: u5, // bit offset: 8 desc: JSQ1
        reserved0: u1 = 0,
        JSQ2: u5, // bit offset: 14 desc: JSQ2
        reserved1: u1 = 0,
        JSQ3: u5, // bit offset: 20 desc: JSQ3
        reserved2: u1 = 0,
        JSQ4: u5, // bit offset: 26 desc: JSQ4
        padding: u1 = 0,
    });
    // byte offset: 96 offset register 1
    pub const OFR1 = MMIO(Address + 0x00000060, u32, packed struct {
        OFFSET1: u12, // bit offset: 0 desc: OFFSET1
        reserved0: u14 = 0,
        OFFSET1_CH: u5, // bit offset: 26 desc: OFFSET1_CH
        OFFSET1_EN: bool, // bit offset: 31 desc: OFFSET1_EN
    });
    // byte offset: 100 offset register 2
    pub const OFR2 = MMIO(Address + 0x00000064, u32, packed struct {
        OFFSET2: u12, // bit offset: 0 desc: OFFSET2
        reserved0: u14 = 0,
        OFFSET2_CH: u5, // bit offset: 26 desc: OFFSET2_CH
        OFFSET2_EN: bool, // bit offset: 31 desc: OFFSET2_EN
    });
    // byte offset: 104 offset register 3
    pub const OFR3 = MMIO(Address + 0x00000068, u32, packed struct {
        OFFSET3: u12, // bit offset: 0 desc: OFFSET3
        reserved0: u14 = 0,
        OFFSET3_CH: u5, // bit offset: 26 desc: OFFSET3_CH
        OFFSET3_EN: bool, // bit offset: 31 desc: OFFSET3_EN
    });
    // byte offset: 108 offset register 4
    pub const OFR4 = MMIO(Address + 0x0000006c, u32, packed struct {
        OFFSET4: u12, // bit offset: 0 desc: OFFSET4
        reserved0: u14 = 0,
        OFFSET4_CH: u5, // bit offset: 26 desc: OFFSET4_CH
        OFFSET4_EN: bool, // bit offset: 31 desc: OFFSET4_EN
    });
    // byte offset: 128 injected data register 1
    pub const JDR1 = MMIO(Address + 0x00000080, u32, packed struct {
        JDATA1: u16, // bit offset: 0 desc: JDATA1
        padding: u16 = 0,
    });
    // byte offset: 132 injected data register 2
    pub const JDR2 = MMIO(Address + 0x00000084, u32, packed struct {
        JDATA2: u16, // bit offset: 0 desc: JDATA2
        padding: u16 = 0,
    });
    // byte offset: 136 injected data register 3
    pub const JDR3 = MMIO(Address + 0x00000088, u32, packed struct {
        JDATA3: u16, // bit offset: 0 desc: JDATA3
        padding: u16 = 0,
    });
    // byte offset: 140 injected data register 4
    pub const JDR4 = MMIO(Address + 0x0000008c, u32, packed struct {
        JDATA4: u16, // bit offset: 0 desc: JDATA4
        padding: u16 = 0,
    });
    // byte offset: 160 Analog Watchdog 2 Configuration Register
    pub const AWD2CR = MMIO(Address + 0x000000a0, u32, packed struct {
        reserved0: u1 = 0,
        AWD2CH: u18, // bit offset: 1 desc: AWD2CH
        padding: u13 = 0,
    });
    // byte offset: 164 Analog Watchdog 3 Configuration Register
    pub const AWD3CR = MMIO(Address + 0x000000a4, u32, packed struct {
        reserved0: u1 = 0,
        AWD3CH: u18, // bit offset: 1 desc: AWD3CH
        padding: u13 = 0,
    });
    // byte offset: 176 Differential Mode Selection Register 2
    pub const DIFSEL = MMIO(Address + 0x000000b0, u32, packed struct {
        reserved0: u1 = 0,
        DIFSEL_1_15: u15, // bit offset: 1 desc: Differential mode for channels 15 to 1
        DIFSEL_16_18: u3, // bit offset: 16 desc: Differential mode for channels 18 to 16
        padding: u13 = 0,
    });
    // byte offset: 180 Calibration Factors
    pub const CALFACT = MMIO(Address + 0x000000b4, u32, packed struct {
        CALFACT_S: u7, // bit offset: 0 desc: CALFACT_S
        reserved0: u9 = 0,
        CALFACT_D: u7, // bit offset: 16 desc: CALFACT_D
        padding: u9 = 0,
    });
};
pub const ADC1_2 = extern struct {
    pub const Address: u32 = 0x50000300;
    // byte offset: 0 ADC Common status register
    pub const CSR = MMIO(Address + 0x00000000, u32, packed struct {
        ADDRDY_MST: bool, // bit offset: 0 desc: ADDRDY_MST
        EOSMP_MST: bool, // bit offset: 1 desc: EOSMP_MST
        EOC_MST: bool, // bit offset: 2 desc: EOC_MST
        EOS_MST: bool, // bit offset: 3 desc: EOS_MST
        OVR_MST: bool, // bit offset: 4 desc: OVR_MST
        JEOC_MST: bool, // bit offset: 5 desc: JEOC_MST
        JEOS_MST: bool, // bit offset: 6 desc: JEOS_MST
        AWD1_MST: bool, // bit offset: 7 desc: AWD1_MST
        AWD2_MST: bool, // bit offset: 8 desc: AWD2_MST
        AWD3_MST: bool, // bit offset: 9 desc: AWD3_MST
        JQOVF_MST: bool, // bit offset: 10 desc: JQOVF_MST
        reserved0: u5 = 0,
        ADRDY_SLV: bool, // bit offset: 16 desc: ADRDY_SLV
        EOSMP_SLV: bool, // bit offset: 17 desc: EOSMP_SLV
        EOC_SLV: bool, // bit offset: 18 desc: End of regular conversion of the slave ADC
        EOS_SLV: bool, // bit offset: 19 desc: End of regular sequence flag of the slave ADC
        OVR_SLV: bool, // bit offset: 20 desc: Overrun flag of the slave ADC
        JEOC_SLV: bool, // bit offset: 21 desc: End of injected conversion flag of the slave ADC
        JEOS_SLV: bool, // bit offset: 22 desc: End of injected sequence flag of the slave ADC
        AWD1_SLV: bool, // bit offset: 23 desc: Analog watchdog 1 flag of the slave ADC
        AWD2_SLV: bool, // bit offset: 24 desc: Analog watchdog 2 flag of the slave ADC
        AWD3_SLV: bool, // bit offset: 25 desc: Analog watchdog 3 flag of the slave ADC
        JQOVF_SLV: bool, // bit offset: 26 desc: Injected Context Queue Overflow flag of the slave ADC
        padding: u5 = 0,
    });
    // byte offset: 8 ADC common control register
    pub const CCR = MMIO(Address + 0x00000008, u32, packed struct {
        MULT: u5, // bit offset: 0 desc: Multi ADC mode selection
        reserved0: u3 = 0,
        DELAY: u4, // bit offset: 8 desc: Delay between 2 sampling phases
        reserved1: u1 = 0,
        DMACFG: bool, // bit offset: 13 desc: DMA configuration (for multi-ADC mode)
        MDMA: u2, // bit offset: 14 desc: Direct memory access mode for multi ADC mode
        CKMODE: u2, // bit offset: 16 desc: ADC clock mode
        reserved2: u4 = 0,
        VREFEN: bool, // bit offset: 22 desc: VREFINT enable
        TSEN: bool, // bit offset: 23 desc: Temperature sensor enable
        VBATEN: bool, // bit offset: 24 desc: VBAT enable
        padding: u7 = 0,
    });
    // byte offset: 12 ADC common regular data register for dual and triple modes
    pub const CDR = MMIO(Address + 0x0000000c, u32, packed struct {
        RDATA_MST: u16, // bit offset: 0 desc: Regular data of the master ADC
        RDATA_SLV: u16, // bit offset: 16 desc: Regular data of the slave ADC
    });
};
pub const ADC3_4 = extern struct {
    pub const Address: u32 = 0x50000700;
    // byte offset: 0 ADC Common status register
    pub const CSR = MMIO(Address + 0x00000000, u32, packed struct {
        ADDRDY_MST: bool, // bit offset: 0 desc: ADDRDY_MST
        EOSMP_MST: bool, // bit offset: 1 desc: EOSMP_MST
        EOC_MST: bool, // bit offset: 2 desc: EOC_MST
        EOS_MST: bool, // bit offset: 3 desc: EOS_MST
        OVR_MST: bool, // bit offset: 4 desc: OVR_MST
        JEOC_MST: bool, // bit offset: 5 desc: JEOC_MST
        JEOS_MST: bool, // bit offset: 6 desc: JEOS_MST
        AWD1_MST: bool, // bit offset: 7 desc: AWD1_MST
        AWD2_MST: bool, // bit offset: 8 desc: AWD2_MST
        AWD3_MST: bool, // bit offset: 9 desc: AWD3_MST
        JQOVF_MST: bool, // bit offset: 10 desc: JQOVF_MST
        reserved0: u5 = 0,
        ADRDY_SLV: bool, // bit offset: 16 desc: ADRDY_SLV
        EOSMP_SLV: bool, // bit offset: 17 desc: EOSMP_SLV
        EOC_SLV: bool, // bit offset: 18 desc: End of regular conversion of the slave ADC
        EOS_SLV: bool, // bit offset: 19 desc: End of regular sequence flag of the slave ADC
        OVR_SLV: bool, // bit offset: 20 desc: Overrun flag of the slave ADC
        JEOC_SLV: bool, // bit offset: 21 desc: End of injected conversion flag of the slave ADC
        JEOS_SLV: bool, // bit offset: 22 desc: End of injected sequence flag of the slave ADC
        AWD1_SLV: bool, // bit offset: 23 desc: Analog watchdog 1 flag of the slave ADC
        AWD2_SLV: bool, // bit offset: 24 desc: Analog watchdog 2 flag of the slave ADC
        AWD3_SLV: bool, // bit offset: 25 desc: Analog watchdog 3 flag of the slave ADC
        JQOVF_SLV: bool, // bit offset: 26 desc: Injected Context Queue Overflow flag of the slave ADC
        padding: u5 = 0,
    });
    // byte offset: 8 ADC common control register
    pub const CCR = MMIO(Address + 0x00000008, u32, packed struct {
        MULT: u5, // bit offset: 0 desc: Multi ADC mode selection
        reserved0: u3 = 0,
        DELAY: u4, // bit offset: 8 desc: Delay between 2 sampling phases
        reserved1: u1 = 0,
        DMACFG: bool, // bit offset: 13 desc: DMA configuration (for multi-ADC mode)
        MDMA: u2, // bit offset: 14 desc: Direct memory access mode for multi ADC mode
        CKMODE: u2, // bit offset: 16 desc: ADC clock mode
        reserved2: u4 = 0,
        VREFEN: bool, // bit offset: 22 desc: VREFINT enable
        TSEN: bool, // bit offset: 23 desc: Temperature sensor enable
        VBATEN: bool, // bit offset: 24 desc: VBAT enable
        padding: u7 = 0,
    });
    // byte offset: 12 ADC common regular data register for dual and triple modes
    pub const CDR = MMIO(Address + 0x0000000c, u32, packed struct {
        RDATA_MST: u16, // bit offset: 0 desc: Regular data of the master ADC
        RDATA_SLV: u16, // bit offset: 16 desc: Regular data of the slave ADC
    });
};
pub const SYSCFG_COMP_OPAMP = extern struct {
    pub const Address: u32 = 0x40010000;
    // byte offset: 0 configuration register 1
    pub const SYSCFG_CFGR1 = MMIO(Address + 0x00000000, u32, packed struct {
        MEM_MODE: u2, // bit offset: 0 desc: Memory mapping selection bits
        reserved0: u3 = 0,
        USB_IT_RMP: bool, // bit offset: 5 desc: USB interrupt remap
        TIM1_ITR_RMP: bool, // bit offset: 6 desc: Timer 1 ITR3 selection
        DAC_TRIG_RMP: bool, // bit offset: 7 desc: DAC trigger remap (when TSEL = 001)
        ADC24_DMA_RMP: bool, // bit offset: 8 desc: ADC24 DMA remapping bit
        reserved1: u2 = 0,
        TIM16_DMA_RMP: bool, // bit offset: 11 desc: TIM16 DMA request remapping bit
        TIM17_DMA_RMP: bool, // bit offset: 12 desc: TIM17 DMA request remapping bit
        TIM6_DAC1_DMA_RMP: bool, // bit offset: 13 desc: TIM6 and DAC1 DMA request remapping bit
        TIM7_DAC2_DMA_RMP: bool, // bit offset: 14 desc: TIM7 and DAC2 DMA request remapping bit
        reserved2: u1 = 0,
        I2C_PB6_FM: bool, // bit offset: 16 desc: Fast Mode Plus (FM+) driving capability activation bits.
        I2C_PB7_FM: bool, // bit offset: 17 desc: Fast Mode Plus (FM+) driving capability activation bits.
        I2C_PB8_FM: bool, // bit offset: 18 desc: Fast Mode Plus (FM+) driving capability activation bits.
        I2C_PB9_FM: bool, // bit offset: 19 desc: Fast Mode Plus (FM+) driving capability activation bits.
        I2C1_FM: bool, // bit offset: 20 desc: I2C1 Fast Mode Plus
        I2C2_FM: bool, // bit offset: 21 desc: I2C2 Fast Mode Plus
        ENCODER_MODE: u2, // bit offset: 22 desc: Encoder mode
        reserved3: u2 = 0,
        FPU_IT: u6, // bit offset: 26 desc: Interrupt enable bits from FPU
    });
    // byte offset: 4 CCM SRAM protection register
    pub const SYSCFG_RCR = MMIO(Address + 0x00000004, u32, packed struct {
        PAGE0_WP: bool, // bit offset: 0 desc: CCM SRAM page write protection bit
        PAGE1_WP: bool, // bit offset: 1 desc: CCM SRAM page write protection bit
        PAGE2_WP: bool, // bit offset: 2 desc: CCM SRAM page write protection bit
        PAGE3_WP: bool, // bit offset: 3 desc: CCM SRAM page write protection bit
        PAGE4_WP: bool, // bit offset: 4 desc: CCM SRAM page write protection bit
        PAGE5_WP: bool, // bit offset: 5 desc: CCM SRAM page write protection bit
        PAGE6_WP: bool, // bit offset: 6 desc: CCM SRAM page write protection bit
        PAGE7_WP: bool, // bit offset: 7 desc: CCM SRAM page write protection bit
        padding: u24 = 0,
    });
    // byte offset: 8 external interrupt configuration register 1
    pub const SYSCFG_EXTICR1 = MMIO(Address + 0x00000008, u32, packed struct {
        EXTI0: u4, // bit offset: 0 desc: EXTI 0 configuration bits
        EXTI1: u4, // bit offset: 4 desc: EXTI 1 configuration bits
        EXTI2: u4, // bit offset: 8 desc: EXTI 2 configuration bits
        EXTI3: u4, // bit offset: 12 desc: EXTI 3 configuration bits
        padding: u16 = 0,
    });
    // byte offset: 12 external interrupt configuration register 2
    pub const SYSCFG_EXTICR2 = MMIO(Address + 0x0000000c, u32, packed struct {
        EXTI4: u4, // bit offset: 0 desc: EXTI 4 configuration bits
        EXTI5: u4, // bit offset: 4 desc: EXTI 5 configuration bits
        EXTI6: u4, // bit offset: 8 desc: EXTI 6 configuration bits
        EXTI7: u4, // bit offset: 12 desc: EXTI 7 configuration bits
        padding: u16 = 0,
    });
    // byte offset: 16 external interrupt configuration register 3
    pub const SYSCFG_EXTICR3 = MMIO(Address + 0x00000010, u32, packed struct {
        EXTI8: u4, // bit offset: 0 desc: EXTI 8 configuration bits
        EXTI9: u4, // bit offset: 4 desc: EXTI 9 configuration bits
        EXTI10: u4, // bit offset: 8 desc: EXTI 10 configuration bits
        EXTI11: u4, // bit offset: 12 desc: EXTI 11 configuration bits
        padding: u16 = 0,
    });
    // byte offset: 20 external interrupt configuration register 4
    pub const SYSCFG_EXTICR4 = MMIO(Address + 0x00000014, u32, packed struct {
        EXTI12: u4, // bit offset: 0 desc: EXTI 12 configuration bits
        EXTI13: u4, // bit offset: 4 desc: EXTI 13 configuration bits
        EXTI14: u4, // bit offset: 8 desc: EXTI 14 configuration bits
        EXTI15: u4, // bit offset: 12 desc: EXTI 15 configuration bits
        padding: u16 = 0,
    });
    // byte offset: 24 configuration register 2
    pub const SYSCFG_CFGR2 = MMIO(Address + 0x00000018, u32, packed struct {
        LOCUP_LOCK: bool, // bit offset: 0 desc: Cortex-M0 LOCKUP bit enable bit
        SRAM_PARITY_LOCK: bool, // bit offset: 1 desc: SRAM parity lock bit
        PVD_LOCK: bool, // bit offset: 2 desc: PVD lock enable bit
        reserved0: u1 = 0,
        BYP_ADD_PAR: bool, // bit offset: 4 desc: Bypass address bit 29 in parity calculation
        reserved1: u3 = 0,
        SRAM_PEF: bool, // bit offset: 8 desc: SRAM parity flag
        padding: u23 = 0,
    });
    // byte offset: 28 control and status register
    pub const COMP1_CSR = MMIO(Address + 0x0000001c, u32, packed struct {
        COMP1EN: bool, // bit offset: 0 desc: Comparator 1 enable
        COMP1_INP_DAC: bool, // bit offset: 1 desc: COMP1_INP_DAC
        COMP1MODE: u2, // bit offset: 2 desc: Comparator 1 mode
        COMP1INSEL: u3, // bit offset: 4 desc: Comparator 1 inverting input selection
        reserved0: u3 = 0,
        COMP1_OUT_SEL: u4, // bit offset: 10 desc: Comparator 1 output selection
        reserved1: u1 = 0,
        COMP1POL: bool, // bit offset: 15 desc: Comparator 1 output polarity
        COMP1HYST: u2, // bit offset: 16 desc: Comparator 1 hysteresis
        COMP1_BLANKING: u3, // bit offset: 18 desc: Comparator 1 blanking source
        reserved2: u9 = 0,
        COMP1OUT: bool, // bit offset: 30 desc: Comparator 1 output
        COMP1LOCK: bool, // bit offset: 31 desc: Comparator 1 lock
    });
    // byte offset: 32 control and status register
    pub const COMP2_CSR = MMIO(Address + 0x00000020, u32, packed struct {
        COMP2EN: bool, // bit offset: 0 desc: Comparator 2 enable
        reserved0: u1 = 0,
        COMP2MODE: u2, // bit offset: 2 desc: Comparator 2 mode
        COMP2INSEL: u3, // bit offset: 4 desc: Comparator 2 inverting input selection
        COMP2INPSEL: bool, // bit offset: 7 desc: Comparator 2 non inverted input selection
        reserved1: u1 = 0,
        COMP2INMSEL: bool, // bit offset: 9 desc: Comparator 1inverting input selection
        COMP2_OUT_SEL: u4, // bit offset: 10 desc: Comparator 2 output selection
        reserved2: u1 = 0,
        COMP2POL: bool, // bit offset: 15 desc: Comparator 2 output polarity
        COMP2HYST: u2, // bit offset: 16 desc: Comparator 2 hysteresis
        COMP2_BLANKING: u3, // bit offset: 18 desc: Comparator 2 blanking source
        reserved3: u10 = 0,
        COMP2LOCK: bool, // bit offset: 31 desc: Comparator 2 lock
    });
    // byte offset: 36 control and status register
    pub const COMP3_CSR = MMIO(Address + 0x00000024, u32, packed struct {
        COMP3EN: bool, // bit offset: 0 desc: Comparator 3 enable
        reserved0: u1 = 0,
        COMP3MODE: u2, // bit offset: 2 desc: Comparator 3 mode
        COMP3INSEL: u3, // bit offset: 4 desc: Comparator 3 inverting input selection
        COMP3INPSEL: bool, // bit offset: 7 desc: Comparator 3 non inverted input selection
        reserved1: u2 = 0,
        COMP3_OUT_SEL: u4, // bit offset: 10 desc: Comparator 3 output selection
        reserved2: u1 = 0,
        COMP3POL: bool, // bit offset: 15 desc: Comparator 3 output polarity
        COMP3HYST: u2, // bit offset: 16 desc: Comparator 3 hysteresis
        COMP3_BLANKING: u3, // bit offset: 18 desc: Comparator 3 blanking source
        reserved3: u9 = 0,
        COMP3OUT: bool, // bit offset: 30 desc: Comparator 3 output
        COMP3LOCK: bool, // bit offset: 31 desc: Comparator 3 lock
    });
    // byte offset: 40 control and status register
    pub const COMP4_CSR = MMIO(Address + 0x00000028, u32, packed struct {
        COMP4EN: bool, // bit offset: 0 desc: Comparator 4 enable
        reserved0: u1 = 0,
        COMP4MODE: u2, // bit offset: 2 desc: Comparator 4 mode
        COMP4INSEL: u3, // bit offset: 4 desc: Comparator 4 inverting input selection
        COMP4INPSEL: bool, // bit offset: 7 desc: Comparator 4 non inverted input selection
        reserved1: u1 = 0,
        COM4WINMODE: bool, // bit offset: 9 desc: Comparator 4 window mode
        COMP4_OUT_SEL: u4, // bit offset: 10 desc: Comparator 4 output selection
        reserved2: u1 = 0,
        COMP4POL: bool, // bit offset: 15 desc: Comparator 4 output polarity
        COMP4HYST: u2, // bit offset: 16 desc: Comparator 4 hysteresis
        COMP4_BLANKING: u3, // bit offset: 18 desc: Comparator 4 blanking source
        reserved3: u9 = 0,
        COMP4OUT: bool, // bit offset: 30 desc: Comparator 4 output
        COMP4LOCK: bool, // bit offset: 31 desc: Comparator 4 lock
    });
    // byte offset: 44 control and status register
    pub const COMP5_CSR = MMIO(Address + 0x0000002c, u32, packed struct {
        COMP5EN: bool, // bit offset: 0 desc: Comparator 5 enable
        reserved0: u1 = 0,
        COMP5MODE: u2, // bit offset: 2 desc: Comparator 5 mode
        COMP5INSEL: u3, // bit offset: 4 desc: Comparator 5 inverting input selection
        COMP5INPSEL: bool, // bit offset: 7 desc: Comparator 5 non inverted input selection
        reserved1: u2 = 0,
        COMP5_OUT_SEL: u4, // bit offset: 10 desc: Comparator 5 output selection
        reserved2: u1 = 0,
        COMP5POL: bool, // bit offset: 15 desc: Comparator 5 output polarity
        COMP5HYST: u2, // bit offset: 16 desc: Comparator 5 hysteresis
        COMP5_BLANKING: u3, // bit offset: 18 desc: Comparator 5 blanking source
        reserved3: u9 = 0,
        COMP5OUT: bool, // bit offset: 30 desc: Comparator51 output
        COMP5LOCK: bool, // bit offset: 31 desc: Comparator 5 lock
    });
    // byte offset: 48 control and status register
    pub const COMP6_CSR = MMIO(Address + 0x00000030, u32, packed struct {
        COMP6EN: bool, // bit offset: 0 desc: Comparator 6 enable
        reserved0: u1 = 0,
        COMP6MODE: u2, // bit offset: 2 desc: Comparator 6 mode
        COMP6INSEL: u3, // bit offset: 4 desc: Comparator 6 inverting input selection
        COMP6INPSEL: bool, // bit offset: 7 desc: Comparator 6 non inverted input selection
        reserved1: u1 = 0,
        COM6WINMODE: bool, // bit offset: 9 desc: Comparator 6 window mode
        COMP6_OUT_SEL: u4, // bit offset: 10 desc: Comparator 6 output selection
        reserved2: u1 = 0,
        COMP6POL: bool, // bit offset: 15 desc: Comparator 6 output polarity
        COMP6HYST: u2, // bit offset: 16 desc: Comparator 6 hysteresis
        COMP6_BLANKING: u3, // bit offset: 18 desc: Comparator 6 blanking source
        reserved3: u9 = 0,
        COMP6OUT: bool, // bit offset: 30 desc: Comparator 6 output
        COMP6LOCK: bool, // bit offset: 31 desc: Comparator 6 lock
    });
    // byte offset: 52 control and status register
    pub const COMP7_CSR = MMIO(Address + 0x00000034, u32, packed struct {
        COMP7EN: bool, // bit offset: 0 desc: Comparator 7 enable
        reserved0: u1 = 0,
        COMP7MODE: u2, // bit offset: 2 desc: Comparator 7 mode
        COMP7INSEL: u3, // bit offset: 4 desc: Comparator 7 inverting input selection
        COMP7INPSEL: bool, // bit offset: 7 desc: Comparator 7 non inverted input selection
        reserved1: u2 = 0,
        COMP7_OUT_SEL: u4, // bit offset: 10 desc: Comparator 7 output selection
        reserved2: u1 = 0,
        COMP7POL: bool, // bit offset: 15 desc: Comparator 7 output polarity
        COMP7HYST: u2, // bit offset: 16 desc: Comparator 7 hysteresis
        COMP7_BLANKING: u3, // bit offset: 18 desc: Comparator 7 blanking source
        reserved3: u9 = 0,
        COMP7OUT: bool, // bit offset: 30 desc: Comparator 7 output
        COMP7LOCK: bool, // bit offset: 31 desc: Comparator 7 lock
    });
    // byte offset: 56 control register
    pub const OPAMP1_CSR = MMIO(Address + 0x00000038, u32, packed struct {
        OPAMP1_EN: bool, // bit offset: 0 desc: OPAMP1 enable
        FORCE_VP: bool, // bit offset: 1 desc: FORCE_VP
        VP_SEL: u2, // bit offset: 2 desc: OPAMP1 Non inverting input selection
        reserved0: u1 = 0,
        VM_SEL: u2, // bit offset: 5 desc: OPAMP1 inverting input selection
        TCM_EN: bool, // bit offset: 7 desc: Timer controlled Mux mode enable
        VMS_SEL: bool, // bit offset: 8 desc: OPAMP1 inverting input secondary selection
        VPS_SEL: u2, // bit offset: 9 desc: OPAMP1 Non inverting input secondary selection
        CALON: bool, // bit offset: 11 desc: Calibration mode enable
        CALSEL: u2, // bit offset: 12 desc: Calibration selection
        PGA_GAIN: u4, // bit offset: 14 desc: Gain in PGA mode
        USER_TRIM: bool, // bit offset: 18 desc: User trimming enable
        TRIMOFFSETP: u5, // bit offset: 19 desc: Offset trimming value (PMOS)
        TRIMOFFSETN: u5, // bit offset: 24 desc: Offset trimming value (NMOS)
        TSTREF: bool, // bit offset: 29 desc: TSTREF
        OUTCAL: bool, // bit offset: 30 desc: OPAMP 1 ouput status flag
        LOCK: bool, // bit offset: 31 desc: OPAMP 1 lock
    });
    // byte offset: 60 control register
    pub const OPAMP2_CSR = MMIO(Address + 0x0000003c, u32, packed struct {
        OPAMP2EN: bool, // bit offset: 0 desc: OPAMP2 enable
        FORCE_VP: bool, // bit offset: 1 desc: FORCE_VP
        VP_SEL: u2, // bit offset: 2 desc: OPAMP2 Non inverting input selection
        reserved0: u1 = 0,
        VM_SEL: u2, // bit offset: 5 desc: OPAMP2 inverting input selection
        TCM_EN: bool, // bit offset: 7 desc: Timer controlled Mux mode enable
        VMS_SEL: bool, // bit offset: 8 desc: OPAMP2 inverting input secondary selection
        VPS_SEL: u2, // bit offset: 9 desc: OPAMP2 Non inverting input secondary selection
        CALON: bool, // bit offset: 11 desc: Calibration mode enable
        CAL_SEL: u2, // bit offset: 12 desc: Calibration selection
        PGA_GAIN: u4, // bit offset: 14 desc: Gain in PGA mode
        USER_TRIM: bool, // bit offset: 18 desc: User trimming enable
        TRIMOFFSETP: u5, // bit offset: 19 desc: Offset trimming value (PMOS)
        TRIMOFFSETN: u5, // bit offset: 24 desc: Offset trimming value (NMOS)
        TSTREF: bool, // bit offset: 29 desc: TSTREF
        OUTCAL: bool, // bit offset: 30 desc: OPAMP 2 ouput status flag
        LOCK: bool, // bit offset: 31 desc: OPAMP 2 lock
    });
    // byte offset: 64 control register
    pub const OPAMP3_CSR = MMIO(Address + 0x00000040, u32, packed struct {
        OPAMP3EN: bool, // bit offset: 0 desc: OPAMP3 enable
        FORCE_VP: bool, // bit offset: 1 desc: FORCE_VP
        VP_SEL: u2, // bit offset: 2 desc: OPAMP3 Non inverting input selection
        reserved0: u1 = 0,
        VM_SEL: u2, // bit offset: 5 desc: OPAMP3 inverting input selection
        TCM_EN: bool, // bit offset: 7 desc: Timer controlled Mux mode enable
        VMS_SEL: bool, // bit offset: 8 desc: OPAMP3 inverting input secondary selection
        VPS_SEL: u2, // bit offset: 9 desc: OPAMP3 Non inverting input secondary selection
        CALON: bool, // bit offset: 11 desc: Calibration mode enable
        CALSEL: u2, // bit offset: 12 desc: Calibration selection
        PGA_GAIN: u4, // bit offset: 14 desc: Gain in PGA mode
        USER_TRIM: bool, // bit offset: 18 desc: User trimming enable
        TRIMOFFSETP: u5, // bit offset: 19 desc: Offset trimming value (PMOS)
        TRIMOFFSETN: u5, // bit offset: 24 desc: Offset trimming value (NMOS)
        TSTREF: bool, // bit offset: 29 desc: TSTREF
        OUTCAL: bool, // bit offset: 30 desc: OPAMP 3 ouput status flag
        LOCK: bool, // bit offset: 31 desc: OPAMP 3 lock
    });
    // byte offset: 68 control register
    pub const OPAMP4_CSR = MMIO(Address + 0x00000044, u32, packed struct {
        OPAMP4EN: bool, // bit offset: 0 desc: OPAMP4 enable
        FORCE_VP: bool, // bit offset: 1 desc: FORCE_VP
        VP_SEL: u2, // bit offset: 2 desc: OPAMP4 Non inverting input selection
        reserved0: u1 = 0,
        VM_SEL: u2, // bit offset: 5 desc: OPAMP4 inverting input selection
        TCM_EN: bool, // bit offset: 7 desc: Timer controlled Mux mode enable
        VMS_SEL: bool, // bit offset: 8 desc: OPAMP4 inverting input secondary selection
        VPS_SEL: u2, // bit offset: 9 desc: OPAMP4 Non inverting input secondary selection
        CALON: bool, // bit offset: 11 desc: Calibration mode enable
        CALSEL: u2, // bit offset: 12 desc: Calibration selection
        PGA_GAIN: u4, // bit offset: 14 desc: Gain in PGA mode
        USER_TRIM: bool, // bit offset: 18 desc: User trimming enable
        TRIMOFFSETP: u5, // bit offset: 19 desc: Offset trimming value (PMOS)
        TRIMOFFSETN: u5, // bit offset: 24 desc: Offset trimming value (NMOS)
        TSTREF: bool, // bit offset: 29 desc: TSTREF
        OUTCAL: bool, // bit offset: 30 desc: OPAMP 4 ouput status flag
        LOCK: bool, // bit offset: 31 desc: OPAMP 4 lock
    });
};
pub const FMC = extern struct {
    pub const Address: u32 = 0xa0000400;
    // byte offset: 0 SRAM/NOR-Flash chip-select control register 1
    pub const BCR1 = MMIO(Address + 0x00000000, u32, packed struct {
        MBKEN: bool, // bit offset: 0 desc: MBKEN
        MUXEN: bool, // bit offset: 1 desc: MUXEN
        MTYP: u2, // bit offset: 2 desc: MTYP
        MWID: u2, // bit offset: 4 desc: MWID
        FACCEN: bool, // bit offset: 6 desc: FACCEN
        reserved0: u1 = 0,
        BURSTEN: bool, // bit offset: 8 desc: BURSTEN
        WAITPOL: bool, // bit offset: 9 desc: WAITPOL
        reserved1: u1 = 0,
        WAITCFG: bool, // bit offset: 11 desc: WAITCFG
        WREN: bool, // bit offset: 12 desc: WREN
        WAITEN: bool, // bit offset: 13 desc: WAITEN
        EXTMOD: bool, // bit offset: 14 desc: EXTMOD
        ASYNCWAIT: bool, // bit offset: 15 desc: ASYNCWAIT
        reserved2: u3 = 0,
        CBURSTRW: bool, // bit offset: 19 desc: CBURSTRW
        CCLKEN: bool, // bit offset: 20 desc: CCLKEN
        padding: u11 = 0,
    });
    // byte offset: 4 SRAM/NOR-Flash chip-select timing register 1
    pub const BTR1 = MMIO(Address + 0x00000004, u32, packed struct {
        ADDSET: u4, // bit offset: 0 desc: ADDSET
        ADDHLD: u4, // bit offset: 4 desc: ADDHLD
        DATAST: u8, // bit offset: 8 desc: DATAST
        BUSTURN: u4, // bit offset: 16 desc: BUSTURN
        CLKDIV: u4, // bit offset: 20 desc: CLKDIV
        DATLAT: u4, // bit offset: 24 desc: DATLAT
        ACCMOD: u2, // bit offset: 28 desc: ACCMOD
        padding: u2 = 0,
    });
    // byte offset: 8 SRAM/NOR-Flash chip-select control register 2
    pub const BCR2 = MMIO(Address + 0x00000008, u32, packed struct {
        MBKEN: bool, // bit offset: 0 desc: MBKEN
        MUXEN: bool, // bit offset: 1 desc: MUXEN
        MTYP: u2, // bit offset: 2 desc: MTYP
        MWID: u2, // bit offset: 4 desc: MWID
        FACCEN: bool, // bit offset: 6 desc: FACCEN
        reserved0: u1 = 0,
        BURSTEN: bool, // bit offset: 8 desc: BURSTEN
        WAITPOL: bool, // bit offset: 9 desc: WAITPOL
        WRAPMOD: bool, // bit offset: 10 desc: WRAPMOD
        WAITCFG: bool, // bit offset: 11 desc: WAITCFG
        WREN: bool, // bit offset: 12 desc: WREN
        WAITEN: bool, // bit offset: 13 desc: WAITEN
        EXTMOD: bool, // bit offset: 14 desc: EXTMOD
        ASYNCWAIT: bool, // bit offset: 15 desc: ASYNCWAIT
        reserved1: u3 = 0,
        CBURSTRW: bool, // bit offset: 19 desc: CBURSTRW
        padding: u12 = 0,
    });
    // byte offset: 12 SRAM/NOR-Flash chip-select timing register 2
    pub const BTR2 = MMIO(Address + 0x0000000c, u32, packed struct {
        ADDSET: u4, // bit offset: 0 desc: ADDSET
        ADDHLD: u4, // bit offset: 4 desc: ADDHLD
        DATAST: u8, // bit offset: 8 desc: DATAST
        BUSTURN: u4, // bit offset: 16 desc: BUSTURN
        CLKDIV: u4, // bit offset: 20 desc: CLKDIV
        DATLAT: u4, // bit offset: 24 desc: DATLAT
        ACCMOD: u2, // bit offset: 28 desc: ACCMOD
        padding: u2 = 0,
    });
    // byte offset: 16 SRAM/NOR-Flash chip-select control register 3
    pub const BCR3 = MMIO(Address + 0x00000010, u32, packed struct {
        MBKEN: bool, // bit offset: 0 desc: MBKEN
        MUXEN: bool, // bit offset: 1 desc: MUXEN
        MTYP: u2, // bit offset: 2 desc: MTYP
        MWID: u2, // bit offset: 4 desc: MWID
        FACCEN: bool, // bit offset: 6 desc: FACCEN
        reserved0: u1 = 0,
        BURSTEN: bool, // bit offset: 8 desc: BURSTEN
        WAITPOL: bool, // bit offset: 9 desc: WAITPOL
        WRAPMOD: bool, // bit offset: 10 desc: WRAPMOD
        WAITCFG: bool, // bit offset: 11 desc: WAITCFG
        WREN: bool, // bit offset: 12 desc: WREN
        WAITEN: bool, // bit offset: 13 desc: WAITEN
        EXTMOD: bool, // bit offset: 14 desc: EXTMOD
        ASYNCWAIT: bool, // bit offset: 15 desc: ASYNCWAIT
        reserved1: u3 = 0,
        CBURSTRW: bool, // bit offset: 19 desc: CBURSTRW
        padding: u12 = 0,
    });
    // byte offset: 20 SRAM/NOR-Flash chip-select timing register 3
    pub const BTR3 = MMIO(Address + 0x00000014, u32, packed struct {
        ADDSET: u4, // bit offset: 0 desc: ADDSET
        ADDHLD: u4, // bit offset: 4 desc: ADDHLD
        DATAST: u8, // bit offset: 8 desc: DATAST
        BUSTURN: u4, // bit offset: 16 desc: BUSTURN
        CLKDIV: u4, // bit offset: 20 desc: CLKDIV
        DATLAT: u4, // bit offset: 24 desc: DATLAT
        ACCMOD: u2, // bit offset: 28 desc: ACCMOD
        padding: u2 = 0,
    });
    // byte offset: 24 SRAM/NOR-Flash chip-select control register 4
    pub const BCR4 = MMIO(Address + 0x00000018, u32, packed struct {
        MBKEN: bool, // bit offset: 0 desc: MBKEN
        MUXEN: bool, // bit offset: 1 desc: MUXEN
        MTYP: u2, // bit offset: 2 desc: MTYP
        MWID: u2, // bit offset: 4 desc: MWID
        FACCEN: bool, // bit offset: 6 desc: FACCEN
        reserved0: u1 = 0,
        BURSTEN: bool, // bit offset: 8 desc: BURSTEN
        WAITPOL: bool, // bit offset: 9 desc: WAITPOL
        WRAPMOD: bool, // bit offset: 10 desc: WRAPMOD
        WAITCFG: bool, // bit offset: 11 desc: WAITCFG
        WREN: bool, // bit offset: 12 desc: WREN
        WAITEN: bool, // bit offset: 13 desc: WAITEN
        EXTMOD: bool, // bit offset: 14 desc: EXTMOD
        ASYNCWAIT: bool, // bit offset: 15 desc: ASYNCWAIT
        reserved1: u3 = 0,
        CBURSTRW: bool, // bit offset: 19 desc: CBURSTRW
        padding: u12 = 0,
    });
    // byte offset: 28 SRAM/NOR-Flash chip-select timing register 4
    pub const BTR4 = MMIO(Address + 0x0000001c, u32, packed struct {
        ADDSET: u4, // bit offset: 0 desc: ADDSET
        ADDHLD: u4, // bit offset: 4 desc: ADDHLD
        DATAST: u8, // bit offset: 8 desc: DATAST
        BUSTURN: u4, // bit offset: 16 desc: BUSTURN
        CLKDIV: u4, // bit offset: 20 desc: CLKDIV
        DATLAT: u4, // bit offset: 24 desc: DATLAT
        ACCMOD: u2, // bit offset: 28 desc: ACCMOD
        padding: u2 = 0,
    });
    // byte offset: 96 PC Card/NAND Flash control register 2
    pub const PCR2 = MMIO(Address + 0x00000060, u32, packed struct {
        reserved0: u1 = 0,
        PWAITEN: bool, // bit offset: 1 desc: PWAITEN
        PBKEN: bool, // bit offset: 2 desc: PBKEN
        PTYP: bool, // bit offset: 3 desc: PTYP
        PWID: u2, // bit offset: 4 desc: PWID
        ECCEN: bool, // bit offset: 6 desc: ECCEN
        reserved1: u2 = 0,
        TCLR: u4, // bit offset: 9 desc: TCLR
        TAR: u4, // bit offset: 13 desc: TAR
        ECCPS: u3, // bit offset: 17 desc: ECCPS
        padding: u12 = 0,
    });
    // byte offset: 100 FIFO status and interrupt register 2
    pub const SR2 = MMIO(Address + 0x00000064, u32, packed struct {
        IRS: bool, // bit offset: 0 desc: IRS
        ILS: bool, // bit offset: 1 desc: ILS
        IFS: bool, // bit offset: 2 desc: IFS
        IREN: bool, // bit offset: 3 desc: IREN
        ILEN: bool, // bit offset: 4 desc: ILEN
        IFEN: bool, // bit offset: 5 desc: IFEN
        FEMPT: bool, // bit offset: 6 desc: FEMPT
        padding: u25 = 0,
    });
    // byte offset: 104 Common memory space timing register 2
    pub const PMEM2 = MMIO(Address + 0x00000068, u32, packed struct {
        MEMSETx: u8, // bit offset: 0 desc: MEMSETx
        MEMWAITx: u8, // bit offset: 8 desc: MEMWAITx
        MEMHOLDx: u8, // bit offset: 16 desc: MEMHOLDx
        MEMHIZx: u8, // bit offset: 24 desc: MEMHIZx
    });
    // byte offset: 108 Attribute memory space timing register 2
    pub const PATT2 = MMIO(Address + 0x0000006c, u32, packed struct {
        ATTSETx: u8, // bit offset: 0 desc: ATTSETx
        ATTWAITx: u8, // bit offset: 8 desc: ATTWAITx
        ATTHOLDx: u8, // bit offset: 16 desc: ATTHOLDx
        ATTHIZx: u8, // bit offset: 24 desc: ATTHIZx
    });
    // byte offset: 116 ECC result register 2
    pub const ECCR2 = MMIO(Address + 0x00000074, u32, packed struct {
        ECCx: u32, // bit offset: 0 desc: ECCx
    });
    // byte offset: 128 PC Card/NAND Flash control register 3
    pub const PCR3 = MMIO(Address + 0x00000080, u32, packed struct {
        reserved0: u1 = 0,
        PWAITEN: bool, // bit offset: 1 desc: PWAITEN
        PBKEN: bool, // bit offset: 2 desc: PBKEN
        PTYP: bool, // bit offset: 3 desc: PTYP
        PWID: u2, // bit offset: 4 desc: PWID
        ECCEN: bool, // bit offset: 6 desc: ECCEN
        reserved1: u2 = 0,
        TCLR: u4, // bit offset: 9 desc: TCLR
        TAR: u4, // bit offset: 13 desc: TAR
        ECCPS: u3, // bit offset: 17 desc: ECCPS
        padding: u12 = 0,
    });
    // byte offset: 132 FIFO status and interrupt register 3
    pub const SR3 = MMIO(Address + 0x00000084, u32, packed struct {
        IRS: bool, // bit offset: 0 desc: IRS
        ILS: bool, // bit offset: 1 desc: ILS
        IFS: bool, // bit offset: 2 desc: IFS
        IREN: bool, // bit offset: 3 desc: IREN
        ILEN: bool, // bit offset: 4 desc: ILEN
        IFEN: bool, // bit offset: 5 desc: IFEN
        FEMPT: bool, // bit offset: 6 desc: FEMPT
        padding: u25 = 0,
    });
    // byte offset: 136 Common memory space timing register 3
    pub const PMEM3 = MMIO(Address + 0x00000088, u32, packed struct {
        MEMSETx: u8, // bit offset: 0 desc: MEMSETx
        MEMWAITx: u8, // bit offset: 8 desc: MEMWAITx
        MEMHOLDx: u8, // bit offset: 16 desc: MEMHOLDx
        MEMHIZx: u8, // bit offset: 24 desc: MEMHIZx
    });
    // byte offset: 140 Attribute memory space timing register 3
    pub const PATT3 = MMIO(Address + 0x0000008c, u32, packed struct {
        ATTSETx: u8, // bit offset: 0 desc: ATTSETx
        ATTWAITx: u8, // bit offset: 8 desc: ATTWAITx
        ATTHOLDx: u8, // bit offset: 16 desc: ATTHOLDx
        ATTHIZx: u8, // bit offset: 24 desc: ATTHIZx
    });
    // byte offset: 148 ECC result register 3
    pub const ECCR3 = MMIO(Address + 0x00000094, u32, packed struct {
        ECCx: u32, // bit offset: 0 desc: ECCx
    });
    // byte offset: 160 PC Card/NAND Flash control register 4
    pub const PCR4 = MMIO(Address + 0x000000a0, u32, packed struct {
        reserved0: u1 = 0,
        PWAITEN: bool, // bit offset: 1 desc: PWAITEN
        PBKEN: bool, // bit offset: 2 desc: PBKEN
        PTYP: bool, // bit offset: 3 desc: PTYP
        PWID: u2, // bit offset: 4 desc: PWID
        ECCEN: bool, // bit offset: 6 desc: ECCEN
        reserved1: u2 = 0,
        TCLR: u4, // bit offset: 9 desc: TCLR
        TAR: u4, // bit offset: 13 desc: TAR
        ECCPS: u3, // bit offset: 17 desc: ECCPS
        padding: u12 = 0,
    });
    // byte offset: 164 FIFO status and interrupt register 4
    pub const SR4 = MMIO(Address + 0x000000a4, u32, packed struct {
        IRS: bool, // bit offset: 0 desc: IRS
        ILS: bool, // bit offset: 1 desc: ILS
        IFS: bool, // bit offset: 2 desc: IFS
        IREN: bool, // bit offset: 3 desc: IREN
        ILEN: bool, // bit offset: 4 desc: ILEN
        IFEN: bool, // bit offset: 5 desc: IFEN
        FEMPT: bool, // bit offset: 6 desc: FEMPT
        padding: u25 = 0,
    });
    // byte offset: 168 Common memory space timing register 4
    pub const PMEM4 = MMIO(Address + 0x000000a8, u32, packed struct {
        MEMSETx: u8, // bit offset: 0 desc: MEMSETx
        MEMWAITx: u8, // bit offset: 8 desc: MEMWAITx
        MEMHOLDx: u8, // bit offset: 16 desc: MEMHOLDx
        MEMHIZx: u8, // bit offset: 24 desc: MEMHIZx
    });
    // byte offset: 172 Attribute memory space timing register 4
    pub const PATT4 = MMIO(Address + 0x000000ac, u32, packed struct {
        ATTSETx: u8, // bit offset: 0 desc: ATTSETx
        ATTWAITx: u8, // bit offset: 8 desc: ATTWAITx
        ATTHOLDx: u8, // bit offset: 16 desc: ATTHOLDx
        ATTHIZx: u8, // bit offset: 24 desc: ATTHIZx
    });
    // byte offset: 176 I/O space timing register 4
    pub const PIO4 = MMIO(Address + 0x000000b0, u32, packed struct {
        IOSETx: u8, // bit offset: 0 desc: IOSETx
        IOWAITx: u8, // bit offset: 8 desc: IOWAITx
        IOHOLDx: u8, // bit offset: 16 desc: IOHOLDx
        IOHIZx: u8, // bit offset: 24 desc: IOHIZx
    });
    // byte offset: 260 SRAM/NOR-Flash write timing registers 1
    pub const BWTR1 = MMIO(Address + 0x00000104, u32, packed struct {
        ADDSET: u4, // bit offset: 0 desc: ADDSET
        ADDHLD: u4, // bit offset: 4 desc: ADDHLD
        DATAST: u8, // bit offset: 8 desc: DATAST
        BUSTURN: u4, // bit offset: 16 desc: Bus turnaround phase duration
        CLKDIV: u4, // bit offset: 20 desc: CLKDIV
        DATLAT: u4, // bit offset: 24 desc: DATLAT
        ACCMOD: u2, // bit offset: 28 desc: ACCMOD
        padding: u2 = 0,
    });
    // byte offset: 268 SRAM/NOR-Flash write timing registers 2
    pub const BWTR2 = MMIO(Address + 0x0000010c, u32, packed struct {
        ADDSET: u4, // bit offset: 0 desc: ADDSET
        ADDHLD: u4, // bit offset: 4 desc: ADDHLD
        DATAST: u8, // bit offset: 8 desc: DATAST
        BUSTURN: u4, // bit offset: 16 desc: Bus turnaround phase duration
        CLKDIV: u4, // bit offset: 20 desc: CLKDIV
        DATLAT: u4, // bit offset: 24 desc: DATLAT
        ACCMOD: u2, // bit offset: 28 desc: ACCMOD
        padding: u2 = 0,
    });
    // byte offset: 276 SRAM/NOR-Flash write timing registers 3
    pub const BWTR3 = MMIO(Address + 0x00000114, u32, packed struct {
        ADDSET: u4, // bit offset: 0 desc: ADDSET
        ADDHLD: u4, // bit offset: 4 desc: ADDHLD
        DATAST: u8, // bit offset: 8 desc: DATAST
        BUSTURN: u4, // bit offset: 16 desc: Bus turnaround phase duration
        CLKDIV: u4, // bit offset: 20 desc: CLKDIV
        DATLAT: u4, // bit offset: 24 desc: DATLAT
        ACCMOD: u2, // bit offset: 28 desc: ACCMOD
        padding: u2 = 0,
    });
    // byte offset: 284 SRAM/NOR-Flash write timing registers 4
    pub const BWTR4 = MMIO(Address + 0x0000011c, u32, packed struct {
        ADDSET: u4, // bit offset: 0 desc: ADDSET
        ADDHLD: u4, // bit offset: 4 desc: ADDHLD
        DATAST: u8, // bit offset: 8 desc: DATAST
        BUSTURN: u4, // bit offset: 16 desc: Bus turnaround phase duration
        CLKDIV: u4, // bit offset: 20 desc: CLKDIV
        DATLAT: u4, // bit offset: 24 desc: DATLAT
        ACCMOD: u2, // bit offset: 28 desc: ACCMOD
        padding: u2 = 0,
    });
};
pub const NVIC = extern struct {
    pub const Address: u32 = 0xe000e100;
    // byte offset: 0 Interrupt Set-Enable Register
    pub const ISER0 = MMIO(Address + 0x00000000, u32, packed struct {
        SETENA: u32, // bit offset: 0 desc: SETENA
    });
    // byte offset: 4 Interrupt Set-Enable Register
    pub const ISER1 = MMIO(Address + 0x00000004, u32, packed struct {
        SETENA: u32, // bit offset: 0 desc: SETENA
    });
    // byte offset: 8 Interrupt Set-Enable Register
    pub const ISER2 = MMIO(Address + 0x00000008, u32, packed struct {
        SETENA: u32, // bit offset: 0 desc: SETENA
    });
    // byte offset: 128 Interrupt Clear-Enable Register
    pub const ICER0 = MMIO(Address + 0x00000080, u32, packed struct {
        CLRENA: u32, // bit offset: 0 desc: CLRENA
    });
    // byte offset: 132 Interrupt Clear-Enable Register
    pub const ICER1 = MMIO(Address + 0x00000084, u32, packed struct {
        CLRENA: u32, // bit offset: 0 desc: CLRENA
    });
    // byte offset: 136 Interrupt Clear-Enable Register
    pub const ICER2 = MMIO(Address + 0x00000088, u32, packed struct {
        CLRENA: u32, // bit offset: 0 desc: CLRENA
    });
    // byte offset: 256 Interrupt Set-Pending Register
    pub const ISPR0 = MMIO(Address + 0x00000100, u32, packed struct {
        SETPEND: u32, // bit offset: 0 desc: SETPEND
    });
    // byte offset: 260 Interrupt Set-Pending Register
    pub const ISPR1 = MMIO(Address + 0x00000104, u32, packed struct {
        SETPEND: u32, // bit offset: 0 desc: SETPEND
    });
    // byte offset: 264 Interrupt Set-Pending Register
    pub const ISPR2 = MMIO(Address + 0x00000108, u32, packed struct {
        SETPEND: u32, // bit offset: 0 desc: SETPEND
    });
    // byte offset: 384 Interrupt Clear-Pending Register
    pub const ICPR0 = MMIO(Address + 0x00000180, u32, packed struct {
        CLRPEND: u32, // bit offset: 0 desc: CLRPEND
    });
    // byte offset: 388 Interrupt Clear-Pending Register
    pub const ICPR1 = MMIO(Address + 0x00000184, u32, packed struct {
        CLRPEND: u32, // bit offset: 0 desc: CLRPEND
    });
    // byte offset: 392 Interrupt Clear-Pending Register
    pub const ICPR2 = MMIO(Address + 0x00000188, u32, packed struct {
        CLRPEND: u32, // bit offset: 0 desc: CLRPEND
    });
    // byte offset: 512 Interrupt Active Bit Register
    pub const IABR0 = MMIO(Address + 0x00000200, u32, packed struct {
        ACTIVE: u32, // bit offset: 0 desc: ACTIVE
    });
    // byte offset: 516 Interrupt Active Bit Register
    pub const IABR1 = MMIO(Address + 0x00000204, u32, packed struct {
        ACTIVE: u32, // bit offset: 0 desc: ACTIVE
    });
    // byte offset: 520 Interrupt Active Bit Register
    pub const IABR2 = MMIO(Address + 0x00000208, u32, packed struct {
        ACTIVE: u32, // bit offset: 0 desc: ACTIVE
    });
    // byte offset: 768 Interrupt Priority Register
    pub const IPR0 = MMIO(Address + 0x00000300, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 772 Interrupt Priority Register
    pub const IPR1 = MMIO(Address + 0x00000304, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 776 Interrupt Priority Register
    pub const IPR2 = MMIO(Address + 0x00000308, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 780 Interrupt Priority Register
    pub const IPR3 = MMIO(Address + 0x0000030c, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 784 Interrupt Priority Register
    pub const IPR4 = MMIO(Address + 0x00000310, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 788 Interrupt Priority Register
    pub const IPR5 = MMIO(Address + 0x00000314, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 792 Interrupt Priority Register
    pub const IPR6 = MMIO(Address + 0x00000318, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 796 Interrupt Priority Register
    pub const IPR7 = MMIO(Address + 0x0000031c, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 800 Interrupt Priority Register
    pub const IPR8 = MMIO(Address + 0x00000320, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 804 Interrupt Priority Register
    pub const IPR9 = MMIO(Address + 0x00000324, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 808 Interrupt Priority Register
    pub const IPR10 = MMIO(Address + 0x00000328, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 812 Interrupt Priority Register
    pub const IPR11 = MMIO(Address + 0x0000032c, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 816 Interrupt Priority Register
    pub const IPR12 = MMIO(Address + 0x00000330, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 820 Interrupt Priority Register
    pub const IPR13 = MMIO(Address + 0x00000334, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 824 Interrupt Priority Register
    pub const IPR14 = MMIO(Address + 0x00000338, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 828 Interrupt Priority Register
    pub const IPR15 = MMIO(Address + 0x0000033c, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 832 Interrupt Priority Register
    pub const IPR16 = MMIO(Address + 0x00000340, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 836 Interrupt Priority Register
    pub const IPR17 = MMIO(Address + 0x00000344, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 840 Interrupt Priority Register
    pub const IPR18 = MMIO(Address + 0x00000348, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 844 Interrupt Priority Register
    pub const IPR19 = MMIO(Address + 0x0000034c, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
    // byte offset: 848 Interrupt Priority Register
    pub const IPR20 = MMIO(Address + 0x00000350, u32, packed struct {
        IPR_N0: u8, // bit offset: 0 desc: IPR_N0
        IPR_N1: u8, // bit offset: 8 desc: IPR_N1
        IPR_N2: u8, // bit offset: 16 desc: IPR_N2
        IPR_N3: u8, // bit offset: 24 desc: IPR_N3
    });
};
pub const FPU = extern struct {
    pub const Address: u32 = 0xe000ef34;
    // byte offset: 0 Floating-point context control register
    pub const FPCCR = MMIO(Address + 0x00000000, u32, packed struct {
        LSPACT: bool, // bit offset: 0 desc: LSPACT
        USER: bool, // bit offset: 1 desc: USER
        reserved0: u1 = 0,
        THREAD: bool, // bit offset: 3 desc: THREAD
        HFRDY: bool, // bit offset: 4 desc: HFRDY
        MMRDY: bool, // bit offset: 5 desc: MMRDY
        BFRDY: bool, // bit offset: 6 desc: BFRDY
        reserved1: u1 = 0,
        MONRDY: bool, // bit offset: 8 desc: MONRDY
        reserved2: u21 = 0,
        LSPEN: bool, // bit offset: 30 desc: LSPEN
        ASPEN: bool, // bit offset: 31 desc: ASPEN
    });
    // byte offset: 4 Floating-point context address register
    pub const FPCAR = MMIO(Address + 0x00000004, u32, packed struct {
        reserved0: u3 = 0,
        ADDRESS: u29, // bit offset: 3 desc: Location of unpopulated floating-point
    });
    // byte offset: 8 Floating-point status control register
    pub const FPSCR = MMIO(Address + 0x00000008, u32, packed struct {
        IOC: bool, // bit offset: 0 desc: Invalid operation cumulative exception bit
        DZC: bool, // bit offset: 1 desc: Division by zero cumulative exception bit.
        OFC: bool, // bit offset: 2 desc: Overflow cumulative exception bit
        UFC: bool, // bit offset: 3 desc: Underflow cumulative exception bit
        IXC: bool, // bit offset: 4 desc: Inexact cumulative exception bit
        reserved0: u2 = 0,
        IDC: bool, // bit offset: 7 desc: Input denormal cumulative exception bit.
        reserved1: u14 = 0,
        RMode: u2, // bit offset: 22 desc: Rounding Mode control field
        FZ: bool, // bit offset: 24 desc: Flush-to-zero mode control bit:
        DN: bool, // bit offset: 25 desc: Default NaN mode control bit
        AHP: bool, // bit offset: 26 desc: Alternative half-precision control bit
        reserved2: u1 = 0,
        V: bool, // bit offset: 28 desc: Overflow condition code flag
        C: bool, // bit offset: 29 desc: Carry condition code flag
        Z: bool, // bit offset: 30 desc: Zero condition code flag
        N: bool, // bit offset: 31 desc: Negative condition code flag
    });
};
pub const MPU = extern struct {
    pub const Address: u32 = 0xe000ed90;
    // byte offset: 0 MPU type register
    pub const MPU_TYPER = MMIO(Address + 0x00000000, u32, packed struct {
        SEPARATE: bool, // bit offset: 0 desc: Separate flag
        reserved0: u7 = 0,
        DREGION: u8, // bit offset: 8 desc: Number of MPU data regions
        IREGION: u8, // bit offset: 16 desc: Number of MPU instruction regions
        padding: u8 = 0,
    });
    // byte offset: 4 MPU control register
    pub const MPU_CTRL = MMIO(Address + 0x00000004, u32, packed struct {
        ENABLE: bool, // bit offset: 0 desc: Enables the MPU
        HFNMIENA: bool, // bit offset: 1 desc: Enables the operation of MPU during hard fault
        PRIVDEFENA: bool, // bit offset: 2 desc: Enable priviliged software access to default memory map
        padding: u29 = 0,
    });
    // byte offset: 8 MPU region number register
    pub const MPU_RNR = MMIO(Address + 0x00000008, u32, packed struct {
        REGION: u8, // bit offset: 0 desc: MPU region
        padding: u24 = 0,
    });
    // byte offset: 12 MPU region base address register
    pub const MPU_RBAR = MMIO(Address + 0x0000000c, u32, packed struct {
        REGION: u4, // bit offset: 0 desc: MPU region field
        VALID: bool, // bit offset: 4 desc: MPU region number valid
        ADDR: u27, // bit offset: 5 desc: Region base address field
    });
    // byte offset: 16 MPU region attribute and size register
    pub const MPU_RASR = MMIO(Address + 0x00000010, u32, packed struct {
        ENABLE: bool, // bit offset: 0 desc: Region enable bit.
        SIZE: u5, // bit offset: 1 desc: Size of the MPU protection region
        reserved0: u2 = 0,
        SRD: u8, // bit offset: 8 desc: Subregion disable bits
        B: bool, // bit offset: 16 desc: memory attribute
        C: bool, // bit offset: 17 desc: memory attribute
        S: bool, // bit offset: 18 desc: Shareable memory attribute
        TEX: u3, // bit offset: 19 desc: memory attribute
        reserved1: u2 = 0,
        AP: u3, // bit offset: 24 desc: Access permission
        reserved2: u1 = 0,
        XN: bool, // bit offset: 28 desc: Instruction access disable bit
        padding: u3 = 0,
    });
};
pub const STK = extern struct {
    pub const Address: u32 = 0xe000e010;
    // byte offset: 0 SysTick control and status register
    pub const CTRL = MMIO(Address + 0x00000000, u32, packed struct {
        ENABLE: bool, // bit offset: 0 desc: Counter enable
        TICKINT: bool, // bit offset: 1 desc: SysTick exception request enable
        CLKSOURCE: bool, // bit offset: 2 desc: Clock source selection
        reserved0: u13 = 0,
        COUNTFLAG: bool, // bit offset: 16 desc: COUNTFLAG
        padding: u15 = 0,
    });
    // byte offset: 4 SysTick reload value register
    pub const LOAD = MMIO(Address + 0x00000004, u32, packed struct {
        RELOAD: u24, // bit offset: 0 desc: RELOAD value
        padding: u8 = 0,
    });
    // byte offset: 8 SysTick current value register
    pub const VAL = MMIO(Address + 0x00000008, u32, packed struct {
        CURRENT: u24, // bit offset: 0 desc: Current counter value
        padding: u8 = 0,
    });
    // byte offset: 12 SysTick calibration value register
    pub const CALIB = MMIO(Address + 0x0000000c, u32, packed struct {
        TENMS: u24, // bit offset: 0 desc: Calibration value
        reserved0: u6 = 0,
        SKEW: bool, // bit offset: 30 desc: SKEW flag: Indicates whether the TENMS value is exact
        NOREF: bool, // bit offset: 31 desc: NOREF flag. Reads as zero
    });
};
pub const SCB = extern struct {
    pub const Address: u32 = 0xe000ed00;
    // byte offset: 0 CPUID base register
    pub const CPUID = MMIO(Address + 0x00000000, u32, packed struct {
        Revision: u4, // bit offset: 0 desc: Revision number
        PartNo: u12, // bit offset: 4 desc: Part number of the processor
        Constant: u4, // bit offset: 16 desc: Reads as 0xF
        Variant: u4, // bit offset: 20 desc: Variant number
        Implementer: u8, // bit offset: 24 desc: Implementer code
    });
    // byte offset: 4 Interrupt control and state register
    pub const ICSR = MMIO(Address + 0x00000004, u32, packed struct {
        VECTACTIVE: u9, // bit offset: 0 desc: Active vector
        reserved0: u2 = 0,
        RETTOBASE: bool, // bit offset: 11 desc: Return to base level
        VECTPENDING: u7, // bit offset: 12 desc: Pending vector
        reserved1: u3 = 0,
        ISRPENDING: bool, // bit offset: 22 desc: Interrupt pending flag
        reserved2: u2 = 0,
        PENDSTCLR: bool, // bit offset: 25 desc: SysTick exception clear-pending bit
        PENDSTSET: bool, // bit offset: 26 desc: SysTick exception set-pending bit
        PENDSVCLR: bool, // bit offset: 27 desc: PendSV clear-pending bit
        PENDSVSET: bool, // bit offset: 28 desc: PendSV set-pending bit
        reserved3: u2 = 0,
        NMIPENDSET: bool, // bit offset: 31 desc: NMI set-pending bit.
    });
    // byte offset: 8 Vector table offset register
    pub const VTOR = MMIO(Address + 0x00000008, u32, packed struct {
        reserved0: u9 = 0,
        TBLOFF: u21, // bit offset: 9 desc: Vector table base offset field
        padding: u2 = 0,
    });
    // byte offset: 12 Application interrupt and reset control register
    pub const AIRCR = MMIO(Address + 0x0000000c, u32, packed struct {
        VECTRESET: bool, // bit offset: 0 desc: VECTRESET
        VECTCLRACTIVE: bool, // bit offset: 1 desc: VECTCLRACTIVE
        SYSRESETREQ: bool, // bit offset: 2 desc: SYSRESETREQ
        reserved0: u5 = 0,
        PRIGROUP: u3, // bit offset: 8 desc: PRIGROUP
        reserved1: u4 = 0,
        ENDIANESS: bool, // bit offset: 15 desc: ENDIANESS
        VECTKEYSTAT: u16, // bit offset: 16 desc: Register key
    });
    // byte offset: 16 System control register
    pub const SCR = MMIO(Address + 0x00000010, u32, packed struct {
        reserved0: u1 = 0,
        SLEEPONEXIT: bool, // bit offset: 1 desc: SLEEPONEXIT
        SLEEPDEEP: bool, // bit offset: 2 desc: SLEEPDEEP
        reserved1: u1 = 0,
        SEVEONPEND: bool, // bit offset: 4 desc: Send Event on Pending bit
        padding: u27 = 0,
    });
    // byte offset: 20 Configuration and control register
    pub const CCR = MMIO(Address + 0x00000014, u32, packed struct {
        NONBASETHRDENA: bool, // bit offset: 0 desc: Configures how the processor enters Thread mode
        USERSETMPEND: bool, // bit offset: 1 desc: USERSETMPEND
        reserved0: u1 = 0,
        UNALIGN__TRP: bool, // bit offset: 3 desc: UNALIGN_ TRP
        DIV_0_TRP: bool, // bit offset: 4 desc: DIV_0_TRP
        reserved1: u3 = 0,
        BFHFNMIGN: bool, // bit offset: 8 desc: BFHFNMIGN
        STKALIGN: bool, // bit offset: 9 desc: STKALIGN
        padding: u22 = 0,
    });
    // byte offset: 24 System handler priority registers
    pub const SHPR1 = MMIO(Address + 0x00000018, u32, packed struct {
        PRI_4: u8, // bit offset: 0 desc: Priority of system handler 4
        PRI_5: u8, // bit offset: 8 desc: Priority of system handler 5
        PRI_6: u8, // bit offset: 16 desc: Priority of system handler 6
        padding: u8 = 0,
    });
    // byte offset: 28 System handler priority registers
    pub const SHPR2 = MMIO(Address + 0x0000001c, u32, packed struct {
        reserved0: u24 = 0,
        PRI_11: u8, // bit offset: 24 desc: Priority of system handler 11
    });
    // byte offset: 32 System handler priority registers
    pub const SHPR3 = MMIO(Address + 0x00000020, u32, packed struct {
        reserved0: u16 = 0,
        PRI_14: u8, // bit offset: 16 desc: Priority of system handler 14
        PRI_15: u8, // bit offset: 24 desc: Priority of system handler 15
    });
    // byte offset: 36 System handler control and state register
    pub const SHCRS = MMIO(Address + 0x00000024, u32, packed struct {
        MEMFAULTACT: bool, // bit offset: 0 desc: Memory management fault exception active bit
        BUSFAULTACT: bool, // bit offset: 1 desc: Bus fault exception active bit
        reserved0: u1 = 0,
        USGFAULTACT: bool, // bit offset: 3 desc: Usage fault exception active bit
        reserved1: u3 = 0,
        SVCALLACT: bool, // bit offset: 7 desc: SVC call active bit
        MONITORACT: bool, // bit offset: 8 desc: Debug monitor active bit
        reserved2: u1 = 0,
        PENDSVACT: bool, // bit offset: 10 desc: PendSV exception active bit
        SYSTICKACT: bool, // bit offset: 11 desc: SysTick exception active bit
        USGFAULTPENDED: bool, // bit offset: 12 desc: Usage fault exception pending bit
        MEMFAULTPENDED: bool, // bit offset: 13 desc: Memory management fault exception pending bit
        BUSFAULTPENDED: bool, // bit offset: 14 desc: Bus fault exception pending bit
        SVCALLPENDED: bool, // bit offset: 15 desc: SVC call pending bit
        MEMFAULTENA: bool, // bit offset: 16 desc: Memory management fault enable bit
        BUSFAULTENA: bool, // bit offset: 17 desc: Bus fault enable bit
        USGFAULTENA: bool, // bit offset: 18 desc: Usage fault enable bit
        padding: u13 = 0,
    });
    // byte offset: 40 Configurable fault status register
    pub const CFSR_UFSR_BFSR_MMFSR = MMIO(Address + 0x00000028, u32, packed struct {
        reserved0: u1 = 0,
        IACCVIOL: bool, // bit offset: 1 desc: Instruction access violation flag
        reserved1: u1 = 0,
        MUNSTKERR: bool, // bit offset: 3 desc: Memory manager fault on unstacking for a return from exception
        MSTKERR: bool, // bit offset: 4 desc: Memory manager fault on stacking for exception entry.
        MLSPERR: bool, // bit offset: 5 desc: MLSPERR
        reserved2: u1 = 0,
        MMARVALID: bool, // bit offset: 7 desc: Memory Management Fault Address Register (MMAR) valid flag
        IBUSERR: bool, // bit offset: 8 desc: Instruction bus error
        PRECISERR: bool, // bit offset: 9 desc: Precise data bus error
        IMPRECISERR: bool, // bit offset: 10 desc: Imprecise data bus error
        UNSTKERR: bool, // bit offset: 11 desc: Bus fault on unstacking for a return from exception
        STKERR: bool, // bit offset: 12 desc: Bus fault on stacking for exception entry
        LSPERR: bool, // bit offset: 13 desc: Bus fault on floating-point lazy state preservation
        reserved3: u1 = 0,
        BFARVALID: bool, // bit offset: 15 desc: Bus Fault Address Register (BFAR) valid flag
        UNDEFINSTR: bool, // bit offset: 16 desc: Undefined instruction usage fault
        INVSTATE: bool, // bit offset: 17 desc: Invalid state usage fault
        INVPC: bool, // bit offset: 18 desc: Invalid PC load usage fault
        NOCP: bool, // bit offset: 19 desc: No coprocessor usage fault.
        reserved4: u4 = 0,
        UNALIGNED: bool, // bit offset: 24 desc: Unaligned access usage fault
        DIVBYZERO: bool, // bit offset: 25 desc: Divide by zero usage fault
        padding: u6 = 0,
    });
    // byte offset: 44 Hard fault status register
    pub const HFSR = MMIO(Address + 0x0000002c, u32, packed struct {
        reserved0: u1 = 0,
        VECTTBL: bool, // bit offset: 1 desc: Vector table hard fault
        reserved1: u28 = 0,
        FORCED: bool, // bit offset: 30 desc: Forced hard fault
        DEBUG_VT: bool, // bit offset: 31 desc: Reserved for Debug use
    });
    // byte offset: 52 Memory management fault address register
    pub const MMFAR = MMIO(Address + 0x00000034, u32, packed struct {
        MMFAR: u32, // bit offset: 0 desc: Memory management fault address
    });
    // byte offset: 56 Bus fault address register
    pub const BFAR = MMIO(Address + 0x00000038, u32, packed struct {
        BFAR: u32, // bit offset: 0 desc: Bus fault address
    });
    // byte offset: 60 Auxiliary fault status register
    pub const AFSR = MMIO(Address + 0x0000003c, u32, packed struct {
        IMPDEF: u32, // bit offset: 0 desc: Implementation defined
    });
};
pub const NVIC_STIR = extern struct {
    pub const Address: u32 = 0xe000ef00;
    // byte offset: 0 Software trigger interrupt register
    pub const STIR = MMIO(Address + 0x00000000, u32, packed struct {
        INTID: u9, // bit offset: 0 desc: Software generated interrupt ID
        padding: u23 = 0,
    });
};
pub const FPU_CPACR = extern struct {
    pub const Address: u32 = 0xe000ed88;
    // byte offset: 0 Coprocessor access control register
    pub const CPACR = MMIO(Address + 0x00000000, u32, packed struct {
        reserved0: u20 = 0,
        CP: u4, // bit offset: 20 desc: CP
        padding: u8 = 0,
    });
};
pub const SCB_ACTRL = extern struct {
    pub const Address: u32 = 0xe000e008;
    // byte offset: 0 Auxiliary control register
    pub const ACTRL = MMIO(Address + 0x00000000, u32, packed struct {
        DISMCYCINT: bool, // bit offset: 0 desc: DISMCYCINT
        DISDEFWBUF: bool, // bit offset: 1 desc: DISDEFWBUF
        DISFOLD: bool, // bit offset: 2 desc: DISFOLD
        reserved0: u5 = 0,
        DISFPCA: bool, // bit offset: 8 desc: DISFPCA
        DISOOFP: bool, // bit offset: 9 desc: DISOOFP
        padding: u22 = 0,
    });
};
test "register struct sizes" {
    comptime {
        @setEvalBranchQuota(955000);
        const expectEqual = @import("std").testing.expectEqual;
        expectEqual(32, @bitSizeOf(GPIOA.MODER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOA.OTYPER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOA.OSPEEDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOA.PUPDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOA.IDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOA.ODR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOA.BSRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOA.LCKR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOA.AFRL.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOA.AFRH.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOA.BRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOB.MODER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOB.OTYPER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOB.OSPEEDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOB.PUPDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOB.IDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOB.ODR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOB.BSRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOB.LCKR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOB.AFRL.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOB.AFRH.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOB.BRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOC.MODER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOC.OTYPER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOC.OSPEEDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOC.PUPDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOC.IDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOC.ODR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOC.BSRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOC.LCKR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOC.AFRL.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOC.AFRH.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOC.BRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOD.MODER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOD.OTYPER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOD.OSPEEDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOD.PUPDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOD.IDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOD.ODR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOD.BSRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOD.LCKR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOD.AFRL.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOD.AFRH.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOD.BRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOE.MODER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOE.OTYPER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOE.OSPEEDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOE.PUPDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOE.IDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOE.ODR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOE.BSRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOE.LCKR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOE.AFRL.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOE.AFRH.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOE.BRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOF.MODER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOF.OTYPER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOF.OSPEEDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOF.PUPDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOF.IDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOF.ODR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOF.BSRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOF.LCKR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOF.AFRL.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOF.AFRH.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOF.BRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOG.MODER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOG.OTYPER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOG.OSPEEDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOG.PUPDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOG.IDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOG.ODR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOG.BSRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOG.LCKR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOG.AFRL.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOG.AFRH.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOG.BRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOH.MODER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOH.OTYPER.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOH.OSPEEDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOH.PUPDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOH.IDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOH.ODR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOH.BSRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOH.LCKR.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOH.AFRL.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOH.AFRH.underlaying_type()));
        expectEqual(32, @bitSizeOf(GPIOH.BRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.IER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.ICR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.IOHCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.IOASCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.IOSCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.IOCCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.IOGCSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.IOG1CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.IOG2CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.IOG3CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.IOG4CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.IOG5CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.IOG6CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.IOG7CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TSC.IOG8CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(CRC.DR.underlaying_type()));
        expectEqual(32, @bitSizeOf(CRC.IDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(CRC.CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(CRC.INIT.underlaying_type()));
        expectEqual(32, @bitSizeOf(CRC.POL.underlaying_type()));
        expectEqual(32, @bitSizeOf(Flash.ACR.underlaying_type()));
        expectEqual(32, @bitSizeOf(Flash.KEYR.underlaying_type()));
        expectEqual(32, @bitSizeOf(Flash.OPTKEYR.underlaying_type()));
        expectEqual(32, @bitSizeOf(Flash.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(Flash.CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(Flash.AR.underlaying_type()));
        expectEqual(32, @bitSizeOf(Flash.OBR.underlaying_type()));
        expectEqual(32, @bitSizeOf(Flash.WRPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RCC.CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RCC.CFGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RCC.CIR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RCC.APB2RSTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RCC.APB1RSTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RCC.AHBENR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RCC.APB2ENR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RCC.APB1ENR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RCC.BDCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RCC.CSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RCC.AHBRSTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RCC.CFGR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(RCC.CFGR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.IFCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CCR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CNDTR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CPAR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CMAR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CCR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CNDTR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CPAR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CMAR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CCR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CNDTR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CPAR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CMAR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CCR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CNDTR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CPAR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CMAR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CCR5.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CNDTR5.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CPAR5.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CMAR5.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CCR6.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CNDTR6.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CPAR6.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CMAR6.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CCR7.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CNDTR7.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CPAR7.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA1.CMAR7.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.IFCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CCR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CNDTR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CPAR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CMAR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CCR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CNDTR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CPAR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CMAR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CCR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CNDTR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CPAR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CMAR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CCR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CNDTR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CPAR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CMAR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CCR5.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CNDTR5.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CPAR5.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CMAR5.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CCR6.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CNDTR6.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CPAR6.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CMAR6.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CCR7.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CNDTR7.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CPAR7.underlaying_type()));
        expectEqual(32, @bitSizeOf(DMA2.CMAR7.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.SMCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.DIER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.EGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.CCMR1_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.CCMR1_Input.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.CCMR2_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.CCMR2_Input.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.CCER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.CNT.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.PSC.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.ARR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.CCR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.CCR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.CCR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.CCR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.DCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM2.DMAR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.SMCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.DIER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.EGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.CCMR1_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.CCMR1_Input.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.CCMR2_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.CCMR2_Input.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.CCER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.CNT.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.PSC.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.ARR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.CCR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.CCR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.CCR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.CCR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.DCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM3.DMAR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.SMCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.DIER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.EGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.CCMR1_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.CCMR1_Input.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.CCMR2_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.CCMR2_Input.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.CCER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.CNT.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.PSC.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.ARR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.CCR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.CCR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.CCR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.CCR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.DCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM4.DMAR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.SMCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.DIER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.EGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.CCMR1_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.CCMR1_Input.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.CCER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.CNT.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.PSC.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.ARR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.RCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.CCR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.CCR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.BDTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.DCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM15.DMAR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.DIER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.EGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.CCMR1_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.CCMR1_Input.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.CCER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.CNT.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.PSC.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.ARR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.RCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.CCR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.BDTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.DCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.DMAR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM16.OR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.DIER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.EGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.CCMR1_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.CCMR1_Input.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.CCER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.CNT.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.PSC.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.ARR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.RCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.CCR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.BDTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.DCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM17.DMAR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART1.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART1.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART1.CR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART1.BRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART1.GTPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART1.RTOR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART1.RQR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART1.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART1.ICR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART1.RDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART1.TDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART2.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART2.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART2.CR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART2.BRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART2.GTPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART2.RTOR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART2.RQR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART2.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART2.ICR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART2.RDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART2.TDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART3.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART3.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART3.CR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART3.BRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART3.GTPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART3.RTOR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART3.RQR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART3.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART3.ICR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART3.RDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USART3.TDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART4.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART4.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART4.CR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART4.BRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART4.GTPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART4.RTOR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART4.RQR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART4.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART4.ICR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART4.RDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART4.TDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART5.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART5.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART5.CR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART5.BRR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART5.GTPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART5.RTOR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART5.RQR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART5.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART5.ICR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART5.RDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(UART5.TDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI1.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI1.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI1.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI1.DR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI1.CRCPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI1.RXCRCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI1.TXCRCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI1.I2SCFGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI1.I2SPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI2.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI2.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI2.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI2.DR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI2.CRCPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI2.RXCRCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI2.TXCRCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI2.I2SCFGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI2.I2SPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI3.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI3.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI3.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI3.DR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI3.CRCPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI3.RXCRCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI3.TXCRCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI3.I2SCFGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI3.I2SPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S2ext.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S2ext.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S2ext.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S2ext.DR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S2ext.CRCPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S2ext.RXCRCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S2ext.TXCRCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S2ext.I2SCFGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S2ext.I2SPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S3ext.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S3ext.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S3ext.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S3ext.DR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S3ext.CRCPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S3ext.RXCRCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S3ext.TXCRCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S3ext.I2SCFGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2S3ext.I2SPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI4.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI4.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI4.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI4.DR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI4.CRCPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI4.RXCRCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI4.TXCRCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI4.I2SCFGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SPI4.I2SPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(EXTI.IMR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(EXTI.EMR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(EXTI.RTSR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(EXTI.FTSR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(EXTI.SWIER1.underlaying_type()));
        expectEqual(32, @bitSizeOf(EXTI.PR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(EXTI.IMR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(EXTI.EMR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(EXTI.RTSR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(EXTI.FTSR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(EXTI.SWIER2.underlaying_type()));
        expectEqual(32, @bitSizeOf(EXTI.PR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(PWR.CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(PWR.CSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.MCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.MSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.TSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.RF0R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.RF1R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.IER.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.ESR.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.BTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.TI0R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.TDT0R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.TDL0R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.TDH0R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.TI1R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.TDT1R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.TDL1R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.TDH1R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.TI2R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.TDT2R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.TDL2R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.TDH2R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.RI0R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.RDT0R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.RDL0R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.RDH0R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.RI1R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.RDT1R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.RDL1R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.RDH1R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.FMR.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.FM1R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.FS1R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.FFA1R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.FA1R.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F0R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F0R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F1R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F1R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F2R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F2R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F3R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F3R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F4R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F4R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F5R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F5R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F6R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F6R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F7R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F7R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F8R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F8R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F9R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F9R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F10R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F10R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F11R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F11R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F12R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F12R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F13R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F13R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F14R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F14R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F15R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F15R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F16R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F16R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F17R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F17R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F18R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F18R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F19R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F19R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F20R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F20R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F21R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F21R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F22R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F22R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F23R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F23R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F24R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F24R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F25R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F25R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F26R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F26R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F27R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(CAN.F27R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(USB_FS.USB_EP0R.underlaying_type()));
        expectEqual(32, @bitSizeOf(USB_FS.USB_EP1R.underlaying_type()));
        expectEqual(32, @bitSizeOf(USB_FS.USB_EP2R.underlaying_type()));
        expectEqual(32, @bitSizeOf(USB_FS.USB_EP3R.underlaying_type()));
        expectEqual(32, @bitSizeOf(USB_FS.USB_EP4R.underlaying_type()));
        expectEqual(32, @bitSizeOf(USB_FS.USB_EP5R.underlaying_type()));
        expectEqual(32, @bitSizeOf(USB_FS.USB_EP6R.underlaying_type()));
        expectEqual(32, @bitSizeOf(USB_FS.USB_EP7R.underlaying_type()));
        expectEqual(32, @bitSizeOf(USB_FS.USB_CNTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USB_FS.ISTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USB_FS.FNR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USB_FS.DADDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(USB_FS.BTABLE.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C1.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C1.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C1.OAR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C1.OAR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C1.TIMINGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C1.TIMEOUTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C1.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C1.ICR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C1.PECR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C1.RXDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C1.TXDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C2.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C2.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C2.OAR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C2.OAR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C2.TIMINGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C2.TIMEOUTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C2.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C2.ICR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C2.PECR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C2.RXDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C2.TXDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C3.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C3.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C3.OAR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C3.OAR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C3.TIMINGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C3.TIMEOUTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C3.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C3.ICR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C3.PECR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C3.RXDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(I2C3.TXDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(IWDG.KR.underlaying_type()));
        expectEqual(32, @bitSizeOf(IWDG.PR.underlaying_type()));
        expectEqual(32, @bitSizeOf(IWDG.RLR.underlaying_type()));
        expectEqual(32, @bitSizeOf(IWDG.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(IWDG.WINR.underlaying_type()));
        expectEqual(32, @bitSizeOf(WWDG.CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(WWDG.CFR.underlaying_type()));
        expectEqual(32, @bitSizeOf(WWDG.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.TR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.DR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.PRER.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.WUTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.ALRMAR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.ALRMBR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.WPR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.SSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.SHIFTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.TSTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.TSDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.TSSSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.CALR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.TAFCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.ALRMASSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.ALRMBSSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP0R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP1R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP2R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP3R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP4R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP5R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP6R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP7R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP8R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP9R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP10R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP11R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP12R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP13R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP14R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP15R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP16R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP17R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP18R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP19R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP20R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP21R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP22R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP23R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP24R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP25R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP26R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP27R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP28R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP29R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP30R.underlaying_type()));
        expectEqual(32, @bitSizeOf(RTC.BKP31R.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM6.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM6.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM6.DIER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM6.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM6.EGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM6.CNT.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM6.PSC.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM6.ARR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM7.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM7.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM7.DIER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM7.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM7.EGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM7.CNT.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM7.PSC.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM7.ARR.underlaying_type()));
        expectEqual(32, @bitSizeOf(DAC.CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(DAC.SWTRIGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(DAC.DHR12R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(DAC.DHR12L1.underlaying_type()));
        expectEqual(32, @bitSizeOf(DAC.DHR8R1.underlaying_type()));
        expectEqual(32, @bitSizeOf(DAC.DHR12R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(DAC.DHR12L2.underlaying_type()));
        expectEqual(32, @bitSizeOf(DAC.DHR8R2.underlaying_type()));
        expectEqual(32, @bitSizeOf(DAC.DHR12RD.underlaying_type()));
        expectEqual(32, @bitSizeOf(DAC.DHR12LD.underlaying_type()));
        expectEqual(32, @bitSizeOf(DAC.DHR8RD.underlaying_type()));
        expectEqual(32, @bitSizeOf(DAC.DOR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(DAC.DOR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(DAC.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(DBGMCU.IDCODE.underlaying_type()));
        expectEqual(32, @bitSizeOf(DBGMCU.CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(DBGMCU.APB1FZ.underlaying_type()));
        expectEqual(32, @bitSizeOf(DBGMCU.APB2FZ.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.SMCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.DIER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.EGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.CCMR1_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.CCMR1_Input.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.CCMR2_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.CCMR2_Input.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.CCER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.CNT.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.PSC.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.ARR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.RCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.CCR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.CCR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.CCR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.CCR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.BDTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.DCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.DMAR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.CCMR3_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.CCR5.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.CCR6.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM1.OR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.SMCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.DIER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.EGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.CCMR1_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.CCMR1_Input.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.CCMR2_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.CCMR2_Input.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.CCER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.CNT.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.PSC.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.ARR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.RCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.CCR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.CCR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.CCR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.CCR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.BDTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.DCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.DMAR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.CCMR3_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.CCR5.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.CCR6.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM20.OR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.CR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.CR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.SMCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.DIER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.SR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.EGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.CCMR1_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.CCMR1_Input.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.CCMR2_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.CCMR2_Input.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.CCER.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.CNT.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.PSC.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.ARR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.RCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.CCR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.CCR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.CCR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.CCR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.BDTR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.DCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.DMAR.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.CCMR3_Output.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.CCR5.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.CCR6.underlaying_type()));
        expectEqual(32, @bitSizeOf(TIM8.OR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.IER.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.CFGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.SMPR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.SMPR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.TR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.TR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.TR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.SQR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.SQR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.SQR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.SQR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.DR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.JSQR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.OFR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.OFR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.OFR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.OFR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.JDR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.JDR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.JDR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.JDR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.AWD2CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.AWD3CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.DIFSEL.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1.CALFACT.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.IER.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.CFGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.SMPR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.SMPR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.TR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.TR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.TR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.SQR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.SQR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.SQR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.SQR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.DR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.JSQR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.OFR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.OFR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.OFR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.OFR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.JDR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.JDR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.JDR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.JDR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.AWD2CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.AWD3CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.DIFSEL.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC2.CALFACT.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.IER.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.CFGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.SMPR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.SMPR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.TR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.TR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.TR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.SQR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.SQR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.SQR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.SQR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.DR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.JSQR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.OFR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.OFR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.OFR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.OFR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.JDR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.JDR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.JDR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.JDR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.AWD2CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.AWD3CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.DIFSEL.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3.CALFACT.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.ISR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.IER.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.CFGR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.SMPR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.SMPR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.TR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.TR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.TR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.SQR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.SQR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.SQR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.SQR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.DR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.JSQR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.OFR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.OFR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.OFR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.OFR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.JDR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.JDR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.JDR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.JDR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.AWD2CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.AWD3CR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.DIFSEL.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC4.CALFACT.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1_2.CSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1_2.CCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC1_2.CDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3_4.CSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3_4.CCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(ADC3_4.CDR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.SYSCFG_CFGR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.SYSCFG_RCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.SYSCFG_EXTICR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.SYSCFG_EXTICR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.SYSCFG_EXTICR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.SYSCFG_EXTICR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.SYSCFG_CFGR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.COMP1_CSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.COMP2_CSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.COMP3_CSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.COMP4_CSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.COMP5_CSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.COMP6_CSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.COMP7_CSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.OPAMP1_CSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.OPAMP2_CSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.OPAMP3_CSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SYSCFG_COMP_OPAMP.OPAMP4_CSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.BCR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.BTR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.BCR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.BTR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.BCR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.BTR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.BCR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.BTR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.PCR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.SR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.PMEM2.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.PATT2.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.ECCR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.PCR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.SR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.PMEM3.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.PATT3.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.ECCR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.PCR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.SR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.PMEM4.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.PATT4.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.PIO4.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.BWTR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.BWTR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.BWTR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(FMC.BWTR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.ISER0.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.ISER1.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.ISER2.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.ICER0.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.ICER1.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.ICER2.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.ISPR0.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.ISPR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.ISPR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.ICPR0.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.ICPR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.ICPR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IABR0.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IABR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IABR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR0.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR4.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR5.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR6.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR7.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR8.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR9.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR10.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR11.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR12.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR13.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR14.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR15.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR16.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR17.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR18.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR19.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC.IPR20.underlaying_type()));
        expectEqual(32, @bitSizeOf(FPU.FPCCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(FPU.FPCAR.underlaying_type()));
        expectEqual(32, @bitSizeOf(FPU.FPSCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(MPU.MPU_TYPER.underlaying_type()));
        expectEqual(32, @bitSizeOf(MPU.MPU_CTRL.underlaying_type()));
        expectEqual(32, @bitSizeOf(MPU.MPU_RNR.underlaying_type()));
        expectEqual(32, @bitSizeOf(MPU.MPU_RBAR.underlaying_type()));
        expectEqual(32, @bitSizeOf(MPU.MPU_RASR.underlaying_type()));
        expectEqual(32, @bitSizeOf(STK.CTRL.underlaying_type()));
        expectEqual(32, @bitSizeOf(STK.LOAD.underlaying_type()));
        expectEqual(32, @bitSizeOf(STK.VAL.underlaying_type()));
        expectEqual(32, @bitSizeOf(STK.CALIB.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB.CPUID.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB.ICSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB.VTOR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB.AIRCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB.SCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB.CCR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB.SHPR1.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB.SHPR2.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB.SHPR3.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB.SHCRS.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB.CFSR_UFSR_BFSR_MMFSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB.HFSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB.MMFAR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB.BFAR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB.AFSR.underlaying_type()));
        expectEqual(32, @bitSizeOf(NVIC_STIR.STIR.underlaying_type()));
        expectEqual(32, @bitSizeOf(FPU_CPACR.CPACR.underlaying_type()));
        expectEqual(32, @bitSizeOf(SCB_ACTRL.ACTRL.underlaying_type()));
    }
}

