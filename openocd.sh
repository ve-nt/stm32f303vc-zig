#!/bin/bash

itm_out=itm.txt
interface=interface/stlink-v2-1.cfg
target=target/stm32f3x.cfg

cd /tmp

# Start openocd daemon for stm32f3
openocd -f $interface -f $target &
openocd_pid=$!

# Read from the ITM stream
touch $itm_out
itmdump -F -f $itm_out &
itmdump_pid=$!

# Run until the user quits the program.
while read -p $'\nPress `q` to exit, killing openocd and itmdump\n' -r -s -n1 key; do
	if [[ $key == 'q' ]]; then
		kill $openocd_pid
		kill $itmdump_pid
		break
	fi
done
