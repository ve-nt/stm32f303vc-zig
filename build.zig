const std = @import("std");
const Builder = std.build.Builder;
const FileSource = std.build.FileSource;

const Arch = std.Target.Cpu.Arch;
const Os = std.Target.Os;
const Abi = std.Target.Abi;
const cortex_m4 = std.Target.arm.cpu.cortex_m4;
const CpuModel = std.zig.CrossTarget.CpuModel;

pub fn build(b: *Builder) void {
    const mode = b.standardReleaseOptions();

    const exe = b.addExecutable("firmware.elf", "src/main.zig");
    exe.setTarget(.{
        .cpu_arch = Arch.thumb,
        .cpu_model = CpuModel{ .explicit = &cortex_m4 },
        .os_tag = Os.Tag.freestanding,
        .abi = Abi.none,
    });
    exe.setBuildMode(mode);
    exe.setLinkerScriptPath(FileSource.relative("ld/stm32f303.ld"));
    exe.setOutputDir("out");
    exe.addPackagePath("mmio", "./lib/stm32f303-mmio/stm32f303.zig");

    b.default_step.dependOn(&exe.step);
    b.installArtifact(exe);

    const bin_cmd = b.addSystemCommand(&.{
        "llvm-objcopy", "-O", "binary", "out/firmware.elf", "out/firmware.bin",
    });
    bin_cmd.step.dependOn(b.getInstallStep());
    const bin_step = b.step("bin", "Create a firmware binary file");
    bin_step.dependOn(&bin_cmd.step);

    const flash_cmd = b.addSystemCommand(&.{
        "st-flash", "write", "out/firmware.bin", "0x8000000",
    });
    flash_cmd.step.dependOn(&bin_cmd.step);
    const flash_step = b.step("flash", "Flash firmware to target");
    flash_step.dependOn(&flash_cmd.step);

    const clean_step = b.step("clean", "Clean build artifacts");
    clean_step.makeFn = struct {
        fn make(_: *std.build.Step) !void {
            const cwd = std.fs.cwd();
            try cwd.deleteTree("out");
            try cwd.deleteTree("zig-out");
            try cwd.deleteTree("zig-cache");
        }
    }.make;
}
