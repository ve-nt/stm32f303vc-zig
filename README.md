# Zig on an STM32F303VC Discovery Board

Basic project that is able to run on an STM32F303VC.

![board](res/stm32f303vc.png)

## Dependencies

### Build

- zig

- llvm-objcopy (usually part of the `llvm` package)

### Debug

- arm-none-eabi-gdb

- openocd

- [itmdump](https://docs.rs/itm/0.3.1/itm/)

### Flash

- st-flash (usually part of the `stlink` package)

## Usage

To build an ELF file:

```
$ zig build
```

To build a binary file ready for flashing:

```
$ zig build bin
```

To build and flash your firmware:

```
$ zig build flash
```

## Debugging

First, make sure that the board has been flashed, plugged in, and is
running. Then, start your openocd and itmdump process. This can be
done using the included script:

```
./openocd.sh
```

Now that your debugging session has started, connect to it using GDB.
The included 'openocd.gdb' init file will connect to the remote
session and put you in `main`.

```
arm-none-eabi-gdb -x openocd.gdb out/firmware.elf
```

## Links

- [Board Overview](https://www.st.com/en/microcontrollers-microprocessors/stm32f303vc.html#overview)

- [User Manual](https://www.st.com/content/ccc/resource/technical/document/user_manual/8a/56/97/63/8d/56/41/73/DM00063382.pdf/files/DM00063382.pdf/jcr:content/translations/en.DM00063382.pdf)

- [Reference Manual](https://www.st.com/resource/en/reference_manual/dm00043574-stm32f303xbcde-stm32f303x68-stm32f328x8-stm32f358xc-stm32f398xe-advanced-armbased-mcus-stmicroelectronics.pdf)

- [Datasheet](https://www.st.com/resource/en/datasheet/stm32f303vc.pdf)

- [System View Description](https://www.st.com/resource/en/svd/stm32f3_svd.zip)
