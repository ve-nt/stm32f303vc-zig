const mmio = @import("mmio");
const timer = @import("timer.zig");

pub fn lightShow() void {
    const delay: u16 = 400;
    var led: u32 = undefined;
    while (true) {
        led = 0;
        while (led < 8) : (led += 1) {
            setLed(led);
            timer.delayMs(delay);
        }

        led = 0;
        while (led < 8) : (led += 1) {
            resetLed(led);
            timer.delayMs(delay);
        }

        timer.delayMs(delay * 6);
    }
}

fn setLed(led: u32) void {
    mmio.GPIOE.BSRR.ptr().* = @as(u32, 1) << @intCast(u5, led + 8);
}

fn resetLed(led: u32) void {
    mmio.GPIOE.BSRR.ptr().* = @as(u32, 1) << @intCast(u5, led + 8 + 16);
}

pub fn init() void {
    enableGpioePort();
    configureGpioeOutput();
}

fn configureGpioeOutput() void {
    const output = 0x01;
    mmio.GPIOE.MODER.set(.{
        .MODER0 = output,
        .MODER1 = output,
        .MODER2 = output,
        .MODER3 = output,
        .MODER4 = output,
        .MODER5 = output,
        .MODER6 = output,
        .MODER7 = output,
        .MODER8 = output,
        .MODER9 = output,
        .MODER10 = output,
        .MODER11 = output,
        .MODER12 = output,
        .MODER13 = output,
        .MODER14 = output,
        .MODER15 = output,
    });
}

fn enableGpioePort() void {
    mmio.RCC.AHBENR.set(.{
        .IOPEEN = true,
    });
}
