const mmio = @import("mmio");

const TIM6 = 0x4000_1000;
const TIM6_CR1 = @intToPtr(*volatile u16, TIM6 + 0x0);

pub fn init() void {
    // Enable TIM6.
    mmio.RCC.APB1ENR.set(.{ .TIM6EN = true });

    mmio.TIM6.CR1.set(.{
        // Disable counting, toggle it on when we need to when in OPM.
        .CEN = false,
        // Configure to one-pulse mode
        .OPM = true,
    });

    // Set prescaler to roughly 1ms per count.
    mmio.TIM6.PSC.set(.{ .PSC = 7999 });
}

pub fn delayMs(n: u16) void {
    // Set our value for TIM6 to count to.
    mmio.TIM6.ARR.set(.{ .ARR = n });

    // Start the clock using CEN.
    mmio.TIM6.CR1.set(.{ .CEN = true });

    // Wait for TIM6 to set the status register.
    while (!mmio.TIM6.SR.read().UIF) {}

    // Clear the status register.
    mmio.TIM6.SR.set(.{ .UIF = false });
}
