extern var __text_end: u32;
extern var __data_start: u32;
extern var __data_size: u32;
extern var __bss_start: u32;
extern var __bss_size: u32;

export const _reset linksection(".isr_vector") = reset;
export fn reset() noreturn {
    // copy data from flash to RAM
    const data_size = @ptrToInt(&__data_size);
    const data = @ptrCast([*]u8, &__data_start);
    const text_end = @ptrCast([*]u8, &__text_end);
    for (text_end[0..data_size]) |b, i| data[i] = b;

    // clear the bss
    const bss_size = @ptrToInt(&__bss_size);
    const bss = @ptrCast([*]u8, &__bss_start);
    for (bss[0..bss_size]) |*b| b.* = 0;

    // start
    main();
}

const led = @import("led.zig");
const timer = @import("timer.zig");

pub fn main() noreturn {
    led.init();
    timer.init();

    led.lightShow();

    while (true) {}
}
